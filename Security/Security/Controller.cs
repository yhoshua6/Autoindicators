﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace DEOSecurity.Controller
{
    public class DEOController : Model.mSecurity
    {
        public bool bCreateProfile()
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@PK_Profile", Profile.PK_Profile);
            param[1] = new SqlParameter("@vchName", Profile.strName);
            param[2] = new SqlParameter("@vchDescription", Profile.strDescription);
            param[3] = new SqlParameter("@intApp", intApp);
            param[4] = new SqlParameter("@bDelete", Profile.bDelete);

            DataSet dsRes = SqlHelper.ExecuteDataset(strConnection, "[SPROCmanProfile]", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            if (intRes > 0)
            {
                return true;
            }
            else
                return false;
        }
        public void GetModules()
        {
            List<Model.mNodes> lstModules = new List<Model.mNodes>();

            using (Model.DevSQLSecurity dc = new Model.DevSQLSecurity())
            {
                Modules = dc.Database.SqlQuery<Model.mNodes>("SPROCgetNodes @PKApp={0}", new object[] { intApp }).ToList<Model.mNodes>();
            }
        }
        public List<Model.mNodes> GetMenu()
        {
            List<Model.mNodes> lstMenu = new List<Model.mNodes>();
            
            using (Model.DevSQLSecurity dc = new Model.DevSQLSecurity())
            {
                lstMenu = dc.Database.SqlQuery<Model.mNodes>("SPROCgetMenu @idProfile={0}", new object[] { 0 }).ToList<Model.mNodes>();
                
                foreach(Model.mNodes menu in lstMenu)
                {
                    menu.Parameters = dc.Database.SqlQuery<Model.mParameter>("SPROCgetMenuParameters @FKMenu={0}", new object[] { menu.PKNode }).ToList<Model.mParameter>();
                }
            }

            return lstMenu;
        }
        public int intAddMenu()
        {
            SqlConnection conn=new SqlConnection(strConnection);
            int intRes=0;
             
            conn.Open();
            SqlTransaction  tran=conn.BeginTransaction();

            try
            {
                SqlParameter[] param = new SqlParameter[11];

                param[0] = new SqlParameter("@PKNode",Convert.ToInt32(0));
                param[1] = new SqlParameter("@vchName", MenuItem.strName);
                param[2] = new SqlParameter("@vchTarget", MenuItem.strTarget);
                param[3] = new SqlParameter("@vchIcon", MenuItem.strIcon);
                param[4] = new SqlParameter("@vchTagID", MenuItem.strTagID);
                param[5] = new SqlParameter("@vchTitle", MenuItem.strTitle);
                param[6] = new SqlParameter("@vchVersion", MenuItem.strVersion);
                param[7] = new SqlParameter("@FKStatus", 1);
                param[8] = new SqlParameter("@intApp", intApp);
                param[9] = new SqlParameter("@bDelete", false);
                param[10] = new SqlParameter("@vchBasicCFG", "FreeHTML");

                DataSet dsRes = SqlHelper.ExecuteDataset(tran, "[SPORCmanMenu]", param);
                intRes= Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

                tran.Commit();
            }
            catch
            {
                tran.Rollback();

            }
            tran.Dispose();
            conn.Close();
            conn.Dispose();
            return intRes;
        }
        public bool bEditMenu()
        {
            SqlParameter[] param = new SqlParameter[11];

            param[0] = new SqlParameter("@PKNode", MenuItem.PKNode);
            param[1] = new SqlParameter("@vchName", MenuItem.strName);
            param[2] = new SqlParameter("@vchTarget", MenuItem.strTarget);
            param[3] = new SqlParameter("@vchIcon", MenuItem.strIcon);
            param[4] = new SqlParameter("@vchTagID", MenuItem.strTagID);
            param[5] = new SqlParameter("@vchTitle", MenuItem.strTitle);
            param[6] = new SqlParameter("@vchVersion", MenuItem.strVersion);
            param[7] = new SqlParameter("@FKStatus", 1);
            param[8] = new SqlParameter("@intApp", intApp);
            param[9] = new SqlParameter("@bDelete", MenuItem.bDelete==null? false : MenuItem.bDelete);
            param[10] = new SqlParameter("@vchBasicCFG", "FreeHTML");

            DataSet dsRes = SqlHelper.ExecuteDataset(strConnection, "[SPORCmanMenu]", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            return false;
        }
        public int intAddParameter()
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@PK_Parameter", 0);
            param[1] = new SqlParameter("@vchName", MenuItem.Parameter.strParameter);
            param[2] = new SqlParameter("@vchValue", MenuItem.Parameter.strParameterValue);
            param[3] = new SqlParameter("@FK_Node", MenuItem.PKNode);
            param[4] = new SqlParameter("@bDelete", 0);

            DataSet dsRes = SqlHelper.ExecuteDataset(strConnection, "[SPROCmanParameters]", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            return intRes;
            
        }
    }
}
