﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEOSecurity.Model
{
    public class mProfile
    {
        public string PK_Profile { get; set; }
        public string strName { get; set; }
        public string strDescription { get; set; }
        public bool bDelete { get; set; }

    }
}
