﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEOSecurity.Model
{
    public class mNodes
    {
        public int PKNode { get; set; }
        public string strName { get; set; }
        public string strTarget { get; set; }
        public string strIcon { get; set; }
        public string strTagID { get; set; }
        public string strTitle { get; set; }
        public string strVersion { get; set; }
        public string strParameterString
        {
            get
            {
                string strParams = "";

                foreach(mParameter parameter in this.Parameters)
                {
                    strParams += "&"+parameter.strParameter + "=" + parameter.strParameterValue;
                }
                if (strParams.Length > 0)
                    return strParams.Substring(1);
                else
                    return strParams;
            }
        }
        
        public List<mParameter> Parameters { get; set; }
        public mParameter Parameter { get; set; }
        public List<Model> mModels { get; set; }
        public bool bDelete { get; set; }
    }
    public class mParameter
    {
        public string strParameter { get; set; }
        public string strParameterValue { get; set; }
    }
}
