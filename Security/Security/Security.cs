﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEOSecurity.Model
{
    public class mSecurity
    {

        public string strConnection { get; set; }
        public int intApp { get; set; }
        private List<Model> mModels { get; set; }
        public List<Model> ProfileModelCfg { get { return mModels; } }
        public mProfile Profile { get; set; }
        public List<mNodes> Modules { get; set; }
        public mNodes MenuItem { get; set; }
    }

    public class Model
    {
        public string strName { get; set; }
        public List<Property> pProperties { get; set; }
    }

    public class Property
    {
        public string strName { get; set; }
    }

}
