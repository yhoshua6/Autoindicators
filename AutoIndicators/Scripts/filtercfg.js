﻿var Users = [];
var id = 0;

function SaveFilter()
{
    if ($("#cmbFilter").val().length==0)
    {
        $('#warning').show();
        $('#warningMessage').text("Please select a filter");
        return;
    }

    var name = "";

    if ($("#cmbFilter").val() == "new") {
        name = prompt("Title", "");
        $.post("/Filter/Save", { Name: name, Priority: 1, Deleted: false }, function (response) {
            if (response > 0)
                SaveDetails(response);
            else
                alert('Error');
        });
    } else
        SaveDetails($("#cmbFilter").val());
}
function SaveDetails(id) {
    $.post("/Filter/SaveDetails?id="+id, $("#Data").serialize(), function (response) {
        $('#warning').show();
        $('#warningMessage').text("Data Saved");
        window.location.reload();
    });
}
function clear()
{
    var parents = document.querySelectorAll('[data-field-parent]');

    for (i = 0; i < parents.length; i++) {
            (parents[i].firstChild).nextSibling.checked = false;
    }
}
function LoadFilter(Id)
{
    clear();
    if (Id == "" || Id == "new")
        return;
    id = Id;


    $.post("/Filter/LoadFilter", { id: Id }, function (response) {
        var parents = document.querySelectorAll('[data-field-parent]');
        for(j=0; j<response.Details.length; j++)
        for (i = 0; i < parents.length; i++) {
            if ((parents[i].firstChild).nextSibling.value == response.Details[j].strValue)
                (parents[i].firstChild).nextSibling.checked = true;
        }
    });

    $("#filterp").html($("option[value='"+id+"']").text());

    $.post("/Filter/getUsers", { Filter: Id }, function (response) {
        Users = response;
        for (i = 0; i < Users.length > 0; i++) {
            $("#Users").html("<tr><td>" + Users[i].strName + "</td><td><input style=\"color:blue\" data-user=\"" + Users[i].strUser + "\" class=\"btn\" type=\"button\" onclick=\"DeleteUser($(this).data('user'))\" style=\"float:rigth\" value=\"Remove\"></td></tr>");
        }
    }, "json");
   
   // $("#UserFilter").show();
}
function SaveUser()
{
    $.post("/Filter/ManUser", { User: $("#txtUser").val(), Filter: id, bDelete: false }, function (response) {
        Users = response;
        for (i = 0; i < Users.length > 0; i++) {
            $("#Users").html("<tr><td>" + Users[i].strName + "</td><td><input style=\"color:blue\" data-user=\"" + Users[i].strUser + "\" class=\"btn\" type=\"button\" onclick=\"DeleteUser($(this).data('user'))\" value=\"Remove\"></td></tr>");
        }
    }, "json");
}
function DeleteUser(user)
{
    $.post("/Filter/ManUser", { User: user, Filter: id, bDelete: true }, function (response) {
        $("type[data-user='" + user + "']").parent().hide();
    });
}