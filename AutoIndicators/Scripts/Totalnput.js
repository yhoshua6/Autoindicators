﻿var MilestoneModel = {
    id: 0,
    Name: '',
    Definition: '',
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    parse: function(data)
    {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if(data[0] != undefined)
                newObject.id = data[0];
            newObject.Name = data[1];
            newObject.Definition = data[2];
        }
        return newObject;
    }
};
var IndicatorModel = {
    id: 0,
    Name: '',
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    parse: function(data)
    {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined)
                newObject.id =data[0];
            newObject.Name = data[1];
        }
        return newObject;
    }
};
var ProgramTypeModel = {
    id: 0,
    Name: '',
    Link: '',
    Indicators: [],
    Milestones: [],
    create: function (config) {
    var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
    if (config !== undefined) {
        Object.keys(config).forEach(function (key) {
            newObject[key] = config[key];
        });
    }
    return newObject;
    },
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined)
                newObject.id = data[0];
            newObject.Name = data[1];
            newObject.link = data[2];
        }
        return newObject;
    }
}

var ProgramType =ProgramTypeModel;
var Indicator = IndicatorModel;
var Milestone = MilestoneModel;
var Selidx = 0;
var Done = false;
var gridMilestoneLoaded = false;
var gridIndicatorLoaded = false;
var save = 0;
var total = 0;

function changeToGridProg() {
    var tbl = $("#DataProgType");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 600,
        height: 800,
        title: "Program Types",
        //DataGrid Menu
        editable: true,
        toolbar: {
            items: [{ type: 'button', label: 'Save Changes', listeners: [{ click: SavePT }] }
                , {
                    type: 'button', label: 'Add New', listeners: [{
                        "click": function () {
                            if (ProgramTypeModel.parse($("#gridProgtype").pqGrid("getRowData", { rowIndxPage: 0 })).Name.length > 0) {
                                $("#gridProgtype").pqGrid("addRow", { rowData: {}, rowIndx: 0 });
                            }
                        }
                    }]
                }
                //, { type: 'button', label: 'Delete', listeners: [{
                //    "click": function () {
                //        $.post('/Totalnput/SaveProgType', { id: ProgramType.id, Description: ProgramType.Name, Link: ProgramType.Link, Delete: true }, function (response) {
                //            if (Done) {
                //                $('#gridProgtype').pqGrid("deleteRow", { rowIndx: Selidx });
                //            }
                //        });
                //        }
                //    }] 
                //}
                //, { type: 'button', label: 'Indicator Config', listeners: [{ "click": AddTemplates }]}
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    
    newObj.colModel[1].width = 250;
    newObj.colModel[2].width = 300;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }
    
    newObj.pageModel = { rPP: 30, type: "local" };
    
    $gridPT = $("#gridProgtype").pqGrid(newObj);


    try {

        $gridPT.pqGrid("option", "dataModel.sortIndx", 0);
        $gridPT.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $gridPT.pqGrid("saveEditCell");
            }
        });
        $gridPT.pqGrid({
            rowClick: function (event, ui) {
                if (ui.rowData) {
                    ProgramType = ProgramTypeModel.parse($gridPT.pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));

                    if (ProgramType.id > 0) {
                        ShowMilestones(ProgramType.id);
                        ShowIndicator(ProgramType.id);

                        $('.milestones').hide();
                        $('.indicators').hide();

                        $('#grid-m-' + ProgramType.id).toggle('slide', 500);
                        $('#grid-i-' + ProgramType.id).toggle('slide', 500);

                        if (!$("#Arrows").is(":visible"))
                            $("#Arrows").toggle('slide', 500);

                        $($($('#grid-m-' + ProgramType.id).children()[0]).children()[1]).click()
                        $($($('#grid-m-' + ProgramType.id).children()[0]).children()[1]).click()

                        $($($('#grid-i-' + ProgramType.id).children()[0]).children()[1]).click()
                        $($($('#grid-i-' + ProgramType.id).children()[0]).children()[1]).click()
                    }
                }
            }
        });
    } catch (err) { }
}
function ShowMilestones(prog) {
    var tbl = $("#M-"+prog);
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 500,
        height: 400,
        title: "Milestones",
        //DataGrid Menu
        editable: false,
        toolbar: {
            items: [{ type: 'button', label: 'Add / Edit', listeners: [{ click: AddMilestone }] }
                //,{ type: 'button', label: 'Delete', listeners: [{ click:function(){
                //    $.post("/Totalnput/SavePTMilestone", { PTID: ProgramType.id, MilestoneID: ProgramType.Milestones.id, Delete: true }, function (response) {
                //        if(Done)
                //        {
                //            $('#grid-m-' + prog).pqGrid("deleteRow", { rowIndx: Selidx });
                //        }
                //    })
                //}
                //}] }
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    newObj.colModel[0].width = 15;
    newObj.colModel[1].width = 150;
    newObj.colModel[2].width = 335;


    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.pageModel = { rPP: 30, type: "local" };
    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            ProgramType.Milestones = Milestone.parse($('#grid-m-'+prog).pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));
            Selidx = ui.rowIndx;
        }
    };
    $grid = $("#grid-m-"+prog).pqGrid(newObj);


    try {
        $grid.pqGrid("option", "dataModel.sortIndx", 0);
        $grid.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $grid.pqGrid("saveEditCell");
            }
        });

    } catch (err) { }
}
function ShowIndicator(prog) {
    var tbl = $("#I-" + prog);
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 500,
        height: 400,
        title: "Indicators",
        editable: false,
        //DataGrid Menu
        toolbar: {
            items: [{ type: 'button', label: 'Add / Edit', listeners: [{ click: AddIndicators }] }
            //,{
            //    type: 'button', label: 'Delete', listeners: [{
            //        click: function () {
            //            $.post("/Totalnput/SavePTIndicator", { PTID: ProgramType.id, IndicatorID: ProgramType.Indicators.id, Delete: true }, function (response) {
            //                if (Done) {
            //                    $('#grid-i-' + prog).pqGrid("deleteRow", { rowIndx: Selidx });
            //                }
            //            })
            //        }
            //    }]
            //}
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    newObj.colModel[0].width = 15;
    newObj.colModel[1].width = 150;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }
    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            ProgramType.Indicators = Indicator.parse($('#grid-i-' + prog).pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));
            Selidx = ui.rowIndx;
        }
    };
    newObj.pageModel = { rPP: 30, type: "local" };
    $grid = $("#grid-i-" + prog).pqGrid(newObj);


    try {
        $grid.pqGrid("option", "dataModel.sortIndx", 0);
        $grid.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $grid.pqGrid("saveEditCell");
            }
        });
    } catch (err) { }
}
function AddMilestone()
{
    $('#Modal').data('url', 'MilestoneCatalog?ProgramType=' + ProgramType.id);
    $('#myModalLabel').html('Add a milestone');
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        changeToGridMilestone();
    });

}
function AddTemplates()
{
    $('#Modal').data('url', 'TemplatesCatalog?ProgramType=' + ProgramType.id);
    $('#myModalLabel').html('Add a Template');
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        //changeToGridMilestone();
    });
}
function changeToGridMilestone() {
    gridMilestoneLoaded = false;
    var tbl = $("#MilestonCat");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 550,
        height: 800,
        title: "Milestones",
        editable: true,
        //DataGrid Menu
        toolbar: {
            items: [{ type: 'button', label: 'Save Changes', listeners: [{ click: SaveMilestone }] }
                , {
                    type: 'button', label: 'Add New', listeners: [{
                        click: function () {
                            if (ProgramTypeModel.parse($("#gridMilestonCat").pqGrid("getRowData", { rowIndxPage: 0 })).Name.length > 0) {
                                $("#gridMilestonCat").pqGrid("addRow", { rowData: {}, rowIndx: 0 });
                            }
                        }
                    }]
                }
                //,{ type: 'button', label: 'Delete', listeners: [{ click: SaveMilestone(true) }] }
                ,{ type: 'button', label: 'Add to Program Type', listeners: [{ click: AddMilestoneToPT }] }
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            ProgramType.Milestones = Milestone.parse($('#gridMilestonCat').pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));
            Selidx = ui.rowIndx;
        }
    };
    newObj.colModel[1].width = 250;
    newObj.colModel[2].width = 300;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.colModel[0].editable = false;

    newObj.pageModel = { rPP: 30, type: "local" };
    $gridMile = $("#gridMilestonCat").pqGrid(newObj);


    try {
        $gridMile.pqGrid("option", "collapsible", false);
        $gridMile.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $grid.pqGrid("saveEditCell");
            }
        });
        $($($('#gridMilestonCat').children()[0]).children()[1]).click()
        $($($('#gridMilestonCat').children()[0]).children()[1]).click()
        $gridMile.pqGrid("option", "selectionModel", { mode: 'range' });
        gridMilestoneLoaded = true;
    } catch (err) { }
}
function changeToGridIndicator()
{
    gridIndicatorLoaded = false;
    var tbl = $("#IndicatorCat");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 400,
        height: 800,
        title: "Indicators",
        editable: true,
        //DataGrid Menu
        toolbar: {
            items: [{ type: 'button', label: 'Save Changes', listeners: [{ click: SaveIndicators }] }
                , {
                    type: 'button', label: 'Add New', listeners: [{
                        click: function () {
                            if (ProgramTypeModel.parse($("#gridIndicatorCat").pqGrid("getRowData", { rowIndxPage: 0 })).Name.length > 0) {
                                $("#gridIndicatorCat").pqGrid("addRow", { rowData: {}, rowIndx: 0 });
                            }
                        }
                    }]
                }
                //,{ type: 'button', label: 'Delete', listeners: [{ click: SaveIndicators(true) }] }
                , { type: 'button', label: 'Add to Program Type', listeners: [{ click: AddIndicatorToPT }] }
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            ProgramType.Indicators = Indicator.parse($('#gridIndicatorCat').pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));
            Selidx = ui.rowIndx;
        }
    };
    newObj.colModel[1].width = 300;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.colModel[0].editable = false;

    newObj.pageModel = { rPP: 30, type: "local" };
    $gridIndicator = $("#gridIndicatorCat").pqGrid(newObj);


    try {
        
        //$gridIndicator.pqGrid("option", "dataModel.sortIndx", 0);
        gridIndicatorLoaded = true;
        $gridIndicator.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $grid.pqGrid("saveEditCell");
            }
        });
        $gridIndicator.pqGrid("option", "collapsible", false);
        $gridIndicator.pqGrid("option", "selectionModel", { mode: 'range' });
        $($($('#gridIndicatorCat').children()[0]).children()[1]).click()
        $($($('#gridIndicatorCat').children()[0]).children()[1]).click()
    } catch (err) { }
}
function AddIndicators()
{
    $('#Modal').data('url', 'IndicatorsCatalog?ProgramType=' + ProgramType.id);
    var url = $('#Modal').data('url');
    $('#myModalLabel').html('Add a indicator');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(450);
        changeToGridIndicator();
    });
}
function SavePT() {
    total = $("#gridProgtype").pqGrid("option", "dataModel.data").length;

    for (i = 0; i < total; i++)
    {
        ProgramType = ProgramTypeModel.parse($("#gridProgtype").pqGrid("getRowData", { rowIndxPage: i }));
        if (ProgramType.Name!=undefined && ProgramType.Name.length > 0)
            $.post('/Totalnput/SaveProgType', { id: ProgramType.id, Description: ProgramType.Name, Link: ProgramType.Link, Delete: false });
    }

    window.location.reload();
}
function SaveMilestone() {
    if (gridMilestoneLoaded) {
        total = $("#gridMilestonCat").pqGrid("option", 'dataModel').data.length;
        save = 0;
        for (i = 0; i < total; i++) {
            ProgramType.Milestones = Milestone.parse($("#gridMilestonCat").pqGrid("option", 'dataModel').data[i]);
            if (ProgramType.Milestones.Name!=undefined && ProgramType.Milestones.Name.length > 0) {
                $.post("/Totalnput/SaveMilestone", { id: ProgramType.Milestones.id, Name: ProgramType.Milestones.Name, Definition: ProgramType.Milestones.Definition, Delete: false }, function (response) {
                    if (total == save) {
                        location.reload();
                    }
                });
            }
        }
    }
}
function SaveIndicators() {
    if (gridIndicatorLoaded) {
        total = $("#gridIndicatorCat").pqGrid("option", 'dataModel').data.length;
        save = 0;
        for (i = 0; i < total; i++) {
            ProgramType.Indicators = Indicator.parse($("#gridIndicatorCat").pqGrid("option", 'dataModel').data[i]);
            if (ProgramType.Indicators.Name!=undefined && ProgramType.Indicators.Name.length > 0) {
                $.post("/Totalnput/SaveIndicator", { id: ProgramType.Indicators.id, Name: ProgramType.Indicators.Name, Delete: false }, function (response) {
                    if (total == save) {
                        location.reload();
                    }
                });
            }
        }
    }
}
function AddMilestoneToPT()
{
    if (gridMilestoneLoaded) {
        var total = $("#gridMilestonCat").pqGrid("selection", { type: 'row', method: 'getSelection' }).length;
        save = 0;

        if (total == 0) {
            $('#warning').show();
            $('#warningMessage').text("Select an indicator");
            return;
        }
        for (i = 0; i < total; i++) {
            ProgramType.Milestones = Milestone.parse($("#gridMilestonCat").pqGrid("selection", { type: 'row', method: 'getSelection' })[i].rowData);

            if (ProgramType.Milestones.id > 0) {
                $.post("/Totalnput/SavePTMilestone", { PTID: $('#PT').html(), MilestoneID: ProgramType.Milestones.id, Delete: false }, function (response) {
            
                    if(total==save)
                    {
                        location.reload();
                    }
                });
            }
        }
    }
}
function AddIndicatorToPT()
{
    if (gridIndicatorLoaded) {
        var total = $("#gridIndicatorCat").pqGrid("selection", { type: 'row', method: 'getSelection' }).length;
        save = 0;

        if (total == 0) {
            $('#warning').show();
            $('#warningMessage').text("Select an indicator");
            return;
        }
        for (i = 0; i < total; i++) {
            ProgramType.Indicators = Indicator.parse($("#gridIndicatorCat").pqGrid("selection", { type: 'row', method: 'getSelection' })[i].rowData);

            if (ProgramType.Indicators.id > 0) {
                $.post("/Totalnput/SavePTIndicator", { PTID: $('#PT').html(), IndicatorID: ProgramType.Indicators.id, Delete: false }, function (response) {
                    if (total == save) {
                        location.reload();
                    }
                });
            }
        }
        if (save > 0) {
            location.reload();
        }
    }
}