﻿var indicatorType = {
    id: 0,
    name: '',
    width: 800,
    border: 0,
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    }
};
var indicatorTypeDetail = {
    id : '',
    caption : null,
    axis : null,
    isArea: 0,
    idIndicatorType: 0,
    order: 0,
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    parse: function(data)
    {
        var newObject = Object.create(this);
        if (data !== undefined) {
            newObject.id =data.length>3? data[0] : 0;
            newObject.caption = data[1];
            newObject.axis = data[2] == "Column" ? 'X' : 'Y';
            newObject.order = data[3];
            newObject.isArea = data[4]=="Yes"? true : false;
        }
        return newObject;
    }
}
var cDG = {
    idIndicatorType: 0,
    data: [],
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    }
}

var indicator = null;
var DG = null;
var detail = null;
var indicators = [];
var datagrids = [];
var indicatorsDetail = [];
var obj=null;
var rowselected = 0;


function changeToGridTemplates(that,id) {
    var tbl = $("#Datas-"+id);
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 700,
        height: 600,
        title: "Details of Template",
        editModel: {
            saveKey: $.ui.keyCode.ENTER,
            select: false,
            keyUpDown: false,
            cellBorderWidth: 0
        },
        hoverMode: 'cell',
        selectionModel: { type: 'cell' },
        editor: { type: "textbox" },
        //DataGrid Menu
        toolbar: {
            items: [
                {
                    type: "button", label: 'Add Template',
                    listeners: [{ click: SaveDetail }]
                },
                {
                    type: "button", label: 'Add New Row',
                    listeners: [{ click: addNew }]
                },
                {
                    type: "button", label: 'Delete Row',
                    listeners: [{ click: DeleteRow }]
                }
                ,
                {
                    type: "button", label: 'Delete Template',
                    listeners: [{ click: DeleteTemplate }]
                }
            ]
        },
        stripeRows: true,
        collapsible:false,
        rowBorders: true
    };

    newObj.dataModel = { data: obj.data };

    newObj.colModel = [
        {
            title: "Id",
            width: 20,
            editable: false,
            Type: "string",
            visible: false,
            hidden: true
        //    align: "center"
        },
        {
            title: "Label",
            width: 400,
            dataType: "string",
            align: "left"
        },
        {
            title: "Type",
            editor: { type: 'select', options: ["Column", "Row"] },
            width: 100,
            dataType: "string",
            align: "center"
        },
        {
            title: "Order",
            width: 100,
            dataType: "integer",
            align: "center"
        },
        {
            title: "Is Text Area",
            editor: { type: 'select', options: ["No", "Yes"] },
            width: 30,
            dataType: "string",
            align: "center"
        }];
  
    newObj.pageModel = { rPP: 20, type: "local" };

    $("#grid_array").pqGrid(newObj);

    try {
        if ($("#grid_array").pqGrid() != null)
            $("#grid_array").pqGrid("destroy");
   

    var dg =$("#grid_array").pqGrid(newObj);
    
    //Actions
    dg.on("pqgridquiteditmode", function (evt, ui) {
        if (evt.keyCode != $.ui.keyCode.ESCAPE && evt.keyCode != $.ui.keyCode.TAB) {
            dg.pqGrid("saveEditCell");
        }
    });

    dg.on("pqgridcellbeforesave", function (evt, ui) {
        var isValid = dg.pqGrid("isValid", { dataIndx: ui.dataIndx, value: ui.newVal }).valid;
        if (!isValid) {
            dg.find(".pq-editor-focus").css({ "border-color": "red" });
            return false;
        }
    });
    dg.on("pqgridrowclick", function (event, ui) {
        rowselected = ui.rowIndx;
        $("#grid_array").pqGrid("editFirstCellInRow", { rowIndx: rowselected });

    });
    } catch (err) { }

}

function addNew() {
    $("#grid_array").pqGrid("addRow",
      { rowData: {}, rowIndx: 0 }
    );
}
function DeleteTemplate()
{
    $.post("../IndicatorTypes/DeleteTemplate", { Id: $("#template").val(), ContinuityID: $("#CID").val() })
 //   alert('Template Removed');
    window.location.reload();
}
function SaveDetail()
{

    var total = $("#grid_array").pqGrid("option", "dataModel.data").length;

    for (i = 0; i < total; i++)
    {
        var data = indicatorTypeDetail.parse( $("#grid_array").pqGrid("getRowData", { rowIndxPage: i }));
        $.post("../IndicatorTypes/SaveCfg",
            { id: data.id, caption: data.caption, axis: data.axis, area: data.isArea, typeid: $("#template").val(), cid: $("#CID").val(), WWID: $("#WWID").val(), order: data.order, deleted: false }
            , function (response) {
                if (response > 1)
                    alert('Saved');
        });
    }

  //  alert('Saved');
    window.location.reload();
}
function DeleteRow()
{
    if (confirm("Do you want delete this Label?")) {
        var total = $("#grid_array").pqGrid("option", "dataModel.data").length;

        var data = indicatorTypeDetail.parse($("#grid_array").pqGrid("getRowData", { rowIndxPage: rowselected }));
        $.post("../IndicatorTypes/SaveCfg",
            { id: data.id, caption: data.caption, axis: data.axis, area: data.isArea, typeid: $("#template").val(), cid: $("#CID").val(), WWID: $("#WWID").val(), order: data.order, deleted: true }
            , function (response) {
                if (response > 1)
                    alert('Saved');
            });


        $("#grid_array").pqGrid("deleteRow", { rowIndx: rowselected });
        window.location.reload();
    }

}
function dataTemplate(id)
{
    if ($("#dataTemplate").is(":Visible"))
        $("#dataTemplate").hide();
    else
        $("#dataTemplate").show();
}
function LoadTemplateDetail(id)
{
    //for (i = 0; i < indicators.length; i++)
    //{
    //    var indicator = indicatorType.create(indicators[i]);

    //    if(indicator.id==id)
    //    {
    //        $("#tmpName").val(indicator.name);
    //        $("#tmpBorder").val(indicator.border);
    //        $("#tmpWidth").val(indicator.width);

            
    //    }
    //}
    try{
        changeToGridTemplates(this, id);
    }catch(err){ return null; }
    
}