﻿function ProductCostFunction($resource) {
    return $resource('/routesDemo/ProductCostService');
};
ProductCostFunction.$inject = ['$resource'];
var ProductCostController = function (ProductCost, $http, $log, $filter) {
    var vm = this;
    ProductCost.save().$promise.then(function (data) {
        var aux;
        vm.PCOSArrayAux = [];
        vm.PCOSArray = [];
        angular.forEach(data.list, function (value) {
            aux = vm.PCOSArrayAux.indexOf(value.SKU)
            if (aux == -1) {
                vm.PCOSArrayAux.push(value.SKU);
                vm.objaux = {
                    show: -1,
                    info: -1,
                    SKU: value.SKU,
                    PXTName: value.PXTName,
                    ProgName: value.ProgName,
                    ProgDesc: value.ProgDesc,
                    ProgType: value.ProgType,
                    GEO: value.GEO,
                    PLCStatus: value.PLCStatus,
                    OwnerName: value.OwnerName,
                    CostCategory: value.CostCategory,
                    TargetValue: value.TargetValue,
                    ActualValue: value.ActualValue,
                    TrendValue: value.TrendValue,
                    GAP: value.TargetValue - value.TrendValue,
                    hijo: []
                };
                value.GAP = value.TargetValue - value.TrendValue;
                vm.objaux.hijo.push(value);
                vm.PCOSArray.push(vm.objaux);
            } else {
                angular.forEach(vm.PCOSArray, function (x) {
                    if (x.SKU == value.SKU) {
                        x.TargetValue = x.TargetValue + value.TargetValue;
                        x.ActualValue = x.ActualValue + value.ActualValue;
                        x.TrendValue = x.TrendValue + value.TrendValue;
                        x.GAP = x.GAP + (value.TargetValue - value.TrendValue),
                        value.GAP = value.TargetValue - value.TrendValue;
                        x.hijo.push(value);
                    }
                });
            }
        });
    }).catch(function (response) {
        $log.info(response);
    });
};

ProductCostController.$inject = ['ProductCost', '$http', '$log', '$filter'];