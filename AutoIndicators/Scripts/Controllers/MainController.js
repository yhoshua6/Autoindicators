﻿function MainFunction($resource) {
    return $resource('/routesDemo/MainService');
};
function SearchAcronymsFunction($resource) {
    return $resource('/Snapshot/SearchAcronyms');
};
function SnapshotFunction($resource) {
    return $resource('/Snapshot/Snapshot');
};
MainFunction.$inject = ['$resource'];
var MainController = function (Main, SearchAcronyms, Snapshot, $http, $log, $filter) {
    var vm = this;
    vm.loading = true;
    Main.save().$promise.then(function (data) {
        vm.loading = false;
        vm.list = $filter('filter')(data.myModel, { Key: 'list' })[0].Value;
        angular.forEach(vm.list, function (value) {
            value.info = -1;
            value.flagRichText = -1;
        });
        $log.info(vm.list);
        vm.PlCList = $filter('filter')(data.myModel, { Key: 'PlCList' })[0];
        vm.validUsers = $filter('filter')(data.myModel, { Key: 'validUsers' })[0];
        vm.VerticalList = $filter('filter')(data.myModel, { Key: 'VerticalList' })[0];
        vm.WWID = $filter('filter')(data.myModel, { Key: 'WWID' })[0];
    }).catch(function (response) {
        $log.info(data);
    });
    vm.loadingSA = false;
    vm.searchAcronym = function (x, view) {
        vm.loadingSA = true;
        SearchAcronyms.save({ TextAcronyms: x.Updatetxt, WWID: x.WWID, ContinuityID: x.ContinuityID, Owner: x.OwnerId, ProgType: x.ProgTypeID, PXT: x.PXTId, View: view }).$promise.then(function (response) {
            $log.info(x);
            vm.myRichText = "";
            angular.forEach(response, function (value, index) {
                if (String(value) != 'true' && String(value) != '[object Object]') {
                    vm.myRichText = vm.myRichText + String(value);
                }
            });
            x.Updatetxt = vm.myRichText;
            vm.loadingSA = false;
        }).catch(function (response) {
            $log.info(response);
        });
    };

    vm.saveSnapshot = function (x, theNewVal) {
        vm.loadingSaving = true;
        Snapshot.save({ PWID: x.PWID, opType: 1, newVal: theNewVal, WWID: vm.WWID }).$promise.then(function (response) {
            $log.info(response);
            x.flagRichText = -1;
            vm.loadingSaving = false;
        }).catch(function (response) {
            $log.info(response);
        });
    };
    vm.manual = function () {
        vm.selectedText = '';
        if (window.getSelection) {
            vm.selectedText = window.getSelection().toString();
        } else if (document.selection) {
            vm.selectedText = document.selection.createRange().text;
        }
        $log.info(vm.selectedText);
    };
};

MainController.$inject = ['Main', 'SearchAcronyms', 'Snapshot', '$http', '$log', '$filter'];