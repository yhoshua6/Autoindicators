﻿function CCBFunction($resource) {
    return $resource('/routesDemo/ControlChangeBoardService');
};
CCBFunction.$inject = ['$resource'];
var CCBController = function (CCB, $http, $log, $filter) {
    var vm = this;
    vm.loading = true;

    vm.resetShows = function (list, y) {
        angular.forEach(list, function (x) {
            if (x.intStatus != y.intStatus) {
                x.selected = -1
            } else {
                x.selected = 1;
            }
        });
    }

    CCB.save().$promise.then(function (data) {
        vm.thisCCBSID = -1;
        vm.CCBStatusList = data.CCBStatusList;
        vm.CatCCBStatus = data.CatCCBStatus;
        vm.CCBList = data.CCBlist;
        vm.CCBAffected = data.CCBAffected;
        $log.info(data.CCBAffected);
        var nonSnapshot = {
            ContinuityID: 'NON-SNAPSHOT',
            PXTName: 'Non Snapshot',
            ProgName: 'Non Snapshot',
            ProgDesc: 'Non Snapshot',
            ProgType: 'Undefined',
            GEO: 'Undefined',
            PLCStatus: 'Undefined',
            OwnerName: 'Undefined'      
        }
        vm.CCBList.push(nonSnapshot);

        angular.forEach(vm.CatCCBStatus, function (x) {
            x.hijos = [];
            angular.forEach(vm.CCBStatusList, function (y) {
                if (x.intStatus == y.intStatus) {
                    y.dttCreateDate = new Date(parseInt(y.dttCreateDate.substr(6)));
                    x.hijos.push(y);
                }
            });
        });

        angular.forEach(vm.CCBList, function (y) {
            y.CCBStatus = [];
            y.info = -1;
            y.showTable = -1;
            y.selectedAll = -1;
            y.total = 0;
        });
        angular.forEach(vm.CatCCBStatus, function (x) {
            angular.forEach(vm.CCBList, function (y) {
                var newObj = {
                    selected: -1,
                    intStatus: x.intStatus,
                    strName: x.strName,
                    strShortName: x.strShortName,
                    hijos: $filter('filter')(x.hijos, { ContinuityID: y.ContinuityID })
                }
                y.total = y.total + newObj.hijos.length;
                y.CCBStatus.push(newObj);
            });
        });

        angular.forEach(vm.CCBAffected, function (x) {
            x.intCCBid = x.intID;
            x.strName = x.strStatus;
            x.dttCreateDate = new Date(parseInt(x.dttDate.substr(6)));;
            x.strUser = x.strOwner;
        });
        
        angular.forEach(vm.CCBList, function (y) {
            var newObj = {
                selected: -1,
                intStatus: -3,
                strName: 'Affected By',
                strShortName: 'Affected By',
                hijos: $filter('filter')(vm.CCBAffected, { ContinuityID: y.ContinuityID })
            }
            y.total = y.total + newObj.hijos.length;
            y.CCBStatus.push(newObj);
        });

        vm.loading = false;
    }).catch(function (response) {
        $log.info(response);
    });
};

CCBController.$inject = ['CCB', '$http', '$log', '$filter'];