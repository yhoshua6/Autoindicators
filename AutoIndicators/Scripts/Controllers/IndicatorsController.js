﻿function IndicatorsFunction($resource) {
    return $resource('/routesDemo/Indicator');
};
function ProgramsFunction($resource) {
    return $resource('/routesDemo/getProgramsAngular');
};
IndicatorsFunction.$inject = ['$resource'];
ProgramsFunction.$inject = ['$resource'];
var IndicatorsController = function (Indicators, Programs, $http, $log, $filter, $interval) {
    var vm = this;
    vm.clock = { time: "", interval: 1000 };
    $interval(function () { vm.clock.time = Date.now(); }, vm.clock.interval);

    Programs.save().$promise.then(function (data) {
        vm.programs = data.Mylits;
    }).catch(function (response) {
        $log.info(data);
    });
    vm.setProgram = function (x) {
        vm.thisProgram = $filter('filter')(vm.programs, { ContinuityID: x })[0];
        vm.getProgramData(vm.thisProgram.ContinuityID, vm.thisProgram.WWID, vm.thisProgram.ProgTypeID);
    }
    vm.show = -1;
    vm.all = false;
    vm.getProgramData = function (ContID, WW, PTID) {
        var aux;
        vm.loading = true;
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 1 }).$promise.then(function (data1) {
            vm.Snapshot = data1.Snapshot[0];
            vm.Snapshot.bandera = -1;
            Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 2 }).$promise.then(function (data2) {
                vm.Healthlist = data2.Healthlist;
                vm.Healthlist.bandera = -1;
                Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 3 }).$promise.then(function (data3) {
                    vm.Schedulelist = data3.Schedulelist;
                    vm.Schedulelist.bandera = -1;
                    Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 4 }).$promise.then(function (data4) {
                        vm.PCOSList = data4.PCOSList;
                        vm.PCOSArrayAux = [];
                        vm.PCOSArray = [];
                        vm.PCOSArray.bandera = -1;
                        angular.forEach(vm.PCOSList, function (value) {
                            aux = vm.PCOSArrayAux.indexOf(value.SKU)
                            if (aux == -1) {
                                vm.PCOSArrayAux.push(value.SKU);
                                vm.objaux = {
                                    show:-1,
                                    SKU:value.SKU,
                                    TargetValue:value.TargetValue,
                                    ActualValue:value.ActualValue,
                                    TrendValue: value.TrendValue,
                                    hijo:[]
                                };
                                vm.objaux.hijo.push(value);
                                vm.PCOSArray.push(vm.objaux);
                            } else {
                                angular.forEach(vm.PCOSArray, function (x) {
                                    if (x.SKU == value.SKU) {
                                        x.TargetValue = x.TargetValue + value.TargetValue;
                                        x.ActualValue = x.ActualValue + value.ActualValue;
                                        x.TrendValue = x.TrendValue + value.TrendValue;
                                        x.hijo.push(value);
                                    }
                                });
                            }
                        });
                        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 5 }).$promise.then(function (data5) {
                            vm.Budgetlist = data5.Budgetlist;
                            vm.BudgetHeaders = data5.BudgetHeaders[0];
                            var categoryIds = [];
                            vm.BudgetArray = [];
                            vm.BudgetArray.bandera = -1;
                            angular.forEach(data5.Budgetlist, function (value) {
                                aux = categoryIds.indexOf(value.CategoryId)
                                if (aux == -1) {
                                    categoryIds.push(value.CategoryId);
                                    vm.objaux = {
                                        show: -1,
                                        CategoryId: value.CategoryId,
                                        CategoryName: value.CategoryName,
                                        Data1: value.Data1,
                                        Data2: value.Data2,
                                        Data3: value.Data3,
                                        Data4: value.Data4,
                                        Data5: value.Data5,
                                        Trend: value.Trend,
                                        RYG: value.RYG,
                                        Comments: value.Comments,
                                        hijo: []
                                    };
                                    vm.objaux.hijo.push(value);
                                    vm.BudgetArray.push(vm.objaux);
                                } else {
                                    angular.forEach(vm.BudgetArray, function (x) {
                                        if (x.CategoryId == value.CategoryId) {
                                            x.Data1 = x.Data1 + value.Data1;
                                            x.Data2 = x.Data2 + value.Data2;
                                            x.Data3 = x.Data3 + value.Data3;
                                            x.Data4 = x.Data4 + value.Data4;
                                            x.Data5 = x.Data5 + value.Data5;
                                            x.hijo.push(value);
                                        }
                                    });
                                }
                            });
                            Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 6 }).$promise.then(function (data6) {
                                vm.thisCCBSID = -1;
                                vm.CCBStatusList = data6.CCBStatusList;
                                vm.CCBStatusCat = data6.CCBStatusCat;
                                vm.CCBListProj = data6.CCBListProj;
                                angular.forEach(vm.CCBStatusCat, function (y) {
                                    y.count = 0;
                                });
                                angular.forEach(vm.CCBStatusList, function (x) {
                                    x.dttCreateDate = new Date(parseInt(x.dttCreateDate.substr(6)))
                                    angular.forEach(vm.CCBStatusCat, function (y) {
                                        if (x.intStatus == y.intStatus) {
                                            y.count += 1;
                                            y.show = -1;
                                        }
                                    });
                                });
                                angular.forEach(vm.CCBListProj, function (y) {
                                    y.intStatus = -2;
                                    y.dttCreateDate = new Date(parseInt(y.dttDate.substr(6)));
                                    y.intCCBid = y.intID;
                                    y.strTitle = y.strTitle;
                                    y.strName = y.strStatus;
                                    y.strUser = y.strOwner;
                                    vm.CCBStatusList.push(y);
                                });
                                vm.loading = false;
                            }).catch(function (data6) {
                                $log.info(data6);
                            });
                        }).catch(function (data5) {
                            $log.info(data5);
                        });
                    }).catch(function (data4) {
                        $log.info(data4);
                    });
                }).catch(function (data3) {
                    $log.info(data3);
                });
            }).catch(function (data2) {
                $log.info(data2);
            });
        }).catch(function (data1) {
            $log.info(data1);
        });
    }

    vm.getSnapshotComment_loading = false;
    vm.getHeatlhIndicatorComment_loading = false;
    vm.lstScheduleComment_loading = false;

    vm.getSnapshotComment = function (ContID, WW, PTID) {
        vm.lstSnapshotComment = null;
        vm.getSnapshotComment_loading = true;
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 11 }).$promise.then(function (data) {
            vm.lstSnapshotComment = data.lstSnapshotComment;
            vm.lstSnapshotComment.limit = 1;
            vm.getSnapshotComment_loading = false;
        });
    };
    vm.getHeatlhIndicatorComment = function (ContID, WW, PTID) {
        vm.lstHeatlhIndicatorComment = null
        vm.getHeatlhIndicatorComment_loading = true;
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 22 }).$promise.then(function (data) {
            vm.lstHeatlhIndicatorComment = data.lstHeatlhIndicatorComment;
            vm.lstHeatlhIndicatorComment.limit = 1;
            vm.getHeatlhIndicatorComment_loading = false;
        });
    };
    vm.getScheduleComment = function (ContID, WW, PTID) {
        vm.lstScheduleComment = null
        vm.lstScheduleComment_loading = true;
        vm.lstScheduleComment = {
            limit: 1,
            comments: []
        }
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 33 }).$promise.then(function (data) {
            var arrayAux = [];
            angular.forEach(data.lstScheduleComment, function (x) {
                if (arrayAux.indexOf(x.Timestamp) == -1) {
                    arrayAux.push(x.Timestamp);
                }
            });
            angular.forEach(arrayAux, function (y, key) {
                var aux = $filter('filter')(data.lstScheduleComment, { Timestamp: y });
                aux.id = key;
                vm.lstScheduleComment.comments.push(aux);
            });
            vm.lstScheduleComment_loading = false;
        });
    };
    vm.getPCOSListComment = function (ContID, WW, PTID) {
        vm.lstPCOSComment = null
        vm.getPCOSListComment_loading = true;
        vm.lstPCOSComment = {
            limit: 1,
            comments: []
        }
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 44 }).$promise.then(function (data) {
            var arrayAux = [];
            angular.forEach(data.lstPCOSComment, function (x) {
                if (arrayAux.indexOf(x.Timestamp) == -1) {
                    arrayAux.push(x.Timestamp);
                }
            });
            angular.forEach(arrayAux, function (y, key) {
                var aux = $filter('filter')(data.lstPCOSComment, { Timestamp: y });
                aux.id = key;
                vm.lstPCOSComment.comments.push(aux);
            });
            vm.getPCOSListComment_loading = false;
        });
    };
    vm.getlstBudgetComment = function (ContID, WW, PTID) {
        vm.lstBudgetComment = null
        vm.lstBudgetComment_loading = true;
        Indicators.save({ ContinuityID: ContID, WWID: WW, ProgTypeID: PTID, wich: 55 }).$promise.then(function (data) {
            vm.lstBudgetComment = data.lstBudgetComment;
            vm.lstBudgetComment.limit = 1;
            vm.lstBudgetComment_loading = false;
        });
    };
};

IndicatorsController.$inject = ['Indicators', 'Programs', '$http', '$log', '$filter','$interval'];