﻿
/** @ngInject */
function acmeNavbar() {
    var directive = {
        restrict: 'E',
        templateUrl: 'components/Navbar',
        controller: 'NavbarController',
        controllerAs: 'myNavbar'
    };

    return directive;
}