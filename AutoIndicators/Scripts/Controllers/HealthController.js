﻿function HealthFunction($resource) {
    return $resource('/routesDemo/HealthService');
};
HealthFunction.$inject = ['$resource'];
var HealthController = function (Health, $http, $log, $filter) {
    var vm = this;
    vm.healthIds = [];
    vm.projects = [];
    vm.loading = true;
    Health.save().$promise.then(function (data) {
        angular.forEach(data.list, function (value) {
            if (vm.healthIds.indexOf(value.ContinuityID) == -1) {
                vm.healthIds.push(value.ContinuityID);
            }
        });
        angular.forEach(vm.healthIds, function (value) {
            vm.thisproject = $filter('filter')(data.list, { ContinuityID: value });
            var project = {
                ProgName: vm.thisproject[0].ProgName,
                ProgDesc: vm.thisproject[0].ProgDesc,
                ProgType: vm.thisproject[0].ProgType,
                GEO: vm.thisproject[0].GEO,
                PLCStatus: vm.thisproject[0].PLCStatus,
                OwnerName: vm.thisproject[0].OwnerName,
                info: -1,
                PXTName: vm.thisproject[0].PXTName,
                ContinuityID: vm.thisproject[0].ContinuityID,
                Fields: vm.thisproject
            }
            vm.projects.push(project);
        });
        vm.loading = false;
    }).catch(function (response) {
        $log.info(response);
    });
};

HealthController.$inject = ['Health', '$http', '$log', '$filter'];