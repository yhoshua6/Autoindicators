﻿function ScheduleFunction($resource) {
    return $resource('/routesDemo/ScheduleService');
};
ScheduleFunction.$inject = ['$resource'];
var ScheduleController = function (Schedule, SearchAcronyms, $http, $log, $filter) {
    var vm = this;
    vm.loading = true;
    Schedule.save().$promise.then(function (data) {
        vm.schedule = $filter('filter')(data.myModel, { Key: 'Schedule' })[0].Value;
        vm.projectsKeys = [], vm.projects = [];
        angular.forEach(vm.schedule, function (value) {
            if (vm.projectsKeys.indexOf(value.ContinuityID) == -1) {
                vm.projectsKeys.push(value.ContinuityID);
            }
        });
        angular.forEach(vm.projectsKeys, function (value) {
            vm.thisproject = $filter('filter')(vm.schedule, { ContinuityID: value });
            var project = {
                info: -1,
                Clasification: vm.thisproject[0].Clasification,
                ClasificationName: vm.thisproject[0].ClasificationName,
                ContinuityID: vm.thisproject[0].ContinuityID,
                ProgName: vm.thisproject[0].ProgName,
                ProgDesc: vm.thisproject[0].ProgDesc,
                ProgType: vm.thisproject[0].ProgType,
                GEO: vm.thisproject[0].GEO,
                PLCStatus: vm.thisproject[0].PLCStatus,
                OwnerName: vm.thisproject[0].OwnerName,
                PXTName: vm.thisproject[0].PXTName,
                Fields: vm.thisproject
            }
            vm.projects.push(project);
        });
        vm.loading = false;
    }).catch(function (response) {
        $log.info(response);
    });
    vm.setModal = function (x, y) {
        vm.modalSchedule = x;
        vm.modalField = y;
        $log.info(vm.modalSchedule);
        $log.info(vm.modalField);
    };
    vm.myUpdatetxt = '';
    vm.searchAcronym = function (x, view) {
        vm.loadingSA = true;
        $log.info(x);
        SearchAcronyms.save({ TextAcronyms: vm.myUpdatetxt, WWID: x.WWID, ContinuityID: x.ContinuityID, Owner: x.OwnerId, ProgType: x.ProgTypeID, PXT: x.PXTId, View: view }).$promise.then(function (response) {
            $log.info(x);
            vm.myRichText = "";
            angular.forEach(response, function (value, index) {
                if (String(value) != 'true' && String(value) != '[object Object]') {
                    vm.myRichText = vm.myRichText + String(value);
                }
            });
            vm.myUpdatetxt = vm.myRichText;
            vm.loadingSA = false;
        }).catch(function (response) {
            $log.info(response);
        });
    };

    vm.manual = function () {
        vm.selectedText = '';
        if (window.getSelection) {
            vm.selectedText = window.getSelection().toString();
        } else if (document.selection) {
            vm.selectedText = document.selection.createRange().text;
        }
        $log.info(vm.selectedText);
    };
};
ScheduleController.$inject = ['Schedule', 'SearchAcronyms', '$http', '$log', '$filter'];