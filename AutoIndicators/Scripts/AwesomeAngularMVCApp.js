﻿var AwesomeAngularMVCApp = angular.module('AwesomeAngularMVCApp', ['ngResource', 'ui.router', 'ngSanitize', 'textAngular']);

AwesomeAngularMVCApp.factory('Main', MainFunction);
AwesomeAngularMVCApp.factory('SearchAcronyms', SearchAcronymsFunction);
AwesomeAngularMVCApp.factory('Snapshot', SnapshotFunction);

AwesomeAngularMVCApp.factory('Health', HealthFunction);
AwesomeAngularMVCApp.factory('Schedule', ScheduleFunction);
AwesomeAngularMVCApp.factory('ProductCost', ProductCostFunction);
AwesomeAngularMVCApp.factory('CCB', CCBFunction);
AwesomeAngularMVCApp.factory('Indicators', IndicatorsFunction);
AwesomeAngularMVCApp.factory('Programs', ProgramsFunction);

AwesomeAngularMVCApp.directive('acmeNavbar', acmeNavbar);
AwesomeAngularMVCApp.controller('NavbarController', NavbarCtrl);
AwesomeAngularMVCApp.controller('LandingPageController', LandingPageController);
AwesomeAngularMVCApp.controller('MainController', MainController);
AwesomeAngularMVCApp.controller('HealthController', HealthController);
AwesomeAngularMVCApp.controller('ScheduleController', ScheduleController);
AwesomeAngularMVCApp.controller('ProductCostController', ProductCostController);
AwesomeAngularMVCApp.controller('CCBController', CCBController);
AwesomeAngularMVCApp.controller('CCBRegisterController', CCBRegisterController);
AwesomeAngularMVCApp.controller('IndicatorsController', IndicatorsController);

var configFunction = function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('Snapshot', {
            url: '/Snapshot',
            templateUrl: 'routesDemo/Snapshot'
        })
        .state('HealthIndicator', {
            url: '/HealthIndicator',
            templateUrl: 'routesDemo/HealthIndicator'
        })
        .state('ProductCost', {
            url: '/ProductCost',
            templateUrl: 'routesDemo/ProductCost'
        })
        .state('ControlChangeBoard', {
            url: '/ControlChangeBoard',
            templateUrl: 'routesDemo/ControlChangeBoard'
        })
        .state('CCBRegister', {
            url: '/CCBRegister/:ContID/:proj/:ccbid',
            templateUrl: 'routesDemo/CCBRegister'
        })
        .state('Schedule', {
            url: '/Schedule',
            templateUrl: 'routesDemo/Schedule'
        })
        .state('Indicators', {
            url: '/Indicators',
            templateUrl: 'routesDemo/Indicators'
        });
}
configFunction.$inject = ['$stateProvider', '$urlRouterProvider'];

AwesomeAngularMVCApp.config(configFunction);