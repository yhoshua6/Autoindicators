﻿index = 1;
var items = [];
var send = true;
var selectedStatus = 0;
var idDecision = 0;
var item = {
    id: 0,
    email: '',
    role: '',
    estatusId: 0,
    rapidId : 0,
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    add : function(){
        items[items.length]=this;
    }
}
var Aorigin = [];
function ShowComment(id,type)
{
    if (type == 'D') {
        var objs = $('div[data-type="D"]');
        $('div[data-type="D"]').hide();
    }

    $("#reject-" + id).show();
}
function HideComment(id,type) {
    $("#reject-" + id).hide();
    if (type == 'D') {
        var objs = $('div[data-type="D"]');
        $('div[data-type="D"]').hide();
    }
}
function add(sector) {
    var obj = $("#item-" + sector + "-0").clone();

    if (obj.children().length > 3) {
        try
        {
            var obj2 = obj.children()[5].children;
            $(obj.children()[5]).attr('id', 'reject-' + sector + '-100' + index);
            $(obj2).attr('id', 'txtComment-' + sector + '-100' + index);
            $(obj.children()[5]).attr('name', 'reject-' + sector + '-100' + index);
            $(obj2).attr('name', 'txtComment-' + sector + '-100' + index);
        }catch(exc){}
    }
    var newItem = item;

    obj.attr("id", "item-" + sector + "-" + index)
    $(obj.children()[1]).val('');
    $(obj.children()[2]).val('');
    $(obj.children()[1]).attr('id','txtEmail-'+sector+'-100'+index);
    $(obj.children()[2]).attr('id', 'txtRole-' + sector + '-100' + index);
    $(obj.children()[3]).attr('id', 'chk-' + sector + '-100' + index);
    $(obj.children()[3]).attr('onclick',"HideComment('"+sector + "-100" + index+"')");
    $(obj.children()[4]).attr('id', 'chk-' + sector + '-100' + index);
    $(obj.children()[4]).attr('onclick',"ShowComment('"+sector + "-100" + index+"')");
    $(obj.children()[1]).attr('name', 'txtEmail-' + sector + '-100' + index);
    $(obj.children()[2]).attr('name', 'txtRole-' + sector + '-100' + index);
    $(obj.children()[3]).attr('name', 'chk-' + sector + '-100' + index);
    $(obj.children()[4]).attr('name', 'chk-' + sector + '-100' + index);
    
    $(obj).show();

    $("#items-" + sector).append(obj);

    var n = $("#item-" + sector + "-" + (index - 1)).children();
    newItem.email = $(n[0]).val();
    newItem.role = $(n[1]).val();
    newItem.rapidId = sector;
    newItem.add();

    var height = $("#detail-" + sector).css("height").replace('px', '');

    $("#detail-" + sector).css("height", parseInt(height) + 50);
    index++;
}
function add(sector) {
    var obj = $("#item-" + sector + "-0").clone();

    if (obj.children().length > 3) {
        try {
            var obj2 = obj.children()[5].children;
            $(obj.children()[5]).attr('id', 'reject-' + sector + '-100' + index);
            $(obj2).attr('id', 'txtComment-' + sector + '-100' + index);
            $(obj.children()[5]).attr('name', 'reject-' + sector + '-100' + index);
            $(obj2).attr('name', 'txtComment-' + sector + '-100' + index);
        } catch (exc) { }
    }
    var newItem = item;

    obj.attr("id", "item-" + sector + "-" + index)
    $(obj.children()[1]).val('');
    $(obj.children()[2]).val('');
    $(obj.children()[1]).attr('id', 'txtEmail-' + sector + '-100' + index);
    $(obj.children()[1]).attr('name', 'txtEmail-' + sector + '-100' + index);
    $(obj.children()[2]).attr('id', 'txtRole-' + sector + '-100' + index);
    $(obj.children()[2]).attr('name', 'txtRole-' + sector + '-100' + index);
    if($(obj.children()[3]).attr('id')=='NR')
    {
        $(obj.children()[3]).hide();
    }else
    {
        $(obj.children()[3]).attr('id', 'chk-' + sector + '-100' + index);
        $(obj.children()[3]).attr('onclick', "HideComment('" + sector + "-100" + index + "')");
    }
    $(obj.children()[3]).attr('name', 'chk-' + sector + '-100' + index);
    $(obj.children()[4]).attr('id', 'chk-' + sector + '-100' + index);
    $(obj.children()[4]).attr('onclick', "ShowComment('" + sector + "-100" + index + "')");
    $(obj.children()[4]).attr('name', 'chk-' + sector + '-100' + index);

    $(obj).show();

    $("#items-" + sector).append(obj);

    var n = $("#item-" + sector + "-" + (index - 1)).children();
    newItem.email = $(n[0]).val();
    newItem.role = $(n[1]).val();
    newItem.rapidId = sector;
    newItem.add();

    var height = $("#detail-" + sector).css("height").replace('px', '');

    $("#detail-" + sector).css("height", parseInt(height) + 50);
    index++;
}
function addSemicolon(sector,obj) {
    if (obj.length > 0) {
        var emails = obj.split(';')
        $("#chkNRequired").hide();

        for (i = 0; i < emails.length; i++) {
            if (i == 0) {
                $($("#item-" + sector + "-0").children()[1]).val(emails[i]);
                SearchRole(emails[i], $($("#item-" + sector + "-0").children()[2]).attr('id'));
            } else {
                var obj = $("#item-" + sector + "-0").clone();
                $("#item-" + sector + "-0");
                if (obj.children().length > 3) {
                    try {
                        var obj2 = obj.children()[5].children;
                        $(obj.children()[5]).attr('id', 'reject-' + sector + '-100' + index);
                        $(obj2).attr('id', 'txtComment-' + sector + '-100' + index);
                        $(obj.children()[5]).attr('name', 'reject-' + sector + '-100' + index);
                        $(obj2).attr('name', 'txtComment-' + sector + '-100' + index);
                    } catch (exc) { }
                }
                var newItem = item;

                obj.attr("id", "item-" + sector + "-" + index)
                //  $("#item-" + sector + "-0").hide();
                $(obj.children()[1]).val(emails[i]);
                $(obj.children()[1]).attr('id', 'txtEmail-' + sector + '-100' + index);
                $(obj.children()[2]).attr('id', 'txtRole-' + sector + '-100' + index);
                SearchRole(emails[i], 'txtRole-' + sector + '-100' + index);
                $(obj.children()[3]).attr('id', 'chk-' + sector + '-100' + index);
                $(obj.children()[3]).attr('onclick', "HideComment('" + sector + "-100" + index + "')");
                $(obj.children()[4]).attr('id', 'chk-' + sector + '-100' + index);
                $(obj.children()[4]).attr('onclick', "ShowComment('" + sector + "-100" + index + "')");
                $(obj.children()[1]).attr('name', 'txtEmail-' + sector + '-100' + index);
                $(obj.children()[2]).attr('name', 'txtRole-' + sector + '-100' + index);
                $(obj.children()[3]).attr('name', 'chk-' + sector + '-100' + index);
                $(obj.children()[4]).attr('name', 'chk-' + sector + '-100' + index);
                //  $(obj).parent().show();
                $(obj).show();

                $("#items-" + sector).append(obj);

                var n = $("#item-" + sector + "-" + (index - 1)).children();
                newItem.email = $(n[0]).val();
                newItem.role = $(n[1]).val();
                newItem.rapidId = sector;
                newItem.add();

                var height = $("#detail-" + sector).css("height").replace('px', '');

                $("#detail-" + sector).css("height", parseInt(height) + 50);
              
            }
            index++;
        }
    }
}
function AutoP(sector,ini)
{
    Aorigin.splice(0, Aorigin.length);
    Aorigin[Aorigin.length] = $('div[data-type="I"]').clone();
    var itemAdded = 0;

    for (i = 0; i < Aorigin[0].length; i++) {
        var obj=$(Aorigin[0][i]).clone();
        if ($(obj.children()[1]).val().length > 0) {
            index++;

            if (obj.children().length > 3) {
                try {
                    var obj2 = obj.children()[5].children;
                    $(obj.children()[5]).attr('id', 'reject-' + sector + '-100' + index);
                    $(obj2).attr('id', 'txtComment-' + sector + '-100' + index);
                    $(obj.children()[5]).attr('name', 'reject-' + sector + '-100' + index);
                    $(obj2).attr('name', 'txtComment-' + sector + '-100' + index);

                } catch (exc) { }
            }
            var newItem = item;

            obj.attr("id", "item-" + sector + "-" + index)
            obj.attr("data-type",ini)
            $(obj.children()[0]).text('"' + ini + '"');
            $(obj.children()[0]).attr('id','led-'+sector);
            $(obj.children()[1]).attr('id', 'txtEmail-' + sector + '-100' + index);
            $(obj.children()[2]).attr('id', 'txtRole-' + sector + '-100' + index);
            $(obj.children()[3]).attr('id', 'chk-' + sector + '-100' + index);
            $(obj.children()[3]).attr('onclick', "HideComment('" + sector + "-100" + index + "')");
            $(obj.children()[4]).attr('id', 'chk-' + sector + '-100' + index);
            $(obj.children()[4]).attr('onclick', "ShowComment('" + sector + "-100" + index + "')");
            $(obj.children()[1]).attr('name', 'txtEmail-' + sector + '-100' + index);
            $(obj.children()[2]).attr('name', 'txtRole-' + sector + '-100' + index);
            $(obj.children()[3]).attr('name', 'chk-' + sector + '-100' + index);
            $(obj.children()[4]).attr('name', 'chk-' + sector + '-100' + index);

            $(obj).show();
            $("#items-" + sector).append(obj);
            var n = $("#item-" + sector + "-" + (index - 1)).children();
            newItem.email = $(n[0]).val();
            newItem.role = $(n[1]).val();
            newItem.rapidId = sector;
            newItem.add();

            var height = $("#detail-" + sector).css("height").replace('px', '');

            $("#detail-" + sector).css("height", parseInt(height) + 80);
            itemAdded++;
        }
    }
    if ($($($("#items-" + sector).children()[0]).children()[1]).val().length == 0 && itemAdded>0)
        $($("#items-" + sector).children()[0]).hide();
    Aorigin.splice(0, Aorigin.length);
}
function ShowNotification(title,msg)
{

    var notification = new Notification(title, {
        icon: 'Images/intellogoBadge.png',
        body: msg,
        autoHideDelay: 3000,
      //  vibrate: [200,100,200]
    }); 
   
 //   notification.vibrate;
}
document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    $('#expand').click(function () {
      $('#Description').toggle(300, function () {
          if ($('#Description').is(':visible'))
              $('#expand').attr('src', '/Content/images/minus.png');
          else
              $('#expand').attr('src', '/Content/images/plus.jpg');
      }); 
    });

    $('#Copy').click(function () {
        Form.submit();
       
       
        //$(this).hide();
        
        
    })

    $('#btnRevise').click(function () {
        $('#btnRevise').hide();
        $('#Copy').show();
        $('img[id="R"').hide();
        $('#txtRecommend').val("");
        $('#RAPID').fadeToggle(800, "linear").delay(400).fadeToggle(800, "linear");
        $('#txtRev').val($('#idRapid').val());
        $('#idRapid').val('0');
        $.post('/RAPID/GetNextRev?idDecision=' + idDecision, function (data) {
            eval(data);
        });
    })
});
$(document).ready(function () {
    function disableBack() {
        if ($('#Copy').is(':visible')) {
            window.history.forward();
        } 
//        //else
////            window.history.back();
    }
    window.onload = disableBack();
    window.onpageshow = function (evt) { if (evt.persisted && $('#Copy').is(':visible'))  disableBack() }
});
function hideOptA(id,type) {
    if (!$('#txtEmail-' + id + '-1000').is(":disabled")) {
        $('#txtEmail-' + id + '-1000').prop( "disabled", true );
        $('#txtRole-' + id + '-1000').prop("disabled", true);
        $('#' + type).hide();
    } else {
        $('#txtEmail-' + id + '-1000').prop("disabled", false);
        $('#txtRole-' + id + '-1000').prop("disabled", false);
        $('#' + type).show();
    }
}
function createNotification(type) {
    if (type == '')
        type = 'normal';

    if (type != 'html') {
        var title = "Haz recibido una notificación con HTML5.";
        var msg = "El contenido va aquí......";
        var notification = window.Notifications.createNotification("1.png", title, msg);
    }
    else {
        var notification = window.Notifications.createHTMLNotification('content.html');
    }
    notification.show();
}
function validateEmail(email) {

    var reurl = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    if(!reurl.test(email))
    {
        $('#warning').show();
        $('#warningMessage').text("Please enter a valid email");
        return false;
    }else
        $('#warning').hide();

    return true;
}
function ShowCmbRel(type) {
    
    HideCmbRel();
    switch (type)
    {
        case "1":
            $("#CCB").show();
            break;
    }
}
function HideCmbRel()
{
    $("#CCB").hide();
}
function isEmpty()
{
    if ($("#txtTitle").val().length ==0) {
        $('#warning').show();
        $('#warningMessage').text("The title is required");
        return true;
    }else
    {
        $('#warning').hide();
        return false;
    }
}
function Upload(id,comment)
{
    if (comment.length > 0 && selectedStatus>0)
        $.post('/RAPID/SaveComments?idDetail=' + id + '&Comment=' + comment + '&idStatus=' + selectedStatus, function (data) { eval(data) });
    else
        alert('This comment is empty');
}
function SearchRole(email,role)
{
    if (email.length > 0)
        $.post('/RAPID/GetRol?strEmail=' + email + '&strRole=' + role, function (data) { eval(data) });
}
function expandinfo(item)
{
    $('.item[data-pack="' + item + '"]').toggle('blind', 300, function () {
        if ($('.item[data-pack="' + item + '"]').is(':visible')) {
            $('#expandinfo' + item).attr('src', '/Content/images/minus.png');
            var height = $("#detail-" + item).css("height").replace('px', '');

            $("#detail-" + item).css("height", parseInt(height) + 80);
        }
        else {
            $("#detail-" + item).css("height", 100);
            $('#expandinfo' + item).attr('src', '/Content/images/plus.jpg');
        }
    });
}

