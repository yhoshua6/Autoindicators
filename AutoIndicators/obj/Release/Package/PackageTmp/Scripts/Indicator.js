﻿var difrow = '';
var statbef = '';

function ShowMoreCommentsSchedule(value,btn) {
    $("tr[data-block-Schedule]").each(function (index) {
        if ($(this).attr("data-block-Schedule") <= value){
            $(this).show();
        }
        if ($(this).attr("data-block-Schedule") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-Schedule") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function ShowMoreCommentsValidation(value, btn) {
    $("tr[data-block-validation]").each(function (index) {
        if ($(this).attr("data-block-validation") <= value) {
            $(this).show();
        }
        if ($(this).attr("data-block-validation") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-validation") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if ($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function ShowMoreCommentsBudget(value, btn) {
    $("tr[data-block-Budget]").each(function (index) {
        if ($(this).attr("data-block-Budget") <= value) {
            $(this).show();
        }
        if ($(this).attr("data-block-Budget") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-Budget") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if ($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function ShowMoreCommentsHealth(value, btn) {
    $("tr[data-block-Health]").each(function (index) {
        if ($(this).attr("data-block-Health") <= value) {
            $(this).show();
        }
        if ($(this).attr("data-block-Health") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-Health") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if ($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function ShowMoreCommentsSnap(value, btn) {
    $("tr[data-block-Snap]").each(function (index) {
        if ($(this).attr("data-block-Snap") <= value) {
            $(this).show();
        }
        if ($(this).attr("data-block-Snap") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-Snap") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if ($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function ShowMoreCommentsPCOS(value, btn) {
    $("tr[data-block-PCOS]").each(function (index) {
        if ($(this).attr("data-block-PCOS") <= value) {
            $(this).show();
        }
        if ($(this).attr("data-block-PCOS") == value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                $(this).show();
        }
        if ($(this).attr("data-block-PCOS") > value + 1) {
            if ($(btn).attr('src') == '/Content/images/minus.png')
                $(this).hide();
            else
                if ($(this).is(":visible"))
                    $(this).hide();
        }
    })
    expand(btn);
}
function expand(btn) {
    if ($(btn).attr('src') == '/Content/images/minus.png')
        $(btn).attr('src', '/Content/images/plus.jpg');
    else
        $(btn).attr('src', '/Content/images/minus.png');
}
function expandComments(id) {
    if ($("#comment-" + id).is(":visible")) {
        $("#comment-" + id).hide();
    } else {
        $("#comment-" + id).show();
    }
}
function showDetailCCB(detail, row) {
    var components = document.querySelectorAll('[data-detail]');



    if (difrow != row) {
        for (i = 0; i < components.length; i++) {
            if (components[i] != null)
                components[i].style.display = 'none';
        }
        if (difrow.length > 0) {
            $("#" + difrow).hide();
        }
        $("#" + row).show();

        if (!$("#" + statbef).is(":visible"))
            $("#imgDetail-" + statbef).attr("src", "../Content/images/plus.jpg");
        else
            $("#imgDetail-" + statbef).attr("src", "../Content/images/minus.png");

        if ($("#" + detail).is(":visible"))
            $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
        else
            $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");



        $("#" + detail).show();

        difrow = row;
        statbef = detail;
    } else {


        if (statbef != detail) {
            if (statbef.length > 0) {
                if ($("#" + statbef).is(":visible")) {
                    $("#imgDetail-" + statbef).attr("src", "../Content/images/plus.jpg");
                    $("#" + statbef).hide();
                    $("#" + detail).show();
                } else
                    $("#" + detail).show();

                $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                $("#" + row).show();
            } else {
                for (i = 0; i < components.length; i++) {
                    if (components[i] != null)
                        components[i].style.display = 'none';
                }
                if ($("#" + detail).is(":visible")) {
                    $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
                    $("#" + detail).hide();
                    $("#" + row).hide();
                }
                else {
                    $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                    $("#" + detail).show();
                    $("#" + row).show();
                }
            }

            statbef = detail;

        } else {
            if ($("#" + detail).is(":visible")) {
                $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
                $("#" + detail).hide();
                $("#" + row).hide();
            }
            else {
                $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                $("#" + detail).show();
                $("#" + row).show();
            }
        }
    }


}
