﻿var FreeHtmlModel = {
    id: 0,
    Property: '',
    Value: '',
    Users: [],
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined)
                newObject.id = data[0];
            newObject.Name = data[1];
            newObject.Value = data[2];
        }
        return newObject;
    }
};
var UserFreeHtmlModel = {
    User: '',
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined)
                newObject.User = data[1];
        }
        return newObject;
    }
};

var Free = FreeHtmlModel;
var User = UserFreeHtmlModel;
var Selidx = null;

function changeToGridProg() {
    var tbl = $("#DataFree");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 500,
        height: 800,
        title: "Free Html",
        //DataGrid Menu
        editable: false,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;

    newObj.colModel[0].hidden = true;
    newObj.colModel[1].width = 200;
    newObj.colModel[2].width = 300;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.pageModel = { rPP: 30, type: "local" };

    $gridFREE = $("#gridDataFree").pqGrid(newObj);


    try {

        $gridFREE.pqGrid("option", "dataModel.sortIndx", 0);
        $gridFREE.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $gridFREE.pqGrid("saveEditCell");
            }
        });
        $($($('#gridDataFree').children()[0]).children()[1]).click()
        $($($('#gridDataFree').children()[0]).children()[1]).click()

        $gridFREE.pqGrid({
            rowDblClick: function (event, ui) {
                if (ui.rowData) {
                    Free = Free.parse($gridFREE.pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));

                    if (Free.id > 0) {
                        $('#myUserModalLabel').html(Free.Name+ " Users");
                        $('#UserModal').data('url', '/FreeHTML/Users');
                        var url = $('#UserModal').data('url');
                        $.post(url, { id: Free.id }, function (data) {
                            $('#UsermodalContainer').html(data);
                            $('#UserModal').modal("show");
                            $('#Usermodalcontent').width(600);
                            //changeToGridUser();
                        });
                    }
                }
            }
        });
    } catch (err) { }
}
function changeToGridUser() {
    var tbl = $("#DataFreeUsers");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 500,
        height: 800,
        title: "Users Free Html",
        //DataGrid Menu
        toolbar: {
        items: [{ type: 'button', label: 'Add New', listeners: [{ click: AddNewUser }] }
                , {
                    type: 'button', label: 'Delete', listeners: [{
                        "click": function () {
                            $.post("/FreeHTML/SaveUser", { id: $("#FHV").val(), User: User.User, Delete: true });
                            $('#gridDataFreeUsers').pqGrid("deleteRow", { rowIndx: Selidx });
                        }
                    }]
                }
            ]
        },
        editable: false,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    newObj.colModel[0].width = 200;
    newObj.colModel[1].width = 150;
    

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.pageModel = { rPP: 30, type: "local" };

    $gridFREEU = $("#gridDataFreeUsers").pqGrid(newObj);


    try {

        $gridFREEU.pqGrid("option", "dataModel.sortIndx", 0);
        $gridFREEU.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $gridFREEU.pqGrid("saveEditCell");
            }
        });
        $gridFREEU.pqGrid({
            rowClick: function (event, ui) {
                if (ui.rowData) {
                    User = UserFreeHtmlModel.parse($gridFREEU.pqGrid("getRowData", { rowIndxPage: ui.rowIndx }));
                    Selidx = ui.rowIndx;
                }
            }
        });

        $($($('#gridDataFreeUsers').children()[0]).children()[1]).click()
        $($($('#gridDataFreeUsers').children()[0]).children()[1]).click()

    } catch (err) { }
}


function AddNewUser()
{
    $.get("/FreeHTML/SaveUser", { id: $("#FHV").val(), User: prompt(), Delete:false }, function (response) {
    })
    $("#UserModal").modal('hide');
}

