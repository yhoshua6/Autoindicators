﻿var id = 0;
var idUpdate=0;
var Assign=[];
var descr;
var descrUp;
var Notf;

function openTab(tab) {
    var i;
    var x = document.getElementsByClassName("Tabs");
    for (i = 0; i < x.length; i++) {
        $(x[i]).hide();
    }
    $('#'+tab).show();
    if(tab=='NewNotif')
    {
        id =0;
        $('#pNotifName').val('');
        $('#pType').val(0);
        $('#pDueDate').val('');
        $('#pStatus').val(0);
        $('#StatusN').hide();
        $('#pDescr').val('');
        $('#Assignees').html('');
        $('#AddAssignees').hide();
        //$('#emails').val('');
    }
        

    if (id > 0) {
        $('#trUpdates').show();
    }
    else {
        $('#trUpdates').hide();
        $('#AddUpdate').hide();
    }
        
}
function SaveNotif()
{
    var status = 1
    var Desc = "";

    if ($('#pNotifName').val() == 0)
    {
        $('#warning').toggle('blind', {}, 500);
        $('#warningMessage').text('Please enter a notif name');
        return;
    } else if ($('#pDueDate').val() == 0) {
        $('#warning').toggle('blind', {}, 500);
        $('#warningMessage').text('Please enter a notif due date');
        return;
    } else if ($('#pType').val() == null) {
        $('#warning').toggle('blind', {}, 500);
        $('#warningMessage').text('Please select a notif type');
        return;
    }

    if ($('#pStatus').val() != null)
        status = $('#pStatus').val();
    if (tinyMCE.get('pDescr') != null)
        Desc=tinyMCE.get('pDescr').getContent();
    else
        Desc=$('#dDesc').html();


    $.post('/Notif/NotifSave', { notifId: id, NotifType: $('#pType').val(), Descr: Desc, refId: $('#Datas').data('continuity'), NotifName: $('#pNotifName').val(), dueDate: $('#pDueDate').val(), fatherApp: $('#Datas').data('view'), Statusid: status, AdditionalId: $('#Datas').data('additional'), SubType: $('#Datas').data('subtype') }, function (response) {
        
        if(response.NotifId>0)
        {
            notifi = response;
            if ($('#Datas').data('string').length > 0) {
                $.post('/Notif/NotifQuery', { NotifData: $('#Datas').data('string'), Refresh: true }, function(response){
                    $($($("#grid_table").children()[0]).children()[1]).click();
                    $($($("#grid_table").children()[0]).children()[1]).click();
                });
            }
            else
                LoadMyNotif();
            $('#success').toggle( 'blind', {}, 500 );
            $('#successMessage').text('Data Saved');

            if (id == 0) {
                id = response.NotifId;
                AddAssignees();
                openTab('PrevNotif');

            }

        }else
        {
            $('#warning').toggle('blind', {}, 500);
            $('#warningMessage').text('Error');
        }
    })
}
function EditNotif($row,update)
{
    openTab('NewNotif');
    NewUpdate();

    id = $($row.children()[0]).text();

    
    $('#pNotifName').val($($row.children()[1]).text());
    $('#pType').val($('#NotifType-'+id).text());
    $('#pDueDate').val($($row.children()[3]).text());
    $('#pStatus').val($('#NotifStatus-' + id).text());
    $('#StatusN').show();

    if (update != undefined)
    {
        $('#dType').html($('#pType option:selected').text());
        $('#dTitle').html($('#pNotifName').val());
        $('#dDesc').html($($row.children()[6]).text());
        $('#dDue').html($('#pDueDate').val());
        $('#dCreationDate').html('<b>Creation Date</b><br/>'+$($row.children()[8]).text());
        //$('#dStatus').html($('#pStatus option:selected').text());

        $('#pNotifName').hide();
        $('#pType').hide();
        $('#pDueDate').hide();
        //$('#pStatus').hide();
        $('#pDescr').hide();
        $('#fType').hide();
        $('#fName').hide();
        $('#fDesc').hide();
        $('#fAssign').hide();
        $('#fDue').hide();
        $('#fStatus').hide();
    } else
    {
        if (tinyMCE.editors.length > 0) {
            tinyMCE.get('pDescr').setContent($($row.children()[6]).text())
        }
    }

    $('#appIcon').attr('src', $('#AppIcon-' + id).text().replace('~', ''));
    $('#AddAssignees').show();
    GetUpdates();
    $('#trUpdates').show();

    $.post('/Notif/Assignees', { NotifId: id }, function (response) {
        if (response.length > 0) {
            $.each(response, function () {
                if (update != undefined) {
                    $('#Assignees').append('<div>' + this.Name + '<div>');
                } else {
                    $('#Assignees').append('<div><span style="cursor:pointer" onclick="RemoveAssignees(\'' + this.Email + '\',$(this).parent())">X</span> ' + this.Name + '<div>');
                }
            })
            $('#emails').val('');
        }
    })
}
function ShowAcknowledge(id)
{
    $('#grid_table').css('width', '75%');

    if (!$('#Akcnowledge').is(':visible'))
        $('#Akcnowledge').toggle('slide', 800);

    $.post('/Notif/Assignees', { NotifId: id }, function (response) {
       

        if (response.length > 0) {
            $.each(response, function () {
                var Acknow = this.Acknowledge ? "Yes" : "No";
                $('#DAssign tbody').append("<tr><td>" + this.Name + "</td><td>" + Acknow + "</td></tr>");
            })
        }
        
    })
}
function Acknowledge(id,val)
{
    $.post('/Notif/IsAkcowledge', { NotifId: id, Val: val });
}
function AddAssignees()
{
    if ($('#emails').val().indexOf(';') > 0)
        Assign = $('#emails').val().split(';');
    else
        Assign = [$('#emails').val()];

    if (id > 0) {
        $.each(Assign, function () {
            var email = this;

            if (email.indexOf('<') > -1)
                email = email.substring(email.indexOf('<') + 1, email.indexOf('>'));

            $.post('/Notif/AssigneesSave', { NotifId: id, Email: email, Delete: false, Notif:notifi }, function (response) {
                if(response.Email.length>0)
                    $('#Assignees').append('<div><span style="cursor:pointer" onclick="RemoveAssignees(\'' + response.Email + '\',$(this).parent())">X</span> ' + response.Name + '<div>');
            });
        })
        $('#emails').val('');
    }
}
function RemoveAssignees(email,$obj)
{
    $.post('/Notif/AssigneesSave', { NotifId: id, Email: email, Delete: true, Notif: notifi }, function (response) {
        $obj.hide();
    });

}
function AddUpdate()
{
    $('#AddUpdate').toggle('blind', null, 500);

    if ($('#AddUpdate').is(':visible'))
        $('#btnAddUpdate').val('Updates ▲');     
    else
        $('#btnAddUpdate').val('Updates ▼');
 
    if (tinyMCE.editors['pDescrUp'] != undefined)
        tinyMCE.editors['pDescrUp'].destroy();

    $("#pStatusUpdate").val(1);

    $("#pTypeUpdate").val(1);
    setTimeout(CreateTxtUpdate, 500);
}
function CreateTxtUpdate()
{
    descrUp = tinymce.init({
        selector: 'textarea#pDescrUp' // change this value according to your HTML
           , width: '500px'
           , statusbar: false
           , plugins: 'textcolor save link'
           , menu: 'edit'
           , default_link_target: '_blank'
           , toolbar: 'backcolor | forecolor | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | styleselect | formatselect | cut | copy | paste | bullist | numlist | outdent | indent | blockquote | mybutton | mybuttonPL | AcronymsSearch | LinkAdd '
           , setup: function (editor) {
               editor.addButton('AcronymsSearch', {
                   text: 'Auto',
                   icon: 'link',
                   onclick: function () {
                       $.post("/Snapshot/SearchAcronyms", { TextAcronyms: tinyMCE.get('pDescrUp').getContent(), WWID: wwid, ContinuityID: ContinuityID, Owner: Owner, ProgType: ProgramType, PXT: PXT, View: 0 }, function (data) {
                           tinyMCE.get('pDescrUp').setContent(data);
                       });
                   }
               });
               editor.addButton('LinkAdd', {
                   text: 'Manual',
                   icon: 'link',
                   onclick: function () {
                       AddLinkStandAlone('pDescrUp');
                   }
               });
           }
    });
}
function SaveUpdates() {
    var Assignees = '';

    if ($('#hAssgin').val().length > 0)
        Assignees = $('#hAssgin').val()+';'+$('#pAssignUpdate').val().trim();
    else
        Assignees = $('#pAssignUpdate').val().trim();

    var status=1;

    if ($('#pStatusUpdate').val() != undefined)
        status = $('#pStatusUpdate').val();

    if (id > 0) {
        $.post('/Notif/UpdateSave', { Id: idUpdate, Type: $('#pTypeUpdate').val(), Desc: tinyMCE.get('pDescrUp').getContent(), NotifId: id, Aknowledge: true, Status: status, Assignees: Assignees }, function (response) {
            GetUpdates();
            NewUpdate();
            tinyMCE.get('pDescrUp').setContent("");
            AddUpdate();
        })
    }
}
function cleanUpdates() {
    $('#pTypeUpdate').val(0);
    tinyMCE.get('pDescriptionUpdate').setContent('')
    $('#pStatusUpdate').val('');
    $('#pAssignUpdate').val('');
}
function GetUpdates()
{
    $('#NotifUpdateTable > tbody').html('');
    $.post('/Notif/Updates', { NotifID: id }, function (response) {
        var table = '';
        
        $.each(response, function () {
            var Assign = this.assigneeEmail;
            table+= '<tr>';
            table += '<td>' + this.UpdateId + '</td>';
            //table += '<td>' + this.value + '</td>';
            table += '<td>' + this.UpdateType + '</td>';
            table += '<td>' + this.Descr + '</td>';
            //if (this.acknowledge)
            //    table += '<td>Yes</td>';
            //else
            //    table += '<td>No</td>';
            table += '<td>' + this.UpdateStatus + '</td>';
            //table += '<td>' + this.createDate + '</td>';
            //table += '<td>' + this.createUser + '</td>';
            //if(this.UpdateDate!=null)
            //    table += '<td>' + this.UpdateDate + '</td>';
            //else
            //    table += '<td></td>';
            //if(this.UpdateUser != null)
            //    table += '<td>' + this.UpdateUser + '</td>';
            //else
            //    table += '<td></td>';
            table += '<td><input type="button" value="Edit" onclick="LoadUpdate($(this).parent().parent())" /></td>';
            if (this.Assignees.length > 0) {
                table += '<td id="UpdateAssignees' + this.UpdateId + '" style="display:none">';

                $.each(this.Assignees, function () {
                    if(this.trim().length>0)
                        table += '<div><span style="cursor:pointer" onclick="DropUpdateAssignee(\''+this.trim()+'\',\''+Assign+'\',$(this).parent())">X</span> ' + this.trim() + '<br></div>';
                })
                table += '</td>';
            }
            table += '</tr>';
        })

        $('#NotifUpdateTable > tbody').html(table);
    });
}
function LoadUpdate($this)
{
    var row = $($this.children());
    AddUpdate();

    setTimeout(function () {
        idUpdate = $(row[0]).text();

        $("#pTypeUpdate").val($("#pTypeUpdate option:contains('" + $(row[1]).text() + "')").val());
        $("#fStatusUpdate").show();


        $("#pStatusUpdate").val($("#pStatusUpdate option:contains('" + $(row[3]).text() + "')").val());
        $("#AssignUpdate").html('');

        $(Assignees).show();
        $("#AssignUpdate").html($("#UpdateAssignees" + idUpdate).children().clone());
        tinyMCE.get('pDescrUp').setContent($(row[2]).text());
    }, 1000);

   
}
function DropUpdateAssignee(email,emails,obj)
{
    var emailU = $('#hAssgin').val();

    if (emailU.length == 0)
        $('#hAssgin').val(emails.replace(email+';', ''));
    else
        $('#hAssgin').val(emailU.replace(email+';', ''));

    $.post('/Notif/UpdateSave', { Id: idUpdate, Type: $('#pTypeUpdate').val(), Desc: $('#pDescriptionUpdate').val(), NotifId: id, Aknowledge: $('#pStatusUpdate').is(':checked'), Status: $('#pStatusUpdate').val(), Assignees: Assignees }, function (response) {
    
    })

    $(obj).hide();
}
function NewUpdate()
{
    idUpdate = 0;
    $('#pAssignUpdate').val('');
    $('#pDescriptionUpdate').val('');
    $('#pTypeUpdate').val(0);
    $('#pStatusUpdate').val(0);
    $("#AssignUpdate").html('');
}
function LoadMyNotif()
{
    $('#Notification').load('NotiDash', {WWID : wwid}, function (response) {
        $('#NewNoti').hide();
    });
}
function GotoApp(obj)
{

    $.post('/Notif/GotoApp', { ReferenceId: $(obj).data('reference'), App: $(obj).data('app'), WWID: wwid, AdditionalID: $(obj).data('additional') }, function (response) {
        window.location.href=$('#' + response.appName).attr('href');
    })
}
function changeToGridNotif(popup) {
    var tbl = $("#NotifTable");
    var type = $("#NotifTable").data("type");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: '100%',
        height: 850,
        title: "Details",
        //DataGrid Menu
        editable: true,
        //collapsible: false,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local',
        scrollModel: { autoFit: true }
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
    if (!popup) {
        newObj.colModel[0].datatype = "int";
        newObj.colModel[0].height = 50;
        newObj.colModel[1].datatype = "string";
        newObj.colModel[1].width = 450;
        newObj.colModel[2].datatype = "string";
        newObj.colModel[2].width = 200;
        newObj.colModel[3].datatype = "string";
        newObj.colModel[3].width = 100;
        newObj.colModel[4].datatype = "string";
        newObj.colModel[4].width = 100;
        newObj.colModel[5].datatype = "string";
        newObj.colModel[5].width = 100;
        newObj.colModel[6].datatype = "string";
        newObj.colModel[6].width = 500;
        newObj.colModel[8].width = 100;
    } else
    {
        newObj.colModel[0].datatype = "int";
        newObj.colModel[0].height = 50;
        newObj.colModel[1].datatype = "string";
        newObj.colModel[1].width = 200;
        newObj.colModel[2].datatype = "string";
        newObj.colModel[2].width = 100;
        newObj.colModel[3].datatype = "string";
        newObj.colModel[3].width = 100;
        newObj.colModel[4].datatype = "string";
        newObj.colModel[4].width = 80;
        newObj.colModel[5].hidden = true;
        newObj.colModel[6].datatype = "string";
        newObj.colModel[6].width = 250;
    }

    var items = newObj.dataModel.data;
    var result = [];

    for (x = 0; x < newObj.colModel.length; x++) {
        Val = [];
        for (var item, i = 0; item = items[i++];) {
            if (Val.indexOf(item[x]) < 0) {
                Val[Val.length] = item[x];
            }
        }
        result[result.length] = Val;
    }
    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = {
            type: 'select',
            attr: "multiple",
            cache: null,
            options: result[i],
            condition: 'range',
            listeners: ['change'],
            init: function () {
                $(this).pqSelect({ checkbox: true, radio: true, width: '100%' });
            }
        };
    }
    newObj.colModel[1].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    newObj.colModel[7].filter = null;
    newObj.colModel[8].filter = null;
    newObj.colModel[9].filter = null;

    newObj.pageModel = { rPP: 50, type: "local" };
    $grid = $("#grid_table").pqGrid(newObj);

    try {
        if ($("#grid_table").pqGrid() != null)
            $("#grid_table").pqGrid("destroy");

        $grid = $("#grid_table").pqGrid(newObj);
        $("#grid_table").pqGrid("option", "dataModel.sortIndx", 0);
        $("#grid_table").pqGrid("option", "editable", false);
        $("#grid_table").pqGrid( "option", "selectionModel", {type: 'row', mode: 'single'} );

        if (popup) {
            $($($("#grid_table").children()[0]).children()[1]).click();
            $($($("#grid_table").children()[0]).children()[1]).click();
        }
    } catch (err) { }
}
function OpenNotifFromReport(report, notifid, info, proj)
{
    var area = '';

    switch(report)
    {
        case 1: area = "Snapshot";
            break;
        case 2: area = "Health Indicator";
            break;
        case 3: area = "Schedule";
            break;
    }

    $.post('/Notif/NotifQuery', { NotifData: info, Update:true }, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#myModalLabel').html('Notifications in ' + area + ' for ' + proj);
        $('#modalcontent').width(1500);
        $('#modalcontent').height('100%');
        $('.modal-dialog').css('margin-left', '15%');
        EditNotif($('#Notif-' + notifid),true);
        $('#NotiDash').hide();
        $('#NewNoti').hide();
    });
}
function AddLinkStandAlone(origin) {
    var word = tinyMCE.get(origin).selection.getContent({ format: 'text' }).trim();

    if (word.length > 0) {
        var urlLink = "/Snapshot/AddAcronymLink?Row=" + origin + "&Word=" + word.replace('&', '^') + '&WWID=' + wwid + '&ContinuityID=' + ContinuityID + '&Owner=' + Owner + '&ProgType=' + ProgramType + '&PXT=' + PXT;
        $('#myBModalLabel').html("Save a Link");

        $('#BModal').data('url', urlLink);
        var url = $('#BModal').data('url');
        $.get(url, function (data) {
            $('#BmodalContainer').html(data);
            $('#BModal').modal("show");
            $('#Bmodalcontent').width(600);
            $('#Bmodalcontent').height(450);
            $('#BModal').css("zIndex", 1100)
        });
    } else
        alert('The word is empty');
}
function CreateRT()
{


    if (tinyMCE.editors['pDescr'] != undefined)
        tinyMCE.editors['pDescr'].destroy();
    if (tinyMCE.editors['pDescrUp'] != undefined)
        tinyMCE.editors['pDescrUp'].destroy();


    descr=tinymce.init({
        selector: 'textarea#pDescr' // change this value according to your HTML
      , width: '800px'
      , statusbar: false
      , plugins: 'textcolor save link'
      , menu: 'edit'
      , default_link_target: '_blank'
      , toolbar: 'backcolor | forecolor | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | styleselect | formatselect | cut | copy | paste | bullist | numlist | outdent | indent | blockquote | mybutton | mybuttonPL | AcronymsSearch | LinkAdd '
      , setup: function (editor) {
          editor.addButton('AcronymsSearch', {
              text: 'Auto',
              icon: 'link',
              onclick: function () {
                  $.post("/Snapshot/SearchAcronyms", { TextAcronyms: tinyMCE.get('pDescr').getContent(), WWID: wwid, ContinuityID: ContinuityID, Owner: Owner, ProgType: ProgramType, PXT: PXT, View: 0 }, function (data) {
                      tinyMCE.get('pDescr').setContent(data);
                  });
              }
          });
          editor.addButton('LinkAdd', {
              text: 'Manual',
              icon: 'link',
              onclick: function () {
                  AddLinkStandAlone('pDescr');
              }
          });
      }
    });

  
}