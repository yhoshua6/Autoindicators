﻿var difrow = '';
var statbef = '';
var reload = true;
var selid = 0;
var selidpast = 0;
var WWID = 0;
var WWName = '';
var hiid=0;

function showOption(opt, continuity) {
    $('#ReportsExt').hide();
    $("table[id$='-" + continuity + "']").hide();

    if (opt.indexOf('-') > -1)
    {
        $('#ReportsExt').show();
        $('#ReportCont').html('<iframe  id="IContent" src="' + opt.split('-')[1] + '"&output=embed" width="' + $('#ReportsExt').width() + '" height=600 frameborder="0"></iframe>');
        $('option[value="'+opt+'"]').attr('selected', 'selected');
    } else
    {    
        $("#" + opt + "-" + continuity).show();
    }
}
function showDetail(detail, row) {
    var components = document.querySelectorAll('[data-detail]');

    if (difrow != row) {
        for (i = 0; i < components.length; i++) {
            if (components[i] != null)
                components[i].style.display = 'none';
        }
        if (difrow.length > 0) {
            $("#" + difrow).hide();
        }
        $("#" + row).show();

        if (!$("#" + statbef).is(":visible"))
            $("#imgDetail-" + statbef).attr("src", "../Content/images/plus.jpg");
        else
            $("#imgDetail-" + statbef).attr("src", "../Content/images/minus.png");

        if ($("#" + detail).is(":visible"))
            $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
        else
            $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");



        $("#" + detail).show();

        difrow = row;
        statbef = detail;
    } else {


        if (statbef != detail) {
            if (statbef.length > 0) {
                if ($("#" + statbef).is(":visible")) {
                    $("#imgDetail-" + statbef).attr("src", "../Content/images/plus.jpg");
                    $("#" + statbef).hide();
                    $("#" + detail).show();
                } else
                    $("#" + detail).show();

                $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                $("#" + row).show();
            } else {
                for (i = 0; i < components.length; i++) {
                    if (components[i] != null)
                        components[i].style.display = 'none';
                }
                if ($("#" + detail).is(":visible")) {
                    $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
                    $("#" + detail).hide();
                    $("#" + row).hide();
                }
                else {
                    $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                    $("#" + detail).show();
                    $("#" + row).show();
                }
            }

            statbef = detail;

        } else {
            if ($("#" + row).is(":visible")) {
                $("#imgDetail-" + detail).attr("src", "../Content/images/plus.jpg");
                $("#" + detail).hide();
                $("#" + row).hide();
            }
            else {
                $("#imgDetail-" + detail).attr("src", "../Content/images/minus.png");
                $("#" + detail).show();
                $("#" + row).show();
            }
        }
    }
}
function ShowDetails(rep, row, col) {

    var urlLink = "/Validation/Detail?RepOrigin=" + rep + "&Row=" + row + "&Column=" + col + "&WWID=" + WWID + "&WWName="+WWName;
    window.open(urlLink);
    //$('#myModalLabel').html("Defect Details " + col+ " in "+row);

    //$('#Modal').data('url', urlLink);
    //var url = $('#Modal').data('url');
    //$.post(url, function (data) {
    //    $('#modalContainer').html(data);
    //    $('#Modal').modal("show");
    //    $('#modalcontent').width(1250);
    //    $('#modalcontent').height(1000);
    //    $('.modal-dialog').css('margin-left', '8%');
    //});
}

function changeToGrid(that) {
    var tbl = $("#Data");
    var type = $("#Data").data("type");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: '100%',
        height: 850,
        title: "Details",
        //DataGrid Menu
        stripeRows: true,
        //collapsible: false,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;

    var items = newObj.dataModel.data;
    var result = [];

    for(x=0; x<newObj.colModel.length; x++)
    {
        Val = [];
        for (var item, i = 0; item = items[i++];) {
            if (Val.indexOf(item[x]) < 0)
            {
                Val[Val.length] = item[x];
            }
        }
        result[result.length] = Val;
    }

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = {
            type: 'select',
            attr: "multiple",
            cache: null,
            options: result[i],
            condition: 'range',
            listeners: ['change'],
            init: function () {
                $(this).pqSelect({ checkbox: true, radio: true, width: '100%' });
            }
        };
    }
    if (newObj.colModel.length > 2) {
        if (type == "AO" || type =="AS" || type=="FAS") {
            //newObj.colModel[newObj.colModel.length - 2].width = 20;
            newObj.colModel[newObj.colModel.length - 2].datatype = "string";
            newObj.colModel[newObj.colModel.length - 3].title = "Last Touched (d)";
            //newObj.colModel[newObj.colModel.length - 3].width = 100;
            newObj.colModel[newObj.colModel.length - 3].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
            newObj.colModel[newObj.colModel.length - 4].hidden = true;
            //newObj.colModel[newObj.colModel.length - 5].width = 100;
            newObj.colModel[newObj.colModel.length - 5].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
            newObj.colModel[newObj.colModel.length - 6].hidden = true;
            newObj.colModel[newObj.colModel.length - 9].hidden = true;
        }
        if(type=="FO")
        {
            //newObj.colModel[newObj.colModel.length - 2].width = 20;
            newObj.colModel[newObj.colModel.length - 3].title = "Last Touched (d)";
            //newObj.colModel[newObj.colModel.length - 3].width = 100;
            newObj.colModel[newObj.colModel.length - 3].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
            //newObj.colModel[newObj.colModel.length - 4].width = 100;
            newObj.colModel[newObj.colModel.length - 4].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
        }
    } else
    {
        //newObj.colModel[0].width = 400;
        //newObj.colModel[1].width = 20;
        newObj.colModel[1].datatype = "string";
    }
    newObj.pageModel = { rPP: 50, type: "local" };
    $grid = $("#grid_table").pqGrid(newObj);


    try {
        if ($("#grid_table").pqGrid() != null)
            $("#grid_table").pqGrid("destroy");

        $("#grid_table").pqGrid(newObj);
        $("#grid_table").pqGrid("option", "dataModel.sortIndx", 0);
        $("#grid_table").pqGrid("option", "editable", false);


        //if (load) {
        //    load = false;
        //    setTimeout($("#grid_table").pqGrid("refresh"), 5000);
        //    changeToGrid(this);
        //}
    } catch (err) { }
}
function SaveCommentsVal(id, WW, Continuity, WName) {
    $.post("/Validation/SaveComment", { comment: tinyMCE.get('txtDescription_' + id).getContent(), PWContinuityID: Continuity, pWWID: WW, HIID: hiid, pWeekName: WName }, function (response) {
        $("#txtDesc_" + id).data('hiid', response);
        $("#txtDesc_" + id).html(tinyMCE.get('txtDescription_' + id).getContent());
        cancelEdit(id);
    })
}
function SaveValLead()
{
    $.post("/Validation/SaveValData", $("#ValDataForm").serialize(), function (response) {
        $('#aValLead_' + response.continuity).attr("href", response.valOwner);
        $('#aValLead_' + response.continuity).text(response.ValName);
        $('.close').click();
    });
};
function EditVal(id, WName, WWID, continuityID, owner, programType, pxt) {
    ContinuityID = continuityID;
    Owner = owner;
    ProgramType = programType;
    PXT = pxt;
    hiid = $("#txtDesc_" + id).data("hiid");
    cleanTextRich();
    $('#txtDescription_' + id).show();
    showRichText(id, WWID, hiid, WName)
    $("#txtDesc_" + id).hide();
}
function showHIPopUp(Information, HIID, Owner, ProgType, PXT) {
    if (PXT == undefined)
        PXT = 0;


    if (tinyMCE.editors.length > 0) {
        tinyMCE.editors[0].destroy();
    }

    var urlLink = "/HealthIndicator/CommentView?HIID=" + HIID + "&Owner=" + Owner + "&ProgType=" + ProgType + "&PXT=" + PXT;

    $('#myModalLabel').html("Validation Status Change for " + Information);
    $('#Modal').data('url', urlLink);
    $('#modalcontent').height('auto');

    var url = $('#Modal').data('url');
    $.post(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
    });
}
function SaveHI() {
    $('#txtDescription_').val(tinyMCE.get('txtDescription_').getContent());
    $.post('/HealthIndicator/Save', $('#CommentForm').serialize(), function (response) {
        var id = $('#HealthID').val();
        var color = '';
        window.location.reload();
    })
}
function showRYG(Header, WWID, WeekName, ContinuityID) {
    var urlLink = "/Validation/RYG?WWID=" + WWID + "&WeekName=" + WeekName + "&ContinuityID=" + ContinuityID;
    $('#myModalLabel').html(Header);

    $('#Modal').data('url', urlLink);
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(400);

    });
}
function SaveRYG() {
    $.post("/Validation/SaveRYG", { PWContinuityID: $("#txtContinuity").val(), pWWID: $("#txtWWID").val(), RYG: $("#RYG").val(), pWeekName: $("#txtWW").val() }, function (response) {
        location.reload();
    })
}
$(document).ready(function () {
    $('#aValidation').css("background-color", "#0062a8");
});

var show = false;

function UploadFile(valink, continuity, project) {
    $.post("/Validation/UploadFile", { Project: project, ContinuityID: continuity, ValLink: valink }, function (response) {
        eval(response);
    })
}
function ShowBigGraph() {
    var obj = $('#Graph').clone();
    $(obj).css('width', '');
    $(obj).css('height', '');
    $('#modalContainer').html(obj);
    $('#Modal').modal("show");
    $('#modalcontent').width($(obj).width);
    $('#modalcontent').height('auto');
    $('.modal-dialog').css('margin-left', '8%');
    $('#modalcontent').width($(obj).css('width'));
}
function Delete(id)
{
    $.post('DeletePhases', { Phases: id }, function (response) {
        eval(response);
    });
}
function ShowBigDetailReport(url)
{
    $('#myModalLabel').html("Detail");
    $('#modalcontent').width(1400);
    $('#modalcontent').height(800);
    $('.modal-dialog').css('margin-left', '10%');
    $('#Modal').modal("show");

    $('#modalContainer').html('<iframe  id="IContent" src="' + url + '"&output=embed" style="width:100%; height:800px" frameborder="0"></iframe>');
    $('#Modal').modal("show");
}
