﻿var letter = '';
var Qty = [];
var addSpaces = [];
var deleteSpaces = [];
var EditVal = '';
var EditData = 0;
var CampusId = 0;
var ColEdit = '';
var objDynamic = null;
var labsOri = '';
var showing = null;
var SpaceShowing = 0;
var Geo = 0;
var duplicated=[];
var objShowing = [];
var NameOld = '';
var gIndex = 0;
var DeleteSpace = false;
var idExtraCol = 0;
var newSpace = false;
var changes = false;
var newSpacesCounter = 0;
var mustEdit = false;

$(document).ready(function () {
    if (getCookie('newSpaces').length > 0)
        newSpacesCounter = getCookie('newSpaces');
});


var SpaceModel = {
    Name: '',
    TypeName: '',
    Type:0,
    Lin: 0,
    Win: 0,
    BBKCPlat: 0,
    InUse: 0,
    Util: 0,
    Comments: '',
    RB: 0,
    Section: '',
    Spot: '',
    Order: 0,
    BKCLocation: '',
    BR: 0,
    Updater: '',
    ProjectCode: 0,
    ProjectCodeName:'',
    Org1: '',
    Org2: '',
    Org3: '',
    VAL: '',
    BBKCID: '',
    BKCWin: 0,
    BKCMore: '',
    PlatType: '',
    CrnStartWW: '',
    CrnEndWW: '',
    NextPrj: '',
    NextStartWW: '',
    PriorityCN: '',
    BUM: '',
    AssetNumber: '',
    FurnType: '',
    LastTouched:'',
    LPID: '',
    Id:0,
    Checked: false,
    Delete: false,
    Duplicated: false,
    WWUpdate: '',
    AssetNumber: '',
    BBKCWin: 0,
    extracols:0,
    create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
    GetArray: function ()
    {
        var Data = [
            '<input type="checkbox" @(space.Checked ? "checked=checked" : "") value="false" onclick="Check(\'newContinuityId\',$(this));" />',
            '<input type="button" value="Drop" onclick="if (confirm(\'Do you want remove this Lab?\')) { DeleteSpaces(\'newContinuityId\',$($(this).parent().parent().parent()).attr(\'pq-row-indx\')); }"><input type="button" value="Copy" onclick="CopySpace(\'newContinuityId\',this);">',
            this.RB,
            this.Section,
            this.Spot,
            this.ProjectCodeName,
            this.Name,
            this.BBKCID,
            this.BBKCPlat,
            this.InUse,
            this.Util,
            this.Org1,
            this.Org2,
            this.Org3,
            this.VAL,
            this.BUM,
            this.Org1+'/'+this.Org2+'/'+this.Org3+'/'+this.Val,
            this.WWUpdate,
            this.Updater,            
            this.TypeName,
            this.Comments,
            this.BBKCWin,
            this.Win,
            this.Lin,
            this.BKCLocation,
            this.BR,
            this.BKCMore,
            this.PlatType,
            this.CrnStartWW,
            this.CrnEndWW,
            this.NextPrj,
            this.NextStartWW,
            this.PriorityCN,
            this.AssetNumber,
            this.FurnType,
            this.LastTouched,
            this.Order 
        ];
        return Data;
    },
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined) {
                newObject.Type = data[19];
                
                switch (newObject.Type) {
                    case "0": newObject.TypeName = "White Space";
                        break;
                    case "1": newObject.TypeName = "Benches";
                        break;
                    case "2": newObject.TypeName = "Racks";
                        break;
                    case "3": newObject.TypeName = "Storage";
                        break;
                    default: newObject.TypeName = data[19] == undefined ? this.TypeName : data[19];
                } 
                switch(newObject.TypeName)
                {
                    case "White Space": newObject.Type = 0;
                        break;
                    case "WhiteSpace": newObject.Type = 0;
                        break;
                    case "Benches": newObject.Type = 1;
                        break;
                    case "Racks": newObject.Type = 2;
                        break;
                    case "Storage": newObject.Type = 3;
                        break;
                }
                
                newObject.ProjectCode = data[5] == undefined || data[5].length==0 ? this.ProjectCode : data[5];
                
                if (!(Number(newObject.ProjectCode) >-1))
                {
                    newObject.ProjectCodeName = newObject.ProjectCode;

                    $.each(Campus, function () {
                        if (this.id = CampusId) {
                            $.each(this.DataProjects, function () {
                                if (this.ProgCode == newObject.ProjectCodeName)
                                    newObject.ProjectCode = this.id;
                            })
                        }

                    });
                } else
                {
                    $.each(Campus, function () {
                        if (this.id = CampusId) {
                            $.each(this.DataProjects, function () {
                                if (this.id == newObject.ProjectCode)
                                    newObject.ProjectCodeName = this.ProgCode;
                            })
                        }

                    });
                }

                newObject.Name = data[6] == undefined ? this.Name : data[6];
                newObject.Lin = data[23] == undefined ? this.Lin : data[23];
                newObject.Win = data[22] == undefined ? this.Win : data[22];
                newObject.BBKCPlat = data[8] == undefined ? this.BBKCPlat : data[8];
                newObject.InUse = data[9] == undefined ? this.InUse : data[9];
                newObject.Util = data[10] == undefined ? this.Util : data[10];
                newObject.Comments = data[20] == undefined ? this.Comments : data[20];
                newObject.RB = data[2] == undefined ? this.RB : data[2];
                newObject.Section = data[3] == undefined ? this.Section : data[3];
                newObject.Spot = data[4] == undefined ? this.Spot : data[4];
                newObject.Order = data[38] == undefined ? this.Order : data[38];
                newObject.Checked = $(data[0]).children().val() == undefined ? this.Checked : $(data[0]).children().val();
                newObject.Id = data[36] == undefined && this.Id==0 ? this.Id : data[36];
                newObject.LPID = data[37] == undefined ? this.LPID : data[37];
                newObject.BKCLocation = data[24] == undefined ? this.BKCLocation : data[24];
                newObject.BR = data[25] == undefined ? this.BR : data[25];
                newObject.Updater = data[18] == undefined ? this.Updater : data[18];
                
                newObject.Org1 = data[11] == undefined ? this.Org1 : data[11];
                newObject.Org2 = data[12] == undefined ? this.Org2 : data[12];
                newObject.Org3 = data[13] == undefined ? this.Org3 : data[13]
                newObject.VAL = data[14] == undefined ? this.VAL : data[14];
                newObject.BBKCID = data[7] == undefined ? this.BBKCID : data[7];
                newObject.BKCWin = data[21] == undefined ? this.BKCWin : data[21];
                newObject.BKCMore = data[26] == undefined ? this.BKCMore : data[26];
                newObject.PlatType = data[27] == undefined ? this.PlatType : data[27];
                newObject.CrnStartWW = data[28] == undefined ? this.CrnStartWW : data[28];
                newObject.CrnEndWW = data[29] == undefined ? this.CrnEndWW : data[29];
                newObject.NextPrj = data[30] == undefined ? this.NextPrj : data[30];
                newObject.NextStartWW = data[31] == undefined ? this.NextStartWW : data[31];
                newObject.PriorityCN = data[32] == undefined ? this.PriorityCN : data[32];
                newObject.BUM = data[15] == undefined ? this.BUM : data[15];
                newObject.AssetNumber = data[33] == undefined ? this.AssetNumber : data[33];
                newObject.FurnType = data[34] == undefined ? this.FurnType : data[34];
                newObject.LastTouched = data[35] == undefined ? this.LastTouched : data[35];
            }
        }
        return newObject;
    }
};
var SpaceTeamModel={
    idTeam:0,
    idSpace:0
}

var ProjectModel = {
    id: 0,
    ProjCode: '',
    Org1: '',
    Org2: '',
    Org3: '',
    Val: '',
    CrnStart: '',
    CrnEnd: '',
    BuMgr: '',
    BuMgrName: '',
    OldName: '',
    OldName2: '',
    Comments: '',
    CC: '',
    BKCMoraSqft: '',
    Delete: false,
    idCampus: 0,
    GetArray: function ()   
    {
    var Data = [
        this.ProjCode,
        this.Org1,
        this.Org2,
        this.Org3,
        this.Val,
        this.CrnStart,
        this.CrnEnd,
        this.BuMgr,
        this.OldName,
        this.OldName2,
        this.Comments,
        this.CC,
        this.BKCMoraSqft,
        this.id
    ];
    return Data;
    },
    Create: function (config) {
    var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
    if (config !== undefined) {
        Object.keys(config).forEach(function (key) {
            newObject[key] = config[key];
        });
    }
    return newObject;
    },
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined) {
                newObject.ProjCode = data[0];
                newObject.Org1= data[1];
                newObject.Org2 = data[2];
                newObject.Org3 = data[3];
                newObject.Val = data[4];
                newObject.CrnStart = data[5];
                newObject.CrnEnd = data[6];
                newObject.BuMgr = data[7];
                newObject.OldName = data[8];
                newObject.OldName2 = data[9];
                newObject.Comments = data[10];
                newObject.CC = data[11];
                newObject.BKCMoraSqft = data[12];
                newObject.id = data[13]!=undefined? data[13] : 0;
                newObject.idCampus = $('#ProjCampus').text();
            }
        }
        return newObject;
    }
}
var CampusModel = {
    id: 0,
    Projects: [],
    DataProjects: [],
    Create: function (config) {
        var newObject = Object.create(this);
        //If a config object has been used to initialize the
        //sprite, assign those values to the new object
        if (config !== undefined) {
            Object.keys(config).forEach(function (key) {
                newObject[key] = config[key];
            });
        }
        return newObject;
    },
}

var Campus = []; 
var Project = ProjectModel.Create();
var Space = SpaceModel;
var SpaceTeam=SpaceTeamModel;

var Spaces = [];

function NewLab()
{
    $('#myModalLabel').html("Create a New Lab");
    
    $.post('/Lab/DataLab', function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        $('#Modal').css('left', '1%');
    });
}
function Campus() {
    $('#myModalLabel').html("Campus Catalog");

    $.post('/Lab/Campus', function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(650);
        $('#Modal').css('left', '1%');
    });
}
function SaveCampus(id,name,geoid,manager)
{
    
    if (name.length > 0 && name != "New") {
        $.post('/Lab/SaveCampus/', { ID: id, Name: name, GEOID: geoid, Manager: manager }, function (response) {
            if (id == undefined || id == 0) {
                var campus = '<tr><td>' + response.id + '</td>';
                campus+='<td id="Name-'+response.id+'" class="edit" onblur="SaveCampus('+response.id+',$(this).text(),$(\'#ddlGeo-'+response.id+'\').val(),$(\'#Manager-'+response.id+'\').text())">' + name + '</td>';
                campus += '<td><select id="ddlGeo-' + response.id + '" onchange="SaveCampus(' + response.id + ',\'' + name + '\',this.value,$(\'#Manager-' + response.id + '\').text())">' + $('#ddlGeo').html() + '</td>';
                campus+='<td id="Manager-'+response.id+'" class="edit" data-email="'+manager+'" onclick="$(this).text($(this).data(\'email\'))" onblur="SaveCampus('+response.id+',$(\'#Name-'+response.id+'\').text(),$(\'#ddlGeo'+response.id+'\').val(),$(\'#Manager-'+response.id+'\').text())">' + response.ManagerName + '</td></td></tr>';
                $(campus).prependTo('#Campus > tbody');
                id = response.id;
            }
            objDynamic = response;
            $('#CampusMan-' + id).text(response.ManagerName);
            $('#CampusMan-' + id).attr("onfocus", '$(this).text(\'' + response.ManagerName + '\')');
            $('#Manager-' + id).attr("data-email", manager);
            $('#ddlGeo-' + id + ' option[value=' + geoid + ']').attr('Selected', 'Selected');
            var geo = $('[title*="/'+name+'"]').text().split('/');
            $('[title*="/'+name+'"]').text(geo[0]+'/'+ name);
        })
    }
}
function SaveCampusComment(id,comments)
{
    $.post("/Lab/SaveCampusComments", { CampusId: id, Comments: comments, WWID: wwid });
}
function SaveCampusManager(id,name,geo,manager,obj)
{
    if (manager.length > 0)//event.keyCode==13 && 
    {
        SaveCampus(id, name, geo, manager);
        return false;
    }
}
function SaveSpaceComments(obj) {  
    
    ColEdit =$($('#'+obj).parent()).data('col');
    EditSpace($('#'+obj).parent().parent().children());
}
function LabDetailEdit(lab,labname)
{
    $('#myModalLabel').html("Edit "+labname);

    $.post('/Lab/DataLab?LabID='+lab, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        $('#Modal').css('left', '1%');
    });
}
function AddSpaceChange(name, type, lpid)
{
    var spec=Space.parse({Name:name,Type:type,LPID:lpid});
    if(!Spaces.indexOf(spec)>-1)
        Spaces[Space.length]=spec;
}
function AddRemoveSpaceTeam(idTeam,idSpace)
{
    SpaceTeam = SpaceTeamModel;
    SpaceTeam.idSpace = idSpace;
    SpaceTeam.idTeam = idTeam;

    if (addSpaces.indexOf(SpaceTeam) > -1) {
        deleteSpaces[deleteSpaces.length] = SpaceTeam;
        addSpaces.slice(SpaceTeam);
    } else
        addSpaces[addSpaces.length] = SpaceTeam;
}
function EditTeam(id,obj,idSpace)
{
    if ($(obj).is(":checked")) {
        SaveTeam(id, $(obj).text(), idSpace, false,lpid);
        return;
    } else {
        SaveTeam(id, $(obj).text(), idSpace, true,lpid);
        return;
    }

    if (event.keyCode = 13) {
        SaveTeam(id, $(obj).text(), idSpace,false,lpid);
        event.returnValue = false;
        $(obj).focusout();
        return;
    }
    
}
function SaveLab()
{
    if ($('#txtName').val().length == 0)
        return;

    $.post('/Lab/Save', { LabId: $('#txtId').val(), Name: $('#txtName').val(), Layout: $('#txtLayout').val(),Champeon: $('#txtChampeon').val(), GeoID: Geo, WWID: wwid, Delete: false })
    LoadLabs();
}
function UpdateChampion(obj, id, name, layout,Champ)
{
    $.post('/Lab/UpdateChampion', { LabId: id, Name: name, Layout: layout, Champeon: $(obj).text(), GeoID: Geo, WWID: wwid, Delete: false }, function (response) {
        $(obj).text(response.ChampionName);
    });
}
function DeleteLab(id,name,geo)
{
    $.post('/Lab/Save', { LabId: id, strName: name, strLayout: '', GeoID: geo, WWID: wwid, Delete: true })
    $('tr[data-labid='+id+']').hide()
}
function AddLink()
{
    if ($('#txtLinkName').val().length == 0 || $('#txtLinkName').val().length == 0)
        return;

    $.post('/Lab/ManLink', { LabID: $('#txtId').val(), Name: $('#txtLinkName').val(), Link: $('#txtLink').val(), Delete: false }, function (response) {
        $('#links').html($('#links').html() + "<div><a href=\""+$('#txtLink').val()+"\" target=\"_blank\">" + $('#txtLinkName').val() + "</a><div class=\"btnDel\" onclick=\"DelLink('" + $('#txtLink').val() + "',this)\">X</div></div>");
        $('#txtLink').val('');
        $('#txtLinkName').val('');
    })
}
function DelLink(link,obj)
{
    $.post('/Lab/ManLink', { LabID: $('#txtId').val(), Link: link, Delete: true }, function (response) {
        $(obj).parent().hide();
    })
}
function UpdateLabsPWW(field,value,continuityID,obj,campus)
{
    $.post('/Lab/UpdateLabPerWeek', { Field: field, Value: value, ContinuityID: continuityID, WWID: wwid }, function (response) {
        if (value.indexOf('@') > -1) {
            $(obj).text(response);
        }
        else if ($(obj).attr('id') == undefined)
        {
            var tot = 0;
            tot=(parseInt($('#Tot-' + campus + '-' + field.replace('int', '')).text() - EditVal)) + parseInt(value);
            $('#Tot-' + campus + '-' + field.replace('int', '')).text(parseInt(tot));
        }
        if($(obj).attr('id')!=undefined && $(obj).attr('id').indexOf('txtDescription')>-1)
        {
            $('#txtDesc_' + continuityID).html(response);
            cancelEdit(continuityID);
        }
    })
}
function ShowCampus(geo)
{
    var campus = document.querySelectorAll('[data-geoid]');
    $(campus).hide();
    $('*[data-geoid="' + geo + '"]').show();
}
function SaveTeam(id,name,space,deleted)
{
    $.post('/Lab/SaveTeam', { TeamID: id, Name: name, SpaceID: space, Deleted: deleted
    }, function (response) {
        if (id == 0) {
            $('#Teams').load('/Lab/Team?Detail=true');
        }else
        {
            $('#Team-' + id).hide();
        }


    });
}
function AddMembersToTeam(teamid)
{
    var member = prompt('Member Email(s):', '').split(';');
    $.each(member, function () {
        $.post('/Lab/SaveTeamMember', { TeamID: teamid, Member: this, MemberID: 0, Deleted: false }, function (response) {
            var strMembers = '';
            $.each(response, function () {
                strMembers += '<span style="cursor:pointer; border: solid 1px black" onclick="RemoveMembersTeam(' + teamid + ',' + this.idMember + ')">X</span> ' + this.strName + '<br>';
            });

            $('#members-'+teamid).html(strMembers);
        });
    });

}
function RemoveMembersTeam(teamid,memberid)
{
    $.post('/Lab/SaveTeamMember', { TeamID: teamid,Member: '', MemberID: memberid, Deleted: false }, function (response) {
        var strMembers = '';
        $.each(response, function () {
            strMembers += '<span style="cursor:pointer; border: solid 1px black" onclick="RemoveMembersTeam(' + teamid + ',' + this.idMember + ')">X</span> ' + this.strName + '<br>';
        });

        $('#members-'+teamid).html(strMembers);
    });
}
function ShowTeams()
{
    $('#myModalLabel').html("Teams");

    $.post('/Lab/Team?', function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(1024);
        $('#Modal').css('left', '1%');
    });
}
function LoadLabs()
{
    $("#Labs").html('<table  align="center"><tr><td align="center"><img src="/Content/images/load.gif" style="width:25%; height:25%" /></td></tr><tr><td>Loading ... Please Wait</td></tr></table>');
    $("#Labs").load("/Lab/LabSumary?WWID=" + wwid, function (response) {
        labsOri = $('#Labs').html();
    });
}
function ShowHide(obj,camp)
{
    if ($('*[data-camp="' + camp + '"]').is(":visible"))
    {
        $('*[data-camp="' + camp + '"]').hide();
        $(obj).attr('src', '/Content/images/plus.jpg');
        $('[data-lab-campus="' + camp + '"]').hide();
        $('img[data-imgcampus=' + camp + ']').attr('src', '/Content/images/plus.jpg');
    }else
    {
        $('*[data-camp="' + camp + '"]').show();
        $(obj).attr('src', '/Content/images/minus.png');
    }
    CampusId = camp;

}
function ShowHideSpaces(obj, space) {
    if ($('*[data-lab-spaces="' + space + '"]').is(":visible")) {
        $('*[data-lab-spaces="' + space + '"]').hide();
        $(obj).attr('src', '/Content/images/plus.jpg');
        objShowing.splice(objShowing.indexOf(obj),1);
    } else {
        $('*[data-lab-spaces="' + space + '"]').show();
        $(obj).attr('src', '/Content/images/minus.png');
        objShowing[objShowing.length] = { Obj: obj, Space: space } ;
    }

    $.post("/Lab/GetProj", { Campus: CampusId }, function (res) {
        var camp = CampusModel.Create();
        camp.id=CampusId;
        var i=0;
        $.each(res, function () {
            var val = 'camp.Projects[' + i + ']={' + this.id + ':"' + this.ProgCode + '"}';
            var Proj = ProjectModel.Create();
            Proj.id = this.id;
            Proj.ProgCode = this.ProgCode;
            eval(val);
            camp.DataProjects[i] = Proj;
            i++;
        })
        Campus[Campus.length] = camp;
        changeToGridLab(space);
    });


}
function SpaceRequest(lab)
{
    
}
function SaveRequest(id,ContinuityID)
{
    var requester = $('#Requester').text();
    var team = $('#TeamName').text();
    var manager = $('#BuManager').text();
    var champion = $('#Champion').text();
    var newspace = $('#NewSpace').text();
    var project = $('#Project').text();
    var desire = $('#DesireLoc').text();
    var startww = $('#StartWW').text();
    var startYear = $('#StartYear').text();
    var endww = $('#EndWW').text();
    var endYear = $('#EndYY').text();
    var c10MB = $('#10MB').text();
    var c1GB = $('#1GB').text();
    var c10GB = $('#10GB').text();
    var c40GB = $('#40GB').text();
    
    if (c10MB.length == 0)
        c10MB = 0;
    if (c1GB.length == 0)
        c1GB = 0;
    if (c10GB.length == 0)
        c10GB = 0;
    if (c40GB.length == 0)
        c40GB = 0;

    var full=$('#Full').text();
    var part = $('#Part').text();

    $.post("/Lab/SaveRequest", { Id: id, LabContinuityID: ContinuityID, Requester: requester, TeamName: team, BuManager: manager, LabChampion: champion, NewSpace: newspace, Project: project, DesireLoc: desire, StartWW: startww, StartYear: startYear, EndWW: endww, EndYear: endYear, WWID: wwid }, function (response) {
        id=response.id;
        if(id>0)
        {
            for (i = 1; i < 4; i++) {
                var system6 = $('#Sys6-'+i).text();
                var system19 = $('#Sys19-'+i).text();
                var system23 = $('#Sys23-'+i).text();
                var densityBench = $('#Bench-'+i).text();
                var densityRack = $('#Rack-'+i).text();
                var type = i;

                $.post("/Lab/SaveRequestPlatform", { RequestId: id, strSystem6: system6, System19: system19, System23: system23, DensityBench: densityBench, DensityRack: densityRack, Type: type });
            }
            $.post("/Lab/SaveRequestNetwork", { RequestId: id, C10MB: c10MB, C1GB: c1GB, C10GB: c10GB, C40GB: c40GB });
            $.post("/Lab/SaveRequestTime", {RequestId:id, Full:full,Part:part});
        }
    },'json')
}
function ShowSection(selection)
{
    if(selection==1)
    {
        $("#Labs").load("/Lab/Graph?WWID=" + wwid);
    }else
    {
        LoadLabs();
    }
}
function ShowSpaceMembers(id)
{
    if ($('*[data-team=' + id + ']').is(':visible'))
        $('*[data-team=' + id + ']').hide();
    else
        $('*[data-team='+id+']').show();
}
function DrawMap()
{
    var map = '';
    var total=0;
    var colors = ["background-color: blue; color: white", "background-color:yellow", "background-color: gray", "background-color: green"];
    var used = [];

    $.each(Qty, function () { total += parseFloat(this) || 0; });

    for(i=0; i<Qty.length;i++)
    {
        var perc = (Qty[i] * 100) / total;

        var color=colors[Math.floor(Math.random() * colors.length)];

        if (used.indexOf(color) < 0) {
            used[used.length] = color;
        } else
            color = colors[Math.floor(Math.random() * colors.length)];

        map += '<div align="center" style="height:100%; font-size:20px; float: left; width:' + perc + '%; ' + color + '; border: 1px solid black">' + Qty[i] + '</div>';
    }
    $("#map").html(map);
}
function Check(lpwid,obj)
{
    gIndex = $($(obj).parent().parent().parent()).attr('pq-row-indx');
    var rowData = $("#dgSpaces-" + lpwid).pqGrid("getRowData", { rowIndx: gIndex });
    Space = SpaceModel.parse(rowData);
    Space.Checked = $(obj).is(':checked');
    Space.Duplicated = false;
    Space.Delete = false;

    SaveSpace();
}
function CopySpace(lpwid,obj)
{
    if (!newSpace) {
        gIndex = $($(obj).parent().parent().parent()).attr('pq-row-indx');
        var rowData = $("#dgSpaces-" + lpwid).pqGrid("getRowData", { rowIndx: gIndex });
        Space=SpaceModel.parse(rowData);

        Space.Name = Space.Name + ' D-' + duplicated.length;
        Space.Id = 0;
        Space.Duplicated = true;
        Space.Delete = false;

        SaveSpace();

        duplicated[duplicated.length] = duplicated.length++;

        CrowData = Space.GetArray();

        $("#dgSpaces-" + lpwid).pqGrid("addRow", { rowData: CrowData, rowIndx: gIndex+1 });
        RefreshLabTotal($("#dgSpaces-" + lpwid).pqGrid("option", "dataModel").data, lpwid);
    } else {
        alert('Please save your changes before');
    }
}
function DeleteSpaces(lpwid,obj) 
{
   
    Space = SpaceModel.parse($("#dgSpaces-" + lpwid).pqGrid("getRowData", { rowIndx: gIndex }));
    
    Space.Delete = true;
    Space.Duplicated = false;

    SaveSpace();

    $("#dgSpaces-" + lpwid).pqGrid("deleteRow", { rowIndx: gIndex });

    RefreshLabTotal($("#dgSpaces-" + lpwid).pqGrid("option", "dataModel").data, lpwid);
}
function SaveSpace() {
    //if (Space.Name.trim().length == 0 && Space.Type ==0 && Space.ProjectCode==0 )
    //{
    //    //alert("The form is empty");
    //    return false;
    //}
    $.post("/Lab/SaveSpaces", { Description: Space.Name, Type: Space.Type, Lin: Space.Lin, Win: Space.Win, BBKC: Space.BBKCPlat, Comments: Space.Comments, Row: Space.RB, Section: Space.Section, Spot: Space.Spot, Order: Space.Order, Util: Space.InUse, Check: Space.Checked, LabId: Space.LPID, Delete: Space.Delete, Duplicate: Space.Duplicated, BRSize: Space.BR, ProjectCode: Space.ProjectCode, BKCID: Space.BBKCID, PlatformType: Space.PlatType, NextPrj: Space.NextPrj, AssetNumber: Space.AssetNumber, FurnType: Space.FurnType, Id: Space.Id, WWID: wwid }, function (response) {
        if(Space.Id==0)
        {
            Space.Id = response.SpaceId;
        }
    });

    return true;
}
function RefreshLabTotal(DataLab,id)
{
    var tBR = DataLab.length;
    var tBKC = 0;
    var tIB = 0;
    var tIR = 0;
    var tIS = 0;
    var tUse = 0;

    $.each(DataLab, function () {
        switch($(this)[19])
        {
            case 'Benches': tIB++;
                break;
            case 'Racks': tIR++;
                break;
            case 'Storage': tIR++;
                break;
        }
        tBKC += Number($(this)[8]);
        tUse += Number($(this)[9]);
    });

    $($('[data-lab='+id+']').children()[2]).text(tBR);
    $($('[data-lab=' + id + ']').children()[3]).text(tBKC);
    $($('[data-lab=' + id + ']').children()[4]).text(tUse);
    $($('[data-lab=' + id + ']').children()[5]).text(Math.round((tUse/tBKC)*100));
    $($('[data-lab=' + id + ']').children()[6]).text(tIB);
    $($('[data-lab=' + id + ']').children()[7]).text(tIR);
    //$($('[data-lab=' + id + ']').children()[8]).text(tIS);


    var GTBRS = 0;
    var GTBBKC = 0;
    var GTUse = 0;
    var GTB = 0;
    var GTR = 0;
    var GTS = 0;

    $.each($('[data-lab]'), function () {
        GTBRS += Number($($(this).children()[2]).text());
        GTBBKC += Number($($(this).children()[3]).text());
        GTUse += Number($($(this).children()[4]).text());
        GTB += Number($($(this).children()[6]).text());
        GTR += Number($($(this).children()[7]).text());
        //GTS += Number($($(this).children()[8]).text());
    });

    $('#Tot-' + CampusId + '-CBR').text(GTBRS);
    $('#Tot-' + CampusId + '-SBBKC').text(GTBBKC);
    $('#Tot-' + CampusId + '-Use').text(GTUse);
    $('#Tot-' + CampusId + '-Util').text(Math.round((GTUse/GTBBKC)*100));
    $('#Tot-' + CampusId + '-IR').text(GTR);
    $('#Tot-' + CampusId + '-IB').text(GTB);
    //$('#Tot-' + CampusId + '-IS').text(GTS);
}
function SearchByBR(search)
{
   
    if ($('tr [data-number="' + search + '"]').length > 0) {
        $('[id^=Detail]').hide();
        var campus = $('tr [data-number="' + search + '"]').focus().parent().parent().parent().parent().parent().data('lab-campus');
        var lab = $('tr [data-number="' + search + '"]').focus().parent().parent().parent().parent().parent().data('lab-spaces');

        $('[data-campus]').hide();
        $('[data-camp]').hide();
        $('#Detail-' + lab).show();
        $('[data-lab=' + lab + ']').show();
        $('[data-campus=' + campus + ']').show();
        $('[data-lab-spaces=' + lab + ']').show();
    } else {
        if(search.length==0)
            $('#Labs').html(labsOri);
    }
}
function SaveUpdateInfo(space, field, value) {
    var spaces = '';

    if (space.indexOf(',') > -1)
        spaces = space.split(',');
    else if (space.indexOf(';') > -1)
        spaces = space.split(';');
    else if (space.indexOf(' ') > -1)
        spaces = space.split(' ');

    $.each(spaces, function () {
        $.post("/Lab/SaveUpdateInfo", { Space: this, Field: field, Value: value })
    })
    
}
function LoadInfo()
{
    $('#myModalLabel').html("Load Info");

    $.post('/Lab/LoadInfo', function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(400);
        $('#Modal').css('left', '1%');
    });
}
function ExportExcel()
{
    $.post("/Lab/ExcelExport", { WWID: wwid }, function (response) {
        window.open(response);
    });
}
$(document).ready(function () {
    LoadLabs();
//    $('[data-detail]').hide();
})
function changeToGridLab(id) {
    var tbl = $("#Spaces-"+id);
    var type = $("#Spaces-"+id).data("type");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: '99%',
        height: 700,
        hwrap: false,
        wrap:false,
        title: "Details",
        //DataGrid Menu
        editable: true,
        collapsible: false,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local',
        cellEditKeyDown: function (event, ui) {
            var col = ui.colIndx;
            if (event.keyCode == 37)
            {
                $("#dgSpaces-" + id).pqGrid("saveEditCell");
                $("#dgSpaces-" + id).pqGrid("quitEditMode");


                do {
                    col--;
                }
                while ($("#dgSpaces-" + id).pqGrid("getColumn", { dataIndx: col }).cls.indexOf('blocked') > -1)
                

                $("#dgSpaces-" + id).pqGrid("editCell", { rowIndx: ui.rowIndx, colIndx: col });

                if(col==20)
                    $("#dgSpaces-" + id).pqGrid( "scrollColumn", { colIndx: col } );

                return;
            }
            else if (event.keyCode == 39)
            {
                $("#dgSpaces-" + id).pqGrid("saveEditCell");
                $("#dgSpaces-" + id).pqGrid("quitEditMode");

                do {
                    col++;
                }
                while ($("#dgSpaces-" + id).pqGrid("getColumn", { dataIndx: col }).cls.indexOf('blocked') > -1)
                
                $("#dgSpaces-" + id).pqGrid("editCell", { rowIndx: ui.rowIndx , colIndx: col });

                if (col == 20)
                    $("#dgSpaces-" + id).pqGrid("scrollColumn", { colIndx: col });

                return;
            }
        },
        selectionModel: {
            //type: 'cell'
            type: 'none'
        },
        hoverMode: 'cell',
        editModel: {
            onBlur: 'save',
            saveKey: $.ui.keyCode.ENTER,
            keyUpDown: true
        },
        editor: { type: 'textbox', select: true, style: 'outline:none;' },
        toolbar: {
            items: [
                {
                    type: 'button', icon: 'ui-icon-plus', label: 'Add', listeners: [
                      {
                          "click": function (evt, ui) {
                              var SpaceRow = SpaceModel.GetArray();
                              Space = SpaceModel.parse(SpaceRow);
                              newSpacesCounter++;
                              var Cols = Space.extracols;
                              Space.extracols = Cols;
                              Space.Name = 'New ' + newSpacesCounter;
                              Space.Type = 0;
                              Space.ProjectCode = 0;
                         
                              if (Space.ProjectCode != undefined) {
                                  Space.Duplicated = false;
                                  Space.Delete = false;
                                  Space.LPID = id;

                                  Space[4] = Space.ProjectCodeName;


                                  $.post("/Lab/SaveSpaces", { Description: Space.Name, Type: Space.Type, Lin: Space.Lin, Win: Space.Win, BBKC: Space.BBKCPlat, Comments: Space.Comments, Row: Space.RB, Section: Space.Section, Spot: Space.Spot, Order: Space.Order, Util: Space.InUse, Check: Space.Checked, LabId: Space.LPID, Delete: Space.Delete, Duplicate: Space.Duplicated, BRSize: Space.BR, ProjectCode: Space.ProjectCode, BKCID: Space.BBKCID, PlatformType: Space.PlatType, NextPrj: Space.NextPrj, AssetNumber: Space.AssetNumber, FurnType: Space.FurnType, Id: Space.Id, WWID: wwid }, function (response) {
                                      if (Space.Id == 0) {
                                          Space.Id = response.SpaceId;

                                          setCookie('newSpaces', newSpacesCounter, 365);

                                          newSpace = false;

                                          $.post('/Lab/GetProjInfo', { Campus: CampusId, id: Space.ProjectCode }, function (response) {

                                              Space[11] = response.Org1;
                                              Space[12] = response.Org2;
                                              Space[13] = response.Org3;
                                              Space[14] = response.Val;
                                              Space[15] = response.BUMgr;
                                              Space[16] = response.Org1 + '/' + response.Org2 + '/' + response.Org1;
                                              Space[28] = response.CrmtStart;
                                              Space[29] = response.CrmtEnd;

                                              if (Space.BBKCID.indexOf('-') > -1) {
                                                  Space.BBKCPlat = Number(Space.BBKCID.split('-')[1].substr(0, 2));
                                                  //BBKC Platform
                                                  Space[8] = Space.BBKCPlat;
                                                  //BKC Width In
                                                  Space[21] = Space.BBKCID.split('-')[0].substr(1, 2);

                                              }

                                              //in Use
                                              if (Space.BBKCPlat == 0)
                                                  Space[10] = 0
                                              else
                                                  Space[10] = Math.round((Space.InUse / Space.BBKCPlat) * 100);

                                              RefreshLabTotal($(evt.target).pqGrid("option", "dataModel").data, id);

                                              colext = 40

                                              for (i = 0; i < Space.extracols; i++) {
                                                  if (Space[colext + 5] != null) {
                                                      $.post("/Lab/SaveExtraData", { Value: ui.rowData[colext], ColumnID: ui.rowData[colext + 5], SpaceID: Space.Id });
                                                      colext++;
                                                  }
                                              }

                                          });


                                          Space[18] = Space.TypeName;
                                          SpaceRow[0] = SpaceRow[0].replace('newContinuityId', Space.LPID);
                                          SpaceRow[1] = SpaceRow[1].replace('newContinuityId', Space.LPID);
                                          SpaceRow[19] = 'White Space';
                                          SpaceRow[36] = Space.Id;
                                          SpaceRow[37] = Space.LPID;
                                          $("#dgSpaces-" + id).pqGrid("addRow", { rowData: SpaceRow, rowIndx: 0 });
                                      }
                                  });
                              }
                          }
                      }
                    ]
                }
            ]
        },
        scrollModel: { autoFit: false }
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;
 
    if (mustEdit) {
        newObj.colModel[0].editable = false;
    } else
    {
        newObj.colModel[0].hidden = true;
    }
    newObj.colModel[0].width = "50";
    newObj.colModel[1].width = "200";
    newObj.colModel[1].editable = false;
    if (!mustEdit)
        newObj.colModel[1].hidden = true;

    newObj.colModel[2].width = "70";
    newObj.colModel[3].cls = '';
    newObj.colModel[3].cls = 'pq-grid-editable';
    newObj.colModel[2].validations = { type: 'gte', value: 1, msg: "should be >= 1" };
    newObj.colModel[3].datatype = "int";
    newObj.colModel[3].validations = { type: 'gte', value: 1, msg: "should be >= 1" };
    newObj.colModel[3].width = "80";
    newObj.colModel[4].validations = { type: 'gte', value: 1, msg: "should be >= 1" };
    newObj.colModel[4].width = "50";
    newObj.colModel[6].validations = { type: 'gte', value: 1, msg: "should be >= 1" };
    newObj.colModel[7].validations = { type: 'gte', value: 1, msg: "should be >= 1" };
    

    for (i = 2; i < newObj.colModel.length; i++) {
        newObj.colModel[i].width = "80";
        newObj.colModel[i].cls = "pq-grid-blocked";
    }
    newObj.colModel[20].width = "250";
    newObj.colModel[38].width = "30";
    newObj.colModel[38].width = "150";

    var items = newObj.dataModel.data;
    var result = [];

    for (x = 0; x < newObj.colModel.length; x++) {
        Val = [];
        for (var item, i = 0; item = items[i++];) {
            if (Val.indexOf(item[x]) < 0) {
                if (item[x]!=undefined && item[x].length > 0) {
                    Val[Val.length] = item[x];
                }
            }
        }
        result[result.length] = Val;
    }
    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = {
            type: 'select',
            attr: "multiple",
            cache: null,
            options: result[i],
            condition: 'range',
            listeners: ['change'],
            init: function () {
                $(this).pqSelect({ checkbox: true, radio: true, width: '100%' });
            }
        };
    }
    
    newObj.cellClick = function (evt, ui) {
        if (ui.rowData) {
            gIndex = ui.rowIndx;
        }
    };

    Projs = [];
    $.each(Campus, function () {
        if(this.id=CampusId)
        {
            $.each(this.Projects, function () {
                Projs[Projs.length] = this;
            })
        }

    });
   

    newObj.colModel[0].filter = null;
    newObj.colModel[1].filter = null;
    newObj.colModel[19].editor = { type: 'select', options: [{0: 'White Space' }, { 1: 'Benches' }, { 2: 'Racks' }, { 3: 'Slots' }] };
    newObj.colModel[5].editor = { type: 'select', options: Projs};
    newObj.colModel[2].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[3].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[4].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
   
    newObj.colModel[6].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[7].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[8].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[9].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[10].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[17].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[18].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };

   
    newObj.colModel[20].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[21].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[22].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[23].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[25].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };

    newObj.colModel[27].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[28].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[29].filter = { type: 'textbox', condition: 'contains', listeners: ['keyup'] };
    newObj.colModel[30].hidden = true;
    newObj.colModel[32].hidden = true;
    newObj.colModel[33].hidden = true;
    newObj.colModel[34].hidden = true;
    newObj.colModel[36].hidden = true;
    newObj.colModel[37].hidden = true;
    newObj.colModel[38].hidden = true;
    
    var colext = Number(newObj.colModel.length-1) - Number(Space.extracols);

    for (i = 0; i <= Space.extracols; i++)
    {
        newObj.colModel[colext].hidden = true;
        newObj.colModel[colext].title = colext;
        
            newObj.colModel[colext - Number(Space.extracols)].cls = "pq-grid-editable";
        if (!mustEdit) {
            newObj.colModel[colext - Number(Space.extracols)].editable = false;
        }
        colext++;
    }

    
        newObj.colModel[2].cls = "pq-grid-editable";
        newObj.colModel[3].cls = "pq-grid-editable";
        newObj.colModel[4].cls = "pq-grid-editable";
        newObj.colModel[5].cls = "pq-grid-editable";
        newObj.colModel[6].cls = "pq-grid-editable";
        newObj.colModel[7].cls = "pq-grid-editable";
        newObj.colModel[9].cls = "pq-grid-editable";
        newObj.colModel[19].cls = "pq-grid-editable";
        newObj.colModel[20].cls = "pq-grid-editable";
        newObj.colModel[22].cls = "pq-grid-editable";
        newObj.colModel[23].cls = "pq-grid-editable";
        newObj.colModel[25].cls = "pq-grid-editable";
        newObj.colModel[30].cls = "pq-grid-editable";
        newObj.colModel[33].cls = "pq-grid-editable";
        newObj.colModel[33].cls = "pq-grid-editable";
        newObj.colModel[34].cls = "pq-grid-editable";
        newObj.colModel[38].cls = "pq-grid-editable";
        newObj.colModel[8].editable = false;
        newObj.colModel[10].editable = false;
        newObj.colModel[11].editable = false;
        newObj.colModel[12].editable = false;
        newObj.colModel[13].editable = false;
        newObj.colModel[14].editable = false;
        newObj.colModel[15].editable = false;
        newObj.colModel[16].editable = false;
        newObj.colModel[17].editable = false;
        newObj.colModel[18].editable = false;
        newObj.colModel[21].editable = false;
        newObj.colModel[24].editable = false;
        newObj.colModel[26].editable = false;
        newObj.colModel[27].editable = false;
        newObj.colModel[28].editable = false;
        newObj.colModel[29].editable = false;
        newObj.colModel[31].editable = false;
        newObj.colModel[35].editable = false;

    if (!mustEdit)
    {
        for (i = 2; i < newObj.colModel.length; i++) {
            newObj.colModel[i].editable = false;
        }
    }


    newObj.pageModel = { rPP: 1000, type: "local" };
    $grid = $("#dgSpaces-"+id).pqGrid(newObj);
    

    try {
        if ($("#dgSpaces-"+id).pqGrid() != null)
            $("#dgSpaces-"+id).pqGrid("destroy");

        $grid = $("#dgSpaces-"+id).pqGrid(newObj);
        
        $($($("#dgSpaces-"+id).children()[0]).children()[1]).click();
        $($($("#dgSpaces-" + id).children()[0]).children()[1]).click();

        $("#dgSpaces-" + id).on("pqgridquiteditmode", function (event, ui) {
            $("#dgSpaces-" + id).pqGrid("saveEditCell");

        });
        $("#dgSpaces-" + id).on("pqgridcellsave", function (evt, ui) {
            if (ui.rowData[5].length == 0 || ui.rowData[19].length == 0)
            {
                $("#dgSpaces-" + id).pqGrid("quitEditMode");
                return;
            }
            
            var Cols = Space.extracols;
            Space = SpaceModel.parse(ui.rowData);
            Space.extracols = Cols;

            if (Space.ProjectCode > 0) {
                Space.Duplicated = false;
                Space.Delete = false;
                Space.LPID = id;

                ui.rowData[5] = Space.ProjectCodeName;
                $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 5 });

                if(!SaveSpace())
                {
                    return;
                }

                newSpace = false;
                
                $.post('/Lab/GetProjInfo', { Campus: CampusId, id: Space.ProjectCode }, function (response) {
                    
                    ui.rowData[11] = response.Org1;
                    ui.rowData[12] = response.Org2;
                    ui.rowData[13] = response.Org3;
                    ui.rowData[14] = response.Val;
                    ui.rowData[15] = response.BUMgr;
                    ui.rowData[16] = response.Org1 + '/' + response.Org2 + '/' + response.Org1;
                    ui.rowData[28] = response.CrmtStart;
                    ui.rowData[29] = response.CrmtEnd;

                    if (Space.BBKCID.indexOf('-') > -1) {
                        Space.BBKCPlat = Number(Space.BBKCID.split('-')[1].substr(0, 2));
                        //BBKC Platform
                        ui.rowData[8] = Space.BBKCPlat;
                        //BKC Width In
                        ui.rowData[21] = Space.BBKCID.split('-')[0].substr(1, 2);

                    }

                    //in Use
                    if (Space.BBKCPlat == 0)
                        ui.rowData[10] = 0
                    else
                        ui.rowData[10] = Math.round((Space.InUse / Space.BBKCPlat) * 100);
                    
                    
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 11 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 12 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 13 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 14 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 15 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 16 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 28 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 29 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 8 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 21 });
                    $("#dgSpaces-" + id).pqGrid("refreshCell", { rowIndx: ui.rowIndx, colIndx: 10 });
                   
                    RefreshLabTotal($(evt.target).pqGrid("option", "dataModel").data,id);

                    colext =40

                    for (i = 0; i < Space.extracols; i++) {
                        if (ui.rowData[colext + 5] != null) {
                            $.post("/Lab/SaveExtraData", { Value: ui.rowData[colext], ColumnID: ui.rowData[colext + 5], SpaceID: Space.Id });
                            colext++;
                        }
                    }

                });

            }
            ui.rowData[37] = Space.Id;
            ui.rowData[18] = Space.TypeName;
        });
    } catch (err) { }
}
function ProjectList(Campus)
{
    $('#myModalLabel').html(Campus+" - Project List Catalog");

    $.post('/Lab/ProjList', { Campus: Geo }, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(1700);
        $('#Modal').css('left', '-50%');
    });
}
function changeToGridProjList() {
    var tbl = $('#Projects');
    var type = $('#Projects').data("type");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 1650,
        height: 700,
        hwrap: false,
        wrap: false,
        title: "Details",
        //DataGrid Menu
        editable: true,
        collapsible: true,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local',
        selectionModel: {
            //type: 'cell'
            type: 'none'
        },
        hoverMode: 'cell',
        editModel: {
            //onBlur: 'validate',
            saveKey: $.ui.keyCode.ENTER
        },
        cellEditKeyDown: function (event, ui) {
            var col = ui.colIndx;
            if (event.keyCode == 37) {
                $("#Projectsdg").pqGrid("saveEditCell");
                $("#Projectsdg").pqGrid("quitEditMode");
                col--;
                $("#Projectsdg").pqGrid("editCell", { rowIndx: ui.rowIndx, colIndx: col });
                return;
            }
            else if (event.keyCode == 39) {
                $("#Projectsdg").pqGrid("saveEditCell");
                $("#Projectsdg").pqGrid("quitEditMode");
                col++;
                $("#Projectsdg").pqGrid("editCell", { rowIndx: ui.rowIndx, colIndx: col });
                return;
            }
        },
        editor: { type: 'textbox', select: true, style: 'outline:none;' },
        toolbar: {
            items: [
                {
                    type: 'button', icon: 'ui-icon-plus', label: 'Add', listeners: [
                      {
                          "click": function (evt, ui) {
                              $("#Projectsdg").pqGrid("addRow", { rowData: Project.GetArray(), rowIndx: 0 });
                             
                          }
                      }
                    ]
                }
            ]
        },
        scrollModel: { autoFit: false }
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;

    for (i = 0; i < newObj.colModel.length; i++) {
            newObj.colModel[i].width = "120";
    }
    newObj.colModel[10].width = 200;
    newObj.colModel[11].width = 60;
    newObj.colModel[12].width = 60;

    var items = newObj.dataModel.data;
    var result = [];

    for (x = 0; x < newObj.colModel.length; x++) {
        Val = [];
        for (var item, i = 0; item = items[i++];) {
            if (Val.indexOf(item[x]) < 0) {
                Val[Val.length] = item[x];
            }
        }
        result[result.length] = Val;
    }
    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = {
            type: 'select',
            attr: "multiple",
            cache: null,
            options: result[i],
            condition: 'range',
            listeners: ['change'],
            init: function () {
                $(this).pqSelect({ checkbox: true, radio: true, width: '100%' });
            }
        };
    }

    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            gIndex = ui.rowIndx;
        }
    };

    ProjectList
    newObj.colModel[1].filter = { type: 'textbox', condition: 'between', listeners: ['keyup'] };
    newObj.colModel[13].hidden = true;
    newObj.colModel[14].filter = null;

    newObj.pageModel = { rPP: 1000, type: "local" };

    $grid = $('#Projectsdg').pqGrid(newObj);


    try {
        if ($('#Projectsdg').pqGrid() != null)
            $('#Projectsdg').pqGrid("destroy");

        $grid = $('#Projectsdg').pqGrid(newObj);

        $($($('#Projectsdg').children()[0]).children()[1]).click();
        $($($('#Projectsdg').children()[0]).children()[1]).click();

        $('#Projectsdg').on("pqgridcellsave", function (evt, ui) {

            Project = ProjectModel.parse(ui.rowData);
            Project.idCampus = $('#ProjCampus').text();
            SaveProject();
        });
    } catch (err) { }
}
function DeleteProject()
{
    Project = ProjectModel.parse($("#Projectsdg").pqGrid("getRowData", { rowIndx: gIndex }));
    Project.Delete = true;

    SaveProject();

    $("#Projectsdg").pqGrid("deleteRow", { rowIndx: gIndex });

}
function SaveProject()
{
    $.post('/Lab/SaveProj', { ProjCode: Project.ProjCode, Org1: Project.Org1, Org2: Project.Org2, Org3: Project.Org3, Val: Project.Val, CrntStar: Project.CrnStart, CrntEnd: Project.CrnEnd, BuMgr: Project.BuMgr, OldName: Project.OldName, OldName2: Project.OldName2, Comments: Project.Comments, CC: Project.CC, BKCMora: Project.BKCMoraSqft, Deleted: Project.Delete, idCampus: Project.idCampus, id: Project.id })
}
function cmbSelProject(id,lpwid)
{
    if (id > 0) {
        $.post('/Lab/GetProjInfo', { Campus: CampusId, id: id }, function (response) {
            var data = $("#dgSpaces-" + lpwid).pqGrid("getRowData", { rowIndx: gIndex });
            data[5] = response.ProgCode;
            data[11] = response.Org1;
            data[12] = response.Org2;
            data[13] = response.Org3;
            data[14] = response.Val;
            data[15] = response.BUMgr;
            data[16] = response.Org1 + '/' + response.Org2 + '/' + response.Org1;
            data[28] = response.CrmtStart;
            data[29] = response.CrmtEnd;

            $("#dgSpaces-" + lpwid).pqGrid("refreshRow", { rowIndx: gIndex });
        });
    }
}
function AddExtra(campus)
{
    $('#myModalLabel').html("Load Info");

    $.post('/Lab/AddExtaCol',{CampusId: CampusId}, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#myModalLabel').html('Custom columns for '+campus);
        $('#modalcontent').width(400);
        changes = false;
        $('.close').on("click", function () {
            if (changes) {
                LoadLabs();
            }
        });
    });
}
function addExtraCol() {
    if($('#txtColName').val().length>0)
    {
        $.post('/Lab/SaveExtaColumn', { Id: 0, Name: $('#txtColName').val(), CampusId: CampusId, Deleted: false }, function (data) {
            $($('#Columns tbody')[0]).append('<tr><td onclick="idExtraCol=' + data.id + '" contenteditable="true" onkeydown="editExtraCol(this)">' + data.ColumnName + '</td></tr>')
             changes= true;
        })
    }
}
function editExtraCol(obj) {
    if ($(obj).text().length > 0 && ($(obj).data("delete")==undefined || $(obj).data("delete")==null)) {
        $.post('/Lab/SaveExtaColumn', { Id: idExtraCol, Name: $(obj).text(), CampusId: CampusId, Deleted: false }, function (response) {
            changes = true;
        });
    }else
    {
        if(confirm('Do you want delete this column'))
        {
            $.post('/Lab/SaveExtaColumn', { Id: idExtraCol, Name: $(obj).text(), CampusId: CampusId, Deleted: true }, function (data) {
                $($(obj).parent()).hide();
                changes = true;
            })
        }
    }
}
function ViewUsers(campus)
{
    $('#myModalLabel').html("Users in "+campus);

    $.post('/Lab/Users', { CampusId: CampusId }, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        changes = false;
        $('.close').on("click", function () {
            if (changes) {
                LoadLabs();
            }
        });
    });
}
function AddUser(email,id)
{
    $.post('/Lab/ManUser', { CampusId: CampusId, Email: email, Id: id, bDelete: false }, function (response) {
        changes = true;
        if(id==0)
        {
            var row = '<tr data-user='+response.id+'><td>' + response.Name + '</td><td contenteditable="true" onkeyup="AddUser($(this).text(),'+response.id+')">' + response.Email + '</td><td><input type="button" value="Delete" onclick="DeleteUser('+response.id+')" /></td></tr>';
            $('#Users > tbody').append(row);
        }else
        {
            $($('[data-user=' + id + ']').children()[0]).text(response.Name);
        }
    });
}
function DeleteUser(id)
{
    $.post('/Lab/ManUser', { CampusId: CampusId, Email: '', Id: id, bDelete: true }, function (response) {
        changes = true;
        $('[data-user=' + id + ']').hide();
    });
}
function SaveAvl(obj)
{
    $.post('/Lab/SaveAvlLab', { Continuity: ContinuityID, Value: $(obj).text(), Type: $(obj).data('type'), WW: $(obj).data('ww') });
}
function LabSQF(CampusId,Campus)
{
    $('#myModalLabel').html("Campus "+Campus+" SQF");

    $.post('/Lab/LabSQF', { LabCampus: CampusId, WWID: wwid }, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
    });
}
function SaveLabSQF(obj)
{
    $.post('/Lab/SaveLabSQF', { Size: $(obj).text() , Continuity: $(obj).data('cont') ,WWID: wwid})
}