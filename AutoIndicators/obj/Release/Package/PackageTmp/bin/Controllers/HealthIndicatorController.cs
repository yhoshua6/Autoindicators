﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data;
using System.Collections;

namespace AutoIndicators.Controllers
{
    //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class HealthIndicatorController : Controller
    {
        dynamic myModel = new ExpandoObject();

        public ActionResult OpsQuery(int WWID)
        {
            List<Models.OpsQuery> model = new List<Models.OpsQuery>();
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                model = dc.Database.SqlQuery<Models.OpsQuery>("SPROCPQuery @WWID = {0} ", WWID).ToList<Models.OpsQuery>();
            }
            return View(model);
        }

        public ActionResult Index(bool complete = false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            int? pWWID = null;
            List<Models.HealthIndicator> list = new List<Models.HealthIndicator>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            List<Models.SnapshotIndex> lstProgram = new List<Models.SnapshotIndex>();

            Models.mFilter mFil = new Models.mFilter();

            #region Init Filter

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "HealthIndicator";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "HealthIndicator";


            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", result[0].WWID).ToList<Models.SnapshotIndex>();

                    if (complete)
                    {
                        list = getDBdata(result[0].WWID - 1);
                    }
                    else
                        list = getDBdata(result[0].WWID);

                    #region Build Filter
                    List<object> objFil = new List<object>();


                    if (Session["DataSource"] != null && Session["FilString"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }


                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }


                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter

                        List<string> strValue;

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                            Session["ValSel"] = strValue;
                        }

                        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                        int inti = 0;

                        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                        {
                            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                        {
                            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                        {
                            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                        {
                            strProgValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                        {
                            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                        {
                            strClasification[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();


                        ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        #endregion
                    }else
                    {
                        ViewBag.Program = lstProgram;
                    }

                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    
                    ViewBag.WeekStatus = list[0].weekStatus;
                }
                else
                {
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", pWWID).ToList<Models.SnapshotIndex>();

                    #region Build Filter
                    List<object> objFil = new List<object>();

                    list = getDBdata(int.Parse(pWWID.ToString()));

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }

                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }


                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                   // fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter
                        if (Session["ValSel"] != null)
                        {

                            List<string> strValue = (List<string>)Session["ValSel"];

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        
                        }
                        #endregion
                    }else
                    {
                        ViewBag.Program = lstProgram;
                    }

                    #endregion


                }

                ViewBag.Complete = complete;
             
                

                return View("~/Views/HealthIndicator/Hi.cshtml", list);
            }
        }
        public ActionResult Hi(bool complete=false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            int? pWWID=null;
            List<Models.HealthIndicator> list = new List<Models.HealthIndicator>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();

            

            #region Init Filter

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "HealthIndicator";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "HealthIndicator";


            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count>0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();



                    if (complete)
                    {
                        list = getDBdata(result[0].WWID-1);
                    }else
                        list = getDBdata(result[0].WWID);
                       
                    #region Build Filter
                    List<object> objFil = new List<object>();

                        
                    if (Session["DataSource"] != null && Session["FilString"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }
                        
                            
                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }
                        

                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter

                        List<string> strValue;

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                            Session["ValSel"] = strValue;
                        }

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        
                        #endregion
                    }

                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                       
                    ViewBag.WeekStatus = list[0].weekStatus;
                    
                }
                else
                {
                    #region Build Filter
                    List<object> objFil = new List<object>();

                    list = getDBdata(int.Parse(pWWID.ToString()));

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }
                      
                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }
                    

                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    //fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter
                        if (Session["ValSel"] != null)
                        {

                            List<string> strValue = (List<string>)Session["ValSel"];

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        }
                        #endregion
                    }
                    
                    #endregion
                    
                        
                }

                ViewBag.Complete = complete;


                return View("~/Views/HealthIndicator/HealthIndicatorView.cshtml", list);
            }
        }

        public ActionResult IndexFromSnSh(int? pPWID)
        {
            int? pWWID=null;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            var result = new List<Models.DefaultWW>();
            List<Models.HealthIndicator> list;
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                   
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    pWWID = result[0].WWID;
                }

                list = getDBdata(int.Parse(pWWID.ToString()));
                if (list.Count > 0)
                {
                    List<Models.HealthIndicator> subList = list.Where(a => a.PWID == pPWID).ToList<Models.HealthIndicator>();
                    ViewBag.elementFocus = subList[0].HealthId;
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;
                }
                else
                {
                    ViewBag.WW = result[0].weekName;
                    ViewBag.WWID = pWWID;
                    ViewBag.WeekStatus = result[0].weekstatus;
                }

                ViewBag.Complete = false;
                return View("~/Views/HealthIndicator/HealthIndicatorView.cshtml", list);
            }
        }

        public List<Models.HealthIndicator> getDBdata(int WWID)
        {
            var list = new List<Models.HealthIndicator>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetWWHealthIndicators @WWID = {0} ", WWID).ToList<Models.HealthIndicator>();
            }

            return list;
        }

        public ActionResult CommentView(int? HIID, string weekStatus)
        {
            List<Models.HealthIndicator> result = new List<Models.HealthIndicator>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.HealthIndicator>(@"SPROCGetHealthIndicator @HIID= {0}", new object[] { HIID }).ToList<Models.HealthIndicator>();
            }
            return PartialView(result[0]);
        }
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection)
        {
            string usr = User.Identity.Name;
            string comments = SnapshotController.RemoveUnwantedHtmlTags(collection["txtDescription_"], SnapshotController.unwantedTags());
            var parames = new object[] { collection["HealthID"], collection["Semaphore"], comments, usr };
            var result = new List<Models.DefaultWW>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {

                result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWHealthIndicatorVal @HealthId= {0}, @value = {1},@comments = {2},@systemUser = {3}", parames).ToList<Models.DefaultWW>();
            }
            //System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
            ////return RedirectToAction("Index", "HealthIndicator", new { pWWID = result[0].WWID, weekName = result[0].weekName });
            //string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            //return Redirect(url);
            return Json(result.First());
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HealthIndicatorView(int newHealthID, int WWID, string newValue, string newComments)
        {
            string usr = User.Identity.Name;
            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("SPROCUpdateWWHealthIndicatorVal @HealthId= {0}, @value = {1}, @comments = {2}, @systemUser = {3}", new object[] { newHealthID, newValue, newComments, usr });
                    this.getDBdata(WWID);
                    myModel.WWID = WWID.ToString();
                    return View(myModel);
                }
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                throw;
            }

        }
    }
}