﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoIndicators.Models;
using System.Dynamic;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    public class ProgEditController : Controller
    {
        dynamic myModel = new ExpandoObject();

        public ActionResult Add(FormCollection collection)
        {
            
            //int WWstatus = int.Parse(collection["WWstatus"]);
            string usr = User.Identity.Name;


            var parames = new object[] { collection["WWID"], collection["ddlPXT"], collection["pName"].Replace("'"," "), collection["pDesc"], collection["ddlPT"], 
                                        collection["ddlGEO"], collection["ddlOwner"], collection["pLink"], collection["pDescLink"],collection["pMRD"]
                                        , collection["pPRD"],collection["ddlVertical"],collection["ddlClasification"],"W "+(collection["pWWI"].Length > 1 ? collection["pWWI"] : "0" + collection["pWWI"] )+"'"+collection["pWWYI"]
                                        ,"W "+(collection["pWWE"].Length > 1 ? collection["pWWE"] : "0" + collection["pWWE"] )+"'"+collection["pWWYE"],collection["InSnapshot"]
                                        ,collection["pColor"],collection["pColorW"] ,usr};
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand(@"SPROCCreateProgram @WWID= {0}, @PXT = {1},@PName = {2}, @PDesc = {3},@PType = {4},
                                                @GEO = {5},@POwner = {6}, @PLink = {7},@PDescLink = {8},@PMRD = {9},@PPRD = {10},
                                                @VerticalID={11},@Clasification={12},@RMStartWW={13},@RMEndWW={14},@InSnapshot={15},@Color={16},@perc={17},@systemUser = {18} ", parames);
            }

            string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            return Redirect(url);
        }
        public ActionResult ValidatePT(int PTID)
        {
            List<Models.MilestoneCatalog> lstMilestone = new List<Models.MilestoneCatalog>();
            List<Models.IndicatorCatalog> lstIndicator = new List<Models.IndicatorCatalog>();
            List<Models.ProgTypes> lstProgType = new List<Models.ProgTypes>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstMilestone = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetMilestones").ToList<Models.MilestoneCatalog>();
                lstIndicator = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetHealthIndicatorCat").ToList<Models.IndicatorCatalog>();
                lstProgType = dc.Database.SqlQuery<Models.ProgTypes>("SPROCcatProgTypes").ToList<Models.ProgTypes>();

                foreach (Models.ProgTypes progtype in lstProgType)
                {
                    progtype.Milestones = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[] { progtype.id, 1 }).ToList<Models.MilestoneCatalog>();
                    progtype.HealthIndicator = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[] { progtype.id, 2 }).ToList<Models.IndicatorCatalog>();
                }
            }
            
            Models.ProgTypes PT=lstProgType.Where(x => x.id ==PTID).First();
            return Json(PT);
                
        }
             
        public ActionResult SavePXT(int id,string Name,string Link)
        {
            List<PXTs> pxts = new List<PXTs>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                pxts=dc.Database.SqlQuery<Models.PXTs>(@"SPROCmanPXT @id = {0}, @vchName={1}, @vchLink={2}", new object[]{ id,Name,Link}).ToList<Models.PXTs>();
            }

            return Json(pxts.First());
        }
        public ActionResult PXT()
        {
            List<PXTs> pxts = new List<PXTs>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                pxts = dc.Database.SqlQuery<Models.PXTs>(@"SPROCcatPXT").ToList<Models.PXTs>();
            }

            return View("PXTView", pxts);
        }
        public ActionResult Save(FormCollection collection)
        {
            List<ScalarValue> list = new List<ScalarValue>();
            List<BPOProjReport> errorRel = new List<BPOProjReport>();
            List<BPOProjReport> errorRelComp = new List<BPOProjReport>();
            int WWstatus = int.Parse(collection["WWstatus"]);
            string usr = User.Identity.Name;
            int deactivate = 1;
            string url = "";
            DataSet dsRes = new DataSet();

            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            try
            {
            //kill all  bpo proj ids relations
            var paramt = new object[] { collection["PWID"] };

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PWId", collection["PWID"]);

            SqlHelper.ExecuteNonQuery(tran, "SPROCDeactivateRelBPOProj", param);
           
            

            if (collection["chk"].ToString().Equals("false"))
            {
                deactivate = 0;
                if (collection["lstAddBPO"] != null && collection["lstAddBPO"].ToString().Length>0)
                {
                    string[] arrBPOProjIds = collection["lstAddBPO"].Split(char.Parse(","));
                    
                    foreach (string projid in arrBPOProjIds)
                    {
                        
                            var paramets = new object[] { collection["PWID"], int.Parse(projid), int.Parse(collection["WWID"]), usr };
                            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                            {
                                param = new SqlParameter[4];
                                param[0] = new SqlParameter("@PWId", collection["PWID"]);
                                param[1] = new SqlParameter("@BPOProjId", int.Parse(projid));
                                param[2] = new SqlParameter("@WWID", int.Parse(collection["WWID"]));
                                param[3] = new SqlParameter("@systemUser", usr);

                                dsRes = SqlHelper.ExecuteDataset(tran, "SPROCCreateRelBPOProj", param);

                                dynamic Res;

                                if (dsRes.Tables.Count > 0)
                                {
                                    Res = GeneralClasses.General.ParseTo((object)new Models.BPOProjReport(), dsRes.Tables[0]);

                                    foreach (BPOProjReport BPO in Res)
                                    {
                                        errorRel.Add(BPO);
                                    }

                                    if (errorRel != null && errorRel.Count > 0)
                                    {
                                        foreach (BPOProjReport item in errorRel)
                                        {
                                            if (item.PWContinuityID != collection["PWContinuityID"])
                                            {
                                                errorRelComp.Add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        
                    }
                }
            }

            string StartWW = "W " + (collection["pWWI"].Length > 1 ? collection["pWWI"] : "0" + collection["pWWI"] )+ "'" + collection["pWWYI"];
            string EndWW = "W " + (collection["pWWE"].Length > 1 ? collection["pWWE"] : "0" + collection["pWWE"]) + "'" + collection["pWWYE"];

            if (collection["pWWI"].Length == 0 || collection["pWWYI"].Length == 0)
            {
                StartWW = null;
            }

            if (collection["pWWE"].Length == 0 || collection["pWWYE"].Length == 0)
            {
                EndWW = null;
            }
                param = new SqlParameter[26];
                param[0] = new SqlParameter("@WWID", int.Parse(collection["WWID"]));
                param[1] = new SqlParameter("@PWID", int.Parse(collection["PWID"]));
                param[2] = new SqlParameter("@ProgramID", int.Parse(collection["PID"]));
                param[3] = new SqlParameter("@PXT", int.Parse(collection["ddlPXT"]));
                param[4] = new SqlParameter("@PName", collection["pName"]);
                param[5] = new SqlParameter("@PDesc", collection["pDesc"]);
                param[6] = new SqlParameter("@PDescLink", collection["pDescLink"]);
                param[7] = new SqlParameter("@PType", int.Parse(collection["ddlPT"]));
                param[8] = new SqlParameter("@GEO",collection["ddlGEO"].Length>0 ? int.Parse(collection["ddlGEO"]) :0);
                param[9] = new SqlParameter("@POwner",collection["ddlOwner"].Length>0? int.Parse(collection["ddlOwner"]) :0);
                param[10] = new SqlParameter("@pLink", collection["pLink"]);
                param[11] = new SqlParameter("@deactivate", deactivate);
                param[12] = new SqlParameter("@systemUser", usr);
                param[13] = new SqlParameter("@pPrivate", collection["pPrivate"] == null ? false : Convert.ToBoolean(collection["pPrivate"]));
                param[14] = new SqlParameter("@pSharepoint", collection["pSharepoint"]);
                param[15] = new SqlParameter("@pMRD", collection["pMRD"]);
                param[16] = new SqlParameter("@pPRD", collection["pPRD"]);
                param[17] = new SqlParameter("@VerticalID",collection["ddlVertical"].Length>0 ? int.Parse(collection["ddlVertical"]) :0);
                param[18] = new SqlParameter("@Clasification", int.Parse(collection["ddlClasification"]));
                param[19] = new SqlParameter("@StartWW", StartWW);
                param[20] = new SqlParameter("@EndWW", EndWW);
                param[21] = new SqlParameter("@InSnapshot", collection["InSnapshot"] == "1" || collection["InSnapshot"] == "true" ? true : false);
                param[22] = new SqlParameter("@ColorStart", collection["pColorStart"]);
                param[23] = new SqlParameter("@ColorEnd", collection["pColorEnd"]);
                param[24] = new SqlParameter("@Perc", collection["pColorW"]);
                param[25] = new SqlParameter("@Position", collection["pPosition"]);

                dsRes = SqlHelper.ExecuteDataset(tran, "SPROCUpdateProgramInfo", param);

                tran.Commit();
                tran.Dispose();
                conn.Dispose();
                conn.Close();
                System.Web.HttpContext.Current.Session["elementFocus"] = collection["PWContinuityID"];
                url = HttpContext.Request.UrlReferrer.PathAndQuery;                          
            }
            catch (Exception)
            {
                tran.Rollback();
                tran.Dispose();
                conn.Dispose();
                conn.Close();
            }

            if (url.Length > 0)
                return Redirect(url);
            else
                return new EmptyResult();
        }

        public ActionResult Index(int? PWID, bool InSnapshot=true)
        {
            dynamic myModel = new ExpandoObject(); 
            var GEOList = new List<Models.GEOs>();
            var PXTList = new List<Models.PXTs>();
            var PTList = new List<Models.ProgTypes>();
            var ActorsList = new List<Models.Actors>();
            var Program = new List<Models.getProgram>();
            List<BPOPRJID> ProjectIdsList = new List<BPOPRJID>();
            List<BPOPRJID> AddedProjectIdsList = new List<BPOPRJID>();
            List<Vertical> VerticalList = new List<Vertical>();
            var userList = new List<Models.Users>();

            IEnumerable<SelectListItem> GEOEnum;
            IEnumerable<SelectListItem> PXTEnum;
            IEnumerable<SelectListItem> PTEnum;
            IEnumerable<SelectListItem> ActorsEnum;
            IEnumerable<SelectListItem> BPOProjEnum;
            IEnumerable<SelectListItem> AddedBPOProjEnum;
            IEnumerable<SelectListItem> VerticalEnum;


            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    Program = dc.Database.SqlQuery<Models.getProgram>("[SPROCGetProgram] @PWID = {0}, @InSnapshot={1}",new object[]{PWID,InSnapshot}).ToList<Models.getProgram>();
                    GEOList = dc.Database.SqlQuery<Models.GEOs>("[SPROCcatGEO]").ToList<Models.GEOs>();
                    PXTList = dc.Database.SqlQuery<Models.PXTs>("[SPROCcatPXT]").ToList<Models.PXTs>();
                    PTList = dc.Database.SqlQuery<Models.ProgTypes>("[SPROCcatProgTypes]").ToList<Models.ProgTypes>();
                    ActorsList = dc.Database.SqlQuery<Models.Actors>("[SPROCcatActors]").ToList<Models.Actors>();
                    ProjectIdsList = dc.Database.SqlQuery<Models.BPOPRJID>("[SPROCListBPOProjIDs] @PWID = {0}", PWID).ToList<Models.BPOPRJID>();
                    AddedProjectIdsList = dc.Database.SqlQuery<Models.BPOPRJID>("[SPROCListAddedBPOProjIDs] @PWID = {0}", PWID).ToList<Models.BPOPRJID>();
                    VerticalList = dc.Database.SqlQuery<Models.Vertical>("[SPROCcatVertical]").ToList<Models.Vertical>();
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                    #region Catalog
                    VerticalEnum = VerticalList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });
                    GEOEnum = GEOList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });
                    PXTEnum = PXTList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });
                    PTEnum = PTList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });
                    ActorsEnum = ActorsList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });

                    BPOProjEnum = ProjectIdsList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.BPOProjName,
                        Value = x.BPOProjId.ToString()
                    });

                    AddedBPOProjEnum = AddedProjectIdsList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.BPOProjName,
                        Value = x.BPOProjId.ToString()

                    });
                    #endregion
                }
                myModel.ProgramData = Program;
                myModel.GEOList = GEOEnum;
                myModel.PXTList = PXTList;
                myModel.PTList = PTEnum;
                myModel.ActorsList = ActorsEnum;
                myModel.BPOList = BPOProjEnum;
                myModel.AddedBPOList = AddedBPOProjEnum;
                myModel.BPOInfo = ProjectIdsList;
                myModel.VerticalList = VerticalEnum;
                myModel.Users = userList;
                ViewBag.InSnapshot = InSnapshot? 1:0;
                return View("~/Views/ProgEdit/ProgEdit.cshtml", myModel);
            }
   
            catch (Exception ex)
            {
                
                throw ex;
            }

        }

        
        public ActionResult ProgramManagerAdd() 
        {   
            return View("~/Views/ProgEdit/ProgramManagerAdd.cshtml");
        }

        public ActionResult ProgramManagerDelete(int ID) 
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
               dc.Database.ExecuteSqlCommand("SPROCDeleteActor @PID = {0} ", ID);
            }
            string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            return Redirect(url);  
        }

        public ActionResult ProgramManagerEdit(int ID)
        {

            var list = new List<Models.Actors>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Actors>("SPROCgetsingleActor @PID = {0} ", ID).ToList<Models.Actors>();
            }
            myModel.list = list;
            return View(myModel);
        }

      
        public ActionResult ProgramManagerList(string StartingLetter) 
        {
            var list = new List<Models.Actors>();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (StartingLetter == null)
                    list = dc.Database.SqlQuery<Models.Actors>("SPROCgetallActors").ToList<Models.Actors>();
                else
                    list = dc.Database.SqlQuery<Models.Actors>("SPROCgetletterActors @abbreviationString = {0} ", StartingLetter).ToList<Models.Actors>();
            }
            myModel.list = list;
            return View(myModel);
         
        }

        

        public ActionResult ProgramManagerSave(FormCollection collection)
        {
            var parames = new object[] { collection["pName"], collection["pLastName"], collection["pEmail"] };
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand(@"SPROCCreateActor @PName = {0}, @PLastName = {1}, @PEmail = {2}", parames);
            }

            string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            return Redirect(url);  
        }

        public ActionResult ProgramManagerSaveEdit(FormCollection collection)
        {
            var parames = new object[] {collection["PId"],collection["pName"], collection["pLastName"], collection["pEmail"] };
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand(@"SPROCUpdateActor @PID = {0}, @PName = {1}, @PLastName = {2}, @PEmail = {3}", parames);
            }
            string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            return Redirect(url);
        }
        public List<BPOPRJID> getProjectID(int PWID)
        {
            List<BPOPRJID> AddedProjectIdsList = new List<BPOPRJID>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                AddedProjectIdsList = dc.Database.SqlQuery<Models.BPOPRJID>("[SPROCListAddedBPOProjIDs] @PWID = {0}", PWID).ToList<Models.BPOPRJID>();
            }

            return AddedProjectIdsList;
        }
        public ActionResult CommentsTerminate()
        {
            return PartialView();
        }
        public ActionResult TerminateProgram(int programId, string Continuity, int WWID, string Comments)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ProgramId", programId);
            param[1] = new SqlParameter("@PWContinuityID", Continuity);
            param[2] = new SqlParameter("@WWID", WWID);
            param[3] = new SqlParameter("@Comments", Comments);
            param[4] = new SqlParameter("@TerminateUser", User.Identity.Name);
            string strBodyCreate = "";
            string strBodyHi = "";
            string strBodySchedule = "";
            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCterminateProg", param);

            if (dsRes != null && dsRes.Tables.Count > 0 && dsRes.Tables[0].Rows.Count > 0)
            {
                return Json(true);
            }
            else
                return Json(false);
        }
        public ActionResult AddProgramSnap(int ProgramId,string Link)
        {
            Models.MasterModel prog = new MasterModel();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
               prog= dc.Database.SqlQuery<Models.MasterModel>("[SPROCAddProgramtoSnapshot] @IdProgram = {0}, @PLink={1}, @systemUser={2}", new object[] { ProgramId, Link, User.Identity.Name }).ToList<Models.MasterModel>().SingleOrDefault();
            }

            return Json(prog);
        }
        public ActionResult IndexAdd(int? WWID,bool InSnapshot=true)
        {
            dynamic myModel = new ExpandoObject();
            var GEOList = new List<Models.GEOs>();
            var PXTList = new List<Models.PXTs>();
            var PTList = new List<Models.ProgTypes>();
            var ActorsList = new List<Models.Actors>();
            var VerticalList = new List<Models.Vertical>();

            IEnumerable<SelectListItem> GEOEnum;
            IEnumerable<SelectListItem> PXTEnum;
            IEnumerable<SelectListItem> PTEnum;
            IEnumerable<SelectListItem> ActorsEnum;
            IEnumerable<SelectListItem> VerticalEnum;


            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                GEOList = dc.Database.SqlQuery<Models.GEOs>("[SPROCcatGEO]").ToList<Models.GEOs>();
                PXTList = dc.Database.SqlQuery<Models.PXTs>("[SPROCcatPXT]").ToList<Models.PXTs>();
                PTList = dc.Database.SqlQuery<Models.ProgTypes>("[SPROCcatProgTypes]").ToList<Models.ProgTypes>();
                ActorsList = dc.Database.SqlQuery<Models.Actors>("[SPROCcatActors]").ToList<Models.Actors>();
                VerticalList = dc.Database.SqlQuery<Models.Vertical>("[SPROCcatVertical]").ToList<Models.Vertical>();

                VerticalEnum = VerticalList.Select(x =>
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.id.ToString()
                });
                GEOEnum = GEOList.Select(x =>
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.id.ToString()
                });
                PXTEnum = PXTList.Select(x =>
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.id.ToString()
                });
                PTEnum = PTList.Select(x =>
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.id.ToString()
                });
                ActorsEnum = ActorsList.Select(x =>
                new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.id.ToString()
                });
            }
            myModel.GEOList = GEOEnum;
            myModel.PXTList = PXTEnum;
            myModel.PTList = PTEnum;
            myModel.VerticalList = VerticalEnum;
            myModel.ActorsList = ActorsEnum;
            myModel.WWID = WWID;
            ViewBag.InSnapshot = InSnapshot? 1:0;

            return View("~/Views/ProgEdit/ProgAdd.cshtml", myModel);
        }
	}
}