﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using HtmlAgilityPack;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Reflection;

namespace AutoIndicators.Controllers
{
    public class CCBController : Controller
    {
        dynamic myModel = new ExpandoObject();
        int weekStatus = 0;

        public ActionResult Index(bool complete=false)
        {
            int? WWID = null;
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                var result = new List<Models.DefaultWW>();
                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();
                List<Models.CCB> lstCCb=new List<Models.CCB>();
                List<Models.CCB> lstCCbCat = new List<Models.CCB>();
                #region Filter
                #region Build Filter

                mFil.strField = "VerticalName";
                mFil.strLabelField = "Swim Lanes";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();
                mFil.strField = "PXTName";
                mFil.strLabelField = "PXT";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgType";
                mFil.strLabelField = "Type";
                //      mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "OwnerName";
                mFil.strLabelField = "Owner";
                //       mFil.strLinkedField = "ProgType";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ClasificationName";
                mFil.strLabelField = "Clasification";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgName";
                mFil.strLabelField = "Program";
                //       mFil.strLinkedField = "OwnerName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                ViewBag.Filter = mFilters;
                #endregion

                if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                    WWID = Convert.ToInt32(Session["FilWW"].ToString());
                #endregion
                #region Load WW
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    getDBdata(result[0].WWID); 
                }else
                {
                    getDBdata(WWID);
                }
                #endregion

                #region Load Data

                if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
                {
                    Request.Cookies["Module"].Value = "CCB";
                    Session.Remove("DataSource");
                }
                else
                    Request.Cookies["Module"].Value = "CCB";


                    #region Build Filter
                    List<object> objFil = new List<object>();
                    lstCCbCat = getCCBs(myModel.WWID);
                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }

                    foreach (Models.CCB ccb in lstCCbCat)
                    {
                        Object o = new object();

                        o = (object)ccb;
                        objFil.Add(o);
                    }
                    

                    ViewBag.objFil = objFil;
                    #endregion
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    if (Request.Cookies["FilString"] != null)
                    {
                        #region Load Data Filter

                        fFil.dtData = (DataTable)Session["DataFil"];
                      //  string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        if (Session["ValSel"]!=null)
                        {

                            List<string> strValue;

                            if (Session["ValSel"] != null)
                                strValue = (List<string>)Session["ValSel"];
                            else
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }
                            lstCCb = lstCCbCat.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList<Models.CCB>();

                            var CCBStatusList = new List<Models.CCBStatus>();
                           
                            CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();

                            if (lstCCb.Count ==0)
                            {
                                lstCCb = lstCCbCat;                             
                               

                            }

                            DataTable dtData = new DataTable();

                            foreach (Models.CCB CCB in lstCCb)
                            {
                                var res = CCBStatusList.Where(row => row.ContinuityID == CCB.ContinuityID);
                                if (res.Count() > 1)
                                {
                                    CCB.lStatus = res.ToList();
                                }
                                else
                                {
                                    CCB.lStatus = new List<Models.CCBStatus>();

                                }
                                CCB.WWID = myModel.WWID;
                            }
                        #endregion
                            myModel.CCB = lstCCb;
                        }else
                        {
                            myModel.CCB = getCCBs(myModel.WWID);
                        }
                    }else
                    {
                        myModel.CCB = getCCBs(myModel.WWID);
                    }
                #endregion

                    myModel.AffectedCCB = getCCBAffectedProj();
                    ViewBag.WeekStatus = lstCCbCat.First().weekStatus;
                myModel.CatCCBStatus = getCatCCBStatus();
            }

            ViewBag.Complete = complete;
            return View("CCB",myModel);
        }
        public ActionResult CCBLinksView(int CCBID,int WWID,string ContID,string proj)
        {
            myModel.CCBID=CCBID;
            myModel.WWID = WWID;
            myModel.ContID = ContID;
            myModel.Proj = proj;

            return PartialView(myModel);
        }
        public ActionResult SaveLink(FormCollection collection)
        {
            bool bOk = false;

            if (collection.Count > 0)
            {
                if (collection["txtCCBID"] != null && collection["txtCCBID"]!="0")
                {
                    SqlParameter[] param = new SqlParameter[3];

                    param[0] = new SqlParameter("@IDCCB", collection["txtCCBID"]);
                    param[1] = new SqlParameter("@vchName", collection["txtName"]);
                    param[2] = new SqlParameter("@vchURL", collection["txtUrl"]);

                    DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanLinkCCB", param);

                    if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                        bOk = true;
                }
                else
                { 
                    List<Models.CCBFiles> lstFile=new List<Models.CCBFiles>();
                    Models.CCBFiles mfile=new Models.CCBFiles();

                    if(Session["CCBFiles"]!=null)
                    {
                        lstFile = (List<Models.CCBFiles>)Session["CCBFiles"];
                    }

                    mfile.intId = 0;
                    mfile.strName = collection["txtName"];
                    mfile.strURL = collection["txtUrl"];

                    lstFile.Add(mfile);

                    Session["CCBFiles"] = lstFile;
                    bOk = true;
                }
            }
            return RedirectToAction("/RegisterCCB", new { WWID = collection["txtWWID"], ccbid = collection["txtCCBID"], ContID = collection["txtContID"], proj = collection["txtProj"] });
            
        }
        public ActionResult DeleteFile(string File,int id)
        {
            SqlParameter[] param = new SqlParameter[3];

            string[] values = File.Split(',');
            string strMessage = "";

            param[0] = new SqlParameter("@IDCCB", values[0]);
            param[1] = new SqlParameter("@vchName", values[1]);
            param[2] = new SqlParameter("@vchURL",  values[2]);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanLinkCCB", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                strMessage = "1";
            return Json(new {strMessage});
        }
        public ActionResult StatusDetail(int? WWID, string ContID, int statusID,string proj)
        {
            var CCBStatusList = new List<Models.CCBStatus>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();
            }

            List<Models.CCBStatus> statCCB = new List<Models.CCBStatus>();

            var res = CCBStatusList.Where(row => row.ContinuityID == ContID && row.intStatus == statusID).ToList();
            statCCB = res.ToList();

            myModel.CCBs = statCCB;
            myModel.Proj=proj;
            myModel.WWID = WWID;
            Session["VBWW"] = Session["VBWW"];
            Session["VBWWID"] = Session["VBWWID"];

            return PartialView(myModel);
        }
        public ActionResult OtherProgram()
        {
            List<Models.CCBOtherProgramAffected> OPA = new List<Models.CCBOtherProgramAffected>();
            List<Models.PXTs> PXTList = new List<Models.PXTs>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                PXTList = dc.Database.SqlQuery<Models.PXTs>("[SPROCcatPXT]").ToList<Models.PXTs>();
                OPA = dc.Database.SqlQuery<Models.CCBOtherProgramAffected>("[SPRCgetCCBOtherProgram]").ToList<Models.CCBOtherProgramAffected>();
            }

            myModel = OPA;
            ViewBag.PXT = PXTList;

            return View("OtherProgramView",myModel);
        }
        public ActionResult SaveProgram(string Name,int PXT)
        {
            List<Models.CCBOtherProgramAffected> OPA = new List<Models.CCBOtherProgramAffected>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                OPA = dc.Database.SqlQuery<Models.CCBOtherProgramAffected>("[SPROCmanCCBOtherProgram] @vchName={0}, @PXTID={1}", new object[]{ Name, PXT }).ToList<Models.CCBOtherProgramAffected>();
            }

            return Json(OPA);
        }
        public ActionResult All(bool complete=false)
        {
            List<Models.CCB> ccbs = new List<Models.CCB>();
            List<Models.DefaultWW> results = new List<Models.DefaultWW>();
            int? WWID=null;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (WWID == null)
                {
                    results = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                {
                    results = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();
                }

                ViewBag.WW = results[0].weekName;
                ViewBag.WWID = results[0].WWID;
                WWID = results[0].WWID;

                ccbs = dc.Database.SqlQuery<Models.CCB>("SPROCgetAllCCB @WWID={0}",WWID).ToList<Models.CCB>();

                foreach(Models.CCB ccb in ccbs)
                {
                    ccb.ProjAffected = getCCBProjSel(WWID,ccb.intCCBid);
                    ccb.OPA = getOtherProgramAffected(ccb.intCCBid);
                }
            }
            myModel = ccbs;
           
            ViewBag.Complete = complete;
            return View("CCBAllView", myModel);
        }
        #region CCB Register
        public ActionResult RegisterCCB(int WWID, int? ccbid, string ContID="",string proj="",string WWName="")
        {
            List<Models.CatCCBStatus> CCBStatusList = new List<Models.CatCCBStatus>();
            List<Models.CCBPriority> lstPriority = new List<Models.CCBPriority>();
            List<Models.CCBCategoryChange> lstCCBCategoryChange = new List<Models.CCBCategoryChange>();
            List<Models.PXTs> PXTList = new List<Models.PXTs>();
            List<Models.CCBOtherProgramAffected> OPA = new List<Models.CCBOtherProgramAffected>();
            List<Models.SnapshotIndex> program = new List<Models.SnapshotIndex>();
            getDBdata(WWID);
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBStatusList = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
                program = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                lstPriority = dc.Database.SqlQuery<Models.CCBPriority>("SPROCgetPriorityCCB").ToList<Models.CCBPriority>();
                lstCCBCategoryChange = dc.Database.SqlQuery<Models.CCBCategoryChange>("SPROCgetChangeCategory").ToList<Models.CCBCategoryChange>();
                PXTList = dc.Database.SqlQuery<Models.PXTs>("[SPROCcatPXT]").ToList<Models.PXTs>();
                OPA = dc.Database.SqlQuery<Models.CCBOtherProgramAffected>("[SPRCgetCCBOtherProgram]").ToList<Models.CCBOtherProgramAffected>();
            }

            myModel.Priorities = lstPriority;
            myModel.PorjList = getCCBProj(WWID);
            myModel.ContID = ContID;
            myModel.CCBid = ccbid != null ? ccbid : 0;
            myModel.StatusList = CCBStatusList.ToList();
            myModel.Project = proj;
            myModel.WWID = WWID;
            myModel.WWName = WWName.Replace("_","'");
            myModel.PXT = PXTList;
            myModel.OPA = OPA;
            myModel.Programs = program;

            Models.CCB ccb = new Models.CCB();
            ccb.dttActualImplementedDate = DateTime.Now;
            ccb.dttApprovedDate = DateTime.Now;
            ccb.dttCutInDate = DateTime.Now;
            ccb.dttSubmitDate = DateTime.Now;
            ccb.dttTargetImplementedDate = DateTime.Now;
            myModel.CCB = ccb;
            myModel.lstCategoryChange = lstCCBCategoryChange;

            myModel.TeamListSel = new List<Models.CCBsFunctionalTeam>();
            myModel.ProjListSel = new List<Models.CCBSProj>();

            if (ccbid != null && ccbid != 0)
            {
                Models.CCB Occb = getCCB(ccbid);
                Occb.OwnerId =Occb.ContinuityID=="NON-SNAPSHOT"? 0 : program.Where(x => x.ContinuityID == Occb.ContinuityID).First().OwnerId;
                Occb.PXTId = Occb.ContinuityID == "NON-SNAPSHOT" ? 0 : program.Where(x => x.ContinuityID == Occb.ContinuityID).First().PXTId;
                Occb.ProgTypeID = Occb.ContinuityID == "NON-SNAPSHOT" ? 0 : program.Where(x => x.ContinuityID == Occb.ContinuityID).First().ProgTypeID;
                myModel.CCB = Occb;
                myModel.TeamListSel = getFunctionalTeam(ccbid);
                myModel.ProjListSel = getCCBProjSel(WWID, ccbid);
                myModel.OPASel = getOtherProgramAffected(ccbid);
            }
            else
                myModel.OPASel = null;

            myModel.TeamList = getFunctionalTeam();
            ViewBag.Complete = false;
            return View(myModel);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateComments(string ContinuityID, string Comments,int WWID,string WWName)
        {
            SqlParameter[] param = new SqlParameter[3];

            #region Parameters
            param[0] = new SqlParameter("@txtComments",Comments);
            param[1] = new SqlParameter("@ContinuityID", ContinuityID);
            param[2] = new SqlParameter("@WWID", WWID);
            
            #endregion

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCupdateCommentsCCB", param);

            return RedirectToAction("Index", "CCB", new { WWID=WWID, weekName=WWName });
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult SaveCCB(FormCollection collection, bool Deleted=false)
        {
                string usr = User.Identity.Name;
                string strCCBID="";
                SqlParameter[] param = new SqlParameter[61];
                SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
                conn.Open();
                SqlTransaction sqlTran=conn.BeginTransaction();

                Models.CCB ccb= new Models.CCB();

                #region Parameters
                param[0] = new SqlParameter("@IDCCB", collection["idCCB"]);
                param[1] = new SqlParameter("@vchTitle", collection["txtTitle"]);
                param[2] = new SqlParameter("@vchInitiator", collection["txtInitiator"]);
                param[3] = new SqlParameter("@intPriority", collection["cmbPriority"]);
                param[4] = new SqlParameter("@intCCBStatus", collection["CCBStatus"]);
                param[5] = new SqlParameter("@dttSubmitDate", collection["pSubmitDate"]!=null && collection["pSubmitDate"].Length>0? collection["pSubmitDate"] : null  );
                param[6] = new SqlParameter("@dttAprovedDateDue", collection["pAproveDate"]!=null && collection["pAproveDate"].Length>0 ? collection["pAproveDate"] : null);
                param[7] = new SqlParameter("@dttTargetImplementedDat", collection["pTargetDate"] !=null && collection["pTargetDate"].Length>0? collection["pTargetDate"] : null );
                param[8] = new SqlParameter("@dttActualImplementedDate", collection["pImplementDate"] !=null && collection["pImplementDate"].Length>0? collection["pImplementDate"] : null);
                param[9] = new SqlParameter("@vchChangeDes", collection["txtChangeDesc"]);
                param[10] = new SqlParameter("@vchCangejustified", collection["txtChangeJustify"]);
                param[11] = new SqlParameter("@vchChangeJustLink", collection["txtChangeJustifyLink"]);
                param[12] = new SqlParameter("@bModSpec", 1);
                param[13] = new SqlParameter("@bImpactedFunction", Convert.ToInt32(collection["ImpacFunc"]));
                param[14] = new SqlParameter("@vchSWRequired", collection["SWReq"]); 
                param[15] = new SqlParameter("@vchSWNumber", collection["txtCCBSW"]); 
                param[16] = new SqlParameter("@vchImpactHC", collection["txtProgramHC"]);
                param[17] = new SqlParameter("@vchImpactBTI", collection["txtProgramBTI"]);
                param[18] = new SqlParameter("@vchROIAnalysis", collection["ROI"]);
                param[19] = new SqlParameter("@vchROIAnalysisLink", collection["txtROIAnalysisLink"]);
                param[20] = new SqlParameter("@vchImpactSchedule", collection["txtAProgramSchedule"]);
                param[21] = new SqlParameter("@bSOWReq", 1);
                param[22] = new SqlParameter("@vchSOWReqLink", collection["txtSOWReqLink"]);
                param[23] = new SqlParameter("@vchARsComments", collection["txtARsComments"]);
                param[24] = new SqlParameter("@bDWI", Convert.ToInt32(collection["DWI"]));
                param[25] = new SqlParameter("@vchDWIInvolved", collection["txtWINInvolved"]);
                param[26] = new SqlParameter("@BDWA", Convert.ToInt32(collection["DWA"]));
                param[27] = new SqlParameter("@vchDWAAchieved", collection["txtWINAchieved"]);
                param[28] = new SqlParameter("@vchProjectPhase", collection["PNPI"]);
                param[29] = new SqlParameter("@vchActualBTI", collection["txtCCBActualBTI"]);
                param[30] = new SqlParameter("@active", 1);
                param[31] = new SqlParameter("@createUser", usr);
                param[32] = new SqlParameter("@ContinuityID", collection["ContinuityID"]);
                param[33] = new SqlParameter("@CommonApproved", collection["txtCommondApproved"]);
                param[34] = new SqlParameter("@EmailInitiator", collection["txtInitiatorEmail"]);
                param[35] = new SqlParameter("@CommonEmailApproved", collection["txtEmailCommondApproved"]);
                param[36] = new SqlParameter("@OtherProgram", collection["txtOP"]);
                param[37] = new SqlParameter("@bImpactPS", false);
                param[38] = new SqlParameter("@vchImpactPSLink", "");
                param[39] = new SqlParameter("@bImpactCOEM", Convert.ToInt32(collection["txtbProgramCOEM"]));
                param[40] = new SqlParameter("@vchImpactCOEMLink", collection["txtProgramCOEMLink"]);
                param[41] = new SqlParameter("@bImpactMarketing", Convert.ToInt32(collection["txtbImpactMarketing"]));
                param[42] = new SqlParameter("@vchImpactMarketingLink", collection["txtImpactMarketingLink"]);
                param[43] = new SqlParameter("@bImpactEngine", Convert.ToInt32(collection["txtbImpactEngine"]));
                param[44] = new SqlParameter("@vchImpactEngineLink", collection["txtImpactEngineLink"]);
                param[45] = new SqlParameter("@bImpactDesignSecurity", Convert.ToInt32(collection["txtbImpactDesignSecurity"]));
                param[46] = new SqlParameter("@vchImpactDesignSecurityLink", collection["txtImpactDesignSecurityLink"]);
                param[47] = new SqlParameter("@bImpactOther", Convert.ToInt32(collection["txtbImpactOther"]));
                param[48] = new SqlParameter("@vchImpactOtherLink", collection["txtImpactOtherLink"]);
                param[49] = new SqlParameter("@intCategoryChanges", collection["cmbCategoryChange"]);
                param[50] = new SqlParameter("@bIsHW",collection["thw"]!=null? Convert.ToBoolean(collection["thw"]) : false);
                param[51] = new SqlParameter("@bIsSW",collection["thsw"]!=null? Convert.ToBoolean(collection["thsw"]) :false);
                param[52] = new SqlParameter("@vchSWCategory", collection["pSWCategory"]);
                param[53] = new SqlParameter("@vchReleaseDate", collection["pReleaseDate"]);
                param[54] = new SqlParameter("@vchSupportedBMC", collection["pSupportBMC"]);
                param[55] = new SqlParameter("@vchSupportedBIOS", collection["pSupportBIOS"]);
                param[56] = new SqlParameter("@vchSupportedUtilities", collection["pSupportUtil"]);
                param[57] = new SqlParameter("@vchImpactedFunctionLink", collection["pImpactedFuntionLink"]);
                param[58] = new SqlParameter("@vchImpactHCLink", collection["pImpactedHCLink"]);
                param[59] = new SqlParameter("@vchImpactBTLink", collection["pImpactedBTILink"]);
                param[60] = new SqlParameter("@bDelete",Deleted );
                #endregion

                try
                {

                    if (!Deleted)
                    {
                        if (collection["idCCB"] != null && collection["idCCB"].ToString().Length > 0 && Convert.ToInt32(collection["idCCB"].ToString()) > 0 )
                        {
                            bPrepareRecCheck(Convert.ToInt32(collection["idCCB"].ToString()), sqlTran);

                        }
                    }

                    DataSet dsRes = SqlHelper.ExecuteDataset(sqlTran, "SPROCmanCCB", param);

                    #region Send Create New CCB Notif

                    if (dsRes != null && dsRes.Tables.Count > 0 && dsRes.Tables[0].Rows.Count > 0 && dsRes.Tables[0].Columns.Count > 1)
                    {
                        NotifController notif = new NotifController();
                        List<Models.NotifAssignees> lstOwner = new List<Models.NotifAssignees>();
                        Models.NotifAssignees owner = new Models.NotifAssignees();

                        string strSummary=dsRes.Tables[0].Rows[0]["Msg"].ToString();
                        string strWWName=dsRes.Tables[0].Rows[0]["WWName"].ToString();
                        string strTitle = dsRes.Tables[0].Rows[0]["NotifTitle"].ToString();
                        int WWID=Convert.ToInt32(dsRes.Tables[0].Rows[0]["WWID"].ToString());

                        owner.Email = dsRes.Tables[0].Rows[0]["Assignee"].ToString();
                        lstOwner.Add(owner);

                        notif.SendEmail(WWID, strWWName, strSummary, lstOwner, strTitle);
                    }

                    #endregion

                    if (!Deleted)
                    {
                        List<int> lfunction = new List<int>();
                        List<string> lproject = new List<string>();
                        List<string> lfile = new List<string>();
                        List<int> lOPA = new List<int>();
                        
                        strCCBID = dsRes.Tables[0].Rows[0][0].ToString();
                        //Reg all function
                        foreach (string col in collection.AllKeys)
                        {
                            if (col.StartsWith("Team"))
                                lfunction.Add(Convert.ToInt32(collection[col]));

                        }
                        intRegRelCCBFunct(Convert.ToInt32(strCCBID), lfunction, sqlTran);

                        //Reg All projects
                        foreach (string col in collection.AllKeys)
                        {
                            if (col.StartsWith("Proj"))
                                lproject.Add(collection[col]);

                        }
                        intRegCCBProgram(Convert.ToInt32(strCCBID), lproject, sqlTran);

                        //Other Programs Affected
                        foreach (string col in collection.AllKeys)
                        {
                            if (col.StartsWith("OPA"))
                                lOPA.Add(Convert.ToInt32(collection[col]));

                        }
                        intRegCCBOtherProgram(Convert.ToInt32(strCCBID), lOPA, sqlTran);

                        //Region Files
                        foreach (string col in collection.AllKeys)
                        {
                            if (col.StartsWith("file"))
                                lfile.Add(collection[col]);
                        }

                        if (lfile.Count() > 0)
                        {
                            foreach (string file in lfile)
                            {
                                string[] strFile = file.Split(',');

                                SqlParameter[] paramFile = new SqlParameter[3];

                                paramFile[0] = new SqlParameter("@IDCCB", strCCBID);
                                paramFile[1] = new SqlParameter("@vchName",strFile[0]);
                                paramFile[2] = new SqlParameter("@vchURL", strFile[1]);

                                SqlHelper.ExecuteDataset(sqlTran, "SPROCmanLinkCCB", paramFile);
                            }
                        }
                    }

                    sqlTran.Commit();
                    
                }
                catch(Exception ex)
                {
                    sqlTran.Rollback();
                }

                sqlTran.Dispose();
                conn.Close();
                conn.Dispose();

                Session["VBWW"] = Session["VBWW"];
                Session["VBWWID"] = Session["VBWWID"];
                ViewBag.WW = Session["VBWW"];
                ViewBag.WWID = Session["VBWWID"];

                if (collection["idCCB"] == null || Convert.ToInt32(collection["idCCB"].ToString()) == 0 || Deleted)
                {
                    return RedirectToAction("Index", "CCB"); 
                }
                else
                {
                    return RedirectToAction("/RegisterCCB", new { WWID = collection["txtWWID"], WWName = collection["txtWWName"], ccbid = collection["idCCB"], ContID = collection["ContinuityID"], proj = collection["txtProj"] });
                }
        }
        private bool getDBdata(int? WWID)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            var list = new List<Models.SnapshotIndex>();
            var catPLC = new List<Models.PLCList>();
            var userList = new List<Models.Users>();
            IEnumerable<SelectListItem> PlCList;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                catPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                if (list.Count > 0)
                {
                    this.weekStatus = int.Parse(list[0].weekStatus.ToString());
                }
                PlCList = catPLC.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
            }
            if (list.Count() > 0)
            {
                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
                Session["VBWW"] = ViewBag.WW;
                Session["VBWWID"] = ViewBag.WWID;
                myModel.WWID = list[0].WWID;
                myModel.list = list;
                myModel.PlCList = PlCList;
                myModel.validUsers = userList;
                return true;
            }
            else
            { return false; }
        }
        private int intRegCCBProgram(int intCCBID,List<string> lstProgram,SqlTransaction tran)
        {
            int intRes = 0;
            foreach (string program in lstProgram)
            {
                SqlParameter[] param = new SqlParameter[3];

                Models.CCB ccb = new Models.CCB();

                param[0] = new SqlParameter("@idCCB", intCCBID);
                param[1] = new SqlParameter("@vchContinuityID", program);
                param[2] = new SqlParameter("@bDelete", false);
                DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanCCBProj", param);
                intRes = dsRes.Tables[0].Rows.Count;

            }
            return intRes;
        }
        private int intRegCCBOtherProgram(int intCCBID, List<int> lstProgram, SqlTransaction tran)
        {
            int intRes = 0;
            foreach (int program in lstProgram)
            {
                SqlParameter[] param = new SqlParameter[2];

                Models.CCB ccb = new Models.CCB();

                param[0] = new SqlParameter("@CCBID", intCCBID);
                param[1] = new SqlParameter("@OPID", program);
                DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanCCBOtherProgRel", param);
                intRes = dsRes.Tables[0].Rows.Count;

            }
            return intRes;
        }
        private int intRegRelCCBFunct(int intCCBID,List<int> lstFunction,SqlTransaction tran)
        {
            int intRes=0;
            foreach (int funct in lstFunction)
            {
                SqlParameter[] param = new SqlParameter[2];

                Models.CCB ccb = new Models.CCB();

                param[0] = new SqlParameter("@idCCB", intCCBID);
                param[1] = new SqlParameter("@idFunctional",funct);
                DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanCCBFuncRel", param);
                intRes=dsRes.Tables[0].Rows.Count;

            }
            return intRes;
        }
        private bool bPrepareRecCheck(int intCCBID,SqlTransaction tran)
        {
            int intRes = 1;
            SqlParameter[] param = new SqlParameter[1];

            Models.CCB ccb = new Models.CCB();

            param[0] = new SqlParameter("@idCCB", intCCBID);
            DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCremRegCCB", param);

            if (dsRes != null && dsRes.Tables.Count > 0 && dsRes.Tables[0].Rows.Count > 0)
                intRes =Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());
            
            return intRes==0;
        }
        public Models.CCB getCCB(int? CCBid)
        {
            List<Models.CCB> lstCCB = new List<Models.CCB>();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstCCB=dc.Database.SqlQuery<Models.CCB>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3} ", new object[] { 0, 0, CCBid, "" }).ToList<Models.CCB>();
                lstCCB[0].lsFile = new List<Models.CCBFiles>();
                lstCCB[0].lsFile=dc.Database.SqlQuery<Models.CCBFiles>("SPROCgetFilesCCB @intCCBID={0}", new object[]{CCBid}).ToList<Models.CCBFiles>();
            }
            return lstCCB[0];
        }
        public List<Models.CCBsFunctionalTeam> getFunctionalTeam()
        {
            var CCBListTeam = new List<Models.CCBsFunctionalTeam>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListTeam = dc.Database.SqlQuery<Models.CCBsFunctionalTeam>("SPROCgetFunctionalTeam").ToList<Models.CCBsFunctionalTeam>();
            }
            return CCBListTeam.ToList();
            
        }
        public List<Models.CCBOtherProgramAffected> getOtherProgramAffected(int? CCBID)
        {
            var CCBListOPA = new List<Models.CCBOtherProgramAffected>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListOPA = dc.Database.SqlQuery<Models.CCBOtherProgramAffected>("SPROCgetCCBOtherProgramRel @CCBID={0}", new object[] { CCBID }).ToList<Models.CCBOtherProgramAffected>();
            }

            return CCBListOPA;
        }
        public List<Models.CCBsFunctionalTeam> getFunctionalTeam(int? CCBID)
        {
            var CCBListTeam = new List<Models.CCBsFunctionalTeam>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListTeam = dc.Database.SqlQuery<Models.CCBsFunctionalTeam>("SPROCgetFuncRel @CCBID={0}", new object[]{CCBID}).ToList<Models.CCBsFunctionalTeam>();
            }
            return CCBListTeam.ToList();

        }
        public List<Models.CCBSProj> getCCBProj(int? WWID)
        {
            var CCBListProj = new List<Models.CCBSProj>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListProj = dc.Database.SqlQuery<Models.CCBSProj>("SPROCgetProyCCBReg @WW={0},@CCBid={1}", new object[] { WWID, 0 }).ToList<Models.CCBSProj>();
            }
            return CCBListProj.ToList();
        }
        public List<Models.CCBAffected> getCCBAffectedProj()
        {
            var CCBListProj = new List<Models.CCBAffected>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListProj = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB").ToList<Models.CCBAffected>();
            }
            return CCBListProj;
        }
        public List<Models.CCBSProj> getCCBProjSel(int? WWID, int? CCBID)
        {
            var CCBListProj = new List<Models.CCBSProj>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListProj = dc.Database.SqlQuery<Models.CCBSProj>("SPROCgetProyCCBReg @WW={0},@CCBid={1}", new object[] { WWID, CCBID }).ToList<Models.CCBSProj>();
            }
            return CCBListProj.ToList();
        }
        #endregion
        
        public List<Models.CatCCBStatus> getCatCCBStatus()
        {
            List<Models.CatCCBStatus> CCBStatusList = new List<Models.CatCCBStatus>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBStatusList = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
            }
            return CCBStatusList;

        }
        public List<Models.CCB> getCCBs(int WWID)
        {
            var CCBlist = new List<Models.CCB>();
            var CCBListNon = new List<Models.CCB>();
            var CCBStatusList = new List<Models.CCBStatus>();
            List<Models.CCB> resCCB = new List<Models.CCB>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CCBListNon = dc.Database.SqlQuery<Models.CCB>("SPROCgetNonSnap").ToList<Models.CCB>();
                CCBlist = dc.Database.SqlQuery<Models.CCB>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3} ", new object[] { WWID, 0, 0, "" }).ToList<Models.CCB>();
                

                foreach(Models.CCB ccb in CCBlist)
                {
                    CCBListNon.Add(ccb);
                }

                CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();
            }
            if (CCBListNon.Count > 0)
            {
                DataTable dtData = new DataTable();

                foreach(Models.CCB CCB in CCBListNon)
                {
                    var res = CCBStatusList.Where(row => row.ContinuityID == CCB.ContinuityID);
                    if (res.Count() > 0)
                    {
                        CCB.lStatus = res.ToList();
                    }else
                    {
                        CCB.lStatus = new List<Models.CCBStatus>();

                    }
                    CCB.WWID = WWID;
                    resCCB.Add(CCB);
                }

                ViewBag.WWID = WWID;
                
            }

            return resCCB;


        }
    }
}
