﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using HtmlAgilityPack;
using System.Data;

namespace AutoIndicators.Controllers
{
    public class FreeHTMLController : Controller
    {
        dynamic myModel = new ExpandoObject();
        //
        // GET: /FreeHTML/
        public ActionResult Index(int? CfgFreeHTML, string Module)
        {
            var result = new List<Models.CfgFreeHTML>();
            List<Models.DefaultWW> results = new List<Models.DefaultWW>();

            var userList = new List<Models.Users>();
            int? WWID = null;


            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());


            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {

                if (WWID == null || dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>().Count==0)
                {
                    results = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                {
                    results = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();
                }
            }

            
            DEOSecurity.Model.mNodes menu = new DEOSecurity.Model.mNodes();

            ViewBag.WW = results[0].weekName;
            ViewBag.WWID =results[0].WWID;
            WWID = results[0].WWID;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.CfgFreeHTML>(@"SPROCgetFreeHTMLComponent @vchView={0}, @vchProperty={1}, @vchValue={2}, @WWID={3}", new object[] { "", "Mod", Module,WWID }).ToList<Models.CfgFreeHTML>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                foreach (Models.CfgFreeHTML view in result)
                {
                    view.AllowedUser = dc.Database.SqlQuery<string>("[SPROCgetUsersFreeHTML] @FreeHTMLID = {0}", view.id).ToList<string>();
                }
            }

            if (Module.Length > 0)
            {
                DEOSecurity.Controller.DEOController Security = new DEOSecurity.Controller.DEOController();
                Security.Modules = Security.GetMenu();
                menu = Security.Modules.Where(x => x.strTagID == "a" + Module).Single();
            }

            bool bEdit=false;

            if (result.First().AllowedUser.Count > 0)
            {
               bEdit = result.First().AllowedUser.Any(x => x == AutoIndicators.GeneralClasses.General.FindEmailByAccount(User.Identity.Name));
            }
            ViewBag.Edit = bEdit;
            ViewBag.validUsers = userList;
            ViewBag.Mod = menu;
            ViewBag.Module = Module;
            ViewBag.Complete = false;

            return View("~/Views/FreeHTML/FreeHTML.cshtml", result.Count>0? result.First() : new Models.CfgFreeHTML());
        }
        [ValidateInput(false)]
        public ActionResult Insert(FormCollection collection)
        {
            string usr = User.Identity.Name;
            var parames = new object[] { collection["pFreeHTML"], usr };
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand(@"SPROCCreateNewFreeHTML @htmlString= {0}, @user = {1}", parames);
            }
            return RedirectToAction("Index", "FreeHTML");
            //return RedirectToAction(); //, new { pWWID = result[0].WWID, weekName = result[0].weekName }
        }
        public ActionResult TestView()
        {
            string Module = "TestFreeHTML";
            var result = new List<Models.CfgFreeHTML>();
            var userList = new List<Models.Users>();
            var resultWW = new List<Models.DefaultWW>();
            Models.Calendar cals = new Models.Calendar();
            List<Models.Calendar> calendar = GeneralClasses.ViewDataFilter.Calendar();
            DEOSecurity.Model.mNodes menu = new DEOSecurity.Model.mNodes();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {

                resultWW = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>();
                ViewBag.WWID = resultWW[0].WWID;
                cals = calendar.Where(x => x.id == ViewBag.WWID).Single();
               
               

                result = dc.Database.SqlQuery<Models.CfgFreeHTML>(@"SPROCgetFreeHTMLComponent @vchView={0}, @vchProperty={1}, @vchValue={2}, @WWID={3}", new object[] { "", "Mod", Module, ViewBag.WWID }).ToList<Models.CfgFreeHTML>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }

            //if (Module.Length > 0)
            //{
            //    DEOSecurity.Controller.DEOController Security = new DEOSecurity.Controller.DEOController();
            //    Security.Modules = Security.GetMenu();
            //    menu = Security.Modules.Where(x => x.strTagID == "a" + Module).Single();
            //}

            //ViewBag.validUsers = userList;
            //ViewBag.Mod = menu;
            ViewBag.Module = Module;

            return PartialView(result.Count > 0 ? result.First() : new Models.CfgFreeHTML());
        }
        public ActionResult CreateView(int WWID, string WName)
        {
            ViewBag.WWID = WWID;
            ViewBag.WW = WName;
            return PartialView();
        }
        public ActionResult SaveMenu(FormCollection collection)
        {
            string filePath = "";

            if (Request.Files.Count > 0)
            {
                string usr = User.Identity.Name;
                var file = Request.Files[0];
                string[] nameBreak = file.FileName.Split(char.Parse("."));
                string extension = nameBreak.Last();

                if (extension.Length > 0)
                {
                    if (!extension.ToUpper().Contains("JPG") && !extension.ToUpper().Contains("PNG"))
                        return RedirectToAction("");

                    filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Images\\menu", file.FileName);

                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);

                    file.SaveAs(filePath);

                    filePath = "~/Images/menu/" + file.FileName;
                }
            }
            
            DEOSecurity.Controller.DEOController Security = new DEOSecurity.Controller.DEOController();
            Security.intApp = GeneralClasses.General.intApp;
            Security.strConnection = GeneralClasses.General.strConnectionSecurityString;
            Security.MenuItem = new DEOSecurity.Model.mNodes();

            Security.MenuItem.strName = "FreeHTML";
            Security.MenuItem.strTarget = "Index";
            Security.MenuItem.strIcon = filePath;
            Security.MenuItem.strTagID = "a" + collection["txtName"].Replace(" ", "_");
            Security.MenuItem.strTitle = collection["txtDesc"];
            Security.MenuItem.strVersion = collection["txtVersion"];

            Security.MenuItem.PKNode = Security.intAddMenu();

            if (Security.MenuItem.PKNode > 0)
            {
                Security.MenuItem.Parameter = new DEOSecurity.Model.mParameter();
                Security.MenuItem.Parameter.strParameter = "Module";
                Security.MenuItem.Parameter.strParameterValue = collection["txtName"].Replace(" ", "_");
                Security.intAddParameter();
            }
            System.Web.Routing.RouteValueDictionary rPar = new System.Web.Routing.RouteValueDictionary();

            rPar.Add("Module", collection["txtName"].Replace(" ", "_"));
            rPar.Add("WWID", collection["WWID"]);
            rPar.Add("WName", collection["WW"]);

            return RedirectToAction("Index", "FreeHTML", rPar);
        }
        public ActionResult FreeHTMLRegView(string Property, string Value, string View,int WWID)
        {
             
             List<Models.CfgFreeHTML> result = new List<Models.CfgFreeHTML>();

             ViewBag.Property = Property;
             ViewBag.ValueProperty = Value;
             ViewBag.View = View;
             ViewBag.WWID = WWID;
             List<Models.Users> userList = new List<Models.Users>();
            using(Models.DevSQLUserEntities dc=new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.CfgFreeHTML>(@"SPROCgetFreeHTMLComponent @vchView={0}, @vchProperty={1}, @vchValue={2}, @WWID={3}", new object[] { View.Contains('/')? View.Substring(0,View.IndexOf('/')) : View, Property, Value,WWID }).ToList<Models.CfgFreeHTML>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }
            ViewBag.validUsers = userList;

            DEOSecurity.Controller.DEOController control = new DEOSecurity.Controller.DEOController();
            List<DEOSecurity.Model.mNodes> lstMenu = control.GetMenu();
            myModel.Content = result.Count > 0 ? result.First() : new Models.CfgFreeHTML();

            if (View.Length > 0 && !Property.StartsWith("th") && Value!="TestFreeHTML")
            {
                if (lstMenu.Where(x => x.strTagID == "a" + Value).ToList<DEOSecurity.Model.mNodes>().Count > 0)
                {
                    myModel.Details = lstMenu.Where(x => x.strTagID == "a" + Value).ToList<DEOSecurity.Model.mNodes>().First();
                }
            }

            if (result.Count == 0)
                result.Add(new Models.CfgFreeHTML());

             return PartialView(myModel);

        }

        [ValidateInput(false)]
        public ActionResult SaveCfg(FormCollection collection)
        {
            bool bOk = false;
            string strValue=collection["txtValue"];
            string filePath = "";

            DEOSecurity.Controller.DEOController Security = new DEOSecurity.Controller.DEOController();
            Security.intApp = GeneralClasses.General.intApp;
            Security.strConnection = GeneralClasses.General.strConnectionSecurityString;
            Security.MenuItem = new DEOSecurity.Model.mNodes();

            if (Request.Files.Count > 0)
            {
                string usr = User.Identity.Name;
                var file = Request.Files[0];
                string[] nameBreak = file.FileName.Split(char.Parse("."));
                string extension = nameBreak.Last();

                if (extension.Length > 0)
                {
                    if (!extension.ToUpper().Contains("JPG") && !extension.ToUpper().Contains("PNG"))
                        return RedirectToAction("");

                    filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Images\\menu", file.FileName);

                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);

                    file.SaveAs(filePath);

                    filePath = "~/Images/menu/" + file.FileName;
                    Security.MenuItem.strIcon = filePath;
                }
            }

            if (!collection["txtProperty"].StartsWith("th") && collection["txtValue"] != "TestFreeHTML")
            {
                Security.MenuItem.strVersion = collection["txtVersion"];

                Security.MenuItem.PKNode = Convert.ToInt32(collection["txtMenuID"]);
                Security.bEditMenu();
            }
            if(collection["WW"].Equals("1"))
            {
                strValue=strValue.Replace("jnjhjj",collection["txtReplace"]);
            }
            SqlParameter[] param = new SqlParameter[9];

            param[0] = new SqlParameter("@id", Convert.ToInt32(collection["txtId"]));
            param[1] = new SqlParameter("@vchProperty", collection["txtProperty"]);
            param[2] = new SqlParameter("@vchValue", strValue);
            param[3] = new SqlParameter("@vchView", collection["txtView"].Trim().Length>0? collection["txtView"] :null);
            param[4] = new SqlParameter("@idFreeHTML", collection["txtIdFreeHTML"]);
            param[5] = new SqlParameter("@vchFreeHTML", collection["txtFreeHTML"]);
            param[6] = new SqlParameter("@WWID", collection["txtWWID"].Length>0? Convert.ToInt32(collection["txtWWID"]) :0);
            param[7] = new SqlParameter("@vchUser", User.Identity.Name);
            param[8] = new SqlParameter("@bDelete", false);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanCfgFreeHTML", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                bOk = true;
            
            string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            return Redirect(url);
            
        }
          [ValidateInput(false)]
        public ActionResult ValidHTML(string strCode)
        {
            HtmlDocument HTMLDoc2 = new HtmlDocument();
            string strError="";
            HTMLDoc2.LoadHtml(strCode);

            if (HTMLDoc2.ParseErrors.Count() > 0){
                foreach(HtmlParseError error in HTMLDoc2.ParseErrors )
                {
                    strError+="Error - "+error.Reason+"\\n";
                }
                return JavaScript("alert('You have some errors in the code:\\n"+strError+"'); errorc=true;");
            }
            else
                return JavaScript("errorc=false");
        }
        public ActionResult LoadFreeHTML(int? WWID)
        {
            try
            {
                List<Models.Users> userList = new List<Models.Users>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                }
                ViewBag.validUsers = userList;
                return View();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ActionResult DeleteView(int intMenuID,int intViewID,int intFreeHtmlID)
        {
            DEOSecurity.Controller.DEOController Security = new DEOSecurity.Controller.DEOController();
            Security.intApp = GeneralClasses.General.intApp;
            Security.strConnection = GeneralClasses.General.strConnectionSecurityString;
            Security.MenuItem = new DEOSecurity.Model.mNodes();

            Security.MenuItem.PKNode = intMenuID;
            Security.MenuItem.bDelete = true;
            Security.bEditMenu();

            SqlParameter[] param = new SqlParameter[9];

            param[0] = new SqlParameter("@id", intViewID);
            param[1] = new SqlParameter("@vchProperty",null);
            param[2] = new SqlParameter("@vchValue", null);
            param[3] = new SqlParameter("@vchView", null);
            param[4] = new SqlParameter("@idFreeHTML", intFreeHtmlID);
            param[5] = new SqlParameter("@vchFreeHTML", null);
            param[6] = new SqlParameter("@WWID",null);
            param[7] = new SqlParameter("@vchUser", User.Identity.Name);
            param[8] = new SqlParameter("@bDelete", true);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanCfgFreeHTML", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                return JavaScript("window.location.href='/'");
            else
                return JavaScript("alert('Error'");

        }
        public ActionResult Security()
        {
            List<Models.Users> userCoreList = new List<Models.Users>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                userCoreList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }
            ViewBag.validUsers = userCoreList;
            myModel.Free = GetViews();
            return View("SecurityView",myModel);
        }
        public static List<Models.CfgFreeHTML> GetViews()
        {
            List<Models.CfgFreeHTML> FreeList = new List<Models.CfgFreeHTML>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                FreeList = dc.Database.SqlQuery<Models.CfgFreeHTML>("[SPROCgetFreeHTML]").ToList<Models.CfgFreeHTML>();

                foreach (Models.CfgFreeHTML fhtml in FreeList)
                {
                    fhtml.AllowedUser = dc.Database.SqlQuery<string>("[SPROCgetUsersFreeHTML] @FreeHTMLID = {0}", fhtml.id).ToList<string>();
                }
            }
            return FreeList;
        }
        public ActionResult Users(int id)
        {
            Models.CfgFreeHTML FreeList = new Models.CfgFreeHTML();
            ViewBag.FHV = id;
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
             
               FreeList.AllowedUser = dc.Database.SqlQuery<string>("[SPROCgetUsersFreeHTML] @FreeHTMLID = {0}", id).ToList<string>();
             
            }
            myModel.Free = FreeList;
            return View("SecurityUsersView", myModel);
        }
        public ActionResult SaveUser(int id, string User, bool Delete)
        {
            Models.CfgFreeHTML FreeList = new Models.CfgFreeHTML();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                FreeList.AllowedUser = dc.Database.SqlQuery<string>("[SPROCmanFreeHtmlUser] @FreeHTMLId = {0}, @vchUser={1}, @bDelete={2} ", new object[] { id, User, Delete }).ToList<string>();
            }
            return JavaScript("$(\"#gridDataFreeUsers\").pqgrid(\"addRow\", { rowData: { Name:'"+GeneralClasses.General.FindDisplayNametByEmail(User)+"', Email:'"+User+"'}");
            
        }
    }
}
