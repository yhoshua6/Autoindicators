﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using HtmlAgilityPack;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AutoIndicators.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Search(FormCollection collection,string Module,string Param)
        {
            string strFil = "";
            string[] strField;
            List<string> lstValSel = new List<string>();
            string strPrevField = "";
            string strValsel = "";
            int intWW = 0;
            bool bCompleted = false;

            foreach (string col in collection.AllKeys)
            {
                if ((!col.Equals("Field") && !col.Equals("removed") && !col.Contains("All")) && collection[col].Length > 0)
                {
                    if (col.StartsWith("txt"))
                    {
                        strFil += " AND "+collection["Field"].ToString() + " like '" + collection[col].ToString() + "%'";
                    }
                    else if(col=="cmbfilWW")
                    {
                        var result = new List<Models.DefaultWW>();
                        using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                        {
                            result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                        }

                        if (collection[col].ToString().Equals("Co"))
                        {
                            intWW = Convert.ToInt32(result[0].WWID-1);
                            Session.Remove("Current");
                            bCompleted = true;
                        }
                        else if (collection[col].ToString().Equals("Cu"))
                        {
                            intWW = Convert.ToInt32(result[0].WWID);
                            Session["Current"] = 1;
                        }
                        else
                        {
                            intWW = Convert.ToInt32(collection[col].ToString());
                            Session.Remove("Current");
                        }
                    }
                    else
                    {
                        
                        lstValSel.Add(collection[col]);
                        strField = collection[col].Split(',');
                        if (strPrevField != strField[0])
                            strFil += ") AND (" + strField[0] + "='" + strField[1] + "'"; //and +=
                        else
                            strFil += " OR " + strField[0] + "='" + strField[1] + "'"; //or +=
                        strPrevField = strField[0];

                        strValsel += strField[0] + "," + strField[1]+";";
                    }
                }
                else
                {
                    if(col.Equals("removed"))
                    {
                        string xtreVal = collection[col];
                        if (xtreVal.Length > 0)
                        {
                            xtreVal = xtreVal.Substring(1);
                            string[] newXtraVal = xtreVal.Split(';');

                            lstValSel.AddRange(newXtraVal);
                            strValsel += xtreVal[0] + "," + xtreVal[1]+";";
                        }
                    }
                }

            }
            

            if (strFil.Length > 0) //if existing filter set the seesion
            {
                strFil = strFil.Substring(6).Trim() + ")"; ;

                if (!Request.Cookies["Module"].Value.Contains("Lab"))
                {
                    Response.Cookies["FilString"].Value = strFil;
                    Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(7300);
                    Session["ValSel"] = lstValSel;
                    Response.Cookies["ValSel"].Value = strValsel;
                    Response.Cookies["ValSel"].Expires = DateTime.Now.AddDays(7300);
                }else
                {
                    Response.Cookies["FilStringLab"].Value = strFil;
                    Response.Cookies["FilStringLab"].Expires = DateTime.Now.AddDays(7300);
                    Session["ValSelLab"] = lstValSel;
                    Response.Cookies["ValSelLab"].Value = strValsel;
                    Response.Cookies["ValSelLab"].Expires = DateTime.Now.AddDays(7300);
                }
                Session["DataFil"] = Session["DataFil"];
                Session["DataSource"] = Session["DataSource"];
                Session["CancelActivate"] = 1;
                
            }
            else // if filter is empty then the filter is init
            {
                if (!Request.Cookies["Module"].Value.Contains("Lab"))
                {
                    Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
                    Session.Remove("ValSel");
                }else
                {
                    Response.Cookies["FilStringLab"].Expires = DateTime.Now.AddDays(-1);
                    Session.Remove("ValSelLab");
                }
                Session.Remove("DataFil");
                Session.Remove("DataSource");
                Session.Remove("CancelActivate");
                
            }

            Session["FilWW"] = intWW.ToString();
            
            System.Web.Routing.RouteValueDictionary rPar = new System.Web.Routing.RouteValueDictionary();
            if(bCompleted)
                rPar.Add("complete", bCompleted);

            if (Param != null)
            {
                string[] strParam = Param.Split(';');


                foreach (string strPara in strParam)
                {
                    string[] strValues = strPara.Split(',');

                    rPar.Add(strValues[0], strValues[1]);
                }
            }
            
            return RedirectToAction("../" + Module, rPar);
          
        }
        public ActionResult Cancel(string strModule)
        {
            if (!Request.Cookies["Module"].Value.Contains("Lab"))
            {
                Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["ValSel"].Expires = DateTime.Now.AddDays(-1);
                Session.Remove("ValSel");
            }
            else
            {
                Response.Cookies["FilStringLab"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["ValSelLab"].Expires = DateTime.Now.AddDays(-1);
                Session.Remove("ValSelLab");
            }
            Session.Remove("DataFil");
            Session.Remove("DataSource");
            Session.Remove("CancelActivate");
           

            return RedirectToAction("../"+strModule);
        }
        public ActionResult DestroyWWSession()
        {
            Session.Remove("FilWW");
            return new EmptyResult();
        }
        public ActionResult ActiveDeactive(bool Activate)
        {
            if (!Activate)
            {
                if (!Request.Cookies["Module"].Value.Contains("Lab"))
                {
                    Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
                    
                    Session.Remove("ValSel");
                }else
                {
                    Session.Remove("ValSelLab");
                    Response.Cookies["FilStringLab"].Expires = DateTime.Now.AddDays(-1); 
                }
                Session.Remove("DataFil");
                Session.Remove("DataSource");
                Session.Remove("CancelActivate");
            }else
            {
                if (!Request.Cookies["Module"].Value.Contains("Lab"))
                {
                    Session["ValSel"] = Session["ValSelFil"];
                    Response.Cookies["FilString"].Value = Request.Cookies["FilStringFil"].Value;
                }else
                {
                    Session["ValSelLab"] = Session["ValSelFilLab"];
                    Response.Cookies["FilStringLab"].Value = Request.Cookies["FilStringFilLab"].Value;
                }
                Session["CancelActivate"] = 1;
            }
            Session["Prefilter"] = Activate;
            return new EmptyResult();
        }
        public static Models.FilterCfg LoadFilter(string strUser)
        {
            Models.FilterCfg fil = new Models.FilterCfg();
          
            string UserEmail = GeneralClasses.General.FindEmailByAccount(strUser);

            try
            {
                using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    fil = dc.Database.SqlQuery<Models.FilterCfg>("SPROCgetFilterByUser @vchUser = {0}", UserEmail).ToList<Models.FilterCfg>().Single();
                    fil.Details = dc.Database.SqlQuery<Models.FilterDetails>("SPROCgetFilterDetails @id = {0}", fil.Id).ToList<Models.FilterDetails>();
                }

                return fil;
            }
            catch
            {
                return null;
            }

        }
    }
}
