﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoIndicators.Models;
using System.Dynamic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class BudgetController : Controller
    {
        //
        // GET: /Budget/
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection)
        {
            try
            {

                string usr = User.Identity.Name;
                string RYG = "";
                string Comments = SnapshotController.RemoveUnwantedHtmlTags(Convert.ToBoolean(collection["inline"]) ? collection["txtDescription_"] : collection["txtDescriptionB_"], SnapshotController.unwantedTags()).Trim();
                if (collection["ddlTrend"].Equals("1") || collection["ddlTrend"].Equals("2"))
                {
                    RYG = "R";
                }
                else if (collection["ddlTrend"].Equals("3") || collection["ddlTrend"].Equals("4"))
                {
                    RYG = "Y";
                }else if (collection["ddlTrend"].Equals("5"))
                    RYG = "G";

                var parames = new object[] { collection["pBudgetId"], collection["pTrend"].Trim(), collection["pTrendP"], collection["ddlTrend"], usr, collection["pContinuityId"], collection["pWWID"] };
                var paramHI = new object[] { collection["HealthID"], RYG, Comments, usr };
                var result = new List<Models.DefaultWW>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWBudgetVal @BudgetId= {0}, @Trend = {1}, @TrendP={2}, @RYG= {3}, @systemUser = {4},@ContinuityID={5},@WWID={6}", parames).ToList<Models.DefaultWW>();
                    
                    result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWHealthIndicatorVal @HealthId= {0}, @value = {1},@comments = {2},@systemUser = {3}", paramHI).ToList<Models.DefaultWW>();                    
 
                    System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
                }

                return Json(System.Web.HttpContext.Current.Session["elementFocus"]);
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult ProgramsProgIdsView()
        {
            try
            {
                List<Models.ProgramProjIds> result = new List<Models.ProgramProjIds>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.ProgramProjIds>(@"SPROCGETProgramsProjIds").ToList<Models.ProgramProjIds>();
                }
                return PartialView(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CommentView(int? BudgetID, int? WWID, int? HIID,string Continuity)
        {
            ViewBag.WWID = WWID;
            ViewBag.HHID = HIID;
            try
            {
                List<Models.BudgetModel> result = new List<Models.BudgetModel>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.BudgetModel>(@"SPROCGetBudget @BudgetID= {0}", new object[] { BudgetID }).ToList<Models.BudgetModel>();
                }
                if (result.Count > 0)
                    return PartialView(result[0]);
                else
                {
                    Models.BudgetModel budget = new Models.BudgetModel();
                    budget.ContinuityID=Continuity;
                    budget.BudgetId=0;
                    return PartialView(budget);
                }
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult Charts(bool completed=false)
        {
            int? WWID = null;
            List<Models.BudgetGraph> graph = new List<BudgetGraph>();

             if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());
          

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (WWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    WWID = result[0].WWID;
                    ViewBag.WW = result[0].weekName;
                    ViewBag.weekStatus = result[0].weekstatus;
                    ViewBag.WWID = WWID;
                }

            }
            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@WWID", WWID);

            DataSet dsRes = SqlHelper.ExecuteDataset(conn, "SPROCGetBudgetDiscGraph", param);

            myModel.Data =GeneralClasses.General.ParseTo((object)new Models.BudgetGraph(), dsRes.Tables[0]);
            myModel.POR = GeneralClasses.General.ParseTo((object)new Models.BudgetGraph(), dsRes.Tables[1]);
            myModel.Owners = GeneralClasses.General.ParseTo((object)new Models.BudgetDiscrepancy(), dsRes.Tables[2]);
            myModel.UserChart = GeneralClasses.General.ParseTo((object)new Models.BudgetUserChart(), dsRes.Tables[3]);

            foreach(BudgetDiscrepancy budgetDisc in myModel.Owners)
            {
                DataRow[] row = dsRes.Tables[2].Select("OwnerName='" + budgetDisc.OwnerName+"'");

                if(row.Length>0)
                {
                    budgetDisc.diff = new List<int>();
                    budgetDisc.Title =new List<string>();
                   foreach(DataColumn dc in dsRes.Tables[2].Columns)
                   {
                       if(dc.ColumnName !="ProgName" && dc.ColumnName !="OwnerName" && dc.ColumnName!="CategoryName")
                       {
                           if (dc.ColumnName.Contains("Header"))
                           {
                               budgetDisc.Title.Add(row[0][dc.ColumnName].ToString());
                           }
                           else
                           {
                               budgetDisc.diff.Add(Convert.ToInt32(row[0][dc.ColumnName].ToString()));
                           }
                       }
                   }
                }
            }

            ViewBag.Complete = completed;
            ViewBag.Filter = new List<Models.mFilter>();
            ViewBag.objFil = new List<object>();
            return View("ChartsView",myModel);
        }
        public ActionResult ClearBudget(int WWID)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("SPROCClearBudgetData @WWID={0}", WWID);
            }

            return Json(1);
        }
        public ActionResult SaveDiff(string ContinuityId,string ProjId,int CategoryId,int WWID,int Data1, int Data2, int Data3, int Data4,int Data5)
        {
            Models.BudgetDiscrepancies Diff = new BudgetDiscrepancies();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Diff = dc.Database.SqlQuery<Models.BudgetDiscrepancies>(@"SPROCManBudgetDiscrep @Data1= {0}, @Data2= {1}, @Data3= {2}, @Data4= {3}, @Data5= {4}, @ContinuityID={5}, @ProjID={6}, @CategoryID={7}, @WWID={8}"
                    , new object[] { Data1, Data2, Data3, Data4, Data5, ContinuityId, ProjId, CategoryId, WWID }).ToList<Models.BudgetDiscrepancies>().Single();
            }

            return Json(Diff);
        }
        public ActionResult LoadBudgetInfo(int WWID)
        {
            try
            {
                ViewBag.WWID = WWID;
                return View();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ActionResult LoadShopCartInfo(int WWID)
        {
            try
            {
                ViewBag.WWID = WWID;
                return View();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ActionResult Title(string Title,string Link)
        {
            BudgetHeaders Head = new BudgetHeaders();
            Head.Title = Title;
            Head.Link = Link;

            return PartialView("TitleView", Head);
        }
        private decimal? treatDecimalValue(string val)
        {
            try
            {
            decimal? ret = null;
            if (val.Length > 0)
            {
                if (val.IndexOf("E") > 0)
                { val = val.Substring(0, val.IndexOf(char.Parse("E"))); }

                string[] sDec = val.Split(char.Parse("."));
                if (sDec.Count() > 2)
                {
                    Exception ex = new Exception("position 1 data is corrupted, more than one . found");
                    throw ex;
                }
                else if (sDec.Count() == 1)
                { ret = decimal.Parse(sDec[0]); }

                else if (sDec.Count() == 2)
                {
                    ret = decimal.Parse(sDec[0] + "." + sDec[1].Substring(0, sDec[1].Length > 10 ? 10 : sDec[1].Length));
                }
            }

            return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult Upload(FormCollection collection )
        {
            System.Collections.Generic.List<StringScalarValue> list;
            string filePath = null;
            string BPOImportVersion = null;
            string oldBPOImportVersion = null;
            StreamReader reader = null;
            try
            {
                if (Request.Files.Count > 0)
                {
                    BPOImportVersion = Guid.NewGuid().ToString();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    { list = dc.Database.SqlQuery<StringScalarValue>(@"SPROCGetLatestBPOImportVersion").ToList<StringScalarValue>(); }
                    oldBPOImportVersion = list.Count > 0 ? list[0].text : null;
                    string usr = User.Identity.Name;
                    var file = Request.Files[0];
                    string[] nameBreak = file.FileName.Split(char.Parse("."));
                    string extension = nameBreak.Last();
                    if (!extension.ToUpper().Contains("CSV"))
                    { return RedirectToAction(""); }
                    filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file.FileName);
                    if (System.IO.File.Exists(filePath))
                    { System.IO.File.Delete(filePath); }
                    file.SaveAs(filePath);
                    reader = new StreamReader(System.IO.File.OpenRead(filePath));
                    int count = 0;
                    string PRJID = string.Empty;
                    while (!reader.EndOfStream)
                    {
                        count++;
                        var line = reader.ReadLine();
                        // 0 = projid, 1 = category, 2 = data1, 3 = data2, 4 = data3, 5 = data4
                        List<string> values = line.Split('|').ToList<string>();

                        if (values[0].Length > 0)
                        { 
                            string inputPRJID = values[0];
                            if (inputPRJID.Count() >= 15)
                            {
                                PRJID = inputPRJID.Trim().Substring(0,15).Trim();
                            }
                            else
                            {
                                PRJID = inputPRJID;
                            }
                        }

                        using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                        {
                            object[] parames;
                            //create headers
                            //report has 2 columns less
                            if (values.Count() == 5)
                            {
                                values.Add(string.Empty);
                                values.Add(string.Empty);
                            }
                            if (count == 1)
                            {
                                parames = new object[] { values[2], values[3], values[4], values[5], values[6], BPOImportVersion, collection["WWID"] };
                                
                                dc.Database.ExecuteSqlCommand(@"SPROCLoadBudgetBPOHeaders  @Header1 = {0},@Header2 = {1},
                                @Header3 = {2},@Header4 = {3}, @Header5={4}, @BPOImportVersion = {5}, @WWID ={6}", parames);
                            }
                            else
                            {
                                decimal? dec1 = this.treatDecimalValue(values[2]);
                                decimal? dec2 = this.treatDecimalValue(values[3]);
                                decimal? dec3 = this.treatDecimalValue(values[4]);
                                decimal? dec4 = this.treatDecimalValue(values[5]);
                                decimal? dec5 = this.treatDecimalValue(values[6]);

                                parames = new object[] { BPOImportVersion, dec1, dec2, dec3, dec4, dec5, usr, PRJID, values[1] };
                                dc.Database.ExecuteSqlCommand(@"SPROCLoadBudgetBPOData @BPOImportVersion = {0}, @Data1 = {1},@Data2 = {2},@Data3 = {3},
                            @Data4 = {4},@Data5={5},@User = {6},@BPOProj = {7},@BPOCategory = {8}  ", parames);
                            }
                        }
                    }


                    if (oldBPOImportVersion != null)
                    {
                        var parames = new object[] { oldBPOImportVersion };
                        using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                        {
                            dc.Database.ExecuteSqlCommand(@"SPROCDeactivateBPOImportVersion @BPOImportVersion = {0}", parames);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    var parames = new object[] { BPOImportVersion };
                    dc.Database.ExecuteSqlCommand(@"SPROCRollBackBudgetBPOHeaders @BPOImportVersion = {0}", parames);
                }
                throw ex;

            }
            finally
            {
                reader.Close();
                reader.Dispose();
                System.GC.Collect();
                System.IO.File.Delete(filePath);
            }
            return RedirectToAction("Index", "Snapshot");
        }
        public ActionResult LoadMasterProjId( int WWID)
        {
            ViewBag.WWID=WWID;
            return PartialView("LoadProjIds");
        }
        public ActionResult SaveLoadMasterProjId(FormCollection collection)
        {
            string filePath = null;
            var file = Request.Files[0];
            string[] nameBreak = file.FileName.Split(char.Parse("."));
            string extension = nameBreak.Last();
            StreamReader reader = null;
            string PRJID = string.Empty;
            List<Models.ProjIds> lstProjAdd =new List<ProjIds>();

            if (!extension.ToUpper().Contains("CSV"))
            { 
                return RedirectToAction(""); 
            }
            
            filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file.FileName);
            
            if (System.IO.File.Exists(filePath))
            { 
                System.IO.File.Delete(filePath); 
            }

            file.SaveAs(filePath);

            try
            {
                reader = new StreamReader(System.IO.File.OpenRead(filePath));

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    string[] ProjData = line.Split('-');
                    Models.ProjIds proj = new ProjIds();

                    if (ProjData.Length > 1)
                    {
                        proj.ProjID = ProjData[0];
                        proj.Sufix = ProjData[1];

                        lstProjAdd.Add(proj);
                    }
                }

                reader.Close();
                reader.Dispose();

                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    foreach (Models.ProjIds proj in lstProjAdd)
                    {
                        dc.Database.ExecuteSqlCommand("SPROCLoadBudgetProjs @vchProjId={0}, @vchSufix={1}, @WWID={2}", new object[] { proj.ProjID, proj.Sufix, collection["WWID"] });
                    }
                }

                System.IO.File.Delete(filePath);

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        [HttpPost]
        public ActionResult UploadShopCart(FormCollection collection)
        {
            string filePath = null;
            string ShopCartImportVersion = null;
            StreamReader reader = null;
            try
            {
                if (Request.Files.Count > 0)
                {
                    ShopCartImportVersion = Guid.NewGuid().ToString();
                    string usr = User.Identity.Name;
                    var file = Request.Files[0];
                    string[] nameBreak = file.FileName.Split(char.Parse("."));
                    string extension = nameBreak.Last();
                    if (!extension.ToUpper().Contains("CSV"))
                    { return RedirectToAction(""); }
                    filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file.FileName);
                    if (System.IO.File.Exists(filePath))
                    { System.IO.File.Delete(filePath); }
                    file.SaveAs(filePath);
                    reader = new StreamReader(System.IO.File.OpenRead(filePath));
                    int count = 0;
                    string PRJID = string.Empty;
                    while (!reader.EndOfStream)
                    {
                        count++;
                        var line = reader.ReadLine();
                        // 0 = projid, 1 = date, 2 = ammount
                        List<string> values = line.Split('|').ToList<string>();

                        if (values[0].Length > 0)
                        {
                            if (values[0].Length > 10)
                            { values[0] = values[0].Substring(2, values[0].Length - 2); }
                            PRJID = values[0]; 

                            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                            {
                                object[] parames;

                                DateTime valDate = DateTime.Parse(values[1]);
                                decimal? ammt;
                                try
                                {
                                    ammt = this.treatDecimalValue(values[2]);
                                }
                                catch (Exception ex)
                                {                                    
                                    throw ex;
                                }
          

                                parames = new object[] { ShopCartImportVersion, valDate, ammt, PRJID, usr};
                                dc.Database.ExecuteSqlCommand(@"SPROCLoadBudgetShopCartData @ShopCartImportVersion = {0},@valDate = {1},@Ammt = {2}, @PrjId = {3}, @Usr = {4}", parames);                            
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    var parames = new object[] { ShopCartImportVersion };
                    dc.Database.ExecuteSqlCommand(@"SPROCRollBackBudgetShopCartImport @ShopCartImportVersion = {0}", parames);
                }
                throw ex;

            }
            finally
            {
                reader.Close();
                reader.Dispose();
                System.GC.Collect();
                System.IO.File.Delete(filePath);
            }
            return RedirectToAction("Index", "Snapshot");
        }

        private dynamic myModel = new ExpandoObject();

        public ActionResult Index(bool complete=false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            List<Models.BudgetModel> list = new List<Models.BudgetModel>();
            List<Models.BudgetModel> Budget = new List<Models.BudgetModel>();

            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            List<object> objFil = new List<object>();
            List<string> lstValSel = new List<string>();
            int? pWWID = null;

            #region Filter
            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Budget";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Budget";

            #region Init Filter

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();
            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            #endregion

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());
            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                   
                    list = getDBdata(result[0].WWID);
                        
                    foreach (Models.BudgetModel budget in list)
                    {
                        Object o = new object();

                        o = (object)budget;
                        objFil.Add(o);
                    }
                    ViewBag.objFil = objFil;
                }
                else
                {
                    #region Build Filter

                    list = getDBdata(int.Parse(pWWID.ToString()));

                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }
                    
                    foreach (Models.BudgetModel budget in list)
                    {
                        Object o = new object();

                        o = (object)budget;
                        objFil.Add(o);
                    }
                    

                    ViewBag.objFil = objFil;
                    #endregion
                     
                }
                myModel.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
                fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data Filter
                List<Models.BudgetModel> resFil = new List<Models.BudgetModel>();
                if (Request.Cookies["FilString"] != null)
                {
                    fFil.dtData = (DataTable)Session["DataFil"];
                    //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                    #region Apply Filter

                    List<string> strValue;

                    if (Session["ValSel"] != null)
                        strValue = (List<string>)Session["ValSel"];
                    else
                    {
                        strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                        Session["ValSel"] = strValue;
                    }

                    string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                    string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                    string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                    string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                    string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                    string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                    int inti = 0;

                    foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                    {
                        strPXTValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                    {
                        strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                    {
                        strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                    {
                        strProgValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                    {
                        strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                    {
                        strClasification[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    Budget = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                    #endregion
                }
                else
                    Budget = list;
                #endregion
            }

            ViewBag.WW = list[0].weekName;
            ViewBag.WWID = list[0].WWID;
            ViewBag.WeekStatus = list[0].weekStatus;
            ViewBag.Complete = complete;

            myModel.List = Budget;
            return View("~/Views/Budget/BudgetView.cshtml", myModel);
        }

        public List<Models.BudgetModel> getDBdata(int WWID)
        {
            try
            {
                var RelatedSummary = new List<SummaryBudget>();
                var nonRelatedSummary = new List<SummaryBudget>();
                var TotalSummary = new List<SummaryBudget>();
                var RelatedProjs = new List<StringScalarValue>();
                var nonRelatedProjs = new List<StringScalarValue>();
                var TotalProjs = new List<StringScalarValue>();
                var list = new List<Models.BudgetModel>();
                var userList = new List<Models.Users>();
                var BudgetSummaryData = new List<BudgetSummaryData>();
                var Headers = new List<Models.BudgetHeaders>();
                var Summaries = new List<List<SummaryBudget>>();

                
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {                                        
                    Headers = dc.Database.SqlQuery<Models.BudgetHeaders>("SPROCGetBudgetBPOHeaders @WWID = {0}", WWID).ToList<Models.BudgetHeaders>();
                    //get quarter end date
                    string currentQ = Headers[0].Header3.Substring(Headers[0].Header3.IndexOf(char.Parse("Q")) + 1, 1);
                    int year = Headers[0].Date.Year;
                    string EdgeDate;

                    if (currentQ.Equals("4"))
                    { year = year + 1; }

                    switch (currentQ)
                    {
                        case "1":
                            EdgeDate = year.ToString() + "-04-01";
                            break;
                        case "2":
                            EdgeDate = year.ToString() + "-07-01";
                            break;
                        case "3":
                            EdgeDate = year.ToString() + "-10-01";
                            break;
                        case "4":
                            EdgeDate = year.ToString() + "-01-01";
                            break;
                        default:
                            EdgeDate = "1900-01-01";
                            break;
                    }
                    
                    list = dc.Database.SqlQuery<Models.BudgetModel>("SPROCGetWWBudget @WWID = {0},  @QedgeDate = {1} ", new object[] { WWID, EdgeDate }).ToList<Models.BudgetModel>();
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                    RelatedSummary = dc.Database.SqlQuery<Models.SummaryBudget>("SPROCGetWWSummaryBudget @WWID = {0}, @related = {1} ", new object[] { WWID, null }).ToList<Models.SummaryBudget>();
                    nonRelatedSummary = dc.Database.SqlQuery<Models.SummaryBudget>("SPROCGetWWSummaryBudget @WWID = {0}, @related = {1} ", new object[] { WWID, 0 }).ToList<Models.SummaryBudget>();
                    TotalSummary = dc.Database.SqlQuery<Models.SummaryBudget>("SPROCGetWWSummaryBudget @WWID = {0}, @related = {1} ", new object[] { WWID, 1 }).ToList<Models.SummaryBudget>();
                    //gets the trend, comments and RYG data
                    BudgetSummaryData = dc.Database.SqlQuery<Models.BudgetSummaryData>("SPROCGetWWSummaryBudgetIds @WWID = {0}", WWID).ToList<Models.BudgetSummaryData>();
                }
                Summaries.Add(TotalSummary);
                Summaries.Add(RelatedSummary);
                Summaries.Add(nonRelatedSummary);
                ViewBag.validUsers = userList;
                //myModel.RelatedSummary = RelatedSummary;
                //myModel.nonRelatedSummary = nonRelatedSummary;
                //myModel.TotalSummary = TotalSummary;
                myModel.BudgetSummaryData = BudgetSummaryData;
                myModel.Headers = Headers;
                myModel.List = list;
                myModel.Summaries = Summaries;
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateBudgetTitle(string Title,int WWID)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@vchTitle", Title);
            param[1] = new SqlParameter("@WWID", WWID);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString,"SPROCUpdateBudgetTitle",param);

            return Json(dsRes.Tables[0].Rows[0][0]);
        }
        public ActionResult getBudgetID(string ContinuityID,int WWID)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ContinuityID", ContinuityID);
            param[1] = new SqlParameter("@WWID", WWID);
            int ID = 0;

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetBudgetIDForHI", param);
            if(dsRes.Tables[0].Rows.Count>0)
                ID = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            return JavaScript(ID.ToString());
        }
        public ActionResult Noncompliant(int WWID)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                myModel = dc.Database.SqlQuery<Models.BudgetNoncompliant>("SPROCgetBudgetEmptyTrendRYG @WWID = {0} ", WWID).ToList<Models.BudgetNoncompliant>();
            }

            return PartialView(myModel);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendChart(string Body)
        {
            string img = Body.Substring(Body.LastIndexOf(",") + 1);
            byte[] imageBytes = Convert.FromBase64String(img);
            string imgPath=AppDomain.CurrentDomain.BaseDirectory + "\\Images\\screenshot.png";
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                if(System.IO.File.Exists(imgPath))
                {
                    System.IO.File.Delete(imgPath);
                }
                image.Save(imgPath);
            }
            GeneralClasses.General.SenEmail("Test", "", "jorge.luisx.orozco.ruiz@intel.com", imgPath);
            
            return new EmptyResult();
        }
        public ActionResult NotifNonCompliant(int WWID,string WWName)
        {
            List<Models.BudgetNoncompliant> budget = new List<BudgetNoncompliant>();
            List<Models.NotifAssignees> lstNotifAssign = new List<Models.NotifAssignees>();
            NotifController notif = new NotifController();
            string strBody = "";
            WWName = WWName.Replace("_", "'");

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                budget = dc.Database.SqlQuery<Models.BudgetNoncompliant>("SPROCSendNotifBudget @WWID = {0} ", WWID).ToList<Models.BudgetNoncompliant>();
            }

            foreach (BudgetNoncompliant own in budget.Distinct().ToList())
            {
                NotifAssignees assig=new NotifAssignees();

                strBody = "Incomplete entry for trend on snapshot budget app on " + WWName + " for programs:<br><br>";
                assig.Email=own.Email;
                assig.Email = "raul.lobato.wong@intel.com";
                lstNotifAssign.Add(assig);
                foreach(BudgetNoncompliant detail in budget.Where(x=> x.Email==own.Email))
                {
                    strBody += detail.Project + "<br/>";
                }


                notif.SendEmail(WWID, WWName, strBody,lstNotifAssign , "snapshot - incomplete budget data on " + WWName);
                lstNotifAssign.Clear();
            }
           
            return new EmptyResult();
        }
    }
}