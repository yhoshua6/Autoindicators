﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    public class IndicatorController : Controller
    {
        //
        // GET: /Indicator/
        dynamic myModel = new ExpandoObject();
        [ValidateInput(false)]
        public ActionResult Index(int WWID, string ContinuityID, string weekName, int? intern)
        {
            try
            {

                string BPOImportVersion;

                Models.MasterModel master = new Models.MasterModel();

                List<Models.BudgetModel> Budgetlist = new List<Models.BudgetModel>();
                List<Models.ScheduleV> Schedulelist = new List<Models.ScheduleV>();
                List<Models.BudgetHeaders> BudgetHeaders = new List<Models.BudgetHeaders>();
                List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();
                List<Models.CCB> CCBlist = new List<Models.CCB>();
                List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
                List<Models.CatCCBStatus> CCBStatusCat = new List<Models.CatCCBStatus>();
                List<Models.SnapshotIndex> Snapshot = new List<Models.SnapshotIndex>();
                List<Models.Pcos> PCOSlist = new List<Models.Pcos>();
                List<Models.HealthIndicator> Healthlist = new List<Models.HealthIndicator>();
                List<Models.CommentsForIndicator> StoredComments = new List<Models.CommentsForIndicator>();
                List<Models.AllComments> CommentsSelected = new List<Models.AllComments>();
                List<Models.PLCList> CatPLC = new List<Models.PLCList>();
                IEnumerable<SelectListItem> PlCList;
                List<Models.IndicatorTypes> Types = new List<Models.IndicatorTypes>();
                List<Models.MasterModel> dataFilList = new List<Models.MasterModel>();

                List<Models.Users> userList = new List<Models.Users>();
                List<Models.AllComments> Comments = new List<Models.AllComments>();
                List<Models.IndicatorScheduleComment> lstScheduleComment = new List<Models.IndicatorScheduleComment>();
                List<Models.IndicatorHealthIndicatorComment> lstHeatlhIndicatorComment = new List<Models.IndicatorHealthIndicatorComment>();
                List<Models.IndicatorBudgetComment> lstBudgetComment = new List<Models.IndicatorBudgetComment>();
                List<Models.IndicatorValidationComment> lstValidationComment = new List<Models.IndicatorValidationComment>();
                List<Models.IndicatorSnapshotComment> lstSnapshotComment = new List<Models.IndicatorSnapshotComment>();
                List<Models.IndicatorsPCOSComment> lstPCOSComment = new List<Models.IndicatorsPCOSComment>();
                List<Models.ValidationModel> lstVal = new List<Models.ValidationModel>();
                List<Models.ValidationPhases> Phases = new List<Models.ValidationPhases>();
                List<Models.BPOPRJID> proj =new List<Models.BPOPRJID>();

                AutoIndicators.Controllers.ProgEditController ProgControl = new AutoIndicators.Controllers.ProgEditController();

                if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getPrograms(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                {
                    WWID = Convert.ToInt32(Session["FilWW"].ToString());
                }

                List<Models.Calendar> calendar = GeneralClasses.ViewDataFilter.Calendar();
                Models.Calendar cals = calendar.Where(x => x.id == WWID).Single();
                
                ViewBag.WW = cals.weekName;
                dataFilList = getPrograms(WWID);
                ViewBag.WWID = WWID;

                ViewBag.intern = intern;
                #region Filter
                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();

                #region Init Filter
                if (Session["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Session["Module"].ToString()))
                {
                    Session["Module"] = "Indicator";
                    Session.Remove("DataSource");
                }
                else
                    Session["Module"] = "Indicator";

                mFil.strField = "PXTName";
                mFil.strLabelField = "PXT";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgType";
                mFil.strLabelField = "Type";
                //      mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "OwnerName";
                mFil.strLabelField = "Owner";
                //       mFil.strLinkedField = "ProgType";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgName";
                mFil.strLabelField = "Program";
                //       mFil.strLinkedField = "OwnerName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                ViewBag.Filter = mFilters;
                ViewBag.Module = Session["Module"];
                #endregion
                #region Build Filter
                List<object> objFil = new List<object>();

                foreach (Models.MasterModel ccb in dataFilList)
                {
                    Object o = new object();

                    o = (object)ccb;
                    objFil.Add(o);
                }

                ViewBag.objFil = objFil;
                #endregion
                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
              //  fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data
                List<Models.MasterModel> resProgram = new List<Models.MasterModel>();
                List<Models.MasterModel> programSel = new List<Models.MasterModel>();
                if (Session["FilString"] != null)
                {
                    //fFil.dtData = (DataTable)Session["DataFil"];
                    string strFilString = fFil.strValidFilterString(Session["FilString"].ToString());

                    if (fFil.dtData != null && fFil.dtData.Select(strFilString).Count() > 0)
                    {
                        fFil.dtResults = fFil.dtData.Select(strFilString).CopyToDataTable();
                        fFil.vParseTo((object)new Models.MasterModel());

                        if (fFil.oResults.Count > 0)
                        {
                            foreach (Models.MasterModel Master in fFil.oResults)
                            {
                                resProgram.Add(Master);
                            }
                        }
                    }
                    else
                    {
                        resProgram = dataFilList;
                    }
                    programSel = resProgram;
                }
                else
                {
                    resProgram = dataFilList;
                    programSel = resProgram.Where(row => row.ContinuityID == ContinuityID).ToList<Models.MasterModel>();
                }

                if (programSel.Count() == 0)
                {
                    programSel.Add(resProgram.First());
                }
                var programWos = resProgram.OrderBy(x => x.ProgName.Trim());



                myModel.ProgramSel = programSel.ToList();
                myModel.ProgramWos = programWos.ToList();

                #endregion
                #endregion
                ContinuityID = programSel.First().ContinuityID;
                master = dataFilList.Where(row => row.ContinuityID == ContinuityID).First();

                #region Get Data
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@WWID",WWID);
                param[1] = new SqlParameter("@ContinuityID", master.ContinuityID);
                param[2] = new SqlParameter("@intProgramTypeID", master.ProgTypeID);

                DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetIndicatorData", param);
                
                AutoIndicators.GeneralClasses.ViewDataFilter TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                #region Users
                TableParse.dtResults = dsRes.Tables[0].Copy();
                TableParse.vParseTo((object)new Models.Users());
                
                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.Users data in TableParse.oResults)
                    {
                        userList.Add(data);
                    }
                }
                #endregion
                #region Budget
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[1].Copy();
                TableParse.vParseTo((object)new Models.BudgetModel());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.BudgetModel data in TableParse.oResults)
                    {
                        Budgetlist.Add(data);
                    }

                    if (Budgetlist.Count > 0)
                    {
                        BPOImportVersion = Budgetlist[0].BPOImportVersion;
                    }
                }
                #endregion
                #region Budget Headers
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[2].Copy();
                TableParse.vParseTo((object)new Models.BudgetHeaders());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.BudgetHeaders data in TableParse.oResults)
                    {
                        BudgetHeaders.Add(data);
                    }
                }
                #endregion
                #region Validation
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[3].Copy();
                TableParse.vParseTo((object)new Models.ValidationModel());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.ValidationModel data in TableParse.oResults)
                    {
                        lstVal.Add(data);
                    }
                }
                #endregion
                #region Validation Phases
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[4].Copy();
                TableParse.vParseTo((object)new Models.ValidationPhases());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.ValidationPhases data in TableParse.oResults)
                    {
                        Phases.Add(data);
                    }
                }
                #endregion
                #region Snapshot
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[22].Copy();
                TableParse.vParseTo((object)new Models.SnapshotIndex());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.SnapshotIndex data in TableParse.oResults)
                    {
                        Snapshot.Add(data);
                    }
                }
                #endregion
                #region PLC
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[4].Copy();
                TableParse.vParseTo((object)new Models.PLCList());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.PLCList data in TableParse.oResults)
                    {
                        CatPLC.Add(data);
                    }
                }
                #endregion
                #region Schedule
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[6].Copy();
                TableParse.vParseTo((object)new Models.ScheduleV());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.ScheduleV data in TableParse.oResults)
                    {
                        Schedulelist.Add(data);
                    }
                }
                #endregion
                #region HI
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[7].Copy();
                TableParse.vParseTo((object)new Models.HealthIndicator());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.HealthIndicator data in TableParse.oResults)
                    {
                        Healthlist.Add(data);
                    }
                }
                #endregion
                #region PCOS
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[8].Copy();
                TableParse.vParseTo((object)new Models.Pcos());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.Pcos data in TableParse.oResults)
                    {
                        PCOSlist.Add(data);
                    }
                }
                #endregion
                #region CCB
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[10].Copy();
                TableParse.vParseTo((object)new Models.CCB());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.CCB data in TableParse.oResults)
                    {
                        CCBlist.Add(data);
                    }
                }
                #endregion
                #region CCB Status
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[11].Copy();
                TableParse.vParseTo((object)new Models.CCBStatus());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.CCBStatus data in TableParse.oResults)
                    {
                        CCBStatusList.Add(data);
                    }
                }
                #endregion
                #region CCB Status Cat
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[12].Copy();
                TableParse.vParseTo((object)new Models.CatCCBStatus());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.CatCCBStatus data in TableParse.oResults)
                    {
                        CCBStatusCat.Add(data);
                    }

                    if (CCBlist.Count > 0 && CCBStatusList.Count > 0)
                    {
                        CCBlist[0].lStatus = CCBStatusList;
                    }
                }
                #endregion
                #region All Comments
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[13].Copy();
                TableParse.vParseTo((object)new Models.AllComments());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.AllComments data in TableParse.oResults)
                    {
                        Comments.Add(data);
                    }
                }
                #endregion
                #region Comments Schedule
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[14].Copy();
                TableParse.vParseTo((object)new Models.IndicatorScheduleComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorScheduleComment data in TableParse.oResults)
                    {
                        lstScheduleComment.Add(data);
                    }
                }
                #endregion
                #region Comments HI
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[15].Copy();
                TableParse.vParseTo((object)new Models.IndicatorHealthIndicatorComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorHealthIndicatorComment data in TableParse.oResults)
                    {
                        lstHeatlhIndicatorComment.Add(data);
                    }
                }
                #endregion
                #region Comments Budget
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[16].Copy();
                TableParse.vParseTo((object)new Models.IndicatorBudgetComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorBudgetComment data in TableParse.oResults)
                    {
                        lstBudgetComment.Add(data);
                    }
                }
                #endregion
                #region Comments Validation
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[17].Copy();
                TableParse.vParseTo((object)new Models.IndicatorValidationComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorValidationComment data in TableParse.oResults)
                    {
                        lstValidationComment.Add(data);
                    }
                }
                #endregion
                #region Comments Snapshot
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[18].Copy();
                TableParse.vParseTo((object)new Models.IndicatorSnapshotComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorSnapshotComment data in TableParse.oResults)
                    {
                        lstSnapshotComment.Add(data);
                    }
                }
                #endregion
                #region Comments PCOS
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[19].Copy();
                TableParse.vParseTo((object)new Models.IndicatorsPCOSComment());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorsPCOSComment data in TableParse.oResults)
                    {
                        lstPCOSComment.Add(data);
                    }
                }
                #endregion
                #region Templates
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[20].Copy();
                TableParse.vParseTo((object)new Models.IndicatorTypes());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.IndicatorTypes data in TableParse.oResults)
                    {
                        Types.Add(data);
                    }
                    
                }
                #endregion
                #region ProjIds
                TableParse = new AutoIndicators.GeneralClasses.ViewDataFilter();

                TableParse.dtResults = dsRes.Tables[20].Copy();
                TableParse.vParseTo((object)new Models.BPOPRJID());

                if (TableParse.oResults.Count > 0)
                {
                    foreach (Models.BPOPRJID data in TableParse.oResults)
                    {
                        proj.Add(data);
                    }
                }
                #endregion
                #endregion

                PlCList = CatPLC.Select(x =>
                            new SelectListItem()
                            {
                                Text = x.Name,
                                Value = x.Id.ToString()
                            });

                myModel.PlCList = PlCList;
                myModel.BudgetList = Budgetlist;
                myModel.lstBudgetComments = lstBudgetComment;
                myModel.ScheduleList = Schedulelist;
                myModel.lstScheduleComment = lstScheduleComment;
                myModel.HealthList = Healthlist;
                myModel.lstHealthIndicatorComment = lstHeatlhIndicatorComment;
                myModel.PCOSList = PCOSlist.Where(x => x.ContinuityID == ContinuityID);
                myModel.lstPCOSComment = lstPCOSComment;
                myModel.WWID = WWID;
                myModel.ContinuityID = ContinuityID;
                myModel.CommentsSelected = CommentsSelected;
                myModel.CCBList = CCBlist;
                myModel.CCBStatusCat = CCBStatusCat;
                myModel.Types = Types;
                myModel.Snapshotlist = Snapshot.Where(x => x.ContinuityID == ContinuityID);
                myModel.lstSnapshotComments = lstSnapshotComment;
                myModel.Users = userList;
                myModel.Comments = Comments;
                myModel.ValidationPhases = Phases.Where(x => x.PWContinuityID == ContinuityID);
                myModel.lstValidation = lstVal.Where<Models.ValidationModel>(x => x.ContinuityID == ContinuityID).ToList<Models.ValidationModel>();
                myModel.lstValidationComments = lstValidationComment;

                if (Types != null && Types.Count > 0)
                    myModel.TemplateID = Types[0].id;
                else
                    myModel.TemplateID = master.TemplateID;
                myModel.Template = null;

                myModel.projSel = proj;

                if (BudgetHeaders.Any())
                {
                    myModel.BudgetHeaders = BudgetHeaders;
                }
                else
                {
                    myModel.BudgetHeaders = null;
                }
                return View("IndicatorView", myModel);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult ProgramIndicator()
        {
            return View();
        }

        public ActionResult SaveComments(FormCollection collection)
        {
            string[] initValues = collection[0].Split(char.Parse(","));
            int WWID = int.Parse(initValues[0]);
            string PWcontinuityId = initValues[1];
            string sValue = string.Empty;
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("SPROCDeleteIndicatorComments @WWID = {0}, @PWContinuityID = {1} ", WWID, PWcontinuityId);
                for (int i = 1; i < collection.Count;i++ )
                {
                    string[] values = collection[i].Split(char.Parse(","));
                    dc.Database.ExecuteSqlCommand("SPROCCreateNewCommentColection @WWID = {0}, @PWContinuityID = {1}, @CategoryId = {2}, @CategoryId2 = {3}, @originView = {4}",
                        WWID, PWcontinuityId, values[0], values[1], values[2]);
                }
            }
            return RedirectToAction("Indicator", "Indicator", new { WWID = WWID, ContinuityID = PWcontinuityId });
        }
 

        private List<Models.AllComments> getSelectedComments(List<Models.CommentsForIndicator> StoredComments, int? WWID, string PWContinuityId)
        {
            List<Models.AllComments> Comments = new List<Models.AllComments>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                foreach (Models.CommentsForIndicator item in StoredComments)
                {
                    Comments.AddRange(dc.Database.SqlQuery<Models.AllComments>
                        ("SPROCGetAllViewComments @CategoryId = {0}, @CategoryId2 = {1}, @WWID = {2},@PWContinuityID = {3}, @view = {4} ", 
                        item.category1, item.category2, WWID, PWContinuityId, item.OriginView).ToList<Models.AllComments>());
                }
            }

            return Comments;
        }


        public ActionResult Indicator(int? intern, int? Notif,bool complete=false, string ContinuityID = "")
        {
            try
            {
               
            string BPOImportVersion;
            int? WWID=null;
            Models.MasterModel master = new Models.MasterModel();

            List<Models.BudgetModel> Budgetlist = new List<Models.BudgetModel>();
            List<Models.ScheduleV> Schedulelist = new List<Models.ScheduleV>();
            List<Models.BudgetHeaders> BudgetHeaders = new List<Models.BudgetHeaders>();
            List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();
            List<Models.CCBResume> CCBlist = new List<Models.CCBResume>();
            List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
            List<Models.CatCCBStatus> CCBStatusCat = new List<Models.CatCCBStatus>();
            List<Models.SnapshotIndex> Snapshot = new List<Models.SnapshotIndex>();
            List<Models.Pcos> PCOSlist = new List<Models.Pcos>();
            List<Models.HealthIndicator>  Healthlist = new List<Models.HealthIndicator>();
            List<Models.CommentsForIndicator> StoredComments = new List<Models.CommentsForIndicator>();
            List<Models.AllComments> CommentsSelected = new List<Models.AllComments>();
            List<Models.PLCList> CatPLC = new List<Models.PLCList>();
            IEnumerable<SelectListItem> PlCList;
            List<Models.IndicatorTypes> Types = new List<Models.IndicatorTypes>();
            List<Models.MasterModel> dataFilList = new List<Models.MasterModel>();
            List<Models.Users> userList = new List<Models.Users>();
            List<Models.AllComments> Comments = new List<Models.AllComments>();
            List<Models.IndicatorScheduleComment> lstScheduleComment = new List<Models.IndicatorScheduleComment>();
            List<Models.IndicatorHealthIndicatorComment> lstHeatlhIndicatorComment = new List<Models.IndicatorHealthIndicatorComment>();
            List<Models.IndicatorBudgetComment> lstBudgetComment = new List<Models.IndicatorBudgetComment>();
            List<Models.IndicatorValidationComment> lstValidationComment = new List<Models.IndicatorValidationComment>();
            List<Models.IndicatorSnapshotComment> lstSnapshotComment = new List<Models.IndicatorSnapshotComment>();
            List<Models.IndicatorsPCOSComment> lstPCOSComment = new List<Models.IndicatorsPCOSComment>();
            List<Models.ValidationModel> lstVal = new List<Models.ValidationModel>();
            List<Models.ValidationPhases> Phases = new List<Models.ValidationPhases>();
            List<Models.CCBAffected> CCBListProj = new List<Models.CCBAffected>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            AutoIndicators.Controllers.ProgEditController ProgControl = new AutoIndicators.Controllers.ProgEditController();

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getPrograms(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
            {
                WWID = Convert.ToInt32(Session["FilWW"].ToString());
                Session["FilWW"] = WWID;
            }
            else
            {
                using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    WWID = result[0].WWID;
                }
            }            
            dataFilList = getPrograms(WWID);
           
            
            ViewBag.intern = intern;
            #region Filter
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();

            #region Init Filter
            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Request.Cookies["Module"].Value = "Indicator";
                Session.Remove("DataSource");
            }
            else
                Request.Cookies["Module"].Value = "Indicator";

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;
            ViewBag.Module = Session["Module"];
            #endregion
            #region Build Filter
            List<object> objFil = new List<object>();

            foreach (Models.MasterModel ccb in dataFilList)
            {
                Object o = new object();

                o = (object)ccb;
                objFil.Add(o);
            }

            ViewBag.objFil = objFil;
            #endregion
            #region PreLoad
            AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

            fFil.oSource = objFil;
            fFil.mFiltersToShow = mFilters;
            //fFil.vCreateFilter();

            Session["DataFil"] = fFil.dtData;
            Session["DataSource"] = fFil.oSource;

            #endregion
            #region Load Data
            List<Models.MasterModel> resProgram = new List<Models.MasterModel>();
            List<Models.MasterModel> programSel = new List<Models.MasterModel>();
            if (Request.Cookies["FilString"] != null)
            {
                //fFil.dtData = (DataTable)Session["DataFil"];
                //string strFilString = fFil.strValidFilterString(Session["FilString"].ToString());
                //System.Linq.Expressions.Expression<Func<Models.MasterModel>> PXT;

                List<string> strValue;

                if (Session["ValSel"] != null)
                    strValue = (List<string>)Session["ValSel"];
                else
                {
                    strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                    Session["ValSel"] = strValue;
                }
                
                string[] strPXTValue=new string[strValue.Where(x=> x.StartsWith("PXTName")).Count()];
                string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];

                int inti = 0;

                foreach(string strVal in strValue.Where(x=> x.StartsWith("PXTName")).ToList<string>())
                {
                    strPXTValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                {
                    strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                {
                    strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                {
                    strProgValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                {
                    strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                resProgram = dataFilList.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName)).ToList<Models.MasterModel>();


                //System.Linq.Expressions.Expression<Func<Models.MasterModel>,bool> PXT=(Models.MasterModel o) => o.PXTName.Any()

                //if (fFil.dtData != null && fFil.dtData.Select(strFilString).Count() > 0)
                //{
                //    fFil.dtResults = fFil.dtData.Select(strFilString).CopyToDataTable();
                //    fFil.vParseTo((object)new Models.MasterModel());

                //    if (fFil.oResults.Count > 0)
                //    {
                //        foreach (Models.MasterModel Master in fFil.oResults)
                //        {
                //            resProgram.Add(Master);
                //        }
                //    }
                //}
                //else
                //{
                //    resProgram = dataFilList;
                //}

                if (ContinuityID.Length > 0)
                {
                    programSel = resProgram.Where(row => row.ContinuityID == ContinuityID).ToList<Models.MasterModel>();
                }
                else
                {
                    programSel = resProgram;
                }
            }
            else
            {
                resProgram = dataFilList;
                programSel = resProgram.Where(row => row.ContinuityID == ContinuityID).ToList<Models.MasterModel>();
            }

            if (programSel.Count() == 0)
            {
                programSel.Add(resProgram.First());
            }
            var programWos = resProgram.OrderBy(x => x.ProgName.Trim());
            


            myModel.ProgramSel = programSel.ToList();
            myModel.ProgramWos = programWos.ToList();
           
            #endregion
            #endregion
            
            ContinuityID = programSel.First().ContinuityID;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                 
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                
                if (ContinuityID == null)
                {
                    
                    ContinuityID = list[0].ContinuityID;
                }
                Budgetlist = dc.Database.SqlQuery<Models.BudgetModel>("SPROCGetWWBudget @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.BudgetModel>();
                if (Budgetlist.Count > 0)
                {
                    BPOImportVersion = Budgetlist[0].BPOImportVersion;
                    BudgetHeaders = dc.Database.SqlQuery<Models.BudgetHeaders>("SPROCGetBudgetBPOHeaders @WWID = {0}", WWID).ToList<Models.BudgetHeaders>();
                }

                Snapshot = list.Where(x => x.ContinuityID == ContinuityID).ToList();
                lstVal = dc.Database.SqlQuery<Models.ValidationModel>("SPROCGetWWValidation @WWID = {0}, @ContID={1}",WWID,ContinuityID).ToList<Models.ValidationModel>();
                Phases = dc.Database.SqlQuery<Models.ValidationPhases>("SPROCGetWWValidationPhases @WWID = {0}, @ContID={1}", WWID,ContinuityID).ToList<Models.ValidationPhases>();
                CatPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                Schedulelist = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.ScheduleV>();
                Healthlist = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetWWHealthIndicators @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.HealthIndicator>();
                PCOSlist = dc.Database.SqlQuery<Models.Pcos>("SPROCGetWWPcos @WWID = {0}, @ContID={1}", WWID,ContinuityID).ToList<Models.Pcos>();                
                StoredComments = dc.Database.SqlQuery<Models.CommentsForIndicator>("SPROCGetCommentColection @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.CommentsForIndicator>();
                CCBlist = dc.Database.SqlQuery<Models.CCBResume>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3}, @bIndicator={4} ", new object[] { WWID, 0, 0, ContinuityID,true }).ToList<Models.CCBResume>();
                CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBStatus>().Where(row => row.ContinuityID == ContinuityID).ToList<Models.CCBStatus>();
                CCBStatusCat = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
                Comments = dc.Database.SqlQuery<Models.AllComments>("SPROCGetAllAppComments @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.AllComments>();
                lstScheduleComment = dc.Database.SqlQuery<Models.IndicatorScheduleComment>("SPROCIndicatorCommentsForSchedule @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorScheduleComment>();
                lstHeatlhIndicatorComment = dc.Database.SqlQuery<Models.IndicatorHealthIndicatorComment>("SPROCIndicatorCommentsForHealthIndicator @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorHealthIndicatorComment>();
                lstBudgetComment = dc.Database.SqlQuery<Models.IndicatorBudgetComment>("SPROCIndicatorCommentsForBudget @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorBudgetComment>();
                lstValidationComment = dc.Database.SqlQuery<Models.IndicatorValidationComment>("SPROCIndicatorCommentsForValidation @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorValidationComment>();
                lstSnapshotComment = dc.Database.SqlQuery<Models.IndicatorSnapshotComment>("SPRCIndicatorCommentsSnapshot @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorSnapshotComment>();
                lstPCOSComment = dc.Database.SqlQuery<Models.IndicatorsPCOSComment>("SPROCIndicatorCommentsForPCOS @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorsPCOSComment>();
                CCBListProj = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBAffected>();


                if (CCBlist.Count > 0 && CCBStatusList.Count > 0)
                {
                    CCBlist[0].lStatus = CCBStatusList;
                }
                if (dataFilList.Where(row => row.ContinuityID == ContinuityID).Count() > 0)
                {
                    master = dataFilList.Where(row => row.ContinuityID == ContinuityID).First();
                    Types = dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCgetTemplateContinuity @vchContinuityID = {0}, @intProgramTypeID={1}", new object[] { master.ContinuityID, master.ProgTypeID }).ToList<Models.IndicatorTypes>();

                    PlCList = CatPLC.Select(x =>
                        new SelectListItem()
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });

                    myModel.PlCList = PlCList;
                }
            }
            CommentsSelected = this.getSelectedComments(StoredComments, WWID, ContinuityID);
            
            myModel.BudgetList = Budgetlist;
            myModel.lstBudgetComments = lstBudgetComment;
            myModel.ScheduleList = Schedulelist;
            myModel.lstScheduleComment = lstScheduleComment;
            myModel.HealthList = Healthlist;
            myModel.lstHealthIndicatorComment = lstHeatlhIndicatorComment;
            myModel.PCOSList = PCOSlist.Where(x=> x.ContinuityID==ContinuityID);
            myModel.lstPCOSComment = lstPCOSComment;
            myModel.WWID = WWID;
            myModel.ContinuityID = ContinuityID;
            myModel.CommentsSelected = CommentsSelected;
            myModel.CCBList = CCBlist;
            myModel.CCBStatusCat = CCBStatusCat;
            myModel.Types = Types;
            myModel.Snapshotlist = Snapshot.Where(x => x.ContinuityID == ContinuityID);
            myModel.lstSnapshotComments = lstSnapshotComment;
            myModel.Users = userList;
            myModel.Comments = Comments;
            myModel.ValidationPhases = Phases.Where(x => x.PWContinuityID == ContinuityID);
            myModel.lstValidation = lstVal.Where<Models.ValidationModel>(x => x.ContinuityID == ContinuityID).ToList<Models.ValidationModel>();
            myModel.lstValidationComments = lstValidationComment;
            myModel.AffectedCCB = CCBListProj;
            ViewBag.Complete = complete;

            if (Types != null && Types.Count > 0)
                myModel.TemplateID = Types[0].id;
            else
                myModel.TemplateID = master.TemplateID;
            myModel.Template = null;

            List<Models.BPOPRJID> proj = ProgControl.getProjectID(Snapshot.First().PWID);
            myModel.projSel = proj;

            if (BudgetHeaders.Any())
            {
                myModel.BudgetHeaders = BudgetHeaders;
            }
            else {
                myModel.BudgetHeaders = null;
            }
            if (Notif != null)
            {
                return View("Indicator", myModel);
            }else
            {
                return View("IndicatorTest", myModel);
            }
            
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public ActionResult GetAllComments(int WWID, string PWContinuityID)
        {
            List<Models.AllComments> Comments = new List<Models.AllComments>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Comments = dc.Database.SqlQuery<Models.AllComments>("SPROCGetAllAppComments @WWID = {0}, @PWContinuityID = {1} ", WWID, PWContinuityID).ToList<Models.AllComments>();
            }
            return View("~/Views/Indicator/IndicatorComments.cshtml",Comments);
        }
        private List<Models.MasterModel> getPrograms(int? WWID)
        {

            List<Models.MasterModel> Masterlist=new List<Models.MasterModel>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Masterlist = dc.Database.SqlQuery<Models.MasterModel>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.MasterModel>();
            }

            ViewBag.WW = Masterlist[0].weekName;
            ViewBag.WWID = Masterlist[0].WWID;
            ViewBag.WeekStatus = Masterlist[0].weekStatus;

            return Masterlist;
        }
        public int getTemmplate (int intProgramTypeID)
        {
            List<Models.IndicatorTypes> lsType = new List<Models.IndicatorTypes>();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lsType = dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCGetIndicatorTemplate @ProgramTypeID = {0} ", intProgramTypeID).ToList<Models.IndicatorTypes>();
            }
            if (lsType.Count > 0)
            {
                return lsType[0].id;
            }
            else
                return 0;
        }
    }
}