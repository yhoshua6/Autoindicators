﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ProgTypes
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public List<MilestoneCatalog> Milestones { get; set; }
        public List<IndicatorCatalog> HealthIndicator { get; set;}
    }
}