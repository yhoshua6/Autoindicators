﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BugStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}