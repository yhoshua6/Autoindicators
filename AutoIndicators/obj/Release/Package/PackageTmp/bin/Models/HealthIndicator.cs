﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class HealthIndicator : MasterModel
    {
        public int? HealthId { get; set; }
        public int? PTHIid { get; set; }
        public string value { get; set; }
        public string Comments { get; set; }
        public int? PWID { get; set; }
        public string Milestone { get; set; }
        public int? oldHealthId { get; set; }
        public string oldValue { get; set; }
        public string oldComments { get; set; }
        public DateTime? deactivationDate { get; set; }
        public string currentActor { get; set; }
        public string oldActor { get; set; }
        public string descriptionLink { get; set; }
        public DateTime CDate { get; set; }
    }
}