﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BudgetModel : MasterModel
    {
        public int BudgetId { get; set; }
        public string BPOImportVersion { get; set; }
        public string currentActor { get; set; }
        public int? BPOImportId { get; set; }
        public string BPOProjId { get; set; }
        public string BPOProjName { get; set; }
        public int? BPOProjId_id { get; set; }
        public string CategoryName { get; set; }
        public int? CategoryId { get; set; }
        public int? Data1 { get; set; }
        public int? Data2 { get; set; }
        public int? Data3 { get; set; }
        public int? Data4 { get; set; }
        public int? Data5 { get; set; }
        public int? thisAmmnt { get; set; }
        public int? nextAmmnt { get; set; }
        public int DataDisc1 { get; set; }
        public int DataDisc2 { get; set; }
        public int DataDisc3 { get; set; }
        public int DataDisc4 { get; set; }
        public int DataDisc5 { get; set; }
        public int PWID { get; set; }
        public string Comments { get; set; }
        public int? RYG { get; set; }
        public int? Trend { get; set; }
        public int? TrendP { get; set; }
        public int? oldBudgetId { get; set; }
        public int? oldRYG { get; set; }
        public string oldComments { get; set; }
        public int? oldTrend { get; set; }
        public DateTime? deactivationDate { get; set; }
        public string oldActor { get; set; }
        public DateTime? commentDate { get; set; }
        public DateTime? oldCommentDate { get; set; }
        public int HIid { get; set; }
    }
    public class BudgetDiscrepancies
    {
        public int Data1 { get; set; }
        public int Data2 { get; set; }
        public int Data3 { get; set; }
        public int Data4 { get; set; }
        public int Data5 { get; set; }
        public int CategoryId { get; set; }
        public string BPOProjId { get; set; }
        public string PWContinuityID { get; set; }
        public int WWID { get; set; }
    }
    public class BudgetDiscrepancy : BudgetModel
    {
        public List<int> diff { get; set; }
        public List<string> Title { get; set; }
    }
    public class BudgetGraph
    {
        public int ammnt { get; set; }
        public string categoryName { get; set; }
        public string clasif { get; set; }
    }
    public class BudgetNoncompliant
    {
        public string Owner { get; set; }
        public string Email { get; set; }
        public string Project { get; set; }
        public string Trend { get; set; }
        public string RYG { get; set; }
    }
    public class BudgetUserChart
    {
        public string ProgName {get;set;}
        public string OwnerName {get;set;}
        public int BTI1 {get;set;}
        public int HC1 {get;set;}
        public int BTI2 {get;set;}
        public int HC2 {get;set;}
        public int BTI3 {get;set;}
        public int HC3 {get;set;}
        public int BTI4 {get;set;}
        public int HC4 {get;set;}
        
    }
    public class BudgetExtraColumns
    {
        public int id { get; set; }
        public string ColumnName { get; set; }
        public int WWID { get; set; }
    }
    public class BudgetExtraData
    {
        public int id { get; set; }
        public int Value { get; set; }
        public int ColumnId { get; set; }
        public int CategoryId { get; set; }
        public string ProjId { get;set;}
        public string PWContinuityID { get; set; }
    }
}