﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class CCB : MasterModel
    {
        DateTime _dttSubmitDate;
        DateTime _dttApprovedDate;
        DateTime _dttTargetImplementedDate;
        DateTime _dttActualImplementedDate;
        DateTime _dttCreateDate;
        DateTime _dttCutInDate;

        public int PWID { get; set; }
        public int intNumber { get; set; }
        public string strTitle { get; set; }
        public DateTime dttSubmitDate 
        { 
            get{return _dttSubmitDate ;}
            set
            {
                if (value == null)
                {
                    _dttSubmitDate = new DateTime();
                }
                else
                {
                    _dttSubmitDate = value;
                }
            }
        }
        public DateTime dttApprovedDate
        {
            get { return _dttApprovedDate; }
            set
            {
                if (value == null)
                {
                    _dttApprovedDate = new DateTime();
                }
                else
                {
                    _dttApprovedDate = value;
                }
            }
        }
        public DateTime dttTargetImplementedDate
        {
            get { return _dttTargetImplementedDate; }
            set
            {
                if (value == null)
                {
                    _dttTargetImplementedDate = new DateTime();
                }
                else
                {
                    _dttTargetImplementedDate = value;
                }
            }
        }
        public DateTime dttActualImplementedDate
        {
            get { return _dttActualImplementedDate; }
            set
            {
                if (value == null)
                {
                    _dttActualImplementedDate = new DateTime();
                }
                else
                {
                    _dttActualImplementedDate = value;
                }
            }
        }
        public DateTime dttCreateDate
        {
            get { return _dttCreateDate; }
            set
            {
                if (value == null)
                {
                    _dttCreateDate = new DateTime();
                }
                else
                {
                    _dttCreateDate = value;
                }
            }
        }
        public DateTime dttCutInDate
        {
            get { return _dttCutInDate; }
            set
            {
                if (value == null)
                {
                    _dttCutInDate = new DateTime();
                }
                else
                {
                    _dttCutInDate = value;
                }
            }
        } 
        public int intCCBStatus { get; set; }
        public string strStatus { get; set; }
        public string strInitiator { get; set; }
        public List<int> intProject { set; get; }
        public int intPriority { get; set; }
        public bool bModSpec { get; set; }
        public string strChangeDesc { get; set;}
        public string strChangeJustificaiton { get; set; }
        public string strChangeJustificaitonLink { get; set; }
        public bool bImpactedFunction { get; set; }
        public string strImpactedFunctionLink {get;set;}
        public string strSWRequired { get; set; }
        public bool bSWRequired { get; set; }
        public string strSWNumber { get; set; }
        public string strWINInvolved { get; set; }
        public string strWINAchieved { get; set; }
        public string bWINInvolved { get; set; }
        public string bWINAchieved { get; set; }
        public string strImpactHC { get; set; }
        public string strImpactHCLink {get; set;}
        public string strImpactBTI { get; set; }
        public string strImpactBTILink { get; set; }
        public bool bImpactPS { get; set; }
        public bool bImpactCOEM { get; set; }
        public bool bImpactMarketing { get; set; }
        public bool bImpactEngine { get; set; }
        public bool bImpactDesignSecurity { get; set; }
        public bool bImpactOther { get; set; }
        public string strImpactPSLink { get; set; }
        public string strImpactCOEMLink { get; set; }
        public string strImpactMarketingLink { get; set; }
        public string strImpactEngineLink { get; set; }
        public string strImpactDesignSecurityLink { get; set; }
        public string strImpactOtherLink { get; set; }
        public string strROIAnalysis { get; set; }
        public bool bROIAnalysis { get; set; }
        public string strROIAnalysisLink { get; set; }
        public string strImpactSchedule { get; set; }
        public bool strSOWReq { get; set; }
        public string strSOWReqLink { get; set; }
        public string strARsComments { get; set; }
        public bool bDWI { get; set; }
        public bool bDWA { get; set; }
        public string strActualBTI { get; set; }
        public string strProjectPhase { get; set; } 
        public string strCommetns { get; set; }
        public bool bactive { get; set; }
        public List<CCBStatus> lStatus { get; set; }
        public int intCCBid { get; set; }
        public List<CCBFiles> lsFile { get; set; }
        public string strUser { get; set; }
        public string strCommonApproved { get; set; }
        public string strCommonEmailApproved { get; set; }
        public string strComments { get; set; }
        public string strEmailInitiator { get; set; }
        public string strOtherProgram { get; set; }
        public int ChangeCategoryID { get; set; }
        public string MRD { get; set; }
        public string PRD { get; set; }
        public bool IsHW { get; set; }
        public bool IsSW { get; set; }
        public string strSWCategory { get; set; }
        public string strReleaseDate { get; set; }
        public string strSupportedBMC { get; set; }
        public string strSupportedBIOS { get; set; }
        public string strSupportedUtilities { get; set; }
        public List<CCBOtherProgramAffected> OPA { get; set; }
        public List<Models.CCBSProj> ProjAffected { get; set; }
    }
    public class CCBOtherProgramAffected
    {
        public int id { get; set;}
        public string Name { get; set;}
        public int PXTID {get; set;}
    }
    public class CCBResume:MasterModel
    {
        public List<CCBStatus> lStatus { get; set; }
        public string MRD { get; set; }
        public string PRD { get; set; }
    }
    public class CCBResponse
    {
        public string strRes { get; set; }
    }
    public class CCBPriority
    {
        public string strName { get; set; }
        public int intID { get; set; }
    }
    public class CCBStatus : CCB
    {
        public int intStatus { get; set; }
        public string strName { get; set; }
        public int intNumber { get; set; }

    }
    public class CatCCBStatus
    {
        public int intStatus { get; set; }
        public string strName { get; set; }
        public string strShortName { get; set; }
    }
    public class CCBFiles
    {
        public int intId { get; set; }
        public string strName { get; set; }
        public string strURL { get; set; }
    }
    
    public class CCBSProj
    {
        public string ContinuityID { get; set; }
        public string strPXTName { get; set; }
        public string strProgName { get; set; }
    }
    public class CCBsFunctionalTeam
    {
        public int intId {get; set ;}
        public string strDescription { get; set; }

    }
    public class CCBCategoryChange
    {
        public int intID { get; set; }
        public string strName { get; set; }
    }
    public class CCBAffected
    {
        public int intID { get; set; }
        public string strTitle { get; set; }
        public string strStatus { get; set; }
        public DateTime dttDate { get; set; }
        public string strOwner { get; set; }
        public string ContinuityID { get; set; }
        public string ContinuityIDOwner { get; set; }
        public string ProjectName { get; set; }
    }
}