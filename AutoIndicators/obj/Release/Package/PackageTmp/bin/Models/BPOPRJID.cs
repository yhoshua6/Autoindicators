﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BPOPRJID
    {
        public int BPOProjId { get; set; }
        public string BPOProjName { get; set; }
        public string ProgName { get; set; }
        public string OwnerName { get; set; }
        public string Email { get; set; }
        public string PWContinuityId { get; set; }
    }
}