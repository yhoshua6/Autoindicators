﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class DefaultWW
    {
        public int WWID { get; set; }
        public int weekstatus { get; set; }
        public string weekName { get; set; }
        public int focusID { get; set; }
    }
}