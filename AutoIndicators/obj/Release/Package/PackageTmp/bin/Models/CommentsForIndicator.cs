﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class CommentsForIndicator
    {
        public int id { get; set; }
        public int WWID { get; set; }
        public int category1 { get; set; }
        public int category2 { get; set; }
        public string OriginView { get; set; }
        public string PWContinuityID { get; set; }
    }
}