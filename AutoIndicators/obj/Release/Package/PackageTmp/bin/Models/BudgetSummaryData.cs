﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BudgetSummaryData
    {
        public int BudgetId { get; set; }
        public string Comments { get; set; }
        public int? RYG { get; set; }
        public int? Trend { get; set; }
        public string ContinuityID { get; set; }
        public int? TrendP { get; set; }
    }
}