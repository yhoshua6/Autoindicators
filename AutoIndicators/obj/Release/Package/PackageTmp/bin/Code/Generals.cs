﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.IO;
using System.Drawing;
using System.Data.OleDb;
using System.DirectoryServices;
using System.Net;
using System.Net.Mail;

namespace AutoIndicators.GeneralClasses
{
    public class General
    {
        private string _strConnection;
        
        public static void SenEmail(string strTitle,string strBody,string strTo,string imgUrl)
        {
            MailMessage mailObj = new MailMessage();
            mailObj.From = new MailAddress(strEmailFrom, "Snapshot");
            mailObj.To.Add(strTo);

            if (imgUrl.Length > 0)
            {
                Attachment att=new Attachment(imgUrl);
                
                string id="Img";
                att.ContentId=id;
                att.ContentDisposition.Inline=true;
                att.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;

                string htmlBody = @"<html><body><img src='cid:" + id + @"'/></body></html>";
                mailObj.Body=htmlBody;
                mailObj.Attachments.Add(att);

            }
            else
            {
                mailObj.Body = strBody;
            }
            mailObj.Subject = strTitle;
            
            mailObj.IsBodyHtml = true;

            SmtpClient SMTPServer = new SmtpClient(GeneralClasses.General.strSMTP);
            SMTPServer.Send(mailObj);
            mailObj.Dispose();
        }
        public static string strConnectionString
        {
            get
            {
                string[] strSqlcon = System.Configuration.ConfigurationManager.ConnectionStrings["DevSQLUserEntities"].ConnectionString.Substring(System.Configuration.ConfigurationManager.ConnectionStrings["DevSQLUserEntities"].ConnectionString.IndexOf("data source")).Split(';');
                string strConn = strSqlcon[0] + ";" + strSqlcon[1] + ";" + strSqlcon[2] + ";" + strSqlcon[3] + ";" + strSqlcon[4] + ";" + strSqlcon[5];
                return strConn;
            }
        }
        public static string strValLeadSite
        {
            get{
                return System.Configuration.ConfigurationManager.AppSettings["ValidationLead"]; 
            }
        }
        public static string strConnectionSecurityString
        {
            get
            {
                string[] strSqlcon = System.Configuration.ConfigurationManager.ConnectionStrings["DevSQLSecurity"].ConnectionString.Substring(System.Configuration.ConfigurationManager.ConnectionStrings["DevSQLUserEntities"].ConnectionString.IndexOf("data source")).Split(';');
                string strConn = strSqlcon[0] + ";" + strSqlcon[1] + ";" + strSqlcon[2] + ";" + strSqlcon[3] + ";" + strSqlcon[4] + ";" + strSqlcon[5];
                return strConn;
            }
        }
        public static string strEmailFrom
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["EmailFrom"]; }
        }
        public static int intApp
        {
            get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["App"].ToString()); }
        }
        public static string strSMTP
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["SMTP"]; }
        }
        public static byte[] GetImgFromExcel(string strPath)
        {
            //Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            //Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Open(strPath);

            var app = new Microsoft.Office.Interop.Excel.Application();
            var workbooks = app.Workbooks;

            byte[] bRes = null;

            try
            {
                var wb = workbooks.Open(strPath); // Fixed 


                

                foreach (Microsoft.Office.Interop.Excel.Worksheet ws in wb.Worksheets)
                {
                    Microsoft.Office.Interop.Excel.ChartObjects charts = (Microsoft.Office.Interop.Excel.ChartObjects)ws.ChartObjects(Type.Missing);
                    if (charts.Count > 0)
                    {
                        foreach (Microsoft.Office.Interop.Excel.ChartObject co in charts)
                        {
                            Microsoft.Office.Interop.Excel.Chart chart = (Microsoft.Office.Interop.Excel.Chart)co.Chart;
                            string strOutFile = AppDomain.CurrentDomain.BaseDirectory + "\\ValidationFiles\\1.jpg";
                            if (chart.Export(strOutFile))
                            {
                                MemoryStream ms = new MemoryStream();
                                Image img = Image.FromFile(strOutFile);
                                img.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                                bRes = ms.ToArray();
                                img.Dispose();
                                ms.Close();
                                ms.Dispose();
                                File.Delete(strOutFile);
                            }
                        }
                    }
                }
                wb.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(wb);
                app.Workbooks.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                System.Diagnostics.Process[] myProcesses;
                // Returns array containing all instances of Notepad.
                myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                foreach (System.Diagnostics.Process myProcess in myProcesses)
                {
                    myProcess.Kill();
                }
                
            }catch(Exception Ex)
            {
                StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
                rep.WriteLine("An internal error ocurr in GetDataFromExcel function for " + strPath + " : " + Ex.Message);
                rep.Close();

                app.Workbooks.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                System.Diagnostics.Process[] myProcesses;
                // Returns array containing all instances of Notepad.
                myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                foreach (System.Diagnostics.Process myProcess in myProcesses)
                {
                    myProcess.Kill();
                }
                throw new Exception(Ex.Message);
            }
            return bRes;
        }
        public string ExportDataToExcel(List<object> oSource)
        {
            Type mType = null;
            PropertyInfo[] pPropierties;
            DataTable dtData = new DataTable();

            //Create Table Structure 
            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = pInfo.Name.ToString().Trim();
                    if (!dtData.Columns.Contains(dc.ColumnName))
                    {
                        dtData.Columns.Add(dc);
                    }
                }
            }

            //Fill DataTable
            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();
                DataRow drNew = dtData.NewRow();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    drNew[pInfo.Name] = pInfo.GetValue(obj, null);
                }
                dtData.Rows.Add(drNew);
            }

            WriteToExcel(dtData, System.Web.HttpContext.Current.Server.MapPath("~/ValidationFiles/tmp.xlsx"));

            return "/ValidationFiles/tmp.xlsx";
        }
        public static List<object> ParseTo(object obj, DataTable dtResults)
        {
            List<object> oResults = new List<object>();
   
            ViewDataFilter fil = new ViewDataFilter();
            foreach (DataRow drData in dtResults.Rows)
            {
                Object objRes = new Object();
                objRes = fil.objParseTo(obj.GetType(), dtResults.Columns, drData);
                oResults.Add(objRes);
            }

            return oResults;
        }
        public void WriteToExcel(DataTable dt ,string location)
        {
            //instantiate excel objects (application, workbook, worksheets)
            Microsoft.Office.Interop.Excel.Application XlObj = new Microsoft.Office.Interop.Excel.Application();
            XlObj.Visible = false;
            Microsoft.Office.Interop.Excel.Workbook WbObj = (Microsoft.Office.Interop.Excel.Workbook)(XlObj.Workbooks.Add(""));
            Microsoft.Office.Interop.Excel.Worksheet WsObj = (Microsoft.Office.Interop.Excel.Worksheet)WbObj.ActiveSheet;

            //run through datatable and assign cells to values of datatable
            try
            {
                int row = 1; int col = 1;
                foreach (DataColumn column in dt.Columns)
                {
                    //adding columns
                    WsObj.Cells[row, col] = column.ColumnName;
                  
                    col++;
                }
                //reset column and row variables
                col = 1;
                row++;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //adding data
                    foreach (var cell in dt.Rows[i].ItemArray)
                    {
                        WsObj.Cells[row, col] = cell;
                        col++;
                    }
                    col = 1;
                    row++;
                }

               if(File.Exists(location))
                {
                    File.Delete(location);
                }
              WbObj.SaveCopyAs(location);
                
              System.Runtime.InteropServices.Marshal.ReleaseComObject(WbObj);
              System.Runtime.InteropServices.Marshal.ReleaseComObject(XlObj);
              System.Diagnostics.Process[] myProcesses;
              // Returns array containing all instances of Notepad.
              myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
              foreach (System.Diagnostics.Process myProcess in myProcesses)
              {
                  myProcess.Kill();
              }
              System.Threading.Thread.Sleep(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["time"]));

              StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
              rep.WriteLine("Lab ReportSaved ");
              rep.Close();
            }
            catch(Exception ex)
            {
                WbObj.Close();
                //XlObj.Workbooks.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(XlObj);
                System.Diagnostics.Process[] myProcesses;
                // Returns array containing all instances of Notepad.
                myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                foreach (System.Diagnostics.Process myProcess in myProcesses)
                {
                    myProcess.Kill();
                }
                System.Threading.Thread.Sleep(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["time"]));
                throw ex;
            }
                
            
        }

        public static DateTime GetLastRefresh(string strPath)
        {
            var app = new Microsoft.Office.Interop.Excel.Application();
            try
            {

                var workbooks = app.Workbooks;
                app.Visible = true;
                if (System.IO.File.Exists(strPath))
                {
                    var wb = workbooks.Open(strPath); // Fixed 

                    DateTime dttDateRefresh = DateTime.Parse(wb.SlicerCaches.Item[1].Slicers[1].SlicerCacheLevel.SlicerItems[1].Value.ToString());

                    wb.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wb);
                    app.Workbooks.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    System.Diagnostics.Process[] myProcesses;
                    // Returns array containing all instances of Notepad.
                    myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                    foreach (System.Diagnostics.Process myProcess in myProcesses)
                    {
                        myProcess.Kill();
                    }
                    System.Threading.Thread.Sleep(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["time"]));

                    return dttDateRefresh;
                }
            }
            catch (Exception Ex)
            {
                StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
                rep.WriteLine("An internal error ocurr in GetDataFromExcel function for " + strPath + " : " + Ex.Message);
                rep.Close();

                app.Workbooks.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                System.Diagnostics.Process[] myProcesses;
                // Returns array containing all instances of Notepad.
                myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                foreach (System.Diagnostics.Process myProcess in myProcesses)
                {
                    myProcess.Kill();
                }
                throw new Exception(Ex.Message);
            }
            return DateTime.Parse("01/01/1900");
        }
        public static string getIcon(int index)
        {
            DEOSecurity.Controller.DEOController control = new DEOSecurity.Controller.DEOController();
            List<DEOSecurity.Model.mNodes> lstMenu = control.GetMenu();

            return index-1>-1? lstMenu[index-1].strIcon :"";
        }
        public static DataSet GetDataFromExcel(string strPath)
        {
            DataSet dsData = new DataSet();
            
            var app = new Microsoft.Office.Interop.Excel.Application();
            try
            {
               
                var workbooks = app.Workbooks;
                app.Visible = true;
                if (System.IO.File.Exists(strPath))
                {
                    var wb = workbooks.Open(strPath); // Fixed 


                    foreach (Microsoft.Office.Interop.Excel.Worksheet ws in wb.Worksheets)
                    {
                        Microsoft.Office.Interop.Excel.ChartObjects charts = (Microsoft.Office.Interop.Excel.ChartObjects)ws.ChartObjects(Type.Missing);
                        Microsoft.Office.Interop.Excel.SlicerCaches slicer = (Microsoft.Office.Interop.Excel.SlicerCaches)wb.SlicerCaches;
                        int intTitle = 0;
                        if (charts.Count == 0)
                        {
                            DataTable dtRes = new DataTable();

                            Microsoft.Office.Interop.Excel.Range usedRange = ws.UsedRange;
                            int col = Convert.ToInt32(usedRange.Columns.Count);
                            int row = Convert.ToInt32(usedRange.Rows.Count);
                            dtRes.TableName = ws.Name;
                            int intCol = 0;
                            bool bFinish=false;

                            #region Configurate DT
                            for (int j = 1; j <= row - 1; j++)
                            {
                                if (intTitle == 0)
                                {
                                    for (int k = 1; k <= col; k++)
                                    {
                                        if (((Microsoft.Office.Interop.Excel.Range)usedRange[j, k]).Value != null)
                                        {
                                            dtRes.Columns.Add(((Microsoft.Office.Interop.Excel.Range)usedRange[j, k]).Value.ToString());
                                            intTitle = j;
                                            intCol++;
                                        }
                                    }
                                }
                                else
                                {
                                    int intNextCol = 0;
                                    
                                    for (int y = 1; y <= col; y++)
                                    {
                                        if (((Microsoft.Office.Interop.Excel.Range)usedRange[intTitle + 1, y]).Value != null)
                                        {
                                            intNextCol++;
                                        }
                                    }
                                    if (intCol !=col && !bFinish)
                                    {
                                        intCol = 0;
                                        intTitle++;
                                        if (dtRes.TableName.Equals("Owner List"))
                                            col--;
                                        dtRes = new DataTable();
                                        dtRes.TableName = ws.Name;
                                        for (int k = 1; k <= col; k++)
                                        {
                                            if (((Microsoft.Office.Interop.Excel.Range)usedRange[intTitle, k]).Value != null)
                                            {
                                                dtRes.Columns.Add(((Microsoft.Office.Interop.Excel.Range)usedRange[intTitle, k]).Value.ToString());
                                                intCol++;
                                            }
                                        }
                                        bFinish = true;
                                    }
                                }
                            }
                            #endregion
                            #region Data
                            for (int j = intTitle + 1; j <= row; j++)
                            {

                                DataRow drData = dtRes.NewRow();
                                for (int k = 1; k <= col; k++)
                                {
                                    if (((Microsoft.Office.Interop.Excel.Range)usedRange[j, k]).Value != null)
                                    {
                                        drData[k - 1] = ((Microsoft.Office.Interop.Excel.Range)usedRange[j, k]).Value;
                                    }
                                }
                                dtRes.Rows.Add(drData);
                            }
                            dsData.Tables.Add(dtRes);
                            #endregion
                        }
                    }
                    wb.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wb);
                    app.Workbooks.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    System.Diagnostics.Process[] myProcesses;
                    // Returns array containing all instances of Notepad.
                    myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                    foreach (System.Diagnostics.Process myProcess in myProcesses)
                    {
                        myProcess.Kill();
                    }
                    System.Threading.Thread.Sleep(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["time"]));
                }else
                {
                    throw new Exception("File not found in the side");
                }
            }
            catch(Exception Ex) {
                StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
                rep.WriteLine("An internal error ocurr in GetDataFromExcel function for " + strPath + " : " + Ex.Message);
                rep.Close();
              
                app.Workbooks.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                System.Diagnostics.Process[] myProcesses;
                // Returns array containing all instances of Notepad.
                myProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                foreach (System.Diagnostics.Process myProcess in myProcesses)
                {
                    myProcess.Kill();
                }
                throw new Exception(Ex.Message);
            }
            return dsData;
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public static DataTable LinqQueryToDataTable(IEnumerable<dynamic> v)
        {
            //We really want to know if there is any data at all
            var firstRecord = v.FirstOrDefault();
            if (firstRecord == null)
                return null;

            /*Okay, we have some data. Time to work.*/

            //So dear record, what do you have?
            PropertyInfo[] infos = firstRecord.GetType().GetProperties();

            //Our table should have the columns to support the properties
            DataTable table = new DataTable();

            //Add, add, add the columns
            foreach (var info in infos)
            {

                Type propType = info.PropertyType;

                if (propType.IsGenericType
                    && propType.GetGenericTypeDefinition() == typeof(Nullable<>)) //Nullable types should be handled too
                {
                    table.Columns.Add(info.Name, Nullable.GetUnderlyingType(propType));
                }
                else
                {
                    table.Columns.Add(info.Name, info.PropertyType);
                }
            }

            //Hmm... we are done with the columns. Let's begin with rows now.
            DataRow row;

            foreach (var record in v)
            {
                row = table.NewRow();
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    row[i] = infos[i].GetValue(record) != null ? infos[i].GetValue(record) : DBNull.Value;
                }

                table.Rows.Add(row);
            }

            //Table is ready to serve.
            table.AcceptChanges();

            return table;
        }
        public static Models.Users GetInfoUserAD(string strAccount)
        {
            Models.Users User = new Models.Users();

            String account = strAccount.Substring(strAccount.IndexOf('\\') + 1);
            DirectoryEntry entry = new DirectoryEntry("LDAP://amr.corp.intel.com");
            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = String.Format("(sAMAccountName={0})", account);

                search.PropertiesToLoad.Add("displayname"); 
                search.PropertiesToLoad.Add("mail");  // e-mail addressead
                search.PropertiesToLoad.Add("telephoneNumber");
                search.PropertiesToLoad.Add("department");
                search.PropertiesToLoad.Add("title"); 

                SearchResult result = search.FindOne();
                if (result != null)
                {
                    User.strName = result.Properties["displayname"][0].ToString();
                    User.strEmail=result.Properties["mail"][0].ToString();
                    User.strTelephone = result.Properties["telephoneNumber"][0].ToString();
                    User.strDepartment = result.Properties["department"][0].ToString();
                    User.strTitle = result.Properties["title"][0].ToString();
                }
                else
                {
                    User.strSystemMessage= "Unknown User";
                }
            }
            catch (Exception ex)
            {
                User.strSystemMessage= ex.Message;
            }

            return User;
        }
        public static string FindEmailByAccount(string strAccount)
        {

            String account = strAccount.Substring(strAccount.IndexOf('\\') + 1);
            DirectoryEntry entry = new DirectoryEntry("LDAP://amr.corp.intel.com");
            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();
             int intTry = 0;

            try
            {
                Search:
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = String.Format("(sAMAccountName={0})", account);
                search.PropertiesToLoad.Add("mail");  // e-mail addressead

                SearchResult result = search.FindOne();
                if (result != null)
                {
                    return result.Properties["mail"][0].ToString();
                }
                else
                {

                    switch (intTry)
                    {
                        case 0:
                            entry = new DirectoryEntry("LDAP://ccr.corp.intel.com");
                            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
                            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();
                            intTry++;
                            goto Search;
                            break;
                        default:
                            return "Unknown User";
                    }


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string FindAccountByEmail(string pEmailAddress)
        {
            DirectoryEntry entry = new DirectoryEntry("LDAP://amr.corp.intel.com");
            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();

            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = string.Format("(proxyaddresses=SMTP:{0})", pEmailAddress);
                search.PropertiesToLoad.Add("sAMAccountName");  // e-mail addressead

                SearchResult result = search.FindOne();
                if (result != null)
                {
                    return result.Properties["sAMAccountName"][0].ToString();
                }
                else
                {
                    return "Unknown User";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string FindDisplayNametByEmail(string pEmailAddress)
        {
            DirectoryEntry entry = new DirectoryEntry("LDAP://amr.corp.intel.com");
            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();
            int intTry = 0;

            try
            {
                Search:
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = string.Format("(proxyaddresses=SMTP:{0})", pEmailAddress);
                search.PropertiesToLoad.Add("displayname");  // e-mail addressead

                SearchResult result = search.FindOne();
                if (result != null)
                {
                    return result.Properties["displayname"][0].ToString();
                }
                else
                {
                    
                    switch(intTry)
                    {
                        case 0 :
                            entry = new DirectoryEntry("LDAP://ccr.corp.intel.com");
                            entry.Username = System.Configuration.ConfigurationManager.AppSettings["UserApp"].ToString();
                            entry.Password = System.Configuration.ConfigurationManager.AppSettings["PassUserApp"].ToString();
                            intTry++;
                            goto Search;
                            break;

                        default :
                            return "Unknown User";
                    }
                    
                   
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string GetRandomColor()
        {
            Random rand = new Random();
            Color[] Colors =typeof(Color).GetProperties(BindingFlags.Public | BindingFlags.Static).Select(propInfo => propInfo.GetValue(null, null)).Cast<Color>().ToArray();
            string[] ColorNames =typeof(Color).GetProperties(BindingFlags.Public | BindingFlags.Static).Select(propInfo => propInfo.Name).ToArray();
            return ColorNames[rand.Next(0, Colors.Length)];
        }
    }
    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = true)]
    public class LabelSecurityAttribute : System.Attribute
    {
        public readonly string Label;

        public LabelSecurityAttribute(string label)
        {
            this.Label = label;
        }
    }
    //public class Security
    //{
    //    public static List<DEOSecurity.Model.Property> getAttibutes(object o)
    //    {
    //        List<DEOSecurity.Model.Property> lstAttributes = new List<DEOSecurity.Model.Property>();

    //        PropertyInfo[] pPropierties = o.GetType().GetProperties();

    //        foreach (PropertyInfo pInfo in pPropierties)
    //        {
    //            DEOSecurity.Model.Property prop = new DEOSecurity.Model.Property();
    //            prop.strName=pInfo.CustomAttributes.First().ConstructorArguments.First().Value.ToString();
    //            lstAttributes.Add(prop);
    //        }

    //        return lstAttributes;
    //    }
    //}
    public class ViewDataFilter
    {
        public List<object> oSource { get; set; }
        public List<object> oResults {get ; set; }
        public List<Models.mFilter> mFiltersToShow { get; set; }
        public DataTable dtFilFields { get; set; }
        public DataTable dtData { get; set; }
        public DataTable dtResults { get; set; }
        
        public ViewDataFilter() { }

        public void vCreateFilter()
        {
            Type mType = null;
            PropertyInfo[] pPropierties;
            dtFilFields = new DataTable();

            //Create Table Structure 
            if (oSource == null)
                return;

            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    if (mFiltersToShow != null && mFiltersToShow.Exists(row => row.strField.Equals(pInfo.Name.ToString())))
                    {
                        DataColumn dc = new DataColumn();
                        dc.ColumnName = pInfo.Name;
                        if (!dtFilFields.Columns.Contains(dc.ColumnName))
                        {
                            dtFilFields.Columns.Add(dc);
                        }
                    }
                }
            }
            //Fill DataTable
            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();
                DataRow drNew = dtFilFields.NewRow();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    if (mFiltersToShow != null && mFiltersToShow.Exists(row => row.strField.Equals(pInfo.Name)))
                    {

                        drNew[pInfo.Name] = pInfo.GetValue(obj, null);

                    }
                }
                dtFilFields.Rows.Add(drNew);
            }

            //vCreateData();
            dtData = dtFilFields;
        }
        public void vParseTo(object obj)
        {
            oResults = new List<object>();
            foreach (DataRow drData in dtResults.Rows)
            { 
                Object objRes=new Object();
                objRes = objParseTo(obj.GetType(), dtResults.Columns, drData);
                oResults.Add(objRes);
            }
      }
        public object objParseTo(Type mType,DataColumnCollection colColumns,DataRow drData)
        {
            Object obj = (Object)Activator.CreateInstance(mType);
            
            PropertyInfo[] pPropierties;

            
            pPropierties = mType.GetProperties();

            foreach (DataColumn dcData in colColumns)
            {
                foreach (PropertyInfo pInfoRes in pPropierties)
                {
                    if (pInfoRes.Name.Equals(dcData.ColumnName.ToString()))
                    {
                        try
                        {
                            pInfoRes.SetValue(obj, drData[dcData].ToString());
                        }
                        catch (Exception Ex)
                        {
                            if (Ex.Message.Contains("Int") && !String.IsNullOrEmpty(drData[dcData].ToString()))
                            {
                                pInfoRes.SetValue(obj, Convert.ToInt32(drData[dcData]));
                            }
                            if (Ex.Message.Contains("bool") && !String.IsNullOrEmpty(drData[dcData].ToString()))
                            {
                                pInfoRes.SetValue(obj, Convert.ToBoolean(drData[dcData]));
                            }
                            if(Ex.Message.Contains("Decimal") && !String.IsNullOrEmpty(drData[dcData].ToString()))
                            {
                                pInfoRes.SetValue(obj, Convert.ToDecimal(drData[dcData]));
                            }
                            if (Ex.Message.Contains("DateTime") && !String.IsNullOrEmpty(drData[dcData].ToString()))
                            {
                                pInfoRes.SetValue(obj, Convert.ToDateTime(drData[dcData]));
                            }
                        }
                    }
                }
            }
            return obj;
        }
        private void vCreateData()
        {
            Type mType = null;
            PropertyInfo[] pPropierties;
            dtData = new DataTable();

            //Create Table Structure 
            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = pInfo.Name.ToString().Trim();
                    if (!dtData.Columns.Contains(dc.ColumnName))
                    {
                        dtData.Columns.Add(dc);
                    }
                }
            }

            //Fill DataTable
            foreach (object obj in oSource)
            {
                mType = obj.GetType();

                pPropierties = mType.GetProperties();
                DataRow drNew = dtData.NewRow();

                foreach (PropertyInfo pInfo in pPropierties)
                {
                    drNew[pInfo.Name] = pInfo.GetValue(obj, null);
                }
                dtData.Rows.Add(drNew);
            }
        }
        public string strValidFilterString(string fil)
        {
            string[] strConditonals = fil.Replace(" AND ",",").Split(',');
            string strRes="";

            foreach(string strPart in strConditonals)
            {
                if(dtData.Select(strRes +strPart).Length>0)
                    strRes = strRes + strPart+" AND ";
            }
            if (strRes.Length > 0)
                return strRes.Substring(0, strRes.LastIndexOf(" AND "));
            else
                return strRes;
        }
        public static List<Models.Calendar> Calendar()
        {
            List<Models.Calendar> list = new List<Models.Calendar>();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                List<Models.Calendar> cals = new List<Models.Calendar>();

                for (int i = 0; i < Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NoYear"]); i++)
                {
                    cals = dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", DateTime.Now.Year-i).ToList<Models.Calendar>();

                    foreach (Models.Calendar cal in cals)
                    {
                        cal.Year = DateTime.Now.Year - i;
                        list.Add(cal);
                    }
             
                }

            }
            return list;
        }
    }

}