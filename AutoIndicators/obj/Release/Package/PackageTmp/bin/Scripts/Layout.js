﻿var ctrl = false;
var shift = false;
var module="";
var skip = "../";
var wwid = "0";
var notifi = null;
var confirmResult = false;

$(document).ready(function () {
    //element focus is set at layout from viewbag, thats set by the controller.
    try {
        $('#F' + elementFocus).get(0).scrollIntoView();
    }
    catch (err) {
        //alert(err.message);
    }
    //PutNotify($('[data-addnotify]'));

    $.post('/Notif/TotalNotif', '', function (response) {
        if (response.length > 0) {
            notif = response;
            $('#TotNotif').html('<br />You have <a href="/Notif/index" target="_blank" style="color:white; text-decoration:underline">' + response.length + '</a> notifications');
            $('#TotNotif').show();
        }else
        {
            notifi = null;
            $('#TotNotif').html('<br />You have <a href="/Notif/index" target="_blank" style="color:white; text-decoration:underline">' + 0 + '</a> notifications');
        }
    })
    
});

$('td').click(function () {
    //notifications call
    showPopUpShiftClick(this, skip);
})

$('div').click(function () {
    //notifications call
    showPopUpShiftClick(this, skip);
})

$(document).keydown(function (event) {
    if (event.which == "17")
        ctrl = true;
    if (event.which == "16")
    {
        shift = true;
    }
});

function PutNotify(obj)
{
    for (i = 0; i < obj.length; i++) {
        var style = $(obj[i]).data('alignoti') != undefined ? $(obj[i]).data('alignoti') : 'bottom';
        $(obj[i]).css('vertical-align', 'bottom');
        if ($(obj[i]).data('titlenoti') != undefined) {
            $(obj[i]).append('<div style="position: relative; right: 0px;"><div class="notify" title="' + $(obj[i]).data('titlenoti') + '">N</div></div>');
            $(obj[i]).css('padding', '0px')
            $(obj[i]).css('padding-top', '10px')
            $(obj[i]).css('padding-left', '10px')
        }
        else
            if ($(obj[i]).data('countnotify') != undefined) {
                var num = $(obj[i]).data('countnotify') > 9 ? "+9" : $(obj[i]).data('countnotify');
                if (num > 0)
                    $(obj[i]).append('<div class="notify" style="' + style + ':0">' + num + '</div>');
            } else {
                $(obj[i]).append('<div class="notify" style="' + style + ':0">N</div>');
            }
    }
}

$(document).keyup(function () {
    ctrl = false;
    shift = false;
});

$('th').mouseover(function () {
    if ($(this).attr('id') != undefined || $(this).text().length>0)
        $(this).css('cursor', 'pointer');
    else
        $(this).css('cursor', 'arrow');
})

$('th').click(function () {
    
    if ($(this).attr('id') != undefined) {
        if ($(this).text() == "Comment")
        {
            showPopUpCtrlClick($(this).attr("id"), $(this).text(), $(this).attr("data-view"), skip);
        }else
        {
            showPopUpCtrlClick($(this).attr("id"), $(this).text(), null, skip);
        }
    }
})
$('.FHtml').click(function () {

    if ($(this).attr('id') != undefined) {
            showPopUpCtrlClick($(this).attr("id"), $(this).text(), null, skip);
    }
})

function showPopUpCtrlClick(property, value, module, skip) {
    $('#Modal').data('url', skip + '/FreeHTML/FreeHTMLRegView');
    var url = $('#Modal').data('url');

    if ((ctrl && value.length > 0) || ((ctrl && value.length > 0) && property == 'Mod' && value.length > 0)) {
        ctrl = false;//turn off ctrl down signal.
        $.post(url, { Property: property, Value: value, View: module, WWID: wwid }, function (data) {
            $('#modalContainer').html(data);
            $('#Modal').modal("show");
            $('#myModalLabel').html("Free HTML for " + value);
            $('#modalcontent').width(1050);
            $('#modalcontent').height(1090);
        });
    }
}

function showPopUpShiftClick(obj, skip) {
    $('#Modal').data('url', skip + 'Notif/NotifQuery');
    var url = $('#Modal').data('url');
   
    if (shift && $(obj).data('notif') != undefined) {
        shift = false;//turn off shift down signal. NotifData carries all the context from the app that invoked, every app should set it on razor

        $.post(url, { NotifData: $(obj).data('notif') }, function (data) {
            $('#modalContainer').html(data);
            $('#Modal').modal("show");
            $('#myModalLabel').html('Notifications in ' + module + ' for ' + $(obj).data('notifproj'));
            $('#modalcontent').width(1500);
            $('#modalcontent').height('100%');
            $('.modal-dialog').css('margin-left', '15%');
        });
    }
}
function showPopUpClick(property, value, module, skip) {
    $('#Modal').data('url', skip + 'FreeHTML/FreeHTMLRegView');
    var url = $('#Modal').data('url');

    if (property == 'Mod' && value.length > 0) {
        $.post(url, { Property: property, Value: value, View: module, WWID: wwid }, function (data) {
            $('#modalContainer').html(data);
            $('#Modal').modal("show");
            $('#myModalLabel').html("Free HTML for " + value);
            $('#modalcontent').width(1050);
            $('#modalcontent').height(1090);
        });
    }
}
function ProgDetailEdit(ID) {
    //Show the Modal to Edit the Current Program
    var textPWID = ID.toString();
    var text = "/ProgEdit?PWID=";
    var urlLink = text + textPWID;    
    $('#Modal').data('url',urlLink); 
    var url = $('#Modal').data('url');
    $.get(url, function(data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#myModalLabel').html("Program Edit");
        $('#modalcontent').width(600);
        $('#modalcontent').height(1200);
    });
}

function RedFlag(ID, information) {
    var textWWID = ID.toString();
    var text = "/Snapshot/RedFlag?PWID=";
    var urlLink = text + textWWID;
    $('#Modal').data('url', urlLink);
    $('#modalcontent').width(800);
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#myModalLabel').html("Red Flagged Health Indicators for " + information)
    });
}
function expandProj(btn,ContinuityID) {
    if ($(btn).attr('src') == '/Content/images/minus.png') {
        $('#ProjectDetail-' + ContinuityID).hide();
        $(btn).attr('src', '/Content/images/plus.jpg');
    }
    else {
        $('#ProjectDetail-' + ContinuityID).show();
        $(btn).attr('src', '/Content/images/minus.png');
    }
}
function FancyConfirm(text, action) {
    $("#dialog-confirm").html(text);
    $("#dialog-confirm").attr('title', 'Confirm');
    $(".ui-dialog-titlebar-close").hide();
    $("#dialog-confirm").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        close: function () {
            return confirmResult;
        },
        buttons: {
            "OK": function () {
                eval(action);
                $(this).dialog("close");
                
            },
            Cancel: function () {
               $(this).dialog("close");
            }
        }
    });
}
window.alert = function (text,width, button,closeButton, title)
{
    button = button != undefined ? button : true;
    title = title != undefined ? title : '';
    width = width != undefined ? width : 'auto';
    closeButton=closeButton!=undefined ? closeButton : true;

    $('#dialog').attr('title', title.length > 0 ? title : 'Alert');
    $('#dialog').html(text);
    
    if(button)
    { 
        $('#dialog').dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        })
    }else
    {
        $('#dialog').dialog({
            modal: true
        })
    }
    if(!closeButton)
    {
        $('#dialog').dialog('option', 'dialogClass','no-close');
    } else
    {
        $('#dialog').dialog('option', 'dialogClass', '');
    }
    $('#dialog').dialog('option', 'width', width);
    $('#dialog').css("zIndex", 2000)
}
$(function () {
    if (navigator.appVersion.indexOf('Chrome')==-1) {
        alert('<p>If you want to use this site, you must access through Google Chrome</p><p style="text-align:center;"><a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank"><img src="/Content/images/chrome.jpg" width="325" height="175" /></a></p>', 500, false, false, 'Info')
    }
})

function activedeactive(active)
{
    $.post("/Search/ActiveDeactive", { Activate: active }, function (res) {
        window.location.reload();
    });
    
}
function AddFeedBack()
{
    $('#Modal').data('url', skip + '/BugReporting/AddBug');
    var url = $('#Modal').data('url');

    $.post(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#myModalLabel').html("Feedback ");
        $('#modalcontent').width(800);
        $('#modalcontent').height(600);
    });
}
function Completed() {
    $('#cmbfilWW option[value="Co"]').attr('Selected', 'Selected');
    $('#SearchForm').attr('action', '/Search/Search?Module=Snapshot');
    search();
}
function Current() {
    $('#cmbfilWW option[value="Cu"]').attr('Selected', 'Selected');
    $('#SearchForm').attr('action', '/Search/Search?Module=Snapshot');
    search();
}
function Close() {
    $.post("/Search/DestroyWWSession", function (res) {
        //confirm('bye');
    });
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
