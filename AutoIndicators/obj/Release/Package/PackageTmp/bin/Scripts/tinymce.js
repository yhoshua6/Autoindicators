﻿var ContinuityID = "";
var Owner = "";
var PXT = "";
var ProgramType = "";
var view = 0;
var stand = undefined;

function cancelEdit(row) {
    tinymce.get('txtDescription_' + row).remove();
    $('#txtDescription_' + row).val($('#txtDesc_' + row).html());

    $('#txtDesc_' + row).show();
    $('#txtDescription_' + row).hide();

}
function showAcronyms(Row, abbreviation) {
    if (abbreviation == undefined)
    {
        abbreviation = "";
    }
    var urlLink = "/Acronyms/AcronymList_noLayout?Abbreviation=" + abbreviation + "&Row=" + Row + "&ContinuityID=" + ContinuityID + "&WWID=" + wwid + "&Owner=" + Owner + "&ProgType=" + ProgramType + "&PXT=" + PXT;
    $('#myModalLabel').html("Select an Acronym to add to your comment");

    $('#Modal').data('url', urlLink);
    var url = $('#Modal').data('url');
    $.post(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        $('#modalcontent').height(450);
    });
}
function AddLinks(Row) {
    
    var word = tinyMCE.get('txtDescription_' + Row).selection.getContent({ format: 'text' }).trim();

    if (word.length > 0) {
        var urlLink = "/Snapshot/AddAcronymLink?Row=" + Row + "&Word=" + word.replace('&', '^') + '&WWID=' + wwid + "&ContinuityID=" + ContinuityID + "&Owner=" + Owner + "&ProgType=" + ProgramType + "&PXT=" + PXT;
        $('#myModalLabel').html("Save a Link");

        $('#Modal').data('url', urlLink);
        var url = $('#Modal').data('url');
        $.post(url, function (data) {
            $('#modalContainer').html(data);
            $('#Modal').modal("show");
            $('#modalcontent').width(600);
            $('#modalcontent').height(450);
            $('#Modal').css("zIndex", 1100)
            $('#Modal .close').click(function(){
                $($('#txtDesc_' + Row).parent()).scrollToMe();
            })
            
        });
    } else
        alert('The word is empty');
}

function EditLink(Row, Word) {
    Word = Word.replace("&", "^").trim();

    var urlLink = "/Snapshot/AddAcronymLink?Row=" + Row + "&Word=" + Word + '&WWID=' + wwid + "&ContinuityID=" + ContinuityID + "&Owner=" + Owner + "&ProgType=" + ProgramType + "&PXT=" + PXT;;
    $('#myModalLabel').html("Save a Link");

    $('#Modal').data('url', urlLink);
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        $('#modalcontent').height(450);
    });
}

function showProperLinks(Row) {
    var urlLink = "/ProperLinks/ProperLinkList_noLayout?Row=" + Row;
    $('#myModalLabel').html("Select an Acronym to add to your comment");

    $('#Modal').data('url', urlLink);
    var url = $('#Modal').data('url');
    $.get(url, function (data) {
        $('#modalContainer').html(data);
        $('#Modal').modal("show");
        $('#modalcontent').width(600);
        $('#modalcontent').height(450);
    });
}
function Edit(id, WWID, continuityID, owner, programType, pxt) {
    ContinuityID=continuityID;
    Owner=owner;
    ProgramType = programType;
    PXT = pxt;
    cleanTextRich();
    $('#txtDescription_' + id).show();
    showRichText(id, WWID)
    $("#txtDesc_" + id).hide();
}
function cleanTextRich() {
    $($("div[class*='mce-tinymce']")).parent().each(function (index) {
        var id = $($($("div[class*='mce-tinymce']").parent()[0]).children()[2]).attr("id");
        if (tinymce.get(id) != null) {
            tinymce.get(id).remove();
        }
            $("#" + id).hide();
    })

    var divs = document.querySelectorAll('div[data-div]');
    var area = document.querySelectorAll('textarea[data-area]');

    for (i = 0; i < divs.length; i++) {
        $(divs[i]).show();
    }
}

function validAcronym(word)
{
    if (word!=undefined) {
        if (word.indexOf('"') > -1) {
            return false;
        } else if (word.indexOf('{') > -1) {
            return false;
        } else if (word.indexOf('}') > -1) {
            return false;
        } else if (word.indexOf('-') > -1) {
            return false;
        } else if (word.indexOf('=') > -1) {
            return false;
        } else if (word.indexOf(';') > -1) {
            return false;
        } else if (word.indexOf('.') > -1) {
            return false;
        } else if (word.indexOf(':') > -1) {
            return false;
        } else if (word.indexOf('$') > -1) {
            return false;
        }
    }
    return true;
}
function AutoAcronym(texto,origin) {
    var URL = "/Snapshot/SearchAcronyms";

    if (texto != null) {
        $.post(URL, { TextAcronyms: texto, WWID: wwid, ContinuityID: ContinuityID, Owner: Owner, ProgType: ProgramType, PXT: PXT, View: view }, function (data) {
            tinyMCE.get(origin).setContent(data);
        }
            );
    }

}
function Save(Row) {
    var URL = "/Snapshot/AddDBLink"
    var pAbbreviation = $('#pAbbreviation').val();
    var pLink = $('#pLink').val();
    //var pTerm = $('#pTerm').val();
    var pDefinition = $('#pDefinition').val();
    Row = Row == undefined ? '' : Row;

    if(pLink.length>0)
    {
        if (!validateURL(pLink))
        {
            $('#warning').show();
            $('#warningMessage').text("Please enter a valid Url");
            return;
        }
    }
    if (ContinuityID!=undefined && ContinuityID.length == 0)
    {
        $('#warning').show();
        $('#warningMessage').text("Please select a program before");
        return;
    }
 
    $.post(URL, { Abbreviation: pAbbreviation.replace("&", "^"), Link: pLink, Definition: pDefinition, Text: tinyMCE.activeEditor.getContent(), ContinuityID: ContinuityID, WWID: wwid, View: view }, function (response) {
        if (stand != undefined) {
            $.post("/Snapshot/SearchAcronyms", { TextAcronyms: tinyMCE.activeEditor.getContent(), WWID: wwid, ContinuityID: ContinuityID, Owner: Owner, ProgType: ProgramType, PXT: PXT, View: 0 }, function (data) {
                tinyMCE.get(Row).setContent(data);
            });
        } else {
            $.post("/Snapshot/SearchAcronyms", { TextAcronyms: tinyMCE.activeEditor.getContent(), WWID: wwid, ContinuityID: ContinuityID, Owner: Owner, ProgType: ProgramType, PXT: PXT, View: 0 }, function (data) {
                tinyMCE.get('txtDescription_' + Row).setContent(data);
            });
        }
        
        if($('#BModal').is(':visible'))
            $('#BModal').modal('hide');
        else
            $('#Modal').modal('hide');

        $($('#txtDesc_' + Row).parent()).scrollToMe();
    });
}
function validateURL(url) {

    var reurl = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;

        return reurl.test(url);
}
jQuery.fn.extend({
    scrollToMe: function () {
        var x = jQuery(this).offset().top - 100;
        jQuery('html,body').animate({ scrollTop: x }, 500);
    }
});