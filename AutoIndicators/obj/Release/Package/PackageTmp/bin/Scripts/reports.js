﻿var rptSource = null;

function LoadReport(val)
{
    $('#ReportViewer').load('/Reports/Viewer', { WWID:wwid, ReportType: val });
}
function LoadReportUser(rpt,range)
{
    if (range.split('-').length > 0)
    {
        if (Number(range.split('-')[1]) < Number(range.split('-')[0])) {
            alert('Invalid range. The Second WW must be greater than the first one');
            return;
        }
    }
    $('#ReportViewer').load('/Reports/Viewer', { WWID: 0, ReportType: rpt,Range :range });
}
function changeToGridReport(user,rpt) {
    var tbl = $('#ReporData');
    var type = $('#ReporData').data("type");
    var obj = $.paramquery.tableToArray(tbl);
    var width = 0;
    var height = 0;

    switch(rpt)
    {
        case 1:
        case 7:
            width = '100%';
            height = 600;
            break;
        case 2:
        case 3:
            height = 450;
            width = 1200;
            break;
        case 6: width = 1400;
            height = 600;
            break;
    }

    var newObj = {
        width: width,
        height: height,
        hwrap: false,
        wrap: false,
        title: "Results",
        //DataGrid Menu
        editable: false,
        collapsible: true,
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local',
        selectionModel: {
            //type: 'cell'
            type: 'cell'
        },
        numberCell: { width: 50, resizable: true },
        hoverMode: 'row',
        scrollModel: { autoFit: false }
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].width = "150";
    }
    
    newObj.colModel[0].width = 50;
    newObj.colModel[1].width = 80;
    newObj.colModel[2].width = 400;
    newObj.colModel[3].width = 100;
    newObj.colModel[6].hidden= true;
    

    if (user == 0) {
        newObj.colModel[1].hidden = true;
    } else
    {
        switch(rpt)
        {
            case 1:
                newObj.colModel[0].hidden = true;
                newObj.colModel[1].hidden = true;
                newObj.colModel[2].hidden = true;
                newObj.colModel[3].hidden = true;
                newObj.colModel[7].hidden = true;
                break;
            case 7:
                newObj.colModel[0].hidden = true;
                newObj.colModel[1].hidden = true;
                newObj.colModel[2].hidden = true;
                newObj.colModel[4].hidden = true;
                newObj.colModel[5].hidden = true;
                break;
        }
    }



    var items = newObj.dataModel.data;
    var result = [];

    for (x = 0; x < newObj.colModel.length; x++) {
        Val = [];
        for (var item, i = 0; item = items[i++];) {
            if (Val.indexOf(item[x]) < 0) {
                Val[Val.length] = item[x];
            }
        }
        result[result.length] = Val;
    }
    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = {
            type: 'select',
            attr: "multiple",
            cache: null,
            options: result[i],
            condition: 'range',
            listeners: ['change'],
            init: function () {
                $(this).pqSelect({ checkbox: true, radio: true, width: '100%' });
            }
        };
    }
    newObj.colModel[0].filter = null;

    newObj.cellSelect = function (evt, ui) {
        if (ui.rowData) {
            gIndex = ui.rowIndx;
            if (user > 0 && ui.colIndx!=2) {
                switch (rpt) {
                    case 1: LoadChart(ui.rowData[6],rpt);
                        break;
                    case 2:
                    case 3: //LoadChart(ui.rowData[6], rpt, rptSource);
                        break;
                    case 7: LoadChart(ui.rowData[6], rpt, undefined,ui.rowData[3]);
                        break;
                }
            }
        }
    };

    newObj.pageModel = { rPP: 1000, type: "local" };

    $grid = $('#dgReport').pqGrid(newObj);


    try {
        if ($('#dgReport').pqGrid() != null)
            $('#dgReport').pqGrid("destroy");

        $grid = $('#dgReport').pqGrid(newObj);

        $($($('#dgReport').children()[0]).children()[1]).click();
        $($($('#dgReport').children()[0]).children()[1]).click();

    } catch (err) { }
}
function LoadChart(id,rpt,Source,prog)
{
    if (Source == undefined) {
        $.post('/Reports/Chart', { Owner: id, Report: rpt, Type: 1, WWIDS: $('#From').val(), WWIDE: $('#To').val(), Prog:prog }, function (data) {
            $('#Chart').html(data);
            $('#Chart').show();
            $('#dgReport').hide();
        });
    }else
    {
        $.post('/Reports/Chart', { Owner: id, Report: rpt, Type: 1, WWIDS: $('#From').val(), WWIDE: $('#To').val(), DataHI: Source, DataSchedule: Source }, function (data) {
            $('#Chart').html(data);
            $('#Chart').show();
            if (rpt != 2 && rpt != 3)
                $('#dgReport').hide();
        });
    }
}