﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Dynamic;
using Newtonsoft.Json;
//using System.Text.RegularExpressions;
//using HtmlAgilityPack;
//using System.Text;
//using AutoIndicators.ExtensionMethods;
using System.Data;
//using System.Data.SqlClient;
//using Microsoft.ApplicationBlocks.Data;
//using System.Collections.Specialized;

namespace AutoIndicators.Controllers
{
    public class RoutesDemoController : Controller
    {
        dynamic myModel = new ExpandoObject();
        int weekStatus = 0;
        public ActionResult MainService(bool complete = false)
        {
            int? WWID = null;
            //flag is for user clicking on the completed snapshot link
            int intWeekStatusFlag = 0;
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                //ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"];
                var result = new List<Models.DefaultWW>();

                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();
                try
                {
                    #region Filter
                    if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
                    {
                        Response.Cookies["Module"].Value = "Snapshot";
                        Session.Remove("DataSource");
                    }
                    else
                        Response.Cookies["Module"].Value = "Snapshot";

                    #region Init Filter

                    mFil.strField = "VerticalName";
                    mFil.strLabelField = "Swim Lanes";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();
                    mFil.strField = "PXTName";
                    mFil.strLabelField = "PXT";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ProgType";
                    mFil.strLabelField = "Type";
                    //      mFil.strLinkedField = "PXTName";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "OwnerName";
                    mFil.strLabelField = "Owner";
                    //       mFil.strLinkedField = "ProgType";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ClasificationName";
                    mFil.strLabelField = "Clasification";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ProgName";
                    mFil.strLabelField = "Program";
                    //       mFil.strLinkedField = "OwnerName";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);



                    ViewBag.Filter = mFilters;
                    #endregion
                    ViewBag.Calendar = Calendar();

                    if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())))
                        WWID = Convert.ToInt32(Session["FilWW"].ToString());
                    #endregion
                    if (WWID == null)
                    {
                        result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                        Session["FilWW"] = result[0].WWID;


                        getDBdata(result[0].WWID);
                        myModel.WWID = result[0].WWID;
                        ViewBag.WeekStatus = result[0].weekstatus;


                        #region Build Filter
                        List<object> objFil = new List<object>();
                        List<Models.SnapshotIndex> dataFilList = new List<Models.SnapshotIndex>();

                        //dataFilList = getPrograms(myModel.WWID);
                        dataFilList = myModel.list;

                        if (Session["DataSource"] != null)
                        {
                            Session["CancelActivate"] = true;
                        }

                        foreach (Models.SnapshotIndex snap in dataFilList)
                        {
                            Object o = new object();

                            o = (object)snap;
                            objFil.Add(o);
                        }


                        ViewBag.objFil = objFil;
                        #endregion
                        #region PreLoad
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.oSource = objFil;
                        fFil.mFiltersToShow = mFilters;
                        //        fFil.vCreateFilter();

                        Session["DataFil"] = fFil.dtData;
                        Session["DataSource"] = fFil.oSource;

                        #endregion

                        #region Load Data Filter
                        List<Models.SnapshotIndex> resFil = new List<Models.SnapshotIndex>();
                        List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();

                        if (Request.Cookies["FilString"] != null)
                        {
                            fFil.dtData = (DataTable)Session["DataFil"];
                            //   string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                            #region Apply Filter


                            List<string> strValue;

                            if (Session["ValSel"] != null)
                                strValue = (List<string>)Session["ValSel"];
                            else
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            resFil = ((List<Models.SnapshotIndex>)myModel.list).Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            #endregion
                        }
                        else
                        {
                            resFil = (List<Models.SnapshotIndex>)myModel.list;
                        }

                        myModel.list = resFil;
                        #endregion
                    }
                    else if (WWID != null)
                    {
                        myModel.WWID = WWID;
                        this.getUsers();

                        #region Filter
                        #region Build Filter
                        List<object> objFil = new List<object>();
                        List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();


                        if (getDBdata(myModel.WWID))
                        {
                            list = myModel.list;
                            foreach (Models.SnapshotIndex snap in list)
                            {
                                Object o = new object();

                                o = (object)snap;
                                objFil.Add(o);
                            }
                        }

                        ViewBag.objFil = objFil;
                        #endregion
                        #region PreLoad
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.oSource = objFil;
                        fFil.mFiltersToShow = mFilters;
                        fFil.vCreateFilter();

                        Session["DataFil"] = fFil.dtData;
                        Session["DataSource"] = fFil.oSource;

                        #endregion
                        #region Load Data Filter
                        List<Models.SnapshotIndex> resFil = new List<Models.SnapshotIndex>();


                        if (Request.Cookies["FilString"] != null)
                        {
                            fFil.dtData = (DataTable)Session["DataFil"];

                            // string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                            #region Applay Filter

                            List<string> strValue;

                            if (Session["ValSel"] != null)
                                strValue = (List<string>)Session["ValSel"];
                            else
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }
                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            resFil = ((List<Models.SnapshotIndex>)myModel.list).Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                            #endregion
                        }
                        else
                        {
                            resFil = list;
                        }

                        myModel.list = resFil;
                        #endregion
                        #endregion
                        List<Models.Calendar> cal = GeneralClasses.ViewDataFilter.Calendar();
                        Models.Calendar singleCal = cal.Where(x => x.id == WWID).Single();
                        ViewBag.weekStatus = singleCal.weekStatus;
                    }



                    ViewBag.Complete = complete;

                }
                catch
                {
                    if (Session["Reload"] == null)
                    {
                        Request.Cookies.Remove("Val");
                        Request.Cookies.Remove("FilString");
                        Session["Reload"] = "1";
                        MainService(complete);
                    }
                }
            }

            return Json(new { myModel }, JsonRequestBehavior.AllowGet);
            
        }
        public static List<Models.Calendar> Calendar()
        {
            var list = new List<Models.Calendar>();
            int year = DateTime.Now.Year;
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", year).ToList<Models.Calendar>();
            }

            return list;
        }
        public bool getDBdata(int WWID)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            var list = new List<Models.SnapshotIndex>();
            var catPLC = new List<Models.PLCList>();
            var PTPLC = new List<Models.PLCList>();
            var userList = new List<Models.Users>();
            var VerticalList = new List<Models.Vertical>();
            var NotifList = new List<Models.Notif>();
            NotifController notif = new NotifController();
            List<Models.NotifAssignees> lstNotifAssign = new List<Models.NotifAssignees>();

            IEnumerable<SelectListItem> PlCList;
            IEnumerable<SelectListItem> VerticalEnum;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                catPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                VerticalList = dc.Database.SqlQuery<Models.Vertical>("[SPROCcatVertical]").ToList<Models.Vertical>();
                //lstNotifAssign = dc.Database.SqlQuery<Models.NotifAssignees>("[SPROCSendNotifWeekly]").ToList<Models.NotifAssignees>();

                if (list.Count > 0)
                {
                    this.weekStatus = int.Parse(list[0].weekStatus.ToString());
                }
                //foreach (Models.SnapshotIndex snap in list)
                //{
                //    var PLCStatus = dc.Database.SqlQuery<Models.PLCList>("[SPROCgetPLCStatusByPT] @intPT = {0}", snap.ProgTypeID).ToList<Models.PLCList>();
                //    snap.PTPLCStatus = PLCStatus.Select(x => new SelectListItem()
                //    {
                //        Text = x.Name,
                //        Value = x.Id.ToString()
                //    });
                //}
                string strBody = "You are receiving this mail because you have been identified as a program owner within the snapshot suite application. As of now you are delinquent in making your updates for this week.   Please update your program status ASAP.";


                VerticalEnum = VerticalList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });

                PlCList = catPLC.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                //lstNotifAssign.Clear();

                //if (lstNotifAssign.Count > 0)
                //{
                //    Models.NotifAssignees noti = new Models.NotifAssignees();
                //    noti.Email = "ronda.hall@intel.com";
                //    lstNotifAssign.Add(noti);

                //    noti = new Models.NotifAssignees();

                //    noti.Email = "raul.lobato.wong@intel.com";
                //    lstNotifAssign.Add(noti);

                //    noti = new Models.NotifAssignees();

                //    noti.Email = "jorge.luisx.orozco.ruiz@intel.com";
                //    lstNotifAssign.Add(noti);

                //    notif.SendEmail(WWID, list.First().weekName, strBody, lstNotifAssign, "Snapshot Update for " + list.First().weekName + " not completed");
                //}
            }
            if (list.Count() > 0)
            {
                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
                myModel.list = list;
                myModel.PlCList = PlCList;
                myModel.validUsers = userList;
                myModel.VerticalList = VerticalEnum;
                return true;
            }
            else
            { return false; }
        }
        public bool getUsers()
        {
            var userList = new List<Models.Users>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                myModel.validUsers = userList;
                return true;
            }
        }
        public ActionResult Snapshot()
        {
            return View();
        }

        public ActionResult HealthService(bool complete = false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            int? pWWID = null;
            List<Models.HealthIndicator> list = new List<Models.HealthIndicator>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            List<Models.SnapshotIndex> lstProgram = new List<Models.SnapshotIndex>();

            Models.mFilter mFil = new Models.mFilter();

            #region Init Filter

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "HealthIndicator";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "HealthIndicator";


            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdataHealth(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", result[0].WWID).ToList<Models.SnapshotIndex>();

                    if (complete)
                    {
                        list = getDBdataHealth(result[0].WWID - 1);
                    }
                    else
                        list = getDBdataHealth(result[0].WWID);

                    #region Build Filter
                    List<object> objFil = new List<object>();


                    if (Session["DataSource"] != null && Session["FilString"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }


                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }


                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter

                        List<string> strValue;

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                            Session["ValSel"] = strValue;
                        }

                        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                        int inti = 0;

                        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                        {
                            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                        {
                            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                        {
                            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                        {
                            strProgValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                        {
                            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                        {
                            strClasification[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();


                        ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        #endregion
                    }
                    else
                    {
                        ViewBag.Program = lstProgram;
                    }

                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;

                    ViewBag.WeekStatus = list[0].weekStatus;
                }
                else
                {
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", pWWID).ToList<Models.SnapshotIndex>();

                    #region Build Filter
                    List<object> objFil = new List<object>();

                    list = getDBdataHealth(int.Parse(pWWID.ToString()));

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }

                    foreach (Models.HealthIndicator Hi in list)
                    {
                        Object o = new object();

                        o = (object)Hi;
                        objFil.Add(o);
                    }


                    ViewBag.objFil = objFil;
                    #endregion
                    #region Load Data Filter
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    // fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    List<Models.HealthIndicator> resFil = new List<Models.HealthIndicator>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Apply Filter
                        if (Session["ValSel"] != null)
                        {

                            List<string> strValue = (List<string>)Session["ValSel"];

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                        }
                        #endregion
                    }
                    else
                    {
                        ViewBag.Program = lstProgram;
                    }

                    #endregion


                }

                ViewBag.Complete = complete;



                //return View("~/Views/HealthIndicator/Hi.cshtml", list);
                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public List<Models.HealthIndicator> getDBdataHealth(int WWID)
        {
            var list = new List<Models.HealthIndicator>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetWWHealthIndicators @WWID = {0} ", WWID).ToList<Models.HealthIndicator>();
            }

            return list;
        }
        public ActionResult HealthIndicator()
        {
            return View();
        }

        public ActionResult ProductCost()
        {
            return View();
        }
        public List<Models.Pcos> getDBdataProductCost(int WWID)
        {
            try
            {
                var list = new List<Models.Pcos>();
                var userList = new List<Models.Users>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.Pcos>("SPROCGetWWPcos @WWID = {0} ", WWID).ToList<Models.Pcos>();
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                }

                ViewBag.validUsers = userList;
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ProductCostService(bool complete = false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row

            List<Models.Pcos> list = new List<Models.Pcos>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            int? pWWID = null;

            #region Init Filter

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Pcos";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Pcos";

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();
            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdataProductCost(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());
            #endregion


            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    list = getDBdataProductCost(result[0].WWID);

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                }
                else
                {
                    list = getDBdataProductCost(int.Parse(pWWID.ToString()));
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;
                }


                #region Build Filter
                List<object> objFil = new List<object>();

                if (Session["DataSource"] != null)
                {
                    Session["CancelActivate"] = true;
                }

                foreach (Models.Pcos snap in list)
                {
                    Object o = new object();

                    o = (object)snap;
                    objFil.Add(o);
                }


                ViewBag.objFil = objFil;
                #endregion
                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
                fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data Filter
                List<Models.Pcos> resFil = new List<Models.Pcos>();
                if (Request.Cookies["FilString"] != null)
                {
                    fFil.dtData = (DataTable)Session["DataFil"];
                    //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                    #region Apply Filter

                    List<string> strValue;

                    if (Session["ValSel"] != null)
                        strValue = (List<string>)Session["ValSel"];
                    else
                    {
                        strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                        Session["ValSel"] = strValue;
                    }

                    string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                    string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                    string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                    string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                    string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                    string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                    int inti = 0;

                    foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                    {
                        strPXTValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                    {
                        strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                    {
                        strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                    {
                        strProgValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                    {
                        strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                    {
                        strClasification[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                }
                    #endregion
                #endregion

            }
            ViewBag.Complete = complete;
            //return View("~/Views/Pcos/PcosView.cshtml", list);
            return Json(new { list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Budget()
        {
            return View();
        }

        public ActionResult ControlChangeBoard()
        {
            return View();
        }
        private bool getDBdataCCB(int? WWID)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            var list = new List<Models.SnapshotIndex>();
            var catPLC = new List<Models.PLCList>();
            var userList = new List<Models.Users>();
            IEnumerable<SelectListItem> PlCList;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                catPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                if (list.Count > 0)
                {
                    this.weekStatus = int.Parse(list[0].weekStatus.ToString());
                }
                PlCList = catPLC.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
            }
            if (list.Count() > 0)
            {
                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
                Session["VBWW"] = ViewBag.WW;
                Session["VBWWID"] = ViewBag.WWID;
                myModel.WWID = list[0].WWID;
                myModel.list = list;
                myModel.PlCList = PlCList;
                myModel.validUsers = userList;
                return true;
            }
            else
            { return false; }
        }
        //public List<Models.CCB> getCCBs(int WWID)
        //{
        //    var CCBlist = new List<Models.CCB>();
        //    var CCBListNon = new List<Models.CCB>();
        //    var CCBStatusList = new List<Models.CCBStatus>();
        //    List<Models.CCB> resCCB = new List<Models.CCB>();

        //    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
        //    {
        //        CCBListNon = dc.Database.SqlQuery<Models.CCB>("SPROCgetNonSnap").ToList<Models.CCB>();
        //        CCBlist = dc.Database.SqlQuery<Models.CCB>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3} ", new object[] { WWID, 0, 0, "" }).ToList<Models.CCB>();


        //        foreach (Models.CCB ccb in CCBlist)
        //        {
        //            CCBListNon.Add(ccb);
        //        }

        //        CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();
        //    }
        //    if (CCBListNon.Count > 0)
        //    {
        //        DataTable dtData = new DataTable();

        //        //foreach (Models.CCB CCB in CCBListNon)
        //        //{
        //        //    var res = CCBStatusList.Where(row => row.ContinuityID == CCB.ContinuityID);
        //        //    if (res.Count() > 0)
        //        //    {
        //        //        CCB.lStatus = res.ToList();
        //        //    }
        //        //    else
        //        //    {
        //        //        CCB.lStatus = new List<Models.CCBStatus>();

        //        //    }
        //        //    CCB.WWID = WWID;
        //        //    resCCB.Add(CCB);
        //        //}

        //        ViewBag.WWID = WWID;

        //    }

        //    return resCCB;


        //}
        //public List<Models.CatCCBStatus> getCatCCBStatus()
        //{
        //    List<Models.CatCCBStatus> CCBStatusList = new List<Models.CatCCBStatus>();
        //    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
        //    {
        //        CCBStatusList = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
        //    }
        //    return CCBStatusList;
        //}
        //List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();

        //CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();

        //public List<Models.CCBStatus> getCCBStatusList()
        //{
        //    List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
        //    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
        //    {
        //        CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();
        //    }
        //    return CCBStatusList;
        //}
        //public List<Models.CCBAffected> getCCBAffectedProj()
        //{
        //    var CCBListProj = new List<Models.CCBAffected>();
        //    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
        //    {
        //        CCBListProj = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB").ToList<Models.CCBAffected>();
        //    }
        //    return CCBListProj;
        //}
        public ActionResult ControlChangeBoardService(bool complete = false)
        {
            int? WWID = null;
            //using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            //{
                //var result = new List<Models.DefaultWW>();
                //List<Models.mFilter> mFilters = new List<Models.mFilter>();
                //Models.mFilter mFil = new Models.mFilter();
                //List<Models.CCB> lstCCb = new List<Models.CCB>();
                //List<Models.CCB> lstCCbCat = new List<Models.CCB>();
                //#region Filter
                //#region Build Filter

                //mFil.strField = "VerticalName";
                //mFil.strLabelField = "Swim Lanes";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //mFil = new Models.mFilter();
                //mFil.strField = "PXTName";
                //mFil.strLabelField = "PXT";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //mFil = new Models.mFilter();

                //mFil.strField = "ProgType";
                //mFil.strLabelField = "Type";
                ////      mFil.strLinkedField = "PXTName";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //mFil = new Models.mFilter();

                //mFil.strField = "OwnerName";
                //mFil.strLabelField = "Owner";
                ////       mFil.strLinkedField = "ProgType";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //mFil = new Models.mFilter();

                //mFil.strField = "ClasificationName";
                //mFil.strLabelField = "Clasification";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //mFil = new Models.mFilter();

                //mFil.strField = "ProgName";
                //mFil.strLabelField = "Program";
                ////       mFil.strLinkedField = "OwnerName";
                //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                //mFilters.Add(mFil);

                //ViewBag.Filter = mFilters;
                //#endregion

                //if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                //    WWID = Convert.ToInt32(Session["FilWW"].ToString());
                //#endregion
                //#region Load WW
                //if (WWID == null)
                //{
                //    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                //    getDBdataCCB(result[0].WWID);
                //}
                //else
                //{
                //    getDBdataCCB(WWID);
                //}
                //#endregion

                //#region Load Data

                //if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
                //{
                //    Request.Cookies["Module"].Value = "CCB";
                //    Session.Remove("DataSource");
                //}
                //else
                //    Request.Cookies["Module"].Value = "CCB";


                //#region Build Filter
                //List<object> objFil = new List<object>();
                //lstCCbCat = getCCBs(myModel.WWID);
                //if (Session["DataSource"] != null)
                //{
                //    Session["CancelActivate"] = true;
                //}

                //foreach (Models.CCB ccb in lstCCbCat)
                //{
                //    Object o = new object();

                //    o = (object)ccb;
                //    objFil.Add(o);
                //}


                //ViewBag.objFil = objFil;
                //#endregion
                //#region PreLoad
                //AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                //fFil.oSource = objFil;
                //fFil.mFiltersToShow = mFilters;
                //fFil.vCreateFilter();

                //Session["DataFil"] = fFil.dtData;
                //Session["DataSource"] = fFil.oSource;

                //#endregion
                //if (Request.Cookies["FilString"] != null)
                //{
                //    #region Load Data Filter

                //    fFil.dtData = (DataTable)Session["DataFil"];
                //    //  string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                //    if (Session["ValSel"] != null)
                //    {

                //        List<string> strValue;

                //        if (Session["ValSel"] != null)
                //            strValue = (List<string>)Session["ValSel"];
                //        else
                //        {
                //            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                //            Session["ValSel"] = strValue;
                //        }

                //        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                //        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                //        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                //        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                //        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                //        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                //        int inti = 0;

                //        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                //        {
                //            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }

                //        inti = 0;
                //        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                //        {
                //            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }

                //        inti = 0;
                //        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                //        {
                //            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }

                //        inti = 0;
                //        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                //        {
                //            strProgValue[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }

                //        inti = 0;
                //        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                //        {
                //            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }

                //        inti = 0;
                //        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                //        {
                //            strClasification[inti] = strVal.Split(',')[1].ToString();
                //            inti++;
                //        }
                //        lstCCb = lstCCbCat.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList<Models.CCB>();

                //        // var CCBStatusList = new List<Models.CCBStatus>();
                //        //List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();

                //        //CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();

                //        if (lstCCb.Count == 0)
                //        {
                //            lstCCb = lstCCbCat;


                //        }

                //        DataTable dtData = new DataTable();

                //        /////////////////////////////JOSUE

                //        //foreach (Models.CCB CCB in lstCCb)
                //        //{
                //        //    var res = CCBStatusList.Where(row => row.ContinuityID == CCB.ContinuityID);
                //        //    if (res.Count() > 1)
                //        //    {
                //        //        CCB.lStatus = res.ToList();
                //        //    }
                //        //    else
                //        //    {
                //        //        CCB.lStatus = new List<Models.CCBStatus>();

                //        //    }
                //        //    CCB.WWID = myModel.WWID;
                //        //}

                //        /////////////////////////JOSUE
                //    #endregion
                //        //myModel.CCBStatusList = CCBStatusList;
                //        myModel.CCB = lstCCb;
                //    }
                //    else
                //    {
                //        myModel.CCB = getCCBs(myModel.WWID);
                //    }
                //}
                //else
                //{
                //    myModel.CCB = getCCBs(myModel.WWID);
                //}
                //#endregion

                //myModel.AffectedCCB = getCCBAffectedProj();
                ////ViewBag.WeekStatus = lstCCbCat.First().weekStatus;
                //myModel.CatCCBStatus = getCatCCBStatus();
                //myModel.CCBStatusList = getCCBStatusList();
            //}

            //ViewBag.Complete = complete;
            //return View("CCB", myModel);

            var CCBlist = new List<Models.CCB>();
            List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
            List<Models.CatCCBStatus> CatCCBStatus = new List<Models.CatCCBStatus>();
            var CCBAffected = new List<Models.CCBAffected>();
            var result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                myModel.WWID = result[0].WWID;
                CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB").ToList<Models.CCBStatus>();
                CatCCBStatus = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
                CCBAffected = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB").ToList<Models.CCBAffected>();
                CCBlist = dc.Database.SqlQuery<Models.CCB>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3} ", new object[] { myModel.WWID, 0, 0, "" }).ToList<Models.CCB>();
            }

            return Json(new { CCBlist, CatCCBStatus, CCBStatusList, CCBAffected }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CCBRegister()
        {
            return View();
        }

        public ActionResult ScheduleService(int? WWIDCompare, bool complete = false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            List<Models.ScheduleV> list = new List<Models.ScheduleV>();
            List<Models.ScheduleV> list2 = new List<Models.ScheduleV>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            List<Models.SnapshotIndex> lstProgram = new List<Models.SnapshotIndex>();

            Models.mFilter mFil = new Models.mFilter();
            int? pWWID = null;

            #region Init Filter

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdataSchedule(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].ToString()))
            {
                Response.Cookies["Module"].Value = "ScheduleV";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "ScheduleV";

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", result[0].WWID).ToList<Models.SnapshotIndex>();

                    List<object> objFil = new List<object>();

                    #region Build Filter

                    list = getDBdataSchedule(result[0].WWID);
                    if (Session["DataSource"] != null)
                    {
                        Session["CancelActivate"] = true;
                    }
                    foreach (Models.ScheduleV snap in list)
                    {

                        Object o = new object();

                        o = (object)snap;
                        objFil.Add(o);
                    }


                    ViewBag.objFil = objFil;
                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    #region Load Data Filter
                    List<Models.ScheduleV> resFil = new List<Models.ScheduleV>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Applay Filter

                        List<string> strValue;

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                            Session["ValSel"] = strValue;
                        }

                        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                        int inti = 0;

                        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                        {
                            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                        {
                            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                        {
                            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                        {
                            strProgValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                        {
                            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                        {
                            strClasification[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                        ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                    }
                    else
                    {
                        ViewBag.Program = lstProgram;
                    }
                        #endregion

                    #endregion

                }
                else
                {

                    #region Build Filter
                    List<object> objFil = new List<object>();

                    list = getDBdataSchedule(int.Parse(pWWID.ToString()));

                    foreach (Models.ScheduleV snap in list)
                    {

                        Object o = new object();

                        o = (object)snap;
                        objFil.Add(o);
                    }
                    ViewBag.objFil = objFil;
                    #endregion
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    #region Load Data Filter
                    List<Models.ScheduleV> resFil = new List<Models.ScheduleV>();
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", pWWID).ToList<Models.SnapshotIndex>();

                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        #region Apply Filter
                        if (Session["ValSel"] != null)
                        {

                            List<string> strValue = (List<string>)Session["ValSel"];

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        }
                        else
                        {
                            ViewBag.Program = lstProgram;
                        }
                        #endregion
                    }
                    else
                    {
                        if (fFil.oSource.Count > 0)
                        {
                            foreach (Models.ScheduleV sched in fFil.oSource)
                            {
                                resFil.Add(sched);
                            }

                            list = resFil;
                            ViewBag.Program = lstProgram;
                        }
                    }
                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;


                }
                myModel.Schedule = list;

                if (WWIDCompare != null)
                {
                    list2 = getDBdataSchedule(int.Parse(WWIDCompare.ToString()));
                }
                ViewBag.Complete = complete;
                myModel.ScheduleCompare = list2;
                ViewBag.WWIDC = WWIDCompare;
                //return View("~/Views/ScheduleV/ScheduleView.cshtml", myModel);
                return Json(new { myModel }, JsonRequestBehavior.AllowGet);
            }
        }
        public List<Models.ScheduleV> getDBdataSchedule(int WWID)
        {
            var list = new List<Models.ScheduleV>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0} ", WWID).ToList<Models.ScheduleV>();
            }
            return list;
        }
        public ActionResult Schedule()
        {
            return View();
        }
        public ActionResult Indicator(string ContinuityID, int WWID, int ProgTypeID, int wich)
        {
            switch (wich)
            {
                case 1:
                    List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();
                    List<Models.SnapshotIndex> Snapshot = new List<Models.SnapshotIndex>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                    }
                    Snapshot = list.Where(x => x.ContinuityID == ContinuityID).ToList();
                    return Json(new { Snapshot }, JsonRequestBehavior.AllowGet);
                case 11:
                    List<Models.IndicatorSnapshotComment> lstSnapshotComment = new List<Models.IndicatorSnapshotComment>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        lstSnapshotComment = dc.Database.SqlQuery<Models.IndicatorSnapshotComment>("SPRCIndicatorCommentsSnapshot @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorSnapshotComment>();
                    }
                    return Json(new { lstSnapshotComment }, JsonRequestBehavior.AllowGet);
                case 2:
                    List<Models.HealthIndicator> Healthlist = new List<Models.HealthIndicator>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        Healthlist = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetWWHealthIndicators @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.HealthIndicator>();
                    }
                    return Json(new { Healthlist }, JsonRequestBehavior.AllowGet);
                case 22:
                    List<Models.IndicatorHealthIndicatorComment> lstHeatlhIndicatorComment = new List<Models.IndicatorHealthIndicatorComment>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        lstHeatlhIndicatorComment = dc.Database.SqlQuery<Models.IndicatorHealthIndicatorComment>("SPROCIndicatorCommentsForHealthIndicator @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorHealthIndicatorComment>();
                    }
                    return Json(new { lstHeatlhIndicatorComment }, JsonRequestBehavior.AllowGet);
                case 3:
                    List<Models.ScheduleV> Schedulelist = new List<Models.ScheduleV>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        Schedulelist = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.ScheduleV>();
                    }
                    return Json(new { Schedulelist }, JsonRequestBehavior.AllowGet);
                case 33:
                    List<Models.IndicatorScheduleComment> lstScheduleComment = new List<Models.IndicatorScheduleComment>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        lstScheduleComment = dc.Database.SqlQuery<Models.IndicatorScheduleComment>("SPROCIndicatorCommentsForSchedule @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorScheduleComment>();
                    }
                    return Json(new { lstScheduleComment }, JsonRequestBehavior.AllowGet);
                case 4:
                    List<Models.Pcos> PCOSlist = new List<Models.Pcos>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        PCOSlist = dc.Database.SqlQuery<Models.Pcos>("SPROCGetWWPcos @WWID = {0}, @ContID={1}", WWID, ContinuityID).ToList<Models.Pcos>();
                    }
                    myModel.PCOSList = PCOSlist.Where(x => x.ContinuityID == ContinuityID);
                    return Json(new { myModel.PCOSList }, JsonRequestBehavior.AllowGet);
                case 44:
                    List<Models.IndicatorsPCOSComment> lstPCOSComment = new List<Models.IndicatorsPCOSComment>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        lstPCOSComment = dc.Database.SqlQuery<Models.IndicatorsPCOSComment>("SPROCIndicatorCommentsForPCOS @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorsPCOSComment>();
                    }
                    return Json(new { lstPCOSComment }, JsonRequestBehavior.AllowGet);
                case 5:
                    string BPOImportVersion;
                    List<Models.BudgetModel> Budgetlist = new List<Models.BudgetModel>();
                    List<Models.BudgetHeaders> BudgetHeaders = new List<Models.BudgetHeaders>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        Budgetlist = dc.Database.SqlQuery<Models.BudgetModel>("SPROCGetWWBudget @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.BudgetModel>();
                        if (Budgetlist.Count > 0)
                        {
                            BPOImportVersion = Budgetlist[0].BPOImportVersion;
                            BudgetHeaders = dc.Database.SqlQuery<Models.BudgetHeaders>("SPROCGetBudgetBPOHeaders @WWID = {0}", WWID).ToList<Models.BudgetHeaders>();
                        }
                    }
                    if (BudgetHeaders.Any())
                    {
                        return Json(new { Budgetlist, BudgetHeaders }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        BudgetHeaders = null;
                        return Json(new { Budgetlist, BudgetHeaders }, JsonRequestBehavior.AllowGet);
                    }
                case 55:
                    List<Models.IndicatorBudgetComment> lstBudgetComment = new List<Models.IndicatorBudgetComment>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        lstBudgetComment = dc.Database.SqlQuery<Models.IndicatorBudgetComment>("SPROCIndicatorCommentsForBudget @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorBudgetComment>();
                    }
                    return Json(new { lstBudgetComment }, JsonRequestBehavior.AllowGet);
                case 6:
                    List<Models.CCBResume> CCBlist = new List<Models.CCBResume>();
                    List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
                    List<Models.CatCCBStatus> CCBStatusCat = new List<Models.CatCCBStatus>();
                    List<Models.CCBAffected> CCBListProj = new List<Models.CCBAffected>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        CCBlist = dc.Database.SqlQuery<Models.CCBResume>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3}, @bIndicator={4} ", new object[] { WWID, 0, 0, ContinuityID, true }).ToList<Models.CCBResume>();
                        CCBListProj = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBAffected>();
                        CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBStatus>().Where(row => row.ContinuityID == ContinuityID).ToList<Models.CCBStatus>();
                        CCBStatusCat = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
                        if (CCBlist.Count > 0 && CCBStatusList.Count > 0)
                        {
                            CCBlist[0].lStatus = CCBStatusList;
                        }
                    }
                    return Json(new { CCBlist, CCBListProj, CCBStatusCat, CCBStatusList }, JsonRequestBehavior.AllowGet);
                case 7:
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    { }
                    return Json(new { wich }, JsonRequestBehavior.AllowGet);
                default:
                    return Json(new { wich }, JsonRequestBehavior.AllowGet);

            }
        }
        public ActionResult IndicatorOld(int? intern, int? Notif, bool complete = false, string ContinuityID = "")
        {
            try
            {
                ContinuityID = "ED0BA2A6-1D8D-4D71-ADD3-FD5D28B91510";
                string BPOImportVersion;
                int? WWID = null;
                Models.MasterModel master = new Models.MasterModel();

                List<Models.BudgetModel> Budgetlist = new List<Models.BudgetModel>();
                List<Models.ScheduleV> Schedulelist = new List<Models.ScheduleV>();
                List<Models.BudgetHeaders> BudgetHeaders = new List<Models.BudgetHeaders>();
                List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();
                List<Models.CCBResume> CCBlist = new List<Models.CCBResume>();
                List<Models.CCBStatus> CCBStatusList = new List<Models.CCBStatus>();
                List<Models.CatCCBStatus> CCBStatusCat = new List<Models.CatCCBStatus>();
                List<Models.SnapshotIndex> Snapshot = new List<Models.SnapshotIndex>();
                List<Models.Pcos> PCOSlist = new List<Models.Pcos>();
                List<Models.HealthIndicator> Healthlist = new List<Models.HealthIndicator>();
                List<Models.CommentsForIndicator> StoredComments = new List<Models.CommentsForIndicator>();
                List<Models.AllComments> CommentsSelected = new List<Models.AllComments>();
                List<Models.PLCList> CatPLC = new List<Models.PLCList>();
                IEnumerable<SelectListItem> PlCList;
                List<Models.IndicatorTypes> Types = new List<Models.IndicatorTypes>();
                List<Models.MasterModel> dataFilList = new List<Models.MasterModel>();
                List<Models.Users> userList = new List<Models.Users>();
                List<Models.AllComments> Comments = new List<Models.AllComments>();
                List<Models.IndicatorScheduleComment> lstScheduleComment = new List<Models.IndicatorScheduleComment>();
                List<Models.IndicatorHealthIndicatorComment> lstHeatlhIndicatorComment = new List<Models.IndicatorHealthIndicatorComment>();
                List<Models.IndicatorBudgetComment> lstBudgetComment = new List<Models.IndicatorBudgetComment>();
                List<Models.IndicatorValidationComment> lstValidationComment = new List<Models.IndicatorValidationComment>();
                List<Models.IndicatorSnapshotComment> lstSnapshotComment = new List<Models.IndicatorSnapshotComment>();
                List<Models.IndicatorsPCOSComment> lstPCOSComment = new List<Models.IndicatorsPCOSComment>();
                List<Models.ValidationModel> lstVal = new List<Models.ValidationModel>();
                List<Models.ValidationPhases> Phases = new List<Models.ValidationPhases>();
                List<Models.CCBAffected> CCBListProj = new List<Models.CCBAffected>();
                List<Models.DefaultWW> result = new List<Models.DefaultWW>();

                AutoIndicators.Controllers.ProgEditController ProgControl = new AutoIndicators.Controllers.ProgEditController();

                if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getPrograms(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                {
                    WWID = Convert.ToInt32(Session["FilWW"].ToString());
                    Session["FilWW"] = WWID;
                }
                else
                {
                    using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                        WWID = result[0].WWID;
                    }
                }
                dataFilList = getPrograms(WWID);


                ViewBag.intern = intern;
                #region Filter
                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();

                #region Init Filter
                if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
                {
                    Request.Cookies["Module"].Value = "Indicator";
                    Session.Remove("DataSource");
                }
                else
                    Request.Cookies["Module"].Value = "Indicator";

                mFil.strField = "VerticalName";
                mFil.strLabelField = "Swim Lanes";
                //       mFil.strLinkedField = "OwnerName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "PXTName";
                mFil.strLabelField = "PXT";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgType";
                mFil.strLabelField = "Type";
                //      mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "OwnerName";
                mFil.strLabelField = "Owner";
                //       mFil.strLinkedField = "ProgType";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgName";
                mFil.strLabelField = "Program";
                //       mFil.strLinkedField = "OwnerName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                ViewBag.Filter = mFilters;
                ViewBag.Module = Session["Module"];
                #endregion
                #region Build Filter
                List<object> objFil = new List<object>();

                foreach (Models.MasterModel ccb in dataFilList)
                {
                    Object o = new object();

                    o = (object)ccb;
                    objFil.Add(o);
                }

                ViewBag.objFil = objFil;
                #endregion
                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
                //fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data
                List<Models.MasterModel> resProgram = new List<Models.MasterModel>();
                List<Models.MasterModel> programSel = new List<Models.MasterModel>();
                if (Request.Cookies["FilString"] != null)
                {
                    //fFil.dtData = (DataTable)Session["DataFil"];
                    //string strFilString = fFil.strValidFilterString(Session["FilString"].ToString());
                    //System.Linq.Expressions.Expression<Func<Models.MasterModel>> PXT;

                    List<string> strValue;

                    if (Session["ValSel"] != null)
                        strValue = (List<string>)Session["ValSel"];
                    else
                    {
                        strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                        Session["ValSel"] = strValue;
                    }

                    string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                    string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                    string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                    string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                    string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];

                    int inti = 0;

                    foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                    {
                        strPXTValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                    {
                        strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                    {
                        strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                    {
                        strProgValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    inti = 0;
                    foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                    {
                        strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                        inti++;
                    }

                    resProgram = dataFilList.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName)).ToList<Models.MasterModel>();

                    if (ContinuityID.Length > 0)
                    {
                        programSel = resProgram.Where(row => row.ContinuityID == ContinuityID).ToList<Models.MasterModel>();
                    }
                    else
                    {
                        programSel = resProgram;
                    }
                }
                else
                {
                    resProgram = dataFilList;
                    programSel = resProgram.Where(row => row.ContinuityID == ContinuityID).ToList<Models.MasterModel>();
                }

                if (programSel.Count() == 0)
                {
                    programSel.Add(resProgram.First());
                }
                var programWos = resProgram.OrderBy(x => x.ProgName.Trim());



                myModel.ProgramSel = programSel.ToList();
                myModel.ProgramWos = programWos.ToList();

                #endregion
                #endregion

                ContinuityID = programSel.First().ContinuityID;

                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                    list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();

                    if (ContinuityID == null)
                    {

                        ContinuityID = list[0].ContinuityID;
                    }
                    Budgetlist = dc.Database.SqlQuery<Models.BudgetModel>("SPROCGetWWBudget @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.BudgetModel>();
                    if (Budgetlist.Count > 0)
                    {
                        BPOImportVersion = Budgetlist[0].BPOImportVersion;
                        BudgetHeaders = dc.Database.SqlQuery<Models.BudgetHeaders>("SPROCGetBudgetBPOHeaders @WWID = {0}", WWID).ToList<Models.BudgetHeaders>();
                    }

                    Snapshot = list.Where(x => x.ContinuityID == ContinuityID).ToList();
                    lstVal = dc.Database.SqlQuery<Models.ValidationModel>("SPROCGetWWValidation @WWID = {0}, @ContID={1}", WWID, ContinuityID).ToList<Models.ValidationModel>();
                    Phases = dc.Database.SqlQuery<Models.ValidationPhases>("SPROCGetWWValidationPhases @WWID = {0}, @ContID={1}", WWID, ContinuityID).ToList<Models.ValidationPhases>();
                    CatPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                    Schedulelist = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.ScheduleV>();
                    Healthlist = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetWWHealthIndicators @WWID = {0}, @ContID = {1} ", WWID, ContinuityID).ToList<Models.HealthIndicator>();
                    PCOSlist = dc.Database.SqlQuery<Models.Pcos>("SPROCGetWWPcos @WWID = {0}, @ContID={1}", WWID, ContinuityID).ToList<Models.Pcos>();
                    StoredComments = dc.Database.SqlQuery<Models.CommentsForIndicator>("SPROCGetCommentColection @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.CommentsForIndicator>();
                    CCBlist = dc.Database.SqlQuery<Models.CCBResume>("SPROCgetCCB @WW={0},@intCCBStatus={1},@idCCB={2},@ContinuityID = {3}, @bIndicator={4} ", new object[] { WWID, 0, 0, ContinuityID, true }).ToList<Models.CCBResume>();
                    CCBStatusList = dc.Database.SqlQuery<Models.CCBStatus>("SPROCgetStatusCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBStatus>().Where(row => row.ContinuityID == ContinuityID).ToList<Models.CCBStatus>();
                    CCBStatusCat = dc.Database.SqlQuery<Models.CatCCBStatus>("SPROCgetStatusCatCCB").ToList<Models.CatCCBStatus>();
                    Comments = dc.Database.SqlQuery<Models.AllComments>("SPROCGetAllAppComments @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.AllComments>();
                    lstScheduleComment = dc.Database.SqlQuery<Models.IndicatorScheduleComment>("SPROCIndicatorCommentsForSchedule @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorScheduleComment>();
                    lstHeatlhIndicatorComment = dc.Database.SqlQuery<Models.IndicatorHealthIndicatorComment>("SPROCIndicatorCommentsForHealthIndicator @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorHealthIndicatorComment>();
                    lstBudgetComment = dc.Database.SqlQuery<Models.IndicatorBudgetComment>("SPROCIndicatorCommentsForBudget @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorBudgetComment>();
                    lstValidationComment = dc.Database.SqlQuery<Models.IndicatorValidationComment>("SPROCIndicatorCommentsForValidation @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorValidationComment>();
                    lstSnapshotComment = dc.Database.SqlQuery<Models.IndicatorSnapshotComment>("SPRCIndicatorCommentsSnapshot @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorSnapshotComment>();
                    lstPCOSComment = dc.Database.SqlQuery<Models.IndicatorsPCOSComment>("SPROCIndicatorCommentsForPCOS @WWID = {0}, @PWContinuityID = {1} ", WWID, ContinuityID).ToList<Models.IndicatorsPCOSComment>();
                    CCBListProj = dc.Database.SqlQuery<Models.CCBAffected>("SPROCgetAffectedCCB @ContinuityID={0}", ContinuityID).ToList<Models.CCBAffected>();


                    if (CCBlist.Count > 0 && CCBStatusList.Count > 0)
                    {
                        CCBlist[0].lStatus = CCBStatusList;
                    }
                    if (dataFilList.Where(row => row.ContinuityID == ContinuityID).Count() > 0)
                    {
                        master = dataFilList.Where(row => row.ContinuityID == ContinuityID).First();
                        Types = dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCgetTemplateContinuity @vchContinuityID = {0}, @intProgramTypeID={1}", new object[] { master.ContinuityID, master.ProgTypeID }).ToList<Models.IndicatorTypes>();

                        PlCList = CatPLC.Select(x =>
                            new SelectListItem()
                            {
                                Text = x.Name,
                                Value = x.Id.ToString()
                            });

                        myModel.PlCList = PlCList;
                    }
                }
                CommentsSelected = this.getSelectedComments(StoredComments, WWID, ContinuityID);

                myModel.BudgetList = Budgetlist;
                myModel.lstBudgetComments = lstBudgetComment;
                myModel.ScheduleList = Schedulelist;
                myModel.lstScheduleComment = lstScheduleComment;
                myModel.HealthList = Healthlist;
                myModel.lstHealthIndicatorComment = lstHeatlhIndicatorComment;
                myModel.PCOSList = PCOSlist.Where(x => x.ContinuityID == ContinuityID);
                myModel.lstPCOSComment = lstPCOSComment;
                myModel.WWID = WWID;
                myModel.ContinuityID = ContinuityID;
                myModel.CommentsSelected = CommentsSelected;
                myModel.CCBList = CCBlist;
                myModel.CCBStatusCat = CCBStatusCat;
                myModel.Types = Types;
                myModel.Snapshotlist = Snapshot.Where(x => x.ContinuityID == ContinuityID);
                myModel.lstSnapshotComments = lstSnapshotComment;
                myModel.Users = userList;
                myModel.Comments = Comments;
                myModel.ValidationPhases = Phases.Where(x => x.PWContinuityID == ContinuityID);
                myModel.lstValidation = lstVal.Where<Models.ValidationModel>(x => x.ContinuityID == ContinuityID).ToList<Models.ValidationModel>();
                myModel.lstValidationComments = lstValidationComment;
                myModel.AffectedCCB = CCBListProj;
                ViewBag.Complete = complete;

                if (Types != null && Types.Count > 0)
                    myModel.TemplateID = Types[0].id;
                else
                    myModel.TemplateID = master.TemplateID;
                myModel.Template = null;

                List<Models.BPOPRJID> proj = ProgControl.getProjectID(Snapshot.First().PWID);
                myModel.projSel = proj;

                if (BudgetHeaders.Any())
                {
                    myModel.BudgetHeaders = BudgetHeaders;
                }
                else
                {
                    myModel.BudgetHeaders = null;
                }
                if (Notif != null)
                {
                    //return View("Indicator", myModel);
                    return Json(new { myModel }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //return View("IndicatorTest", myModel);
                    return Json(new { myModel }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult getProgramsAngular()
        {
            var result = new List<Models.DefaultWW>();
            List<Models.MasterModel> Mylits = new List<Models.MasterModel>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
            }
            Mylits = getPrograms(result[0].WWID);
            return Json(new { Mylits }, JsonRequestBehavior.AllowGet);
        }
        private List<Models.MasterModel> getPrograms(int? WWID)
        {

            List<Models.MasterModel> Masterlist = new List<Models.MasterModel>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Masterlist = dc.Database.SqlQuery<Models.MasterModel>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.MasterModel>();
            }

            ViewBag.WW = Masterlist[0].weekName;
            ViewBag.WWID = Masterlist[0].WWID;
            ViewBag.WeekStatus = Masterlist[0].weekStatus;

            return Masterlist;
        }
        private List<Models.AllComments> getSelectedComments(List<Models.CommentsForIndicator> StoredComments, int? WWID, string PWContinuityId)
        {
            List<Models.AllComments> Comments = new List<Models.AllComments>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                foreach (Models.CommentsForIndicator item in StoredComments)
                {
                    Comments.AddRange(dc.Database.SqlQuery<Models.AllComments>
                        ("SPROCGetAllViewComments @CategoryId = {0}, @CategoryId2 = {1}, @WWID = {2},@PWContinuityID = {3}, @view = {4} ",
                        item.category1, item.category2, WWID, PWContinuityId, item.OriginView).ToList<Models.AllComments>());
                }
            }

            return Comments;
        }
        public ActionResult Indicators()
        {
            return View();
        }

        public ActionResult SomeAction()
        {
            return Json(new { SomeValue = "foo bar" }, JsonRequestBehavior.AllowGet);
        }
	}
}