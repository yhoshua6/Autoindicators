﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;


namespace AutoIndicators.Controllers
{
    public class AcronymsController : Controller
    {
        dynamic myModel = new ExpandoObject();
        //
        // GET: /Acronyms/
        public ActionResult Index()
        {

            return View("~/Views/Acronyms/AcronymList.cshtml");
        }

        public bool getDBdata(string abbreviationString, string ContinuityID, int WWID, int Owner, int ProgType,int PXT)
        {
            var lstAcronym = new List<Models.Acronym>();
            var lstAcronymFil = new List<Models.Acronym>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                //list = dc.Database.SqlQuery<Models.Acronym>("SPROCgetletterAcronyms @abbreviationString = {0} ", abbreviationString).ToList<Models.Acronym>();
                lstAcronym = dc.Database.SqlQuery<Models.Acronym>("SPROCgetAllAcronyms @WWID={0}", WWID).ToList<Models.Acronym>(); //We get all the Acronyms that exist
                    if (lstAcronym.Count > 0)
                    {

                        switch (lstAcronym.First().ContextActorId)
                        {
                            case 1:
                                if (lstAcronym.Where(x => x.ContextId == ContinuityID && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                    lstAcronymFil = lstAcronym.Where(x => x.ContextId == ContinuityID && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                else
                                {
                                    if (lstAcronym.Where(x => x.OwnerId == Owner && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                    {
                                        if (lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                        {
                                            lstAcronymFil = lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                        }
                                        else
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                            {
                                                lstAcronymFil = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                            }
                                            else
                                            {
                                                lstAcronymFil = lstAcronym.Where(x => x.OwnerId == Owner && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                            {
                                                lstAcronymFil = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                            }
                                            else
                                            {
                                                lstAcronymFil = lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation.StartsWith(abbreviationString)).ToList();
                                            }
                                        }
                                        else
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).Count() > 0)
                                            {
                                                lstAcronymFil = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation.StartsWith(abbreviationString)).OrderByDescending(x => x.CreationDate).ToList();
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }

            }
            myModel.list = lstAcronymFil;
            return true;
        }

        public ActionResult AddAcronym()
        {
            return View();
        }

        public ActionResult AcronymList(string Abbreviation,string ContinuityID,int WWID,int Owner,int ProgType,int PXT)
        {
            getDBdata(Abbreviation,ContinuityID,WWID,Owner,ProgType,PXT);
            return View(myModel);
        }

        public ActionResult AcronymList_noLayout(string Row, string Abbreviation, string ContinuityID, int WWID, int Owner, int ProgType, int PXT)
        {
            getDBdata(Abbreviation, ContinuityID, WWID, Owner, ProgType, PXT);
            myModel.Row = Row;
            return View(myModel);
        }

        public ActionResult EditAcronym(int ID)
        {
            var list = new List<Models.Acronym>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities()) 
            {
                list = dc.Database.SqlQuery<Models.Acronym>("SPROCgetsingleAcronym @PID = {0} ", ID).ToList<Models.Acronym>();
            }
            myModel.list = list;
            return View(myModel);
        }

        public ActionResult SeeAcronym(int ID)
        {
            var list = new List<Models.Acronym>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Acronym>("SPROCgetsingleAcronym @PID = {0} ", ID).ToList<Models.Acronym>();
            }
            myModel.list = list;
            return View(myModel);

        }

        public ActionResult Save(FormCollection collection)
        {
            string usr = User.Identity.Name;
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (collection["OperationType"] == "1") //Create
                {
                    var parames = new object[] { collection["pTerm"], collection["pAbbreviation"], collection["pDefinition"], usr,collection["pLink"] };
                    dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCCreateAcronym @PTerm = {0},@PAbbreviation = {1}, @PDefinition = {2},@Pactor = {3},@PLink = {4}", parames).ToList<Models.DefaultWW>();
                }
                else if (collection["OperationType"] == "2")    //Update
                {
                    var parames = new object[] { collection["ID"], collection["pTerm"], collection["pAbbreviation"], collection["pDefinition"], usr, collection["pLink"] };
                    dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateAcronym @PID={0},@PTerm = {1},@PAbbreviation = {2}, @PDefinition = {3},@Pactor = {4},@PLink = {5}", parames).ToList<Models.DefaultWW>();
                }
            }
            return RedirectToAction("Index");
        }
        
    }
}