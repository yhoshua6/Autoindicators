﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Dynamic;
using System.IO;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

namespace AutoIndicators.Controllers
{
    public class TotalnputController : Controller
    {
        private dynamic myModel = new ExpandoObject();

        public ActionResult ManProgramType()
        {
            getData();
            ViewBag.Complete = false;
            return View("ManProgramTypeView",myModel);
        }
        public ActionResult SaveProgType(int id, string Description, string Link,string Alias,bool Delete)
        {
            bool bOk = false;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@vchDescription", Description);
            param[2] = new SqlParameter("@vchLink", Link);
            param[3] = new SqlParameter("@vchAlias", Alias);
            param[4] = new SqlParameter("@delete", Delete);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanProgramType", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                bOk = true;

            if (bOk)
                return JavaScript("save++;");
            else
                return JavaScript("alert('Error saving information')");
        }
        public ActionResult SaveMilestone(int id, string Name,string Definition,bool Delete)
        {
            bool bOk = false;
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@vchName", Name);
            param[2] = new SqlParameter("@vchDefinition", Definition);
            param[3] = new SqlParameter("@bDelete", Delete);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanMilestone", param);

            if (Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString())>0)
                bOk = true;

            if (bOk)
                return JavaScript("save++;");
            else
                return JavaScript("");
        }
        public ActionResult SavePTMilestone(int PTID, int MilestoneID,bool Delete)
        {
            bool bOk = false;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@intPT", PTID);
            param[1] = new SqlParameter("@intItem", MilestoneID);
            param[2] = new SqlParameter("@intOrder", 0);
            param[3] = new SqlParameter("@intType", 1);
            param[4] = new SqlParameter("@bDelete", Delete);
            

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanPTItem", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                bOk = true;

            if (bOk)
                return JavaScript("save++;");
            else
                return JavaScript("alert('Error')");
        }
        public ActionResult SaveSilicon(int Id,string Name,string ShortName,string Link, bool Delete)
        {
            bool bOk = false;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@vchName", Name);
            param[1] = new SqlParameter("@vchShortName", ShortName);
            param[2] = new SqlParameter("@vchLink", Link);
            param[3] = new SqlParameter("@id", Id);
            param[4] = new SqlParameter("@bDelete", Delete);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanRelevantSilicon", param);

            if (dsRes.Tables[0].Rows.Count>0)
                bOk = true;

            return Json(bOk);

        }
        public ActionResult SavePTIndicator(int PTID, int IndicatorID, bool Delete)
        {
            bool bOk = false;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@intPT", PTID);
            param[1] = new SqlParameter("@intItem", IndicatorID);
            param[2] = new SqlParameter("@intOrder", 0);
            param[3] = new SqlParameter("@intType", 2);
            param[4] = new SqlParameter("@bDelete", Delete);


            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanPTItem", param);

            if (dsRes.Tables[0].Rows[0][0].ToString().Equals("1"))
                bOk = true;

            if (bOk)
                return JavaScript("save++;");
            else
                return JavaScript("alert('Error saving information')");
        }
        public ActionResult MilestoneCatalog(int ProgramType)
        { 
            List<Models.MilestoneCatalog> lstMilestoneCat = new List<Models.MilestoneCatalog>();
            List<Models.MilestoneCatalog> lstMilestoneSelected = new List<Models.MilestoneCatalog>();
            List<Models.MilestoneCatalog> lstMilestone = new List<Models.MilestoneCatalog>();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstMilestoneCat = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetMilestones").ToList<Models.MilestoneCatalog>();
                lstMilestoneSelected = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[] { ProgramType, 1 }).ToList<Models.MilestoneCatalog>();
            }

            // remove selected items
            foreach(Models.MilestoneCatalog milestone in lstMilestoneCat)
            {
                if(lstMilestoneSelected.Where(x=> x.Id==milestone.Id).Count()==0)
                {
                    lstMilestone.Add(milestone);
                }
            }

            myModel = lstMilestoneCat;
           
            ViewBag.PTID = ProgramType;
            return PartialView("MilestoneCatalogView", myModel);
        }
        public ActionResult IndicatorsCatalog(int ProgramType)
        {
            List<Models.IndicatorCatalog> lstIndicatorCat= new List<Models.IndicatorCatalog>();
            List<Models.IndicatorCatalog> lstIndicatorSelected = new List<Models.IndicatorCatalog>();
            List<Models.IndicatorCatalog> lstIndicator = new List<Models.IndicatorCatalog>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstIndicatorCat = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetHealthIndicatorCat").ToList<Models.IndicatorCatalog>();
                lstIndicatorSelected = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[] {ProgramType, 2 }).ToList<Models.IndicatorCatalog>();
            }

            //exclude selected items
            foreach (Models.IndicatorCatalog indicator in lstIndicatorCat)
            {
                if (lstIndicatorSelected.Where(x => x.Id == indicator.Id).Count() == 0)
                {
                    lstIndicator.Add(indicator);
                }
            }

            myModel = lstIndicatorCat;
            ViewBag.PTID = ProgramType;
            return PartialView("IndicatorsCatalogView", myModel);
        }
        public ActionResult TemplatesCatalog(int ProgramType)
        {
            List<Models.IndicatorTypes> lstTemplate = new List<Models.IndicatorTypes>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstTemplate = dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCgetPTTemplates @PTID={0}", ProgramType).ToList<Models.IndicatorTypes>();

                foreach (Models.IndicatorTypes type in lstTemplate)
                {
                    type.structure = dc.Database.SqlQuery<Models.IndicatorTypesStruct>("SPROCGetIndicatorType @intTypeID = {0}, @bDetail={1} ", new object[] { type.id, 1 }).ToList<Models.IndicatorTypesStruct>();
                }
            }

            myModel = lstTemplate;
            ViewBag.PTID = ProgramType;
            return PartialView("TemplatesCatalogView", myModel);
        }
        public ActionResult RelevantSilicon()
        {
            List<Models.RelSilicon> lstRelative = new List<Models.RelSilicon>();

            using(Models.DevSQLUserEntities dc=new Models.DevSQLUserEntities())
            {
                lstRelative = dc.Database.SqlQuery<Models.RelSilicon>("[SPROCcatRelSilicon]").ToList<Models.RelSilicon>();
            }
            return PartialView("RelevantSiliconView", lstRelative);
        }
        public ActionResult SaveIndicator(int ID,string Name,bool Delete)
        {

            bool bOk = false;
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@id", ID);
            param[1] = new SqlParameter("@vchName", Name);
            param[2] = new SqlParameter("@bDelete", Delete);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanIndicator", param);

            if (Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString())>0)
                bOk = true;

            if (bOk)
                return JavaScript("save++;");
            else
                return JavaScript("");
        }
        private void getData()
        {
            List<Models.MilestoneCatalog> lstMilestone = new List<Models.MilestoneCatalog>();
            List<Models.IndicatorCatalog> lstIndicator= new List<Models.IndicatorCatalog>();
            List<Models.ProgTypes> lstProgType = new List<Models.ProgTypes>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstMilestone = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetMilestones").ToList<Models.MilestoneCatalog>();
                lstIndicator = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetHealthIndicatorCat").ToList<Models.IndicatorCatalog>();
                lstProgType = dc.Database.SqlQuery<Models.ProgTypes>("SPROCcatProgTypes").ToList<Models.ProgTypes>();

                foreach(Models.ProgTypes progtype in lstProgType)
                {
                    progtype.Milestones = dc.Database.SqlQuery<Models.MilestoneCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[]{progtype.id,1}).ToList<Models.MilestoneCatalog>();
                    progtype.HealthIndicator = dc.Database.SqlQuery<Models.IndicatorCatalog>("SPROCgetProgTypeItems @intProgramType={0}, @intItem={1}", new object[]{progtype.id, 2}).ToList<Models.IndicatorCatalog>();
                }
            }

            myModel.ProgTypes = lstProgType;
            myModel.Milestones = lstMilestone;
            myModel.Indicators = lstIndicator;
        }
    }
}
