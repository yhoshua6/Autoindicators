﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoIndicators.Controllers
{
    public class ReportsController : Controller
    {
        dynamic myModel;
        //
        // GET: /Report/
        public ActionResult Index()
        {
            int? WWID=null;
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());
            
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                WWID = result[0].WWID;
            }

            ViewBag.WWID = WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.Complete=false;
            return View("ReportsView");
        }
        public ActionResult Users(int Report)
        {
            int? WWID = null;
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                WWID = result[0].WWID;

                myModel = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>();
            }

            ViewBag.WWID = WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.Complete = false;
            ViewBag.Report = Report;
            return View("ByUsersView",myModel);
        }
        public ActionResult Chart(int Owner, int Report, int Type, int WWIDS,int WWIDE, List<Models.HIReport> DataHI = null, List<Models.ScheduleReport> DataSchedule = null,string Prog="")
        {
            List<Models.DefaultWW> wws = new List<Models.DefaultWW>();

            switch (Report)
            {
                case 1:
                    List<Models.ReportSanpshotChart> data = new List<Models.ReportSanpshotChart>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        data = dc.Database.SqlQuery<Models.ReportSanpshotChart>("SPROCgetReportByUserChart @intReport={0}, @intType={1}, @WWIDS={2}, @WWIDE={3}"
                            , new object[] { Report, Type, WWIDS,WWIDE }).ToList<Models.ReportSanpshotChart>();
                    }
                    myModel = data.Where(x => x.OwnerId == Owner).ToList();
                    break;
                case 2:
                    if (DataHI == null)
                        return new EmptyResult();
                    myModel = DataHI.Where(x=> x.WWID>= WWIDS && x.WWID<=WWIDE).ToList<Models.HIReport>();
                    break;
                case 3:
                    if (DataSchedule == null)
                        return new EmptyResult();
                    myModel = DataSchedule.Where(x => x.WWID >= WWIDS && x.WWID <= WWIDE).ToList<Models.ScheduleReport>();
                    break;
                case 7:
                    List<Models.ReportSanpshotChart> report = new List<Models.ReportSanpshotChart>();
                    using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        myModel = dc.Database.SqlQuery<Models.ReportSanpshotChart>("SPROCgetReportByUserChart @intReport={0}, @intType={1}, @WWIDS={2}, @WWIDE={3}, @Program={4}"
                            , new object[] { 1, Type, WWIDS,WWIDE,Prog }).ToList<Models.ReportSanpshotChart>();
                    }
                    break;
            }

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
               wws= dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>();
            }
            ViewBag.WWS = wws.Where(x => x.WWID == WWIDS).SingleOrDefault().weekName;
            ViewBag.WWE = wws.Where(x => x.WWID == WWIDE).SingleOrDefault().weekName;
            ViewBag.Report = Report;
            ViewBag.Complete = false;

            return PartialView("UserChartView",myModel);
        }
        public ActionResult Consolidate(bool owner=false, int year=0)
        {
            List<Models.NotifReport> rptReport = new List<Models.NotifReport>();

            int? WWID = null;
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                ViewBag.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                WWID = result[0].WWID;

                rptReport = dc.Database.SqlQuery<Models.NotifReport>("SPROCgetReports @intWWID={0},@intYear={1}, @intType={2}", new object[] { WWID,year, 6 }).ToList<Models.NotifReport>();
            }

            if(year>0)
            {
                rptReport = rptReport.Where(x => x.CreateDate.Year == year).ToList();
            }

            ViewBag.ByOwner = owner;
            ViewBag.WWID = WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.Complete = false;
            ViewBag.ByYear = year;
            ViewBag.Archived = result[0].weekstatus == 2 ? true : false;
            return View("ConsolidateView",rptReport);
        }
        public ActionResult Delinquent(bool owner = false)
        {
            List<Models.NotifReport> rptReport = new List<Models.NotifReport>();

            int? WWID = null;
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                ViewBag.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                WWID = result[0].WWID;

                rptReport = dc.Database.SqlQuery<Models.NotifReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, 6 }).ToList<Models.NotifReport>();

            }

            ViewBag.ByOwner = owner;
            ViewBag.WWID = WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.Archived = result[0].weekstatus == 2 ? true : false;
            ViewBag.Complete = false;
            
            return View("DelinquentView", rptReport);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NotifyDelinquent(int WWID,string WWName,string Data,string To)
        {
            NotifController notif = new NotifController();
            Models.NotifAssignees Assignee=new Models.NotifAssignees();
            List<Models.NotifAssignees> lstAssign=new List<Models.NotifAssignees>();

            Assignee.Email=To;

            lstAssign.Add(Assignee);

            Assignee = new Models.NotifAssignees();

            Assignee.Email = GeneralClasses.General.FindEmailByAccount(User.Identity.Name);
            lstAssign.Add(Assignee);

            string strBody="";
            string strTitle="";

            strTitle=WWName.Replace("-","'") + " Delinquent in "+Data;
            strBody=Data.Replace(",","<br>");

            notif.SendEmail(WWID, WWName, strBody, lstAssign, strTitle);

            return Json(true);
        }
        public ActionResult Notifweek(int year,bool byYear=false)
        {
            List<Models.NotifReport> rptReport = new List<Models.NotifReport>();

            int? WWID = null;
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                ViewBag.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                }
                else
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                WWID = result[0].WWID;

                rptReport = dc.Database.SqlQuery<Models.NotifReport>("SPROCgetReports @intWWID={0},@intYear={1}, @intType={2}", new object[] { WWID, year, 6 }).ToList<Models.NotifReport>();
            }

            if (year > 0)
            {
                rptReport = rptReport.Where(x => x.CreateDate.Year == year).ToList();
            }

            ViewBag.WWID = WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.ByYear = byYear;
            ViewBag.Year = year;
            ViewBag.Complete = false;
            ViewBag.Archived = result[0].weekstatus == 2 ? true : false;
            return View("Notifweek", rptReport);
        }
        public ActionResult Viewer(int WWID, int ReportType,string Range="")
        {
            ViewBag.Type = ReportType;
            ViewBag.WWID = WWID;
            int ByUser = WWID == 0 ? 1 : 0;
            string[] WWss=Range.Split('-');
            ViewBag.IsUser = ByUser;
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                int WWS=0;
                int WWE=0;
                if(WWss.Count()==2)
                {
                    WWS=Convert.ToInt32(WWss[0]);
                    WWE=Convert.ToInt32(WWss[1]);
                }
                switch(ReportType)
                {

                    case 1:
                        List<Models.Report> rptSnap = new List<Models.Report>();
                        if (WWss.Count()==2)
                        {
                            rptSnap = dc.Database.SqlQuery<Models.Report>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.Report>().Where(x => x.WWID >= WWS && x.WWID <= WWE).ToList<Models.Report>();
                        }else
                        {
                            rptSnap = dc.Database.SqlQuery<Models.Report>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.Report>();
                        }
                        myModel = rptSnap;
                        break;
                    case 2:
                        if (WWss.Count() == 2)
                        {
                            myModel = dc.Database.SqlQuery<Models.HIReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.HIReport>().Where(x => x.WWID >= WWS && x.WWID <= WWE).ToList<Models.Report>();
                        }
                        else
                        {
                            myModel = dc.Database.SqlQuery<Models.HIReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.HIReport>();
                        }
                        break;
                    case 3:
                    case 4:
                    case 5:
                        if (WWss.Count() == 2)
                        {
                            myModel = dc.Database.SqlQuery<Models.ScheduleReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.ScheduleReport>().Where(x => x.WWID >= WWS && x.WWID <= WWE).ToList();
                        }else
                        {
                            myModel = dc.Database.SqlQuery<Models.ScheduleReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.ScheduleReport>();
                        }
                        break;
                    case 6:
                        myModel = dc.Database.SqlQuery<Models.NotifReport>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, ReportType }).ToList<Models.NotifReport>();
                        break;
                    case 7: myModel = dc.Database.SqlQuery<Models.Report>("SPROCgetReports @intWWID={0}, @intType={1}", new object[] { WWID, 1 }).ToList<Models.Report>().Where(x => x.WWID >= WWS && x.WWID <= WWE).ToList();
                        break;
                }
            }
            return PartialView("ViewerView",myModel);
        }
	}
}