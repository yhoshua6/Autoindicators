﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.IO;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Net;
using System.Text.RegularExpressions;

namespace AutoIndicators.Controllers
{
    public class ValidationController : Controller
    {
        //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]

        private dynamic myModel = new ExpandoObject();

        public ActionResult Index(bool complete=false)
        {
            int? WWID = null;
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            List<Models.ValidationModel> list = new List<Models.ValidationModel>();

            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            List<object> objFil = new List<object>();

            //#region Filter
            //if (Session["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Session["Module"].ToString()))
            //{
            //    Session["Module"] = "Validation";
            //    Session.Remove("DataSource");
            //}
            //else
            //    Session["Module"] = "Validation";

            //#region Init Filter

            //mFil.strField = "PXTName";
            //mFil.strLabelField = "PXT";
            //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            //mFilters.Add(mFil);

            //mFil = new Models.mFilter();

            //mFil.strField = "ProgName";
            //mFil.strLabelField = "Program";
            //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            //mFilters.Add(mFil);

            //mFil = new Models.mFilter();

            //mFil.strField = "ProgType";
            //mFil.strLabelField = "Type";
            //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            //mFilters.Add(mFil);

            //mFil = new Models.mFilter();

            //mFil.strField = "OwnerName";
            //mFil.strLabelField = "Owner";
            //mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            //mFilters.Add(mFil);

            //ViewBag.Filter = mFilters;

            //#endregion

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());
            //#endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (WWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    list = getDBdata(result[0].WWID);


                    //foreach (Models.ValidationModel Validation in list)
                    //{
                    //    Object o = new object();

                    //    o = (object)Validation;
                    //    objFil.Add(o);
                    //}
                    //ViewBag.objFil = objFil;
                }
                else
                {
                    //#region Build Filter

                    list = getDBdata(int.Parse(WWID.ToString()));

                    //foreach (Models.ValidationModel Validation in list)
                    //{
                    //    Object o = new object();

                    //    o = (object)Validation;
                    //    objFil.Add(o);
                    //}


                    ViewBag.objFil = objFil;
                    //#endregion

                }
                //#region PreLoad
                //AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                //fFil.oSource = objFil;
                //fFil.mFiltersToShow = mFilters;
                //fFil.vCreateFilter();

                //Session["DataFil"] = fFil.dtData;
                //Session["DataSource"] = fFil.oSource;

                //#endregion
                //#region Load Data Filter
                //List<Models.ValidationModel> resFil = new List<Models.ValidationModel>();
                //if (Session["FilString"] != null)
                //{
                //    fFil.dtData = (DataTable)Session["DataFil"];
                //    if (fFil.dtData != null && fFil.dtData.Select(Session["FilString"].ToString()).Count() > 0)
                //    {
                //        fFil.dtResults = fFil.dtData.Select(Session["FilString"].ToString()).CopyToDataTable();
                //        fFil.vParseTo((object)new Models.ValidationModel());

                //        if (fFil.oResults.Count > 0)
                //        {
                //            List<Models.ValidationPhases> Phases = new List<Models.ValidationPhases>();
                //            Phases = dc.Database.SqlQuery<Models.ValidationPhases>("SPROCGetWWValidationPhases @WWID = {0}", WWID).ToList<Models.ValidationPhases>();

                //            foreach (Models.ValidationModel Val in fFil.oResults)
                //            {
                //                var PhasesTemp = Phases.Where(x => x.PWContinuityID == Val.ContinuityID).ToList<Models.ValidationPhases>();
                //                Val.Phases = PhasesTemp;

                //                resFil.Add(Val);
                //            }
                //        }
                //        myModel.List = resFil;
                //    }
                //}
                //#endregion

                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
                ViewBag.WeekStatus = list[0].weekStatus;
                ViewBag.Complete = complete;

                string strSite = GeneralClasses.General.strValLeadSite + ((string)ViewBag.WW).Replace("W", "").Replace("'", "%27");
                ViewBag.UrlSharepoint = strSite;
                return View("~/Views/Validation/ValidationView.cshtml", myModel);
            }
        }
        public static string GetLastUpdated(string ContinuityID, int WWID)
        {
            string strLastUpdate = "";

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                List<Models.ValidationImport> import = dc.Database.SqlQuery<Models.ValidationImport>("SPROCgetImportValidation @ContinuityID={0}, @WWID = {1} ", new object[] { ContinuityID, WWID }).ToList<Models.ValidationImport>();
                if (import.Count > 0)
                    strLastUpdate = import.First().strLastRefresh;
            }

            return strLastUpdate;
        }
         public static string GetDirectoryListingRegexForUrl(string url)
        {
            if (url.Equals(url))
            {
                return "<a href=\".*\">(?<name>.*)</a>";
            }
            throw new NotSupportedException();
        }
         private void GetFiles(string strUrl)
         {
             string url = strUrl;
             List<string> strFiles = new List<string>();
             HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
             request.UseDefaultCredentials = true;

             using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
             {
                 using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                 {
                     string html = reader.ReadToEnd();
                     Regex regex = new Regex(GetDirectoryListingRegexForUrl(url));
                     MatchCollection matches = regex.Matches(html);
                     if (matches.Count > 0)
                     {
                         foreach (Match match in matches)
                         {
                             if (match.Success)
                             {
                                strFiles.Add(match.Groups["name"].Value);
                             }
                         }
                     }
                 }
             }
         }
        public List<Models.ValidationModel> getDBdata(int WWID)
        {
            try
            {
                var Phases = new List<Models.ValidationPhases>();
                //var PhasesTemp = new List<Models.ValidationPhases>();
                var list = new List<Models.ValidationModel>();
                var listGeneral = new List<Models.ValidationModel>();
                var userList = new List<Models.Users>();
                List<Models.UploadFile> lstFiles = new List<Models.UploadFile>();
                //string strPath = AppDomain.CurrentDomain.BaseDirectory + "\\ValidationFiles";
                //string[] files = Directory.GetFiles(strPath);
                
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    Phases = dc.Database.SqlQuery<Models.ValidationPhases>("SPROCGetWWValidationPhases @WWID = {0}", WWID).ToList<Models.ValidationPhases>();
                    list = dc.Database.SqlQuery<Models.ValidationModel>("SPROCGetWWValidation @WWID = {0} ", WWID).ToList<Models.ValidationModel>();
                    listGeneral = dc.Database.SqlQuery<Models.ValidationModel>("SPROCgetGeneralValidation @WWID = {0} ", WWID).ToList<Models.ValidationModel>();
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();                    
                }
                

                 //foreach (string strFile in files)
                 //{
                 //    if (!strFile.Contains('~') && strFile.Contains(".xls"))
                 //    {
                 //        Models.UploadFile upload = new Models.UploadFile();
                 //        upload.strFileName = strFile.Substring(strFile.LastIndexOf("\\"));
                 //        lstFiles.Add(upload);
                 //    }
                 //}

                foreach (var item in list)
                {
                    var PhasesTemp = Phases.Where(x => x.PWContinuityID == item.ContinuityID).ToList<Models.ValidationPhases>();
                    item.Phases = PhasesTemp;
                    int intTotal=lstFiles.Where(x => x.strFileName.Contains(item.ProgName)).Count();
                    item.bFileNew = intTotal > 0;
                }
                foreach(var item in listGeneral)
                {
                    var PhasesTemp = Phases.Where(x => x.PWContinuityID == item.ContinuityID).ToList<Models.ValidationPhases>();
                    item.Phases = PhasesTemp;
                }

                ViewBag.validUsers = userList;
                myModel.List = list;
                myModel.ListGeneral = listGeneral;
                //myModel.Phases = Phases;
                return list;
            }
            catch (Exception ex)
            {
                return new List<Models.ValidationModel>();
            }
        }

        public ActionResult SaveRYG(string PWContinuityID, int pWWID, string RYG, string pWeekName,string Comments)
        {
            try
            {
                string usr = User.Identity.Name;
                pWeekName = pWeekName.Replace("_", "'");
                //11 is the validation indicator ID
                var parames = new object[] { PWContinuityID, pWWID, RYG, usr, 11,Comments };
                var result = new List<Models.DefaultWW>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWHealthIndicatorVal @PWContinuityID= {0}, @WWID = {1},@value = {2},@systemUser = {3}, @HIID = {4},@comments = {5}", parames).ToList<Models.DefaultWW>();
                }
                System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
                return RedirectToAction("Index", "Validation", new { WWID = pWWID, weekName = pWeekName });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [ValidateInput(false)]
        public ActionResult SaveComment(string PWContinuityID, int pWWID, string Comment, int HIID, string pWeekName)
        {
            string usr = User.Identity.Name;
            pWeekName=pWeekName.Replace("_","'");
            //11 is the validation indicator ID
            Comment = SnapshotController.RemoveUnwantedHtmlTags(Comment,SnapshotController.unwantedTags());
            var parames = new object[] { PWContinuityID, pWWID, Comment, usr, HIID };
            var result = new List<Models.DefaultWW>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWHealthIndicatorVal @PWContinuityID= {0}, @WWID = {1},@comments = {2},@systemUser = {3}, @HealthId = {4}", parames).ToList<Models.DefaultWW>();                    
            }
            System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
            return JavaScript(result.First().focusID.ToString());
        }

        public ActionResult SaveValData(FormCollection collection)
        {
            string usr = User.Identity.Name;
            try
            {
                var parames = new object[] { collection["pWWID"], collection["pPWContinuityID"], collection["pValOwner"], collection["pvalidationLink"], usr };
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("SPROCCreateNewValidation @WWID = {0}, @PWContinuityID = {1},@Owner = {2},@Link = {3},@Usr = {4}", parames);
                }
                string url = HttpContext.Request.UrlReferrer.PathAndQuery;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(new { continuity = collection["pPWContinuityID"].Replace(" ","_"), valOwner = collection["pValOwner"], ValName = GeneralClasses.General.FindDisplayNametByEmail(collection["pValOwner"]) });
        }
        public ActionResult SavePhase(FormCollection collection)
        {
            string usr = User.Identity.Name;
            try
            {
                var parames = new object[] { collection["pWWID"], collection["pPWContinuityID"], collection["pPhaseName"], collection["pDescription"], collection["pLink"], collection["pStartWeek"], collection["pStartYear"], collection["pEndWeek"], collection["pEndYear"], usr, collection["pPhaseID"].ToString().Length.Equals(0) ? null : collection["pPhaseID"] };
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("SPROCCreateNewValidationPhase @WWID = {0}, @PWContinuityID = {1},@PhaseName = {2},@description = {3},@Link = {4},@dateinitWW = {5},@dateinitYY = {6},@dateendWW = {7},@dateendYY = {8},@Usr = {9},@PhaseID = {10}", parames);
                }
                string url = HttpContext.Request.UrlReferrer.PathAndQuery;
                return Redirect(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult PhasesManage(int WWID, string PWContinuityID,bool Phase=true)
        {
            try
            {
                IEnumerable<SelectListItem> WeekList;
                CalendarController calendar = new CalendarController();
                var Phases = new List<Models.ValidationPhases>();
                var list = new List<Models.ValidationModel>();
                var CalendarList = calendar.getCalendarList(DateTime.Now.Year);
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    Phases = dc.Database.SqlQuery<Models.ValidationPhases>("SPROCGetWWValidationPhases @WWID = {0}, @ContID = {1} ", new object[] { WWID, PWContinuityID }).ToList<Models.ValidationPhases>();
                    list = dc.Database.SqlQuery<Models.ValidationModel>("SPROCGetWWValidation @WWID = {0}, @ContID = {1} ", new object[] { WWID, PWContinuityID }).ToList<Models.ValidationModel>();
                }
                WeekList = CalendarList.Select(x =>
                new SelectListItem()
                {
                    Text = x.weekName,
                    Value = x.id.ToString()
                });
                myModel.Phases = Phases;
                if(list.Count>0)
                {
                    myModel.Validation = list[0];
                }else
                    myModel.Validation= new List<Models.ValidationModel>();
                myModel.WeekList = WeekList;
                ViewBag.WWID = WWID;
                ViewBag.PWContinuityID = PWContinuityID;
                ViewBag.Phase = Phase;
                return View(myModel);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult Import()
        {
            return PartialView("ImportView");
        }
        public ActionResult LoadData()
        {
            int index = 0;
            try
            {

                List<Models.UploadFile> lstFiles = new List<Models.UploadFile>();
                string strPath = AppDomain.CurrentDomain.BaseDirectory + "\\ValidationFiles";
                //strPath= "\\\\gmvwww001\\Autoindicators\\ValidationFiles";
                string[] files = Directory.GetFiles(strPath);

                foreach (string strFile in files)
                {
                    if (!strFile.Contains('~') && strFile.Contains(".xls"))
                    {
                        try
                        {
                            Models.UploadFile upload = new Models.UploadFile();
                            string strFiles = strFile;
                            upload.dsData = GeneralClasses.General.GetDataFromExcel(strFiles);
                            upload.bImage = GeneralClasses.General.GetImgFromExcel(strFiles);
                            upload.dttRefreshDate = GeneralClasses.General.GetLastRefresh(strFiles);
                            upload.strFileName = strFile.Substring(strFile.LastIndexOf("\\"));
                            index++;
                            lstFiles.Add(upload);
                            System.IO.File.Delete(strFile);
                        }
                        catch(Exception ex) {
                            StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
                            rep.WriteLine(DateTime.Now.ToShortDateString()+" "+DateTime.Now.ToShortTimeString()+ " : An error ocurr in "+strFile+" : "+ ex.Message);
                            rep.Close();

                        }
                        
                    }
                }

                Session["Files"] = lstFiles;

                if (lstFiles.Count > 0)
                    return JavaScript("Step2();");
                else

                    return JavaScript("Error('Files not found')");
            }
            catch(Exception Ex)
            {
                return JavaScript("Error(\"" + Ex.Message +" en el"+index+ "\")");
            }

        }
        public ActionResult UploadFile(string Project,string ContinuityID, string ValLink)
        {
            int intFileID = 0;
            int intSheet = 0;
            int intSchemaID = 0;
            
            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);

            conn.Open();
            SqlTransaction tran=conn.BeginTransaction();

                List<Models.UploadFile> lstFiles = new List<Models.UploadFile>();

                string strPath = ValLink;
                
                string strLocalPath = AppDomain.CurrentDomain.BaseDirectory + "\\ValidationFiles\\tmpVal-"+Guid.NewGuid()+".xlsx";

                int inti = 0;
            
                if (System.IO.File.Exists(strLocalPath))
                {
                    System.IO.File.Delete(strLocalPath);
                }
                //Method 1
            Search:
                try
                {
                    string Username = System.Configuration.ConfigurationManager.AppSettings["User2App"].ToString();
                    string Password = System.Configuration.ConfigurationManager.AppSettings["PassUser2App"].ToString();
                    WebClient wc = new WebClient();
                    wc.Credentials = new NetworkCredential(Username, Password);
                    wc.DownloadFile(strPath, strLocalPath);
                    wc.Dispose();
                    GC.Collect();
                }
                catch
                {
                    if (inti == 0)
                    {
                        strPath = "https://spbiprd.intel.com/sites/ISQ/DCG DEG EPSD/" + Project + " Indicators/eMRC table of " + Project + ".xlsx";
                        inti++;
                        goto Search;
                    }else
                    {
                        return JavaScript("alert('File not found')");
                    }
                }
                try
                {
                    Models.UploadFile upload = new Models.UploadFile();
                    upload.dsData = GeneralClasses.General.GetDataFromExcel(strLocalPath);
                    upload.bImage = GeneralClasses.General.GetImgFromExcel(strLocalPath);
                    upload.dttRefreshDate= GeneralClasses.General.GetLastRefresh(strLocalPath);
                    upload.strFileName = strPath.Substring(strPath.LastIndexOf("/")+1);

                    string strImg = "";

                    foreach (byte bit in upload.bImage)
                    {
                        strImg += "|" + bit.ToString();
                    }
                    strImg = strImg.Substring(1);

                    SqlParameter[] param = new SqlParameter[5];
                    param[0] = new SqlParameter("@id", 0);
                    param[1] = new SqlParameter("@vchName", upload.strFileName);
                    param[2] = new SqlParameter("@txtImage", strImg);
                    param[3] = new SqlParameter("@LastRefresh",upload.dttRefreshDate);
                    param[4] = new SqlParameter("@bDelete", false);

                    DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanValidationImports", param);
                    intFileID = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

                    foreach (DataTable dtData in upload.dsData.Tables)
                    {
                        string strCol = "";
                        string strVal = "";

                        foreach (DataColumn dcData in dtData.Columns)
                        {
                            strCol += "|" + dcData.ColumnName;
                        }

                        if (strCol.Trim().Length > 0)
                        {

                            strCol = strCol.Substring(1);

                            param = new SqlParameter[4];
                            param[0] = new SqlParameter("@id", 0);
                            param[1] = new SqlParameter("@vchName", dtData.TableName);
                            param[2] = new SqlParameter("@txtCol", strCol);
                            param[3] = new SqlParameter("@idValidationImport", intFileID);

                            dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanImportSheet", param);

                            intSheet = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

                            foreach (DataRow drData in dtData.Rows)
                            {
                                strVal = "";
                                foreach (DataColumn dcCol in dtData.Columns)
                                {
                                    strVal += '~' + drData[dcCol.ColumnName].ToString().Replace('~', ' ');
                                }

                                if (strVal.Trim().Length > 0)
                                {
                                    strVal = strVal.Substring(1);

                                    param = new SqlParameter[2];
                                    param[0] = new SqlParameter("@txtValue", strVal);
                                    param[1] = new SqlParameter("@SheetID", intSheet);

                                    dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanValidationImportValues", param);
                                }
                            }
                        }
                    }

                    tran.Commit();
                    System.IO.File.Delete(strLocalPath);
                }
                catch (Exception ex)
                {
                    StreamWriter rep = System.IO.File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\validation.txt");
                    rep.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " : An error ocurr in " + strLocalPath + " : " + ex.Message);
                    rep.Close();
                    tran.Rollback();
                    string secondm=ex.Message.Contains("HRESULT: 0x800A01A8")? " : Try again, Please don't close the Excel app" :"";

                    return JavaScript("$('#warning').show(); $('#warningMessage').text(\"" + ex.Message.Replace("\r\n"," ") + secondm + "\"); $('#load-" + ContinuityID + "').hide();");
                }
            tran.Dispose();
            conn.Close();
            conn.Dispose();

            return JavaScript("location.reload()");
        }
        public ActionResult SaveDataFile()
        {
            
            List<Models.UploadFile> lstFiles=new List<Models.UploadFile>();
            lstFiles = (List<Models.UploadFile>)Session["Files"];
            int intFileID = 0;
            int intSheet = 0;
            int intSchemaID = 0;
            
            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);

            conn.Open();
            SqlTransaction tran=conn.BeginTransaction();
            try
            {
                foreach (Models.UploadFile file in lstFiles)
                {
                    string strImg = "";

                    foreach(byte bit in file.bImage)
                    {
                        strImg += "|"+bit.ToString();
                    }
                    strImg = strImg.Substring(1);

                    SqlParameter[] param = new SqlParameter[5];
                    param[0] = new SqlParameter("@id", 0);
                    param[1] = new SqlParameter("@vchName", file.strFileName);
                    param[2] = new SqlParameter("@txtImage", strImg);
                    param[3] = new SqlParameter("@LastRefresh", file.dttRefreshDate);
                    param[4] = new SqlParameter("@bDelete", false);

                    DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanValidationImports", param);
                    intFileID = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

                    foreach (DataTable dtData in file.dsData.Tables)
                    {
                        string strCol = "";
                        string strVal = "";
                        
                        foreach (DataColumn dcData in dtData.Columns)
                        {
                            strCol += "|"+dcData.ColumnName;
                        }

                        strCol = strCol.Substring(1);

                        param = new SqlParameter[4];
                        param[0] = new SqlParameter("@id", 0);
                        param[1] = new SqlParameter("@vchName", dtData.TableName);
                        param[2] = new SqlParameter("@txtCol", strCol);
                        param[3] = new SqlParameter("@idValidationImport", intFileID);

                        dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanImportSheet", param);

                        intSheet = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

                        foreach (DataRow drData in dtData.Rows)
                        {
                            strVal = "";
                            foreach (DataColumn dcCol in dtData.Columns)
                            {
                                strVal += '~' + drData[dcCol.ColumnName].ToString().Replace('~',' ');
                            }

                            strVal = strVal.Substring(1);

                            param = new SqlParameter[2];
                            param[0] = new SqlParameter("@txtValue", strVal);
                            param[1] = new SqlParameter("@SheetID", intSheet);

                            dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanValidationImportValues", param);
                        }

                        
                
                    }
                }

                tran.Commit();
            }
            catch {
                tran.Rollback();
                
            }
            tran.Dispose();
            conn.Close();
            conn.Dispose();
            return JavaScript("Step3();");
        }
        public ActionResult Expanded(int WWID,string ContinuityID,string WWName)
        {
            List<Models.ValidationImport> import = new List<Models.ValidationImport>();
            List<Models.ValidationImportSchema> schema = new List<Models.ValidationImportSchema>();
            List<Models.ValidationReport> list = new List<Models.ValidationReport>();

            DataSet dsRep=new DataSet();
            DataTable dtReportAO = new DataTable();
            DataTable dtReportAS = new DataTable();
            DataTable dtReportFA = new DataTable();
            DataTable dtReportFAO = new DataTable();
            DataTable dtReportFAS = new DataTable();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                import = dc.Database.SqlQuery<Models.ValidationImport>("SPROCgetImportValidation @ContinuityID={0}, @WWID = {1} ", new object []{ContinuityID, WWID}).ToList<Models.ValidationImport>();
                if (import.Count > 0)
                {
                    schema = dc.Database.SqlQuery<Models.ValidationImportSchema>("SPROCgetValidationImportSchema @idValidation={0}", new object[] { import.First().intID }).ToList<Models.ValidationImportSchema>();
                    list = dc.Database.SqlQuery<Models.ValidationReport>("SPROCgetValidationReport @WWID = {0}, @Continuity={1}", new object[] { WWID, ContinuityID }).ToList<Models.ValidationReport>();
                }
            }

            if (schema.Count == 3)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@intSchema", schema[0].intID);

                DataSet dsLastTocuhed = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetDataFromValidationImport", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter("@intSchema", schema[1].intID);

                DataSet dsOwners = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetDataFromValidationImport", param);

                param = new SqlParameter[1];
                param[0] = new SqlParameter("@intSchema", schema[2].intID);

                DataSet dsOpen = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetDataFromValidationImport", param);

                if (dsOwners != null && dsOwners.Tables.Count == 1 && dsLastTocuhed != null && dsLastTocuhed.Tables.Count == 1 && dsOpen != null && dsOpen.Tables.Count == 1)
                {
                    DataTable dtNew = dsOwners.Tables[0].Copy();
                    dtNew.TableName = "Owners";
                    dsRep.Tables.Add(dtNew);
                    dtNew = new DataTable();
                    dtNew = dsLastTocuhed.Tables[0].Copy();
                    dtNew.TableName = "Last";
                    dsRep.Tables.Add(dtNew);
                    dtNew = new DataTable();
                    dtNew.TableName = "Open";
                    dtNew = dsOpen.Tables[0].Copy();
                    dtNew.Columns.Add("Age(d)");

                    foreach (DataRow drData in dtNew.Rows)
                    {
                        if (dtNew.Columns.IndexOf("submit_date") > -1)
                        {
                            if (drData["submit_date"].ToString().Length > 0)
                                drData["Age(d)"] = Math.Round((DateTime.Now - Convert.ToDateTime(drData["submit_date"]).Date).TotalDays);
                        }
                        else
                        {
                            if (drData["submitted_date"].ToString().Length > 0)
                                drData["Age(d)"] = Math.Round((DateTime.Now - Convert.ToDateTime(drData["submitted_date"]).Date).TotalDays);
                        }
                    }
                    dsRep.Tables.Add(dtNew);
                    Session["Reports"] = dsRep;
                }

                if (dsRep != null && dsRep.Tables.Count > 0)
                {

                    DataTable dtData = dsRep.Tables[0];

                    #region AO Report
                    dtReportAO.Columns.Add("Severity");
                    dtReportAO.Columns.Add("Total", typeof(int));
                    dtReportAO.Columns.Add("Owners", typeof(int));
                    dtReportAO.Columns.Add("Age", typeof(double));
                    dtReportAO.Columns.Add("LastTouched", typeof(double));

                    bool bFirst = true;

                    foreach (DataColumn dc in dsRep.Tables[0].Columns)
                    {
                        if (!bFirst && !dc.ColumnName.Contains("Grand"))
                        {
                            DataRow drNew = dtReportAO.NewRow();
                            List<double> dDays = new List<double>();
                            List<double> dLast = new List<double>();
                            string gTotal = dsRep.Tables[0].Select("[Row Labels]='Grand Total'").First()[dc.ColumnName].ToString();

                            drNew["Severity"] = dc.ColumnName;
                            drNew["Total"] = gTotal.Length > 0 ? Convert.ToInt32(gTotal) : 0;

                            DataTable dtResults = dsRep.Tables[0].Select("[" + dc.ColumnName + "]<>'' AND [Row Labels]<>'Grand Total'").CopyToDataTable();
                            DataView dtView = new DataView(dtResults);
                            
                            drNew["Owners"] = dtView.ToTable(true, "Row Labels").Rows.Count;

                            foreach (DataRow drFilDate in dsRep.Tables[2].Select("[Exposure] like '%" + dc.ColumnName + "%'"))
                            {
                                if (dsRep.Tables[2].Columns.IndexOf("submit_date") > -1)
                                {
                                    dDays.Add((DateTime.Now - Convert.ToDateTime(drFilDate["submit_date"]).Date).TotalDays);
                                }else
                                {
                                    dDays.Add((DateTime.Now - Convert.ToDateTime(drFilDate["submitted_date"]).Date).TotalDays);
                                }
                                
                            }

                            foreach (DataRow drFilLast in dsRep.Tables[1].Select("[Exposure] like '%" + dc.ColumnName + "%'"))
                            {
                                dLast.Add(Convert.ToDouble(drFilLast["Last Touched Duration"]));
                            }

                            drNew["Age"] = dDays.Count > 0 ? dDays.Average() : 0;
                            drNew["LastTouched"] = dLast.Count > 0 ? dLast.Average() : 0;

                            dtReportAO.Rows.Add(drNew);
                        }
                        else
                            bFirst = false;
                    }
                    #endregion
                    #region AS Report
                    dtReportAS.Columns.Add("Severity", typeof(string));
                    dtReportAS.Columns.Add("Total", typeof(int));
                    dtReportAS.Columns.Add("NewAssignedRejected", typeof(int));
                    dtReportAS.Columns.Add("Implemented", typeof(int));
                    dtReportAS.Columns.Add("Released", typeof(int));
                    dtReportAS.Columns.Add("Verified", typeof(int));

                    DataRow drDataR = dtReportAS.NewRow();
                    int intNewAssignedRejected = 0;
                    int intImplemented = 0;
                    int intReleased = 0;
                    int intVerified = 0;

                    string strStatus = "Status Reason";

                    if(dsRep.Tables[2].Columns.IndexOf("State")>-1)
                    {
                        strStatus = "State";
                    }

                    drDataR["Severity"] = "Critical";
                    intNewAssignedRejected = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Critical%' AND ([" + strStatus + "] like '%New%' OR [" + strStatus + "] like '%Assigned%' OR [" + strStatus + "] like '%Rejected%')").ToString());
                    intImplemented = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Critical%' AND ["+strStatus+"] like '%Implemented%'").ToString());
                    intReleased = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Critical%' AND [" + strStatus + "] like '%Released%'").ToString());
                    intVerified = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Critical%' AND [" + strStatus + "] like '%Verified%'").ToString());

                    drDataR["NewAssignedRejected"] = intNewAssignedRejected;
                    drDataR["Implemented"] = intImplemented;
                    drDataR["Released"] = intReleased;
                    drDataR["Verified"] = intVerified;
                    drDataR["Total"] = intNewAssignedRejected + intImplemented + intReleased + intVerified;

                    dtReportAS.Rows.Add(drDataR);

                    drDataR = dtReportAS.NewRow();

                    drDataR["Severity"] = "High";
                    intNewAssignedRejected = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%High%' AND ([" + strStatus + "] like '%New%' OR [" + strStatus + "] like '%Assigned%' OR [" + strStatus + "] like '%Rejected%')").ToString());
                    intImplemented = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%High%' AND [" + strStatus + "] like '%Implemented%'").ToString());
                    intReleased = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%High%' AND [" + strStatus + "] like '%Released%'").ToString());
                    intVerified = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%High%' AND [" + strStatus + "] like '%Verified%'").ToString());

                    drDataR["NewAssignedRejected"] = intNewAssignedRejected;
                    drDataR["Implemented"] = intImplemented;
                    drDataR["Released"] = intReleased;
                    drDataR["Verified"] = intVerified;
                    drDataR["Total"] = intNewAssignedRejected + intImplemented + intReleased + intVerified;

                    dtReportAS.Rows.Add(drDataR);

                    drDataR = dtReportAS.NewRow();

                    drDataR["Severity"] = "Medium";
                    intNewAssignedRejected = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Medium%' AND ([" + strStatus + "] like '%New%' OR [" + strStatus + "] like '%Assigned%' OR [" + strStatus + "] like '%Rejected%')").ToString());
                    intImplemented = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Medium%' AND [" + strStatus + "] like '%Implemented%'").ToString());
                    intReleased = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Medium%' AND [" + strStatus + "] like '%Released%'").ToString());
                    intVerified = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Medium%' AND [" + strStatus + "] like '%Verified%'").ToString());

                    drDataR["NewAssignedRejected"] = intNewAssignedRejected;
                    drDataR["Implemented"] = intImplemented;
                    drDataR["Released"] = intReleased;
                    drDataR["Verified"] = intVerified;
                    drDataR["Total"] = intNewAssignedRejected + intImplemented + intReleased + intVerified;

                    dtReportAS.Rows.Add(drDataR);

                    drDataR = dtReportAS.NewRow();

                    drDataR["Severity"] = "Low";
                    intNewAssignedRejected = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Low%' AND ([" + strStatus + "] like '%New%' OR [" + strStatus + "] like '%Assigned%' OR [" + strStatus + "] like '%Rejected%')").ToString());
                    intImplemented = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Low%' AND [" + strStatus + "] like '%Implemented%'").ToString());
                    intReleased = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Low%' AND [" + strStatus + "] like '%Released%'").ToString());
                    intVerified = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "exposure like '%Low%' AND [" + strStatus + "] like '%Verified%'").ToString());

                    drDataR["NewAssignedRejected"] = intNewAssignedRejected;
                    drDataR["Implemented"] = intImplemented;
                    drDataR["Released"] = intReleased;
                    drDataR["Verified"] = intVerified;
                    drDataR["Total"] = intNewAssignedRejected + intImplemented + intReleased + intVerified;

                    dtReportAS.Rows.Add(drDataR);

                    #endregion
                    #region Funct Area Owners
                    dtReportFAO.Columns.Add("Defect Type", typeof(string));
                    dtReportFAO.Columns.Add("Total", typeof(int));
                    dtReportFAO.Columns.Add("NumOwners", typeof(int));
                    dtReportFAO.Columns.Add("AvgAge", typeof(double));
                    dtReportFAO.Columns.Add("AvgLast", typeof(double));

                    if (dsRep.Tables[2].Columns.IndexOf("Suspect Area") > -1)
                    {
                        foreach (DataRow drDat in dsRep.Tables[2].Rows)
                        {
                            if (dtReportFAO.Select("[Defect Type]='" + drDat["Suspect Area"].ToString() + "'").Count() == 0 && drDat["Suspect Area"].ToString().Length > 0)
                            {
                                DataRow drData = dtReportFAO.NewRow();
                                List<double> dDays = new List<double>();
                                List<double> dLast = new List<double>();
                                int intOwner = 0;
                                string strOwner = "";

                                drData["Defect Type"] = drDat["Suspect Area"];

                                DataTable dtResults = dsRep.Tables[2].Select("[Suspect Area]='" + drDat["Suspect Area"] + "'").CopyToDataTable();
                                DataView dtView = new DataView(dtResults);
                                drData["NumOwners"] = dtView.ToTable(true, "Owner").Rows.Count;

                                foreach (DataRow drFilDate in dsRep.Tables[2].Select("[Suspect Area]='" + drDat["Suspect Area"] + "'"))
                                {
                                    if (dsRep.Tables[2].Columns.IndexOf("submit_date") > -1)
                                    {
                                        dDays.Add((DateTime.Now - Convert.ToDateTime(drFilDate["submit_date"]).Date).TotalDays);
                                    }
                                    else
                                    {
                                        dDays.Add((DateTime.Now - Convert.ToDateTime(drFilDate["submitted_date"]).Date).TotalDays);
                                    }

                                }
                                //need
                                //foreach (DataRow drFilLast in dsRep.Tables[1].Select("[Product Found]='" + drDat["Product Found"] + "'"))
                                //{
                                //    dLast.Add(Convert.ToDouble(drFilLast["Last Touched Duration"]));
                                //}

                                drData["AvgAge"] = dDays.Count > 0 ? dDays.Average() : 0;
                                drData["AvgLast"] = dLast.Count > 0 ? dLast.Average() : 0;
                                drData["Total"] = dsRep.Tables[2].Select("[Suspect Area]='" + drDat["Suspect Area"].ToString() + "'").Count();
                                dtReportFAO.Rows.Add(drData);
                            }
                        }
                    }
                    #endregion
                    #region Funct Area Status
                    dtReportFAS.Columns.Add("Defect Type", typeof(string));
                    dtReportFAS.Columns.Add("Total", typeof(int));
                    dtReportFAS.Columns.Add("NewAssignedRejected", typeof(int));
                    dtReportFAS.Columns.Add("Implemented", typeof(int));
                    dtReportFAS.Columns.Add("Released", typeof(int));
                    dtReportFAS.Columns.Add("Verified", typeof(int));

                    if (dsRep.Tables[2].Columns.IndexOf("Suspect Area") > -1)
                    {
                        foreach (DataRow drDat in dsRep.Tables[2].Rows)
                        {
                            if (dtReportFAS.Select("[Defect Type]='" + drDat["Suspect Area"].ToString() + "'").Count() == 0 && drDat["Suspect Area"].ToString().Length > 0)
                            {
                                drDataR = dtReportFAS.NewRow();

                                drDataR["Defect Type"] = drDat["Suspect Area"].ToString();
                                intNewAssignedRejected = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "[Suspect Area]='" + drDat["Suspect Area"] + "' AND ([Status Reason] like '%new%' OR [Status Reason] like '%assigned%' OR [Status Reason] like '%Rejected%')").ToString());
                                intImplemented = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "[Suspect Area]='" + drDat["Suspect Area"] + "' AND [Status Reason] like '%implemented%'").ToString());
                                intReleased = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "[Suspect Area]='" + drDat["Suspect Area"] + "' AND [Status Reason] like '%released%'").ToString());
                                intVerified = Convert.ToInt32(dsRep.Tables[2].Compute("COUNT(ID)", "[Suspect Area]='" + drDat["Suspect Area"] + "' AND [Status Reason] like '%verify%'").ToString());

                                drDataR["NewAssignedRejected"] = intNewAssignedRejected;
                                drDataR["Implemented"] = intImplemented;
                                drDataR["Released"] = intReleased;
                                drDataR["Verified"] = intVerified;
                                drDataR["Total"] = intNewAssignedRejected + intImplemented + intReleased + intVerified;

                                dtReportFAS.Rows.Add(drDataR);
                            }
                        }
                    }
                    #endregion
                    #region Func Area V1
                    dtReportFA.Columns.Add("Defect Type", typeof(string));
                    dtReportFA.Columns.Add("Open", typeof(int));
                    dtReportFA.Columns.Add("Critical", typeof(int));
                    dtReportFA.Columns.Add("High", typeof(int));
                    dtReportFA.Columns.Add("Medium", typeof(int));
                    string strProduct = "Product Found";

                    if(dsRep.Tables[2].Columns.IndexOf("Product")>-1)
                    {
                        strProduct = "Product";
                    }

                    foreach (DataRow drDat in dsRep.Tables[2].Rows)
                    {
                        if (dtReportFA.Select("[Defect Type]='" + drDat[strProduct].ToString() + "'").Count() == 0 && drDat[strProduct].ToString().Length > 0)
                        {
                            DataRow drData = dtReportFA.NewRow();

                            drData["Defect Type"] = drDat[strProduct];
                            drData["Open"] = dsRep.Tables[2].Select("[" + strProduct + "]='" + drDat[strProduct] + "'").Count();
                            drData["Critical"] = dsRep.Tables[2].Select("[" + strProduct + "]='" + drDat[strProduct] + "' AND [exposure] like '%Critical%'").Count();
                            drData["High"] = dsRep.Tables[2].Select("[" + strProduct + "]='" + drDat[strProduct] + "' AND [exposure] like '%High%'").Count();
                            drData["Medium"] = dsRep.Tables[2].Select("["+strProduct+"]='" + drDat[strProduct] + "' AND [exposure] like '%Medium%'").Count();

                            dtReportFA.Rows.Add(drData);
                        }
                    }
                    #endregion

                }
            }
            myModel.AO = dtReportAO;
            myModel.AS = dtReportAS;
            myModel.FA = dtReportFA;
            myModel.FAO = dtReportFAO;
            myModel.FAS = dtReportFAS;
            ViewBag.WW = WWName;
            ViewBag.WWID = WWID;
            myModel.Val = list;

            if (import.Count > 0)
                myModel.Import = import.Single();
            else
                myModel.Import = new Models.ValidationImport();
            
            myModel.ContinuityID = ContinuityID;

            return PartialView("ExpandedView",myModel);
        }
        public ActionResult Detail(string RepOrigin, string Row,string Column,int WWID, string WWName )
        {
            DataSet dsRep = (DataSet)Session["Reports"];
            DataTable dtResult = new DataTable();
            ViewBag.Type = RepOrigin;
            ViewBag.WW = WWName.Replace("_","'");
            ViewBag.WWID = WWID;
            string strCol = "";

            switch(RepOrigin)
            {
                case "FA" :
                    strCol = "Product Found";

                    if(dsRep.Tables[2].Columns.IndexOf("Product")>0)
                    {
                        strCol = "Product";
                    }

                    if (Column.Equals("Open"))
                    {
                        dtResult = dsRep.Tables[2].Select("["+strCol+"]='" + Row + "'").CopyToDataTable();
                    }
                    else
                    {
                        dtResult = dsRep.Tables[2].Select("[" + strCol + "]='" + Row + "' AND [exposure] like '%" + Column + "%'").CopyToDataTable();
                    }
                    break;
                case "AO" :
                    switch (Column)
                    {
                        case "Total": dtResult = dsRep.Tables[2].Copy();
                            break;
                        case "Open":
                            dtResult = JoinDataTables(dsRep.Tables[2].Select("[exposure] like '%" + Row + "%'").CopyToDataTable(), dsRep.Tables[1].Select("[exposure] like '%" + Row + "%'").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                        case "Owners":
                            DataTable dtResults = dsRep.Tables[1].Select("[Exposure] like '%" + Row + "%'").CopyToDataTable();
                            DataView dtView = new DataView(dtResults);
                            dtResult.Columns.Add("Owner");
                            dtResult.Columns.Add("Defects");

                            foreach (DataRow drData in dtView.ToTable(true, "Owner").Rows)
                            {
                                DataRow drNew = dtResult.NewRow();

                                drNew["Owner"] = drData["Owner"];
                                drNew["Defects"] = dtResults.Select("[Owner]='" + drData["Owner"] + "'").Count().ToString();

                                dtResult.Rows.Add(drNew);
                            }
                            break;
                    }
                    break;
                case "FO":
                    if (Column == "NumOwners")
                    {
                        DataTable dtResults = dsRep.Tables[1].Select("[Suspect Area]='" + Row + "'").CopyToDataTable();
                        DataView dtView = new DataView(dtResults);
                        dtResult.Columns.Add("Owner");
                        dtResult.Columns.Add("Projects");

                        foreach (DataRow drData in dtView.ToTable(true, "Owner").Rows)
                        {
                            DataRow drNew = dtResult.NewRow();

                            drNew["Owner"] = drData["Owner"];
                            drNew["Projects"] = dtResults.Select("[Owner]='" + drData["Owner"] + "'").Count().ToString();

                            dtResult.Rows.Add(drNew);
                        }
                    }
                    else
                    {
                        DataTable dtResults = JoinDataTables(dsRep.Tables[1].Select("[Suspect Area]='" + Row + "'").CopyToDataTable(), dsRep.Tables[2].Select("[Suspect Area]='" + Row + "'").CopyToDataTable(), (row1, row2) => row1.Field<string>("Suspect Area") == row2.Field<string>("Suspect Area"));
                        DataView dvRes = new DataView(dtResults);
                        dtResult = dvRes.ToTable("FO", false, "title", "id", "Exposure", "Status Reason", "Suspect Area", "Owner", "Age(d)", "Last Touched Duration", "Priority");
                    }
                    break;
                case "FAS":
                    strCol = "Status Reason";

                    if(dsRep.Tables[2].Columns.IndexOf("State")>0)
                    {
                        strCol = "State";
                    }

                    switch(Column)
                    {
                        case "Total": dtResult = JoinDataTables(dsRep.Tables[2].Select("[Suspect Area]='" + Row + "'").CopyToDataTable(), dsRep.Tables[1].Select("[Suspect Area]='" + Row + "'").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                        case "NewAssignedRejected" :
                            dtResult = JoinDataTables(dsRep.Tables[2].Select("[Suspect Area]='" + Row + "' AND ([" + strCol + "] like '%New%' OR [" + strCol + "] like '%Assigned%' OR [" + strCol + "] like '%Rejected%')").CopyToDataTable(), dsRep.Tables[1].Select("[Suspect Area]='" + Row + "' AND ([" + strCol + "] like '%New%' OR [" + strCol + "] like '%Assigned%' OR [" + strCol + "] like '%Rejected%')").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                        default :
                            dtResult = JoinDataTables(dsRep.Tables[2].Select("[Suspect Area]='" + Row + "' AND [" + strCol + "] like '%" + Column + "%'").CopyToDataTable(), dsRep.Tables[1].Select("[Suspect Area]='" + Row + "' AND [" + strCol + "] like '%" + Column + "%'").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                    }
                    break;
                case "AS":
                    strCol = "Status Reason";

                    if(dsRep.Tables[2].Columns.IndexOf("State")>0)
                    {
                        strCol = "State";
                    }

                    switch (Column)
                    {
                        case "Total": dtResult = JoinDataTables(dsRep.Tables[2].Select("[exposure] like '%" + Row.Replace("_", " ") + "%'").CopyToDataTable(), dsRep.Tables[1].Select("[exposure] like '%" + Row.Replace("_", " ") + "%'").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                        case "NewAssignedRejected":
                            dtResult = JoinDataTables(dsRep.Tables[2].Select("[exposure] like '%" + Row.Replace("_", " ") + "%' AND ([" + strCol + "] like '%New%' OR [" + strCol + "] like '%Assigned%' OR [" + strCol + "] like '%Rejected%')").CopyToDataTable(), dsRep.Tables[1].Select("[exposure] like '%" + Row.Replace("_", " ") + "%' AND ([" + strCol + "] like '%New%' OR [" + strCol + "] like '%Assigned%' OR [" + strCol + "] like '%Rejected%')").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                        default:
                            dtResult = JoinDataTables(dsRep.Tables[2].Select("[exposure] like '%" + Row.Replace("_", " ") + "%' AND [" + strCol + "] like '%" + Column + "%'").CopyToDataTable(), dsRep.Tables[1].Select("[exposure] like '%" + Row.Replace("_", " ") + "%' AND [" + strCol + "] like '%" + Column + "%'").CopyToDataTable(), (row1, row2) => row1.Field<string>("id") == row2.Field<string>("id"));
                            break;
                    }
                    break;
            }

            if (dtResult.Columns.Contains("title"))
            {
                int intIndex = 1;
                DataColumn[] Columns=new DataColumn[dtResult.Columns.Count];
                dtResult.Columns.CopyTo(Columns, 0);
 
                foreach (DataColumn dcColumn in Columns)
                {
                    if (dcColumn.ColumnName.Contains("title"))
                    {
                        dtResult.Columns[dcColumn.ColumnName].SetOrdinal(0);
                    }
                    else
                    {
                        try
                        {
                            dtResult.Columns[dcColumn.ColumnName].SetOrdinal(intIndex);
                            intIndex++;
                        }
                        catch { }
                    }
                }
            }

            myModel.Result = dtResult;
            ViewBag.Complete = false;
            return View("DetailView", myModel);
        }
        private DataTable JoinDataTables(DataTable t1, DataTable t2, params Func<DataRow, DataRow, bool>[] joinOn)
        {
            DataTable result = new DataTable();
            foreach (DataColumn col in t1.Columns)
            {
                if (result.Columns[col.ColumnName] == null)
                    result.Columns.Add(col.ColumnName, col.DataType);
            }
            foreach (DataColumn col in t2.Columns)
            {
                if (result.Columns[col.ColumnName] == null)
                    result.Columns.Add(col.ColumnName, col.DataType);
            }
            foreach (DataRow row1 in t1.Rows)
            {
                var joinRows = t2.AsEnumerable().Where(row2 =>
                {
                    foreach (var parameter in joinOn)
                    {
                        if (!parameter(row1, row2)) return false;
                    }
                    return true;
                });
                foreach (DataRow fromRow in joinRows)
                {
                    DataRow insertRow = result.NewRow();
                    foreach (DataColumn col1 in t1.Columns)
                    {
                        insertRow[col1.ColumnName] = row1[col1.ColumnName];
                    }
                    foreach (DataColumn col2 in t2.Columns)
                    {
                        insertRow[col2.ColumnName] = fromRow[col2.ColumnName];
                    }
                    result.Rows.Add(insertRow);
                }
            }
            return result;
        }
        public ActionResult RYG(int WWID,string WeekName, string ContinuityID)
        {
            ViewBag.WWID = WWID;
            ViewBag.WW = WeekName;
            ViewBag.ContinuityID = ContinuityID;

            return PartialView("RYGView", myModel);
        }
        public ActionResult DeletePhases(int Phases)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PhaseId", Phases);

            DataSet Delete = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCDeleteValidationphase", param);

            return JavaScript("$('#Phase-" + Phases + "').hide()");
        }
        public ActionResult ChartInfo(int WWID,string Continuity)
        {
            ViewBag.WWID = WWID;
            ViewBag.Continuity = Continuity;
            List<Models.ValidationReport> list = new List<Models.ValidationReport>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ValidationReport>("SPROCgetValidationReport @WWID = {0}, @Continuity={1}", new object[] { WWID, Continuity }).ToList<Models.ValidationReport>();
            }

            myModel = list;

            return PartialView("ChartInfoView",myModel);
        }
        [ValidateInput(false)]
        public ActionResult SaveReport(int WWID,string Continuity, string Name,string Link, bool Delete, int Id)
        {
            List<Models.ValidationReport> list = new List<Models.ValidationReport>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ValidationReport>("SPROCUpdateValidationChart @WWID={0}, @ContinuityID = {1}, @ReportName={2}, @ReportLink={3}, @Delete={4}, @Id={5} ", new object[] { WWID, Continuity, Name, Link, Delete, Id }).ToList<Models.ValidationReport>();
                
            }
            return Json(list.First());
        }
        
    }
 
}