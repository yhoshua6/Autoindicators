﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 

namespace AutoIndicators.Controllers
{
    public class CalendarController : Controller
    {
        
        public ActionResult PDFExport()
        {
            int year = DateTime.Now.Year;
            var list = new List<Models.Calendar>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", year).ToList<Models.Calendar>();
            }
            return new Rotativa.ViewAsPdf("Calendar", list) { FileName = "SnapshotWW.pdf" };
        }

    
        public static int getCurrentWorkingWeek()
        {
            int WWID = 0;
            bool isData = false;
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();
            var list = new List<Models.SnapshotIndex>();
            int i = 0;

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>();
                

                do
                {
                    list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", result[i].WWID).ToList<Models.SnapshotIndex>();
                    isData = list.Count() > 0;

                    if (isData)
                    {
                        WWID = result[i].WWID;
                    }
                    else
                    {
                        i++;
                    }
                } while (!isData);

            }
            return WWID;
            
        }

        public bool getDBdata(int WWID)
        {
            var list = new List<Models.SnapshotIndex>();
            var catPLC = new List<Models.PLCList>();
            var userList = new List<Models.Users>();
            IEnumerable<SelectListItem> PlCList;
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                catPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();

                PlCList = catPLC.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
            }
            if (list.Count() > 0)
            {
                return true;
            }
            else
            { return false; }
        }
        // GET: /Calendar/
        public ActionResult Index(int? yearChose)
        {
            if (Session["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Session["Module"].ToString()))
            {
                Session["Module"] = "Calendar";
                Session.Remove("DataSource");
            }
            else
                Session["Module"] = "Calendar";

            int year;
            if (yearChose == null)
            {
                year = DateTime.Now.Year;
            }
            else 
            {
                year = (int)yearChose;
            }
            
            var list = this.getCalendarList(year);

            return View("Calendar", list);
        }

        public List<Models.Calendar> getCalendarList(int year)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
               return dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", year).ToList<Models.Calendar>(); 
            }
        }
	}
}

