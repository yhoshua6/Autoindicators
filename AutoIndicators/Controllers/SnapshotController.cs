﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Text;
using AutoIndicators.ExtensionMethods;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Specialized;

namespace AutoIndicators.Controllers
{

    public class SnapshotController : Controller
    {

        dynamic myModel = new ExpandoObject();
        int weekStatus = 0;
        
        public ActionResult RedFlag(int PWID)
        {
            try
            {
                List<Models.HealthIndicator> list = new List<Models.HealthIndicator>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.HealthIndicator>("SPROCGetHealthIndicatorReds @PWID = {0}", PWID).ToList<Models.HealthIndicator>();
                }
                return View(list);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult onOffWW(int pWWID, int switcho)
        {
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("[SPROCswitchEditWW] @WWID = {0}, @Switch = {1}", new object[] { pWWID, switcho });
            }
            
            return Index();
        }
        public ActionResult NotifWeekly(int WWID)
        {
            List<Models.NotifAssignees> lstNotifAssign = new List<Models.NotifAssignees>();
            NotifController notif = new NotifController();
            var list = new List<Models.SnapshotIndex>();
            string strBody="";

            List<string> People=new List<string>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                lstNotifAssign = dc.Database.SqlQuery<Models.NotifAssignees>("[SPROCSendNotifWeekly] @WWID={0}, @bManual={1}",new object[]{WWID,true}).ToList<Models.NotifAssignees>();
            }

            

            if (lstNotifAssign.Count() > 0)
            {
                strBody = "You are receiving this mail because you have been identified as a program owner within the snapshot suite application. As of now you are delinquent in making your updates for this week.   Please update your program status ASAP.";

                notif.SendEmail(WWID, list.First().weekName, strBody, lstNotifAssign, "Incomplete Snapshot Update for " + list.First().weekName);


                strBody= "List of Delincuents Snapshot update :<br/><br/>";


                foreach (Models.NotifAssignees email in lstNotifAssign)
                {
                    if (!People.Exists(x => x == email.Email))
                    {
                        strBody += "<a href='mailto:" + email.Email + "'>"+email.Name+"</a><br/>";
                        People.Add(email.Email);
                    }
                }

            }

            lstNotifAssign.Clear();
            Models.NotifAssignees noti = new Models.NotifAssignees();
            noti.Email = "ronda.hall@intel.com";
            //noti.Email = "jorge.l.orozco.ruiz@intel.com";
            lstNotifAssign.Add(noti);

            if (strBody.Length== 0)
            {
                strBody = "You don't have Delincuents for " + list.First().weekName;
            }

            notif.SendEmail(WWID, list.First().weekName, strBody, lstNotifAssign, "Incomplete Snapshot Update for " + list.First().weekName + " (Sumary)");

            return new EmptyResult();
        }
        //
        // GET: /Snapshot/
        public bool getDBdata(int WWID)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            var list = new List<Models.SnapshotIndex>();
            var catPLC = new List<Models.PLCList>();
            var PTPLC = new List<Models.PLCList>();
            var userList = new List<Models.Users>();
            var VerticalList = new List<Models.Vertical>();
            var NotifList = new List<Models.Notif>();
            NotifController notif = new NotifController();
            List<Models.NotifAssignees> lstNotifAssign=new List<Models.NotifAssignees>();

            IEnumerable<SelectListItem> PlCList;
            IEnumerable<SelectListItem> VerticalEnum;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                catPLC = dc.Database.SqlQuery<Models.PLCList>("[SPROCcatPLCStatus]").ToList<Models.PLCList>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                VerticalList = dc.Database.SqlQuery<Models.Vertical>("[SPROCcatVertical]").ToList<Models.Vertical>();
                //lstNotifAssign = dc.Database.SqlQuery<Models.NotifAssignees>("[SPROCSendNotifWeekly]").ToList<Models.NotifAssignees>();

                if (list.Count > 0)
                {
                    this.weekStatus = int.Parse(list[0].weekStatus.ToString());
                }
                //foreach (Models.SnapshotIndex snap in list)
                //{
                //    var PLCStatus = dc.Database.SqlQuery<Models.PLCList>("[SPROCgetPLCStatusByPT] @intPT = {0}", snap.ProgTypeID).ToList<Models.PLCList>();
                //    snap.PTPLCStatus = PLCStatus.Select(x => new SelectListItem()
                //    {
                //        Text = x.Name,
                //        Value = x.Id.ToString()
                //    });
                //}
                string strBody = "You are receiving this mail because you have been identified as a program owner within the snapshot suite application. As of now you are delinquent in making your updates for this week.   Please update your program status ASAP.";                
                
                             
                VerticalEnum = VerticalList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString()
                    });

                PlCList = catPLC.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                //lstNotifAssign.Clear();

                //if (lstNotifAssign.Count > 0)
                //{
                //    Models.NotifAssignees noti = new Models.NotifAssignees();
                //    noti.Email = "ronda.hall@intel.com";
                //    lstNotifAssign.Add(noti);

                //    noti = new Models.NotifAssignees();

                //    noti.Email = "raul.lobato.wong@intel.com";
                //    lstNotifAssign.Add(noti);

                //    noti = new Models.NotifAssignees();

                //    noti.Email = "jorge.luisx.orozco.ruiz@intel.com";
                //    lstNotifAssign.Add(noti);

                //    notif.SendEmail(WWID, list.First().weekName, strBody, lstNotifAssign, "Snapshot Update for " + list.First().weekName + " not completed");
                //}
            }
            if (list.Count() > 0)
            {
                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
                myModel.list = list;
                myModel.PlCList = PlCList;
                myModel.validUsers = userList;
                myModel.VerticalList = VerticalEnum;
                return true;
            }
            else
            { return false; }
        }

        public bool getUsers()
        {
            var userList = new List<Models.Users>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                myModel.validUsers = userList;
                return true;
            }
        }


        public List<Models.SnapshotIndex> getDBdataHistoric(int WWID)
        {
            var list = new List<Models.SnapshotIndex>();
            var userList = new List<Models.Users>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }
            if (list.Count > 0)
            {
                ViewBag.WW = list[0].weekName;
                ViewBag.WWID = list[0].WWID;
            }

            ViewBag.validUsers = userList;
            return list;


        }


        #region PDF
        public ActionResult PDFExport()
        {
            int year = DateTime.Now.Year;
            var list = new List<Models.Calendar>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", year).ToList<Models.Calendar>();
            }
            return new Rotativa.ViewAsPdf("SnapshotPDF", list) { FileName = "SnapshotWW.pdf" };
        }
        
        public ActionResult Test()
        {
            string strBodyCreate = "";
            string strBodyHi = "";
            string strBodySchedule = "";

            strBodyCreate = "<p style=\"color:blue\">Program Owners:</p>";
            strBodyCreate += "<p>The <a href='http://indicators.zpn.intel.com'>WW50'16</a> Snapshot is now active. All Snapshot Updates due NLT WW50.5 Friday 5PM Weekly.</p>";
            
            strBodyCreate += "<p style=\"color:blue\">Ground Rules Reminder:<br><ul><li><b>Be Concise</b> in your <a href='https://wiki.ith.intel.com/display/PEDPLC/Exec+Communication+101'>Exec Communication,/a> and please follow the <a href='https://wiki.ith.intel.com/display/PEDPLC/Exec+Communication+101'>TTA</a> Guidance when making your weekly updates</li>";
            strBodyCreate += "<li><b>Link Your Acronyms</b> using the auto link if it has already been created or manual link to create one.</li>";
            strBodyCreate += "<li><b>Default Update:</b>  Consistent with TTA Guidance.  Must have Topic-Impact, Timeline-Tension and Actions clearly articulate.  If your update is longer than 3 sentences then it needs to be blessed by your manager.</li>";
            strBodyCreate += "<li><b>Schedule Updates:</b>  including Milestone Document Posting, for each Milestone upon completion.  This is now the POR Schedule Data for our Projects.</li>";
            strBodyCreate += "<li><b>Project Risk Aging:</b>  Any Red Flag (Show Stopper) for a Program in the Program Health should have comments no older than two Weeks.</li>";
            strBodyCreate += "<li><b>PCOS Data:</b> Program Owners should verify that their PCOS Data for their project is current.  If not then drive closure each week.</li>";
            strBodyCreate += "<li><b>CCB Data:</b> POR CCB Data is the CCB App by end of Oct’16.  Program Owners are to update and instruct their PDT members.  Any program not using it should have an approved Exception. We are working to include an Exemption Flag in the App where appropriate.</li>";
            strBodyCreate += "<li><b>Indicator App:</b>  We will start using the Indicator App for all SPX Programs at the next Purley Night MRC.</li>";
            strBodyCreate += "<li><b>Feedback:</b> Any Program Owners that encounter issues using the App Suite should Click the Feedback in the Upper Left corner of the App to submit their issue.  We will review their feedback and prioritize weekly.  If the issue prevents them from using the App they should contact me to get help.</li></ul></p>";
            strBodyCreate += "<p>This is an automated message please do not respond to this mail as the mailbox is not monitored. Issues or questions contact Ronda Hall.</p>";

            strBodyHi = "<p>You are receiving this mail because your program risks have not been updated for the past <font color=\"red\">two weeks or more</font>.</p>";
            strBodyHi += "<p><font color=\"red\">AR:<p> Please update program risks to show current status.";

            strBodySchedule = "This is a reminder that your program has a ‘schedule milestone’ approaching.   Please remember to update the schedule by posting your milestone documentation.   ";

            List<Models.NotifAssignees> rond = new List<Models.NotifAssignees>();
            Models.NotifAssignees userond = new Models.NotifAssignees();
            userond.Email = "ronda.hall@intel.com";
            rond.Add(userond);

            NotifController notif = new NotifController();
            notif.SendEmail(101, "WW50'16", strBodyCreate, rond, "Snapshot W50’16 Activated");
           // notif.SendEmail(101, "WW50'16", strBodyHi, rond, "Your Program Risks Need Updated");
           // notif.SendEmail(101, "WW50'16", strBodySchedule, rond, "Upcoming Milestone notificaiton");

            return new EmptyResult();
        }
        public ActionResult CreateNewWW()
        {
            string usr = User.Identity.Name;
            string strWWName = "";
            string strWWNameOld="";
            int intWW = 0;
            //var result = new List<Models.DefaultWW>();
            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
            List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();
            NotifController notif = new NotifController();
            List<Models.NotifMailing> mails = new List<Models.NotifMailing>();

            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            try
            {

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@systemUser", usr);
                string strBodyCreate = "";
                string strBodyHi = "";
                string strBodySchedule = "";
                DataSet dsRes = new DataSet();
                dsRes = SqlHelper.ExecuteDataset(tran, "SPROCreateNewWW", param);
                Models.NotifMailing mail = new Models.NotifMailing();
                List<Models.NotifAssignees> lstNotifCreate = new List<Models.NotifAssignees>();
                List<Models.NotifAssignees> lstNotifHI = new List<Models.NotifAssignees>();
                List<Models.NotifAssignees> lstNotifSchedule = new List<Models.NotifAssignees>();
                List<string> lstPeople = new List<string>();

                strWWName = dsRes.Tables[1].Rows[0]["weekName"].ToString();
                intWW = Convert.ToInt32(dsRes.Tables[1].Rows[0]["id"].ToString());

                //strWWName = "WW9'17";
                //intWW = 114;

                #region Creation WW Notif
                List<Models.Notif> lstNotif = notif.Create(0, 2, 1, "Creation " + strWWName, "0", "Creation " + strWWName, DateTime.Now.Date, 1, 1, 0, usr, intWW, tran);
                strBodyCreate = "<p style=\"color:blue\">Program Owners:</p>";
                strBodyCreate += "<p>The <a href='http://indicators.zpn.intel.com'>"+strWWName+"</a> Snapshot is now active. All Snapshot Updates due NLT "+strWWName.Substring(1,3)+".5 Friday 5PM Weekly.</p>";

                strBodyCreate += "<p style=\"color:blue\">Ground Rules Reminder:<br><ul><li><b>Be Concise</b> in your <a href='https://wiki.ith.intel.com/display/PEDPLC/Exec+Communication+101'>Exec Communication</a> and please follow the <a href='https://wiki.ith.intel.com/display/PEDPLC/Exec+Communication+101'>TTA</a> Guidance when making your weekly updates</li>";
                strBodyCreate += "<li><b>Link Your Acronyms</b> using the auto link if it has already been created or manual link to create one.</li>";
                strBodyCreate += "<li><b>Default Update:</b>  Consistent with TTA Guidance.  Must have Topic-Impact, Timeline-Tension and Actions clearly articulate.  If your update is longer than 3 sentences then it needs to be blessed by your manager.</li>";
                strBodyCreate += "<li><b>Schedule Updates:</b>  including Milestone Document Posting, for each Milestone upon completion.  This is now the POR Schedule Data for our Projects.</li>";
                strBodyCreate += "<li><b>Project Risk Aging:</b>  Any Red Flag (Show Stopper) for a Program in the Program Health should have comments no older than two Weeks.</li>";
                strBodyCreate += "<li><b>PCOS Data:</b> Program Owners should verify that their PCOS Data for their project is current.  If not then drive closure each week.</li>";
                strBodyCreate += "<li><b>CCB Data:</b> POR CCB Data is the CCB App by end of Oct’16.  Program Owners are to update and instruct their PDT members.  Any program not using it should have an approved Exception. We are working to include an Exemption Flag in the App where appropriate.</li>";
                strBodyCreate += "<li><b>Indicator App:</b>  We will start using the Indicator App for all SPX Programs at the next Purley Night MRC.</li>";
                strBodyCreate += "<li><b>Feedback:</b> Any Program Owners that encounter issues using the App Suite should Click the Feedback in the Upper Left corner of the App to submit their issue.  We will review their feedback and prioritize weekly.  If the issue prevents them from using the App they should contact me to get help.</li></ul></p>";
                strBodyCreate += "<p>This is an automated message please do not respond to this mail as the mailbox is not monitored. Issues or questions contact Ronda Hall.</p>";

                param = new SqlParameter[1];
                param[0] = new SqlParameter("@intWW", intWW);

                dsRes = SqlHelper.ExecuteDataset(tran, "SPROCgetAssigneesNewNotifWW", param);

                if (dsRes != null && dsRes.Tables.Count > 0)
                {
                    foreach (DataRow drData in dsRes.Tables[0].Rows)
                    {
                        Models.NotifAssignees Assig = new Models.NotifAssignees();
                        Assig.Email = drData["Email"].ToString();
                        lstNotifCreate.Add(Assig);
                    }
                }
                mail.Body = strBodyCreate;
                mail.To = lstNotifCreate;
                mail.Title="Snapshot "+strWWName+" Activated";
                mail.Type = 1;

                mails.Add(mail);
                
                #endregion
                #region Creation AutoNotif
                
                DataSet dsEmail = SqlHelper.ExecuteDataset(tran, "SPROCreateNewWWAutoNotif");

                if (dsEmail != null && dsEmail.Tables.Count > 0)
                {
                    if (dsEmail.Tables[0].Rows.Count > 0)
                    {
                        #region HI
                        //foreach (DataRow drData in dsEmail.Tables[0].Select("NotifType=2").Distinct())
                        //{
                        //    mail = new Models.NotifMailing();
                        //    lstNotifHI.Clear();
                            
                        //    strBodyHi = "<p>You are receiving this mail because your program risks have not been updated for the past <font color=\"red\">two weeks or more</font>.</p>";
                        //    strBodyHi += "<p><font color=\"red\">AR:<p> Please update program risks to show current status.<br/>";

                        //    foreach(DataRow item in dsEmail.Tables[0].Select("NotifType=2 and Email='"+drData["Email"]+"'"))
                        //    {
                        //        strBodyHi += item["Item"] + " for " + item["Program"]+"<br/>";
                        //    }
                            
                        //    Models.NotifAssignees Assig = new Models.NotifAssignees();
                        //    Assig.Email = drData["Email"].ToString();
                        //    lstNotifHI.Add(Assig);

                        //    mail.Body = strBodyHi;
                        //    mail.Type = 2;
                        //    mail.Title = "Your Program Risks Need Updated";
                        //    mail.To = lstNotifHI;
                        //    mails.Add(mail);
                        //}
                        #endregion
                        #region Schedule

                        #region 2 weeks remain
                        foreach (DataRow drData in dsEmail.Tables[0].Select("NotifType=3").Distinct())
                        {
                            mail = new Models.NotifMailing();
                            lstNotifSchedule.Clear();

                            strBodySchedule = "This is a reminder that your program has a ‘schedule milestone’ approaching.   Please remember to update the schedule by posting your milestone documentation.   ";

                            foreach (DataRow item in dsEmail.Tables[0].Select("NotifType=3 and Email='" + drData["Email"] + "'"))
                            {
                                strBodyHi += item["Item"] + " for " + item["Program"] + "<br/>";
                            }

                            Models.NotifAssignees Assig = new Models.NotifAssignees();
                            Assig.Email = drData["Email"].ToString();
                            lstNotifSchedule.Add(Assig);

                            mail.Body = strBodyHi;
                            mail.Title = "Upcoming Milestone notification";
                            mail.To = lstNotifHI;
                            mail.Type = 3;
                            mails.Add(mail);
                        }
                        #endregion
                        #region Due 1 week remain
                        foreach (DataRow drData in dsEmail.Tables[0].Select("NotifType=6").Distinct())
                        {
                            mail = new Models.NotifMailing();
                            lstNotifSchedule.Clear();

                            strBodySchedule = "Due";

                            foreach (DataRow item in dsEmail.Tables[0].Select("NotifType=6 and Email='" + drData["Email"] + "'"))
                            {
                                strBodyHi += item["Item"] + " for " + item["Program"] + "<br/>";
                            }

                            Models.NotifAssignees Assig = new Models.NotifAssignees();
                            Assig.Email = drData["Email"].ToString();
                            lstNotifSchedule.Add(Assig);

                            mail.Body = strBodyHi;
                            mail.Title = "Milestone Due notification";
                            mail.To = lstNotifHI;
                            mail.Type=6;
                            mails.Add(mail);
                        }
                        #endregion
                        #endregion
                    }
                }
               
                #endregion
                tran.Commit();


                foreach (Models.NotifMailing send in mails)
                {
                    if (send.To.Count > 0)
                    {
                         notif.SendEmail(intWW, strWWName, send.Body, send.To, send.Title);
                    }
                }
                #region Notif to Ronda
                    #region HI Delinquent
                    //if (mails.Where(x=> x.Type==2).Count() >0)
                    //{
                    //    string strSummary="";
                    
                    //    foreach(Models.NotifMailing mailSent in mails.Where(x=> x.Type==2).Distinct())
                    //    {
                    //        foreach(Models.NotifAssignees assig in mailSent.To)
                    //        {
                    //            if (!lstPeople.Exists(x => x == assig.Email))
                    //            {
                    //                 strSummary += "<a href=mailto:" + assig.Email + ">" + assig.Name + "</a><br>";
                    //                 lstPeople.Add(assig.Email);
                    //            }
                    //        }
                        
                    //    }

                    //    List<Models.NotifAssignees> rond = new List<Models.NotifAssignees>();
                    //    Models.NotifAssignees userond = new Models.NotifAssignees();
                    //    userond.Email = "ronda.hall@intel.com";
                    //    //userond.Email="jorge.luisx.orozco.ruiz@intel.com";
                    //    rond.Add(userond);

                    //    notif.SendEmail(intWW, strWWName, strSummary, rond, "Your Program Risks Need Updated owners");
                    //}
                    #endregion
                    #region Schedule 2 weeks remain
                        if (mails.Where(x => x.Type == 3).Count() > 0)
                        {
                            string strSummary = "";
                            lstPeople.Clear();
                            foreach (Models.NotifMailing mailSent in mails.Where(x => x.Type == 3).Distinct())
                            {
                                foreach (Models.NotifAssignees assig in mailSent.To)
                                {
                                    if (!lstPeople.Exists(x => x == assig.Email))
                                    {
                                        strSummary += "<a href=mailto:" + assig.Email + ">" + assig.Name + "</a><br>";
                                        lstPeople.Add(assig.Email);
                                    }
                                }
                            }

                            List<Models.NotifAssignees> rond = new List<Models.NotifAssignees>();
                            Models.NotifAssignees userond = new Models.NotifAssignees();
                            userond.Email = "ronda.hall@intel.com";
                            //userond.Email = "jorge.luisx.orozco.ruiz@intel.com";
                            rond.Add(userond);

                            notif.SendEmail(intWW, strWWName, strSummary, rond, "Upcoming Milestone notification owners");
                        }
                    #endregion
                    #region Schedule due 1 week remain
                    if (mails.Where(x => x.Type == 6).Count() > 0)
                    {
                        string strSummary = "";
                        lstPeople.Clear();

                        foreach (Models.NotifMailing mailSent in mails.Where(x => x.Type == 7).Distinct())
                        {
                            foreach (Models.NotifAssignees assig in mailSent.To)
                            {
                                if (!lstPeople.Exists(x => x == assig.Email))
                                {
                                    strSummary += "<a href=mailto:" + assig.Email + ">" + assig.Name + "</a><br>";
                                    lstPeople.Add(assig.Email);
                                }
                            }
                        }

                        List<Models.NotifAssignees> rond = new List<Models.NotifAssignees>();
                        Models.NotifAssignees userond = new Models.NotifAssignees();
                        userond.Email = "ronda.hall@intel.com";
                        //userond.Email = "jorge.luisx.orozco.ruiz@intel.com";
                        rond.Add(userond);

                        notif.SendEmail(intWW, strWWName, strSummary, rond, "Milestone Due notification owners");
                    }
                #endregion
                    #region Schedule Delinquents
                        //if (mails.Where(x => x.Type == 7).Count() > 0)
                        //{
                        //    string strSummary = "";
                        //    lstPeople.Clear();

                        //    foreach (Models.NotifMailing mailSent in mails.Where(x => x.Type == 7).Distinct())
                        //    {
                        //        foreach (Models.NotifAssignees assig in mailSent.To)
                        //        {
                        //            if (!lstPeople.Exists(x => x == assig.Email))
                        //            {
                        //                strSummary += "<a href=mailto:" + assig.Email + ">" + assig.Name + "</a><br>";
                        //                lstPeople.Add(assig.Email);
                        //            }
                        //        }
                        //    }

                        //    List<Models.NotifAssignees> rond = new List<Models.NotifAssignees>();
                        //    Models.NotifAssignees userond = new Models.NotifAssignees();
                        //    userond.Email = "ronda.hall@intel.com";
                        //    //userond.Email = "jorge.luisx.orozco.ruiz@intel.com";
                        //    rond.Add(userond);

                        //    notif.SendEmail(intWW, strWWName, strSummary, rond, "Delinquent owners for schedule");
                        //}
                    #endregion
                #endregion
            }
            catch(Exception ex)
            {
                tran.Rollback();
            }

            tran.Dispose();
            conn.Close();
            conn.Dispose();
            
            return RedirectToAction("/");

        }


        #endregion

        public static List<Models.Calendar> Calendar()
        {
            var list = new List<Models.Calendar>();
            int year = DateTime.Now.Year;
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Calendar>("[SPROCListCalendar] @yearN = {0}", year).ToList<Models.Calendar>();
            }

            return list;
        }

        public ActionResult SnapshotHistoric(int WWID)
        {
            try
            {
                #region Filter
                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();

                #region Init Filter
                
                    Session["Module"] = "SnapshotHistoric";
                    Session.Remove("DataSource");
                
                mFil.strField = "PXTName";
                mFil.strLabelField = "PXT";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgName";
                mFil.strLabelField = "Program";
                mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "ProgType";
                mFil.strLabelField = "Type";
                mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                mFil = new Models.mFilter();

                mFil.strField = "OwnerName";
                mFil.strLabelField = "Owner";
                mFil.strLinkedField = "PXTName";
                mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                mFilters.Add(mFil);

                ViewBag.Filter = mFilters;
                if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())))
                    WWID = Convert.ToInt32(Session["FilWW"].ToString());
                #endregion
                #region Build Filter
                List<object> objFil = new List<object>();
                List<Models.SnapshotIndex> dataFilList = new List<Models.SnapshotIndex>();
                dataFilList = getDBdataHistoric(WWID);

                if (Session["DataSource"] != null)
                {
                    Session["CancelActivate"] = true;
                }

                foreach (Models.SnapshotIndex snap in dataFilList)
                {
                    Object o = new object();

                    o = (object)snap;
                    objFil.Add(o);
                }
                

                ViewBag.objFil = objFil;
                #endregion
                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
                fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data Filter
                List<Models.SnapshotIndex> resFil = new List<Models.SnapshotIndex>();
                if (Request.Cookies["FilString"] != null)
                {
                    fFil.dtData = (DataTable)Session["DataFil"];
                    if (fFil.dtData != null && fFil.dtData.Select(Request.Cookies["FilString"].Value.ToString()).Count() > 0)
                    {
                        fFil.dtResults = fFil.dtData.Select(Request.Cookies["FilString"].Value.ToString()).CopyToDataTable();
                        fFil.vParseTo((object)new Models.SnapshotIndex());

                        if (fFil.oResults.Count > 0)
                        {
                            foreach (Models.SnapshotIndex SnapShot in fFil.oResults)
                            {
                                resFil.Add(SnapShot);
                            }
                        }
                    }
                    else
                    {
                        resFil = dataFilList;
                    }
                }
                else
                {
                    resFil = dataFilList;
                }

                myModel.list = resFil;
                #endregion

                #endregion
                return View("~/Views/Snapshot/SnapshotHistoric.cshtml", myModel.list);
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public ActionResult Filter(string id,string completed="")
        {
            List<string> lstValSel = new List<string>();
            Models.FilterCfg fil = new Models.FilterCfg();
            
            if(id.Length>0)
            {
                try
                {
                    using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        fil = dc.Database.SqlQuery<Models.FilterCfg>("SPROCgetFilter @id={0},@vchName = {1}",new object[]{0, id}).ToList<Models.FilterCfg>().Single();
                        fil.Details = dc.Database.SqlQuery<Models.FilterDetails>("SPROCgetFilterDetails @id = {0}", fil.Id).ToList<Models.FilterDetails>();
                    }
                }
                catch
                {
                    fil = null;
                }

                if (fil != null)
                {
                    if (Session["Prefilter"] == null || (bool)Session["Prefilter"])
                    {
                        foreach (Models.FilterDetails det in fil.Details)
                        {
                            lstValSel.Add(det.strValue);
                        }

                        Session["ValSel"] = lstValSel;
                        Response.Cookies["FilString"].Value = fil.strFilterString;
                        Session["DataFil"] = Session["DataFil"];
                        Session["DataSource"] = Session["DataSource"];
                        Session["CancelActivate"] = 1;
                        Session["Prefilter"] = true;
                        Session["ValSelFil"] = Session["ValSel"] = lstValSel;
                        Response.Cookies["FilStringFil"].Value = fil.strFilterString;
                    }else
                    {
                        Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
                        Session.Remove("DataFil");
                        Session.Remove("DataSource");
                        Session.Remove("CancelActivate");
                        Session.Remove("ValSel");
                    }
                }
                else
                {
                    Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
                    Session.Remove("DataFil");
                    Session.Remove("DataSource");
                    Session.Remove("CancelActivate");
                    Session.Remove("ValSel");
                    Session["Prefilter"] = null;
                }
            }
            else
                Session["Prefilter"] = null;

            return Index(completed.Length>0? true :false);
        }
        public ActionResult Index(bool complete = false)
        {
            return View("~/Views/Josue/josue.cshtml", myModel);
        }

        public ActionResult Main(bool complete = false)
        {
            int? WWID = null;
            //flag is for user clicking on the completed snapshot link
            int intWeekStatusFlag=0;

            //if(clear)
            //{
            //    Session["Prefilter"] = null;
            //    Response.Cookies["FilString"].Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies["ValSel"].Expires = DateTime.Now.AddDays(-1);
            //    Session.Remove("ValSel");
            //}

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                //ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"];
                var result = new List<Models.DefaultWW>();

                List<Models.mFilter> mFilters = new List<Models.mFilter>();
                Models.mFilter mFil = new Models.mFilter();
                try
                {
                    #region Filter
                    if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
                    {
                        Response.Cookies["Module"].Value = "Snapshot";
                        Session.Remove("DataSource");
                    }
                    else
                        Response.Cookies["Module"].Value = "Snapshot";

                    #region Init Filter

                    mFil.strField = "VerticalName";
                    mFil.strLabelField = "Swim Lanes";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();
                    mFil.strField = "PXTName";
                    mFil.strLabelField = "PXT";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ProgType";
                    mFil.strLabelField = "Type";
                    //      mFil.strLinkedField = "PXTName";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "OwnerName";
                    mFil.strLabelField = "Owner";
                    //       mFil.strLinkedField = "ProgType";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ClasificationName";
                    mFil.strLabelField = "Clasification";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);

                    mFil = new Models.mFilter();

                    mFil.strField = "ProgName";
                    mFil.strLabelField = "Program";
                    //       mFil.strLinkedField = "OwnerName";
                    mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

                    mFilters.Add(mFil);



                    ViewBag.Filter = mFilters;
                    #endregion
                    ViewBag.Calendar = Calendar();

                    if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())))
                        WWID = Convert.ToInt32(Session["FilWW"].ToString());
                    #endregion
                    if (WWID == null)
                    {
                        Models.DefaultWW WWs = new Models.DefaultWW();

                        result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>();

                            Models.DefaultWW currentWW = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").SingleOrDefault();

                            if (complete)
                            {
                                WWs = result.Where(x => x.WWID == currentWW.WWID - 1).SingleOrDefault();
                            }
                            else
                            {
                                WWs = currentWW;
                            }
                       

                        getDBdata(WWs.WWID);
                        myModel.WWID = WWs.WWID;
                        ViewBag.WeekStatus = WWs.weekstatus;


                        #region Build Filter
                        List<object> objFil = new List<object>();
                        List<Models.SnapshotIndex> dataFilList = new List<Models.SnapshotIndex>();

                        //dataFilList = getPrograms(myModel.WWID);
                        dataFilList = myModel.list;

                        if (Session["DataSource"] != null)
                        {
                            Session["CancelActivate"] = true;
                        }

                        foreach (Models.SnapshotIndex snap in dataFilList)
                        {
                            Object o = new object();

                            o = (object)snap;
                            objFil.Add(o);
                        }


                        ViewBag.objFil = objFil;
                        #endregion
                        #region PreLoad
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.oSource = objFil;
                        fFil.mFiltersToShow = mFilters;
                //        fFil.vCreateFilter();

                        Session["DataFil"] = fFil.dtData;
                        Session["DataSource"] = fFil.oSource;

                        #endregion

                        #region Load Data Filter
                        List<Models.SnapshotIndex> resFil = new List<Models.SnapshotIndex>();
                        List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();

                        if (Request.Cookies["FilString"] != null)
                        {
                            fFil.dtData = (DataTable)Session["DataFil"];
                            //   string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                            #region Apply Filter


                            List<string> strValue;

                            if (Session["ValSel"] != null)
                                strValue = (List<string>)Session["ValSel"];
                            else
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            resFil = ((List<Models.SnapshotIndex>)myModel.list).Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            #endregion
                        }
                        else
                        {
                            resFil = (List<Models.SnapshotIndex>)myModel.list;
                        }

                        myModel.list = resFil;
                        #endregion
                    }
                    else if (WWID != null)
                    {
                        myModel.WWID = WWID;
                        this.getUsers();

                        #region Filter
                        #region Build Filter
                        List<object> objFil = new List<object>();
                        List<Models.SnapshotIndex> list = new List<Models.SnapshotIndex>();


                        if (getDBdata(myModel.WWID))
                        {
                            list = myModel.list;
                            foreach (Models.SnapshotIndex snap in list)
                            {
                                Object o = new object();

                                o = (object)snap;
                                objFil.Add(o);
                            }
                        }

                        ViewBag.objFil = objFil;
                        #endregion
                        #region PreLoad
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.oSource = objFil;
                        fFil.mFiltersToShow = mFilters;
                        fFil.vCreateFilter();

                        Session["DataFil"] = fFil.dtData;
                        Session["DataSource"] = fFil.oSource;

                        #endregion
                        #region Load Data Filter
                        List<Models.SnapshotIndex> resFil = new List<Models.SnapshotIndex>();


                        if (Request.Cookies["FilString"] != null)
                        {
                            fFil.dtData = (DataTable)Session["DataFil"];

                            // string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                            #region Applay Filter

                            List<string> strValue;

                            if (Session["ValSel"] != null)
                                strValue = (List<string>)Session["ValSel"];
                            else
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }
                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            resFil = ((List<Models.SnapshotIndex>)myModel.list).Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                            #endregion
                        }
                        else
                        {
                            resFil = list;
                        }

                        myModel.list = resFil;
                        #endregion
                        #endregion
                        List<Models.Calendar> cal = GeneralClasses.ViewDataFilter.Calendar();
                        Models.Calendar singleCal = cal.Where(x => x.id == WWID).Single();
                        ViewBag.weekStatus = singleCal.weekStatus;
                    }
  
                }
                catch
                {
                    if (Session["Reload"] == null)
                    {
                        Request.Cookies.Remove("Val");
                        Request.Cookies.Remove("FilString");
                        Session["Reload"] = "1";
                        Index(complete);
                    }
                }
            }

            ViewBag.Complete = complete;
            return View("~/Views/Snapshot/Snapshot.cshtml", myModel);
        }
        public ActionResult projectEdit()
        {
            return PartialView(myModel);
        }
        #region comments content handling
      
        public static string RemoveUnwantedHtmlTags(string html, List<string> unwantedTags)
        {
            if (String.IsNullOrEmpty(html))
            {
                return html;
            }

            var document = new HtmlDocument();
            document.LoadHtml(html);

            //remove all style attributes
            var elementsWithStyleAttribute = document.DocumentNode.SelectNodes("//@style");

            if (elementsWithStyleAttribute != null)
            {
                foreach (var element in elementsWithStyleAttribute)
                {
                    if (!element.Attributes.Any(x => x.Name.Equals("data-art") && x.Value.Equals("1")))
                    {
                            element.Attributes["style"].Remove();
                    }
                }
            }

            elementsWithStyleAttribute = document.DocumentNode.SelectNodes("//@class");

            if (elementsWithStyleAttribute != null)
            {
                foreach (var element in elementsWithStyleAttribute)
                {
                    if (!element.Attributes.Any(x => x.Name.Equals("data-art") && x.Value.Equals("1")))
                    {
                        element.Attributes["class"].Remove();
                    }
                }
            }

            HtmlNodeCollection tryGetNodes = document.DocumentNode.SelectNodes("./*|./text()");

            if (tryGetNodes == null || !tryGetNodes.Any())
            {
                return html;
            }

            var nodes = new Queue<HtmlNode>(tryGetNodes);

            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                var childNodes = node.SelectNodes("./*|./text()");

                if (childNodes != null)
                {
                    foreach (var child in childNodes)
                    {
                        nodes.Enqueue(child);
                    }
                }

                if (unwantedTags.Any(tag => tag == node.Name) && node.Name!="p" && !node.Attributes.Any(x => x.Name.Equals("data-art") && x.Value.Equals("1")))
                {
                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            parentNode.InsertBefore(child, node);
                        }
                    }

                    parentNode.RemoveChild(node);

                }
            }

            return document.DocumentNode.InnerHtml;
        }
        public static string RemoveUnwantedHtmlTagsInProcess(string html, List<string> unwantedTags)
        {
            if (String.IsNullOrEmpty(html))
            {
                return html;
            }

            var document = new HtmlDocument();
            document.LoadHtml(html);

            //remove all style attributes
            var elementsWithStyleAttribute = document.DocumentNode.SelectNodes("//@style");

            if (elementsWithStyleAttribute != null)
            {
                foreach (var element in elementsWithStyleAttribute)
                {
                     element.Attributes["style"].Remove();
                }
            }

            elementsWithStyleAttribute = document.DocumentNode.SelectNodes("//@class");

            if (elementsWithStyleAttribute != null)
            {
                foreach (var element in elementsWithStyleAttribute)
                {
                    element.Attributes["class"].Remove();
                }
            }

            HtmlNodeCollection tryGetNodes = document.DocumentNode.SelectNodes("./*|./text()");

            if (tryGetNodes == null || !tryGetNodes.Any())
            {
                return html;
            }

            var nodes = new Queue<HtmlNode>(tryGetNodes);

            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                var childNodes = node.SelectNodes("./*|./text()");

                if (childNodes != null)
                {
                    foreach (var child in childNodes)
                    {
                        nodes.Enqueue(child);
                    }
                }

                if (unwantedTags.Any(tag => tag == node.Name) && node.Name != "p")
                {
                    if (childNodes != null)
                    {
                        foreach (var child in childNodes)
                        {
                            parentNode.InsertBefore(child, node);
                        }
                    }

                    parentNode.RemoveChild(node);

                }
            }

            return document.DocumentNode.InnerHtml;
        }

        public static List<string> unwantedTags()
        {
            List<string> unwantedTags = new List<string>();
            unwantedTags.Add("span");
            unwantedTags.Add("div");
            unwantedTags.Add("b");
            unwantedTags.Add("i");
            unwantedTags.Add("p");
            unwantedTags.Add("a");
            unwantedTags.Add("strong");
            return unwantedTags;
        }

        #endregion

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Snapshot(int PWID, int opType, string newVal, string WWID)
        {
            List<Models.ScalarValue> list = new List<Models.ScalarValue>();
            List<Models.Acronym> Acronym = new List<Models.Acronym>();
            List<Models.Acronym> AcronymsFound = new List<Models.Acronym>();
            string usr = User.Identity.Name;
            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    if (opType == 1)//comments update
                    {
                        newVal = RemoveUnwantedHtmlTags(newVal, unwantedTags()).Trim();
                        list = dc.Database.SqlQuery<Models.ScalarValue>("SPROCUpdateCommentxWW @newComment= {0}, @PWID = {1}, @systemUser = {2}, @ownerEmail={3}", new object[] { newVal, PWID, usr, GeneralClasses.General.FindEmailByAccount(usr) }).ToList<Models.ScalarValue>();
                        return Json(list[0].ID);
                    }
                    else if (opType == 2)//PLC update
                    {
                        list = dc.Database.SqlQuery<Models.ScalarValue>("SPROCUpdatePLCStatusxWW @newStatus= {0}, @PWID = {1}, @systemUser = {2}", new object[] { newVal, PWID, usr }).ToList<Models.ScalarValue>();
                        return Json(list[0].ID);
                    }
                    else if (opType == 3) //Acronym Search
                    {
                        int i = 0;
                        newVal = RemoveUnwantedHtmlTags(newVal, unwantedTags());
                        foreach (string word in newVal.Split(' '))
                        {
                            Acronym = dc.Database.SqlQuery<Models.Acronym>("SPROCgetAcronyms @abbreviationString= {0}", new object[] { word }).ToList<Models.Acronym>();
                            if (Acronym.Count != 0)
                            {
                                AcronymsFound.Add(Acronym[0]); 
                                i++;
                            }
                         }
                    }
                }
                this.getDBdata(int.Parse(WWID));
                myModel.WWID = WWID.ToString();
                //element to scroll into on webpage, trs on table are identified by the PWID
                //ViewBag.elementFocus = PWID;
                System.Web.HttpContext.Current.Session["elementFocus"] = list[0].ID;
                return View(myModel);


            }
            catch (Exception ex)
            {
                string e = ex.Message;
                throw;
            }

        }
        [ValidateInput(false)] 
        public ActionResult AddAcronymLink(string Row, string Word, int WWID,string ContinuityID,int Owner,int ProgType,int PXT)
        {
            List<Models.Acronym> lstAcronym = new List<Models.Acronym>();
            List<Models.Users> userList=new List<Models.Users>();

            Models.Acronym acron = new Models.Acronym();

            myModel.Row = Row;

            if (Word.Length > 0)
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    lstAcronym = dc.Database.SqlQuery<Models.Acronym>("SPROCgetAllAcronyms @WWID={0}", WWID).ToList<Models.Acronym>(); //We get all the Acronyms that exist
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                    if (lstAcronym.Count > 0)
                    {

                        switch (lstAcronym.First().ContextActorId)
                        {
                            case 1:
                                if (lstAcronym.Where(x => x.ContextId == ContinuityID && x.Abbreviation == Word).Count() > 0)
                                    acron = lstAcronym.Where(x => x.ContextId == ContinuityID && x.Abbreviation == Word).ToList().First();
                                else
                                {
                                    if (lstAcronym.Where(x => x.OwnerId == Owner && x.Abbreviation == Word).Count() > 0)
                                    {
                                        if (lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation == Word).Count() > 0)
                                        {
                                            acron = lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation == Word).ToList().First();
                                        }
                                        else
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).Count() > 0)
                                            {
                                                acron = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).ToList().First();
                                            }
                                            else
                                            {
                                                acron = lstAcronym.Where(x => x.OwnerId == Owner && x.Abbreviation == Word).ToList().First();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation == Word).Count() > 0)
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).Count() > 0)
                                            {
                                                acron = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).ToList().First();
                                            }
                                            else
                                            {
                                                acron = lstAcronym.Where(x => x.ProgType == ProgType && x.Abbreviation == Word).ToList().First();
                                            }
                                        }
                                        else
                                        {
                                            if (lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).Count() > 0)
                                            {
                                                acron = lstAcronym.Where(x => x.PXTId == PXT && x.Abbreviation == Word).OrderByDescending(x => x.CreationDate).ToList().First();
                                            }
                                            else
                                            {
                                                if (lstAcronym.Where(x => x.Abbreviation == Word).Count() > 0)
                                                {
                                                    acron = lstAcronym.Where(x => x.Abbreviation == Word).OrderByDescending(x => x.CreationDate).ToList().First();
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

            }
            myModel.Acronym = acron;
            myModel.Word = Word;
            ViewBag.Users = userList;
            return View(myModel);
        }

        [HttpPost]
        [ValidateInput(false)] 
        public string AddDBLink(string Abbreviation, string Link, string Definition, string Text,string ContinuityID,int WWID, int View) //Function to Save Acronym or ProperLink
        {
            string usr = User.Identity.Name;
            String ConstructingString;
            //The collection will have four values: Link, Definition, Term, and Abbreviation
            //var parames = new object[] { collection["pTerm"], collection["pAbbreviation"], collection["pDefinition"], usr, collection["pLink"]};
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (Link == "") //If the collection doesn't have a Link, it's an Acronym
                {
                    var parames = new object[] { WWID, "Default", Abbreviation, Definition, usr, Link, ContinuityID, View };
                    //Add to DB
                    dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCCreateAcronym @WWID={0}, @PTerm = {1},@PAbbreviation = {2}, @PDefinition = {3},@Pactor = {4},@PLink = {5}, @ContinuityId={6}, @View={7}", parames).ToList<Models.DefaultWW>();
                    //Create Acronym to insert
                    ConstructingString = "<span data-art=1 color='blue' title='" + Definition + "'>" + Abbreviation + "</span>";

                }
                else
                {
                    if (Abbreviation == "") //If it does have a Link, if it doesn't have an Abbreviation, it's a Proper Link
                    {
                        var parames = new object[] { Abbreviation, Definition, Link, usr, null };
                        //Add to DB
                        dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCCreateProperLinks @PName= {0}, @PDefinition = {1}, @PLink = {2},@PCreatorActor = {3},@PContextID = {4}", parames).ToList<Models.DefaultWW>();
                        ConstructingString = "<a data-art=1 href='" + Link + "'" + "title='" + Definition + @"'' style='text-decoration:underline'>" + Abbreviation + "</a>";
                    }
                    else //Otherwise it's a link Acronym
                    {
                        var parames = new object[] { WWID, "Default", Abbreviation, Definition, usr, Link, ContinuityID };
                        dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCCreateAcronym @WWID={0}, @PTerm = {1},@PAbbreviation = {2}, @PDefinition = {3},@Pactor = {4},@PLink = {5}, @ContinuityId={6}", parames).ToList<Models.DefaultWW>();
                        //Create Link to insert
                        ConstructingString = "<a data-art=1 href='" + Link + "'" + "title='" + Definition + "'' style='text-decoration:underline'>" + Abbreviation + "</a>";


                    }
                }
            }
            string res = Text.Trim().Replace(Abbreviation, ConstructingString);
            // "<p>" + Text.Trim().Replace("<p>", "").Replace("</p>", "").Replace(Abbreviation, ConstructingString) + "</p>";
            return res;
        }
        private List<Models.AcronymLinksExternal> GetNoOwnerLinks(string strSource,List<Models.Acronym> Acronyms,string ContinuityID,int Owner,int ProgType,int PXT,out string strText, bool bInternal=false,bool bInProcess=false)
        {
            List<Models.AcronymLinksExternal> words = new List<Models.AcronymLinksExternal>();
            string[] gsdd = Regex.Split(strSource, "<.*? +>|;");

            var document = new HtmlDocument();
            document.LoadHtml(strSource);

            //remove all style attributes
            var elementsWithStyleAttribute = document.DocumentNode.SelectNodes("//@style");

            if (elementsWithStyleAttribute != null)
            {
                foreach (var element in elementsWithStyleAttribute)
                {
                    //if((element.Attributes.Select(x=> x.Name.Equals("data-art") && x.Value.Equals("1")).Count()==0) || bInternal)
                    //{
                        Models.AcronymLinksExternal newAcronym = new Models.AcronymLinksExternal();
                        newAcronym.Link = element.OuterHtml;
                        //SearchRecursiveAcronym(Acronyms, element.InnerText, "", element.InnerText, ContinuityID, Owner, ProgType, PXT); ;
                        newAcronym.Texto = element.InnerText;
                        newAcronym.StartPos = strSource.IndexOf("<"+element.Name);
                        if (newAcronym.StartPos > -1)
                        {
                            if (bInProcess)
                            {
                                if(element.Name=="a")
                                    strSource = strSource.Substring(0, newAcronym.StartPos) + "ѫ" + strSource.Substring(strSource.IndexOf("</a>") + 4);
                                else
                                    strSource = strSource.Substring(0, newAcronym.StartPos) + "ѫ" + strSource.Substring(strSource.IndexOf("</span>") + 7);
                            }
                            else
                            {
                                if(element.Name=="a")
                                    strSource = strSource.Substring(0, newAcronym.StartPos) + "Ѭ" + strSource.Substring(strSource.IndexOf("</a>")+4);
                                else
                                    strSource = strSource.Substring(0, newAcronym.StartPos) + "Ѭ" + strSource.Substring(strSource.IndexOf("</span>") + 7);
                            }
                        }
                        words.Add(newAcronym);
                    ////}
                }
            }

            strText = strSource;
            return words;
        }
        private string InMiddle(string word,out string symbol)
        {
            if(word.Contains("/") &&  !word.StartsWith("/") &&  !word.EndsWith("/"))
            {
                symbol = "/";
                return word.Replace("/"," / ");
            }
            symbol = "";
            return word;
        }
        public string ReplaceMiddle(string word, string symbol)
        {
            if(symbol=="/")
            {
                return word.Replace(" / ", "/");
            }
            return word;
        }
        private string RemoveSymbols(string word, out string symbol,out string pos)
        {
            if (word.StartsWith("/") || word.EndsWith("/"))
            {
                if (word.StartsWith("/") && word.EndsWith("/"))
                {
                    pos = "b";
                }
                else if (word.StartsWith("/"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = "/";
                return word.Replace("/", " ");
            }
            else if (word.StartsWith("-") || word.EndsWith("-"))
            {
                if (word.StartsWith("-") && word.EndsWith("-"))
                {
                    pos = "b";
                }
                else if (word.StartsWith("-"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = "-";
                return word.Replace("-", " ");
            }
            else if (word.StartsWith("?") || word.EndsWith("?"))
            {
                if (word.StartsWith("?") && word.EndsWith("?"))
                {
                    pos = "b";
                }
                else if (word.StartsWith("?"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = "?";
                return word.Replace("?", " ");
            }
            else if (word.StartsWith(",") || word.EndsWith(","))
            {
                if (word.StartsWith(",") && word.EndsWith(","))
                {
                    pos = "b";
                }
                else if (word.StartsWith(","))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = ",";
                return word.Replace(",", " ");
            }
            else if (word.StartsWith(".") || word.EndsWith("."))
            {
                if (word.StartsWith(".") && word.EndsWith("."))
                {
                    pos = "b";
                }
                else if (word.StartsWith("."))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = ".";
                return word.Replace(".", " ");
            }
            else if (word.StartsWith("!") || word.EndsWith("!"))
            {
                if (word.StartsWith("!") && word.EndsWith("!"))
                {
                    pos = "b";
                }
                else if (word.StartsWith("!"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = "!";
                return word.Replace("!", " ");
            }
            else if (word.StartsWith("(") || word.EndsWith("("))
            {
                if (word.StartsWith("(") && word.EndsWith("("))
                {
                    pos = "b";
                }
                else if (word.StartsWith("("))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = "(";
                return word.Replace("(", " ");
            }
            else if (word.StartsWith(")") || word.EndsWith(")"))
            {
                if (word.StartsWith(")") && word.EndsWith(")"))
                {
                    pos = "b";
                }
                else if (word.StartsWith(")"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = ")";
                return word.Replace(")", " ");
            }
            else if (word.StartsWith(":") || word.EndsWith(":"))
            {
                if (word.StartsWith(":") && word.EndsWith(":"))
                {
                    pos = "b";
                }
                else if (word.StartsWith(":"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = ":";
                return word.Replace(":", " ");
            }
            else if (word.StartsWith(";") || word.EndsWith(";"))
            {
                if (word.StartsWith(";") && word.EndsWith(";"))
                {
                    pos = "b";
                }
                else if (word.StartsWith(";"))
                {
                    pos = "s";
                }
                else
                    pos = "e";

                symbol = ";";
                return word.Replace(";", " ");
            }
            pos = "";
            symbol = "";
            return word;
        }
        private string splitCharContains(string word)
        {
            if (word.Contains("/"))
            {
                return "/";
            }
            else if (word.Contains("-"))
            {
                return "-";
            }
            else if (word.Contains("?"))
            {
                return "?";
            }
            else if (word.Contains(","))
            {
                return ",";
            }
            else if (word.Contains("."))
            {
                return ".";
            }
            else if (word.Contains("!"))
            {
                return "!";
            }
            return "";
        }
        private bool invalidWord(string strWord)
        {
            switch(strWord)
            {
                case "/":  return true;
                    break;
                case "-": return true;
                    break;
                case "?": return true;
                    break;
                case ",": return true;
                    break;
                case ".": return true;
                    break;
                case "!": return true;
                    break;
                default: return false;
                    break;
            }
        }
        private string RemoveContainers(string word, out string sContainer)
        {
            if (word.Trim().StartsWith("(") && word.Trim().EndsWith(")"))
            {
                sContainer="(";
                return word.Replace("(", "").Replace(")", "");
            }
            else if (word.Trim().StartsWith("[") && word.Trim().EndsWith("]"))
            {
                sContainer = "[";
                return word.Replace("[", "").Replace("]", "");
            }
            else if (word.Trim().StartsWith("/") && word.Trim().EndsWith("/"))
            {
                sContainer = "/";
                return word.Replace("/", "").Replace("/", "");
            }
            else if (word.Trim().StartsWith("\"") && word.Trim().EndsWith("\""))
            {
                sContainer = "\"";
                return word.Replace("\"", "").Replace("\"", "");
            }
            else if (word.Trim().StartsWith("'") && word.Trim().EndsWith("'"))
            {
                sContainer = "'";
                return word.Replace("'", "").Replace("'", "");
            }
            sContainer = "";
            return word;
        }
        private string SearchRecursiveAcronym(ref List<Models.AcronymLinksExternal> check,List<Models.Acronym> Source,string text, string wordfound, string strSource,string ContinuityID,int Owner, int ProgType, int PXT, int View)
        {
            string word = "";
            string symbol = "";
            string pos = "";
            string sContainer = "";

            //Search the next word
            if (text.IndexOf(" ")>-1)
            {
                if (wordfound.Trim().Length > 0)
                    word = wordfound + " " + text.Substring(0, text.IndexOf(" "));
                else
                    word = text.Substring(0, text.IndexOf(" "));
            }else
            {
                if (wordfound.Trim().Length > 0)
                {
                    word = wordfound + " " + text;
                }
                else
                    word = text;
            }

            if (word.Length > 1)
            {
                word = RemoveSymbols(word, out symbol, out pos);
                word = RemoveContainers(word, out sContainer);
            }
            

            List<Models.Acronym> found = Source.Where(x => x.Abbreviation.StartsWith(word.Trim())).ToList();

            //if exists other word
            if (text.IndexOf(" ") > -1)
            {
                //is an acronym
                if (found.Count > 0)
                {
                    strSource = SearchRecursiveAcronym(ref check,Source, text.Substring(text.IndexOf(" ")).Trim(), word, strSource, ContinuityID, Owner, ProgType, PXT,View);
                }
                else if (found.Count == 0)
                {
                    string[] words=null;

                    if (splitCharContains(word).Length > 0 && wordfound.Length == 0 && !invalidWord(word))
                    {
                        words = word.Split(Char.Parse(splitCharContains(word)));
                    }
                    if(words !=null && words.Length>0)
                    {
                        string strNewWord = "";

                        if (words.Length == 2 && words[0].Length > 1)
                            strNewWord = words[0] + " " + words[1];
                        else if(word.Length> 2)
                            strNewWord =word.Replace(Char.Parse(splitCharContains(word)).ToString()," ");
                        else
                            strNewWord = words[0];

                        strSource = SearchRecursiveAcronym(ref check, Source, text.Replace(word.Trim(),strNewWord.Trim()).Trim(), "", strSource, ContinuityID, Owner, ProgType, PXT, View);
           
                    }
                    else if (splitCharContains(word).Length > 0 && (word != "{/paragraph}" && word != "{paragraph}") && wordfound.Length==0 && !invalidWord(word))
                    {
                        strSource = SearchRecursiveAcronym(ref check, Source, word.Replace(Char.Parse(splitCharContains(word)).ToString()," ") , "", strSource, ContinuityID, Owner, ProgType, PXT, View);
                    }else
                    {
                        wordfound = RemoveContainers(wordfound, out sContainer);
                        wordfound= RemoveSymbols(wordfound, out symbol, out pos);
                        
                        List<Models.Acronym> foundBef = Source.Where(x => x.Abbreviation == wordfound.Trim()).ToList();

                        if (foundBef.Count > 0 && wordfound.Trim().Length>0)
                        {
                            List<Models.Acronym> AcronymContext = new List<Models.Acronym>();

                            #region Decide
                            switch (foundBef.First().ContextActorId)
                            {
                                case 1:
                                    if (foundBef.Where(x => x.ContextId == ContinuityID).Count() > 0)
                                    {
                                        if (foundBef.Where(x => x.ContextId == ContinuityID && x.View==View).Count() > 0)
                                            AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID && x.View==View).ToList();
                                        else
                                            AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID).ToList();
                                       
                                    }
                                    else
                                    {
                                        if (foundBef.Where(x => x.OwnerId == Owner).Count() > 0)
                                        {
                                            if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                            }
                                            else
                                            {
                                                if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                                {
                                                    AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                                }
                                                else
                                                {
                                                    AcronymContext = foundBef.Where(x => x.OwnerId == Owner).ToList();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                            {
                                                if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                                {
                                                    AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                                }
                                                else
                                                {
                                                    AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                                }
                                            }
                                            else
                                            {
                                                if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                                {
                                                    AcronymContext = foundBef.Where(x => x.PXTId == PXT).OrderByDescending(x => x.CreationDate).ToList();
                                                }
                                                else
                                                {
                                                    AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 2:
                                    AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                    break;
                                case 3:
                                    AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                    break;
                                case 4:
                                    AcronymContext = foundBef.Where(x => x.ContextId == PXT.ToString()).ToList();
                                    break;
                                default:
                                    AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                    break;
                            }
                            #endregion

                            if (sContainer.Length > 0)
                            {
                                if (sContainer == "(")
                                    wordfound = "(" + wordfound + ")";
                                else if (sContainer == "[")
                                    wordfound = "[" + wordfound + "]";
                                else if (sContainer == "\"")
                                    wordfound = "\"" + wordfound + "\"";
                                else if (sContainer == "'")
                                    wordfound = "'" + wordfound + "'";
                            }

                            string strNewWord = AcronymContext.First().Link != null && AcronymContext.First().Link.Length > 0 ?
                                        "<a href='" + AcronymContext.First().Link + "'" + " title='" + AcronymContext.First().Definition + "' data-art=1 style='text-decoration: underline'>" + wordfound.Trim() + "</a>" :
                                        "<span data-art=1 style='color: #428bca' title='" + AcronymContext.First().Definition + "'>" + wordfound.Trim() + "</span>";

                            if(pos.Length>0)
                            {
                                if (pos == "s")
                                    strNewWord = strNewWord.Replace(wordfound.Trim(), symbol + wordfound.Trim());
                                else if(pos=="e")
                                    strNewWord = strNewWord.Replace(wordfound.Trim(), wordfound.Trim() + symbol);
                                else
                                    strNewWord = strNewWord.Replace(wordfound.Trim(), symbol + wordfound.Trim() + symbol);
                            }
                            

                            string strNewPrhase = "";
                            List<Models.AcronymLinksExternal> checks = GetNoOwnerLinks(strSource, Source, ContinuityID, Owner, ProgType, PXT, out strSource, true,true);
                            
                            foreach(Models.AcronymLinksExternal acron in checks)
                            {
                                check.Add(acron);
                            }
                            if (Regex.IsMatch(strSource, String.Format(@"\b{0}\b", wordfound.Trim())))
                            {
                                Regex rgx = new Regex(String.Format(@"\b{0}\b", wordfound.Trim()));
                                strNewPrhase = rgx.Replace(strSource, strNewWord, 1);
                                //strNewPrhase = Regex.Replace(strSource, String.Format(@"\b{0}\b", wordfound.Trim()), strNewWord, RegexOptions.ExplicitCapture);
                            }
                            else
                            {
                                strNewPrhase = strSource.Replace(wordfound.Trim(), strNewWord);
                            }

                            strSource = SearchRecursiveAcronym(ref check, Source, text.Substring(text.IndexOf("")).Trim(), "", strNewPrhase, ContinuityID, Owner, ProgType, PXT,View);
                        }
                        else
                        {
                            if (wordfound.Trim().Length != 1)
                            {
                                if (wordfound.Trim().Length > 0)
                                {
                                    strSource = SearchRecursiveAcronym(ref check, Source, text.Replace(wordfound, "").Trim(), "", strSource, ContinuityID, Owner, ProgType, PXT, View); 
                                }
                                else
                                {
                                    strSource = SearchRecursiveAcronym(ref check, Source, text.Substring(text.IndexOf(" ")).Trim(), "", strSource, ContinuityID, Owner, ProgType, PXT, View);
                                }
                            }else
                            {
                                strSource = SearchRecursiveAcronym(ref check, Source, text.Trim(), "", strSource, ContinuityID, Owner, ProgType, PXT, View);
                            }
                        }
                    }
                }
            }
            else
            {
                string strWordS = word;

                if (strWordS.Split(' ').Length > 1 && found.Count==0)
                {
                    wordfound = RemoveContainers(wordfound, out sContainer);
                    wordfound= RemoveSymbols(wordfound, out symbol, out pos);
                    

                    List<Models.Acronym> foundBef = Source.Where(x => x.Abbreviation == wordfound.Trim()).ToList();

                    if (foundBef.Count > 0 && wordfound.Trim().Length > 0)
                    {
                        List<Models.Acronym> AcronymContext = new List<Models.Acronym>();
                        #region Decide
                        switch (foundBef.First().ContextActorId)
                        {
                            case 1:
                                if (foundBef.Where(x => x.ContextId == ContinuityID).Count() > 0)
                                {
                                    if (foundBef.Where(x => x.ContextId == ContinuityID && x.View == View).Count() > 0)
                                        AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID && x.View == View).ToList();
                                    else
                                        AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID).ToList();
                                }
                                else
                                {
                                    if (foundBef.Where(x => x.OwnerId == Owner).Count() > 0)
                                    {
                                        if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                        {
                                            AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                        }
                                        else
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                            }
                                            else
                                            {
                                                AcronymContext = foundBef.Where(x => x.OwnerId == Owner).ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                            }
                                            else
                                            {
                                                AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                            }
                                        }
                                        else
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).OrderByDescending(x => x.CreationDate).ToList();
                                            }
                                            else
                                            {
                                                if (foundBef.Where(x => x.View == View).Count()>0)
                                                {
                                                    AcronymContext = foundBef.Where(x=> x.View==View).ToList();
                                                }
                                                else
                                                {
                                                    AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case 2:
                                AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                break;
                            case 3:
                                AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                break;
                            case 4:
                                AcronymContext = foundBef.Where(x => x.ContextId == PXT.ToString()).ToList();
                                break;
                            default:
                                AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                break;
                        }
                        #endregion

                        if (sContainer.Length > 0)
                        {
                            if (sContainer == "(")
                                wordfound = "(" + wordfound + ")";
                            else if (sContainer == "[")
                                wordfound = "[" + wordfound + "]";
                            else if (sContainer == "\"")
                                wordfound = "\"" + wordfound + "\"";
                            else if (sContainer == "'")
                                wordfound = "'" + wordfound + "'";
                        }

                        string strNewWord = AcronymContext.First().Link != null && AcronymContext.First().Link.Length > 0 ?
                                    "<a href='" + AcronymContext.First().Link + "'" + " title='" + AcronymContext.First().Definition + "' data-art=1 style='text-decoration: underline'>" + wordfound.Trim() + "</a>" :
                                    "<span data-art=1 style='color: #428bca' title='" + AcronymContext.First().Definition + "'>" + wordfound.Trim() + "</span>";

                        if (pos.Length > 0)
                        {
                            if (pos == "s")
                                strNewWord = strNewWord.Replace(wordfound.Trim(), symbol + wordfound.Trim());
                            else if (pos == "e")
                                strNewWord = strNewWord.Replace(wordfound.Trim(), wordfound.Trim() + symbol);
                            else
                                strNewWord = strNewWord.Replace(wordfound.Trim(), symbol + wordfound.Trim() + symbol);
                        }

                        string strNewPrhase = "";
                        List<Models.AcronymLinksExternal> checks = GetNoOwnerLinks(strSource, Source, ContinuityID, Owner, ProgType, PXT, out strSource, true,true);

                        foreach (Models.AcronymLinksExternal acron in checks)
                        {
                            check.Add(acron);
                        }

                        if (Regex.IsMatch(strSource, String.Format(@"\b{0}\b", wordfound.Trim())))
                        {
                            Regex rgx = new Regex(String.Format(@"\b{0}\b", wordfound.Trim()));
                            strNewPrhase = rgx.Replace(strSource, strNewWord, 1);
                            //strNewPrhase = Regex.Replace(strSource, String.Format(@"\b{0}\b", wordfound.Trim()), strNewWord, RegexOptions.ExplicitCapture);
                        }else
                        {
                            strNewPrhase = strSource.Replace(wordfound.Trim(), strNewWord);
                        }

                        strSource = SearchRecursiveAcronym(ref check, Source, text.Substring(text.IndexOf("")).Trim(), "", strNewPrhase, ContinuityID, Owner, ProgType, PXT, View);
                    }

                }
                else
                {
                    strWordS = RemoveContainers(strWordS, out sContainer);
                    strWordS=RemoveSymbols(strWordS, out symbol, out pos);
                    

                    List<Models.Acronym> foundBef = Source.Where(x => x.Abbreviation == strWordS.Trim()).ToList();

                    if (foundBef.Count > 0 && strWordS.Trim().Length > 0)
                    {
                        List<Models.Acronym> AcronymContext = new List<Models.Acronym>();

                        #region Decide
                        switch (foundBef.First().ContextActorId)
                        {
                            case 1:
                                if (foundBef.Where(x => x.ContextId == ContinuityID).Count() > 0)
                                {
                                    if (foundBef.Where(x => x.ContextId == ContinuityID && x.View == View).Count() > 0)
                                        AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID && x.View == View).ToList();
                                    else
                                        AcronymContext = foundBef.Where(x => x.ContextId == ContinuityID).ToList();
                                }
                                else
                                {
                                    if (foundBef.Where(x => x.OwnerId == Owner).Count() > 0)
                                    {
                                        if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                        {
                                            AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                        }
                                        else
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                            }
                                            else
                                            {
                                                AcronymContext = foundBef.Where(x => x.OwnerId == Owner).ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (foundBef.Where(x => x.ProgType == ProgType).Count() > 0)
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).ToList();
                                            }
                                            else
                                            {
                                                AcronymContext = foundBef.Where(x => x.ProgType == ProgType).ToList();
                                            }
                                        }
                                        else
                                        {
                                            if (foundBef.Where(x => x.PXTId == PXT).Count() > 0)
                                            {
                                                AcronymContext = foundBef.Where(x => x.PXTId == PXT).OrderByDescending(x => x.CreationDate).ToList();
                                            }
                                            else
                                            {
                                                AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 2:
                                AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                break;
                            case 3:
                                AcronymContext = foundBef.Where(x => x.ContextId == ProgType.ToString()).ToList();
                                break;
                            case 4:
                                AcronymContext = foundBef.Where(x => x.ContextId == PXT.ToString()).ToList();
                                break;
                            default:
                                AcronymContext = foundBef.OrderByDescending(x => x.CreationDate).ToList();
                                break;
                        }
                        #endregion

                        if (sContainer.Length > 0)
                        {
                            if (sContainer == "(")
                                strWordS = "(" + strWordS + ")";
                            else if (sContainer == "[")
                                strWordS = "[" + strWordS + "]";
                            else if (sContainer == "\"")
                                wordfound = "\"" + wordfound + "\"";
                            else if (sContainer == "'")
                                wordfound = "'" + wordfound + "'";
                        }

                        string strNewWord = AcronymContext.First().Link != null && AcronymContext.First().Link.Length > 0 ?
                                    "<a href='" + AcronymContext.First().Link + "'" + "title='" + AcronymContext.First().Definition + "' data-art=1 style='text-decoration: underline'>" + strWordS.Trim() + "</a>" :
                                    "<span data-art=1 style='color: #428bca' title='" + AcronymContext.First().Definition + "'>" + strWordS.Trim() + "</span>";

                        List<Models.AcronymLinksExternal> checks = GetNoOwnerLinks(strSource, Source, ContinuityID, Owner, ProgType, PXT, out strSource, true, true);

                        foreach (Models.AcronymLinksExternal acron in checks)
                        {
                            check.Add(acron);
                        }

                        if (pos.Length > 0)
                        {
                            if (pos == "s")
                                strNewWord = strNewWord.Replace(strWordS.Trim(), symbol + wordfound.Trim());
                            else if (pos == "e")
                                strNewWord = strNewWord.Replace(strWordS.Trim(), strWordS.Trim() + symbol);
                            else
                                strNewWord = strNewWord.Replace(strWordS.Trim(), symbol + wordfound.Trim() + symbol);
                        }

                        if (Regex.IsMatch(strSource, String.Format(@"\b{0}\b", strWordS.Trim())))
                        {
                            Regex rgx = new Regex(String.Format(@"\b{0}\b", strWordS.Trim()));
                            strSource = rgx.Replace(strSource, strNewWord, 1);
                            //strNewPrhase = Regex.Replace(strSource, String.Format(@"\b{0}\b", wordfound.Trim()), strNewWord, RegexOptions.ExplicitCapture);
                        }
                        else
                        {
                            strSource = strSource.Replace(strWordS.Trim(), strNewWord);
                        }
                        //strSource = Regex.Replace(strSource, String.Format(@"\b{0}\b", strWordS), strNewWord, RegexOptions.ExplicitCapture);
                    }

                }
            }
            
            return strSource;
        }
        public ActionResult DeleteAcronyms(int id,string Continuity, string Acronym)
        {
            List<Models.Acronym> lstAcronym = new List<Models.Acronym>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstAcronym = dc.Database.SqlQuery<Models.Acronym>("DeleteAcronym @id={0}, @ContinuityId={1}, @vchAcronym={2}", new object[] { id, Continuity, Acronym }).ToList<Models.Acronym>(); //We get all the Acronyms that exist
            }

            return new EmptyResult();
        }

       // [AcceptVerbs(HttpVerbs.Get)]
        [ValidateInput(false)]
        public string SearchAcronyms(string TextAcronyms, int WWID,string ContinuityID,int Owner,int ProgType,int PXT,int View)
        {

            List<Models.Acronym> Acronym = new List<Models.Acronym>();
            List<Models.Acronym> AcronymsFound = new List<Models.Acronym>();
            List<Models.ProperLinks> ProperLinks = new List<Models.ProperLinks>();
            List<Models.AcronymLinksExternal> FoundedInProcess = new List<Models.AcronymLinksExternal>();
            int PLetterFound = 0; // Links letter flag
            int Count;
            Models.Acronym LastMatch = null;
            String ConstructedString = null;

            string TextAcronymsSearch = RemoveUnwantedHtmlTagsInProcess(TextAcronyms, unwantedTags());
            string NoHTML = Regex.Replace(TextAcronymsSearch, @"&nbsp;", "&").Trim();
            NoHTML = Regex.Replace(TextAcronymsSearch, @"&amp;", "&");

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Acronym = dc.Database.SqlQuery<Models.Acronym>("SPROCgetAllAcronyms @WWID={0}", new object[] { WWID }).ToList<Models.Acronym>(); //We get all the Acronyms that exist
            }

            //Remove previus acronyms
            List<Models.AcronymLinksExternal> check = GetNoOwnerLinks(Regex.Replace(TextAcronymsSearch, @"&nbsp;", " ").Replace("&amp;", "&").Trim(),Acronym,ContinuityID,Owner,ProgType,PXT,out NoHTML);
            //Replace special tag from text
            #region Special Allowed Tags
            NoHTML = Regex.Replace(NoHTML, "<p>", " {paragraph} ");
            NoHTML = Regex.Replace(NoHTML, "<p ", " {paragraphS} ");
            NoHTML = Regex.Replace(NoHTML, "</p>", " {/paragraph} ");
            NoHTML = Regex.Replace(NoHTML, "<ul>", " {ul} ");
            NoHTML = Regex.Replace(NoHTML, "<ul ", " {ul}  ");
            NoHTML = Regex.Replace(NoHTML, "</ul>", " {/ul} ");
            NoHTML = Regex.Replace(NoHTML, "<ol", " {ol} ");
            NoHTML = Regex.Replace(NoHTML, "</ol>", " {/ol} ");
            NoHTML = Regex.Replace(NoHTML, "<li>", " {li} ");
            NoHTML = Regex.Replace(NoHTML, "<li ", " {li}  ");
            NoHTML = Regex.Replace(NoHTML, "</li>", " {/li} ");
            //NoHTML = NoHTML.Replace("?", " {?} ");
            #endregion
            NoHTML = Regex.Replace(NoHTML, @"<[^>]+>|;", "").Trim(); //<.*? data-art=1>(.|\n)*?</.*?>
            NoHTML = Regex.Replace(NoHTML, @"&nbsp", " ").Replace("&ldquo", "\"").Replace("&bull", " &bull ").Replace("&lt", "<").Replace("&gt", ">").Trim();
            string NoHTMLnormalized = Regex.Replace(NoHTML, @"\s{2,}", " ");
            TextAcronymsSearch = NoHTMLnormalized;
            
            int contador;
            int n=1;
            List<string> strParts = new List<string>();
            //
            if(TextAcronymsSearch.Length>200)
            {
                string strPart = "";
                List<string> Words=TextAcronymsSearch.Split(' ').ToList<string>();

                foreach(string word in Words)
                {
                    if(strPart.Length+word.Length<200)
                    {
                        strPart +=" "+ word;
                    }else
                    {
                        strParts.Add(strPart);
                        strPart = word;
                    }
                }
                strParts.Add(strPart);
            }else
            {
                strParts.Add(TextAcronymsSearch);
            }

            string strResult="";

            foreach (string strPart in strParts)
            {
                string strSymbol="";
                string strPass = InMiddle(strPart.Trim(), out strSymbol);
                string strRow = SearchRecursiveAcronym(ref FoundedInProcess, Acronym, strPass, "", strPass.Trim(), ContinuityID, Owner, ProgType, PXT, View);
                //search acronyms word by word in the text
                strResult +=" "+ ReplaceMiddle(strRow,strSymbol) ;
            }
            
            //rebuild the original struct
            TextAcronymsSearch =strRestructAcronyms(strResult);
            
            //this section is only for recovery external link
            foreach(Models.AcronymLinksExternal restore in check)
            {
                TextAcronymsSearch = TextAcronymsSearch.Substring(0, TextAcronymsSearch.IndexOf("Ѭ")) + restore.Link + TextAcronymsSearch.Substring(TextAcronymsSearch.IndexOf("Ѭ") + 1);
            }
            //this section recovery all internal Acronyms 
            foreach (Models.AcronymLinksExternal restore in FoundedInProcess)
            {
                try
                {
                    TextAcronymsSearch = TextAcronymsSearch.Substring(0, TextAcronymsSearch.IndexOf("ѫ")) + restore.Link + TextAcronymsSearch.Substring(TextAcronymsSearch.IndexOf("ѫ") + 1);
                }catch(Exception ex){};
            }
             
            return TextAcronymsSearch; 
        }
        private string strRestructAcronyms(string TextAcronyms)
        {
            TextAcronyms = TextAcronyms.Replace("&amp;", "'");
            TextAcronyms = TextAcronyms.Replace("&rsquo", "’");
            TextAcronyms = TextAcronyms.Replace("&ndash", "-");
            TextAcronyms = TextAcronyms.Replace("&rdquo", "”");
            
            TextAcronyms = TextAcronyms.Replace("? <a  ", "<a ");
            TextAcronyms = TextAcronyms.Replace(") <a ", "<a ");
            TextAcronyms = TextAcronyms.Replace("* <a ", "<a ");
            TextAcronyms = TextAcronyms.Replace("? <b  ", "<b ");
            TextAcronyms = TextAcronyms.Replace("( <b ", "<b ");
            TextAcronyms = TextAcronyms.Replace("* <b ", "<b ");
            TextAcronyms = TextAcronyms.Replace("?</b>?", "?</b>");
            TextAcronyms = TextAcronyms.Replace("?</a>? ", "?</a>");
            TextAcronyms = TextAcronyms.Replace(")</b>)", ")</b>");
            TextAcronyms = TextAcronyms.Replace(")</a>)", ")</a>");
            TextAcronyms = TextAcronyms.Replace("(</b>(", "(</b>");
            TextAcronyms = TextAcronyms.Replace("(</a>(", "(</a>");
            TextAcronyms = TextAcronyms.Replace("*</b>*", "*</b>");
            TextAcronyms = TextAcronyms.Replace("*</a>*", "*</a>");

            TextAcronyms = TextAcronyms.Replace("{paragraph}", "<p>").Replace("{/paragraph}", "</p>");
            TextAcronyms = TextAcronyms.Replace("{paragraphS}", "<p ");
            TextAcronyms = TextAcronyms.Replace("{ul}  ", "<ul ");
            TextAcronyms = TextAcronyms.Replace("{ul}", "<ul>");
            TextAcronyms = TextAcronyms.Replace("{/ul}", "</ul>");
            TextAcronyms = TextAcronyms.Replace("{ol}", "<ol");
            TextAcronyms = TextAcronyms.Replace("{/ol}", "</ol>");
            TextAcronyms = TextAcronyms.Replace("{li}  ", "<li ");
            TextAcronyms = TextAcronyms.Replace("{li}", "<li>");
            TextAcronyms = TextAcronyms.Replace("{/li}", "</li>");
            TextAcronyms = TextAcronyms.Replace(" &bull ", "<br>•");

            return TextAcronyms;
        }
        private List<Models.MasterModel> getPrograms(int? WWID)
        {
            List<Models.MasterModel> Masterlist = new List<Models.MasterModel>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Masterlist = dc.Database.SqlQuery<Models.MasterModel>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.MasterModel>();
            }
            return Masterlist;
        }

    }


}