﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data;

namespace AutoIndicators.Controllers
{

    //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class ScheduleVController : Controller
    {

        dynamic myModel = new ExpandoObject();

        public ActionResult Index(int? WWIDCompare, bool complete=false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row
            List<Models.ScheduleV> list = new List<Models.ScheduleV>();
            List<Models.ScheduleV> list2 = new List<Models.ScheduleV>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            List<Models.SnapshotIndex> lstProgram = new List<Models.SnapshotIndex>();

            Models.mFilter mFil = new Models.mFilter();
            int? pWWID = null;

            #region Init Filter

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].ToString()))
            {
                Response.Cookies["Module"].Value = "ScheduleV";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "ScheduleV";

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;
           
            #endregion

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", result[0].WWID).ToList<Models.SnapshotIndex>();

                    List<object> objFil = new List<object>();

                    #region Build Filter
                        
                        list = getDBdata(result[0].WWID);
                        if (Session["DataSource"] != null)
                        {
                            Session["CancelActivate"] = true;
                        }
                        foreach (Models.ScheduleV snap in list)
                        {

                            Object o = new object();

                            o = (object)snap;
                            objFil.Add(o);
                        }
                        

                        ViewBag.objFil = objFil;
                        #endregion
                       
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;

                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    #region Load Data Filter
                    List<Models.ScheduleV> resFil = new List<Models.ScheduleV>();
                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                        #region Applay Filter

                        List<string> strValue=new List<string>();

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            if (Request.Cookies["ValSel"] != null)
                            {
                                strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                                Session["ValSel"] = strValue;
                            }
                        }

                        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                        int inti = 0;

                        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                        {
                            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                        {
                            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                        {
                            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                        {
                            strProgValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                        {
                            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                        {
                            strClasification[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                        ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                    }
                    else
                    {
                        ViewBag.Program = lstProgram;
                    }
                        #endregion
                    
                    #endregion
                
                }
                else
                {

                    #region Build Filter
                    List<object> objFil = new List<object>();

                    list = getDBdata(int.Parse(pWWID.ToString()));

                    foreach (Models.ScheduleV snap in list)
                    {

                        Object o = new object();

                        o = (object)snap;
                        objFil.Add(o);
                    }
                    ViewBag.objFil = objFil;
                    #endregion
                    #region PreLoad
                    AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                    fFil.oSource = objFil;
                    fFil.mFiltersToShow = mFilters;
                    fFil.vCreateFilter();

                    Session["DataFil"] = fFil.dtData;
                    Session["DataSource"] = fFil.oSource;

                    #endregion
                    #region Load Data Filter
                    List<Models.ScheduleV> resFil = new List<Models.ScheduleV>();
                    lstProgram = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", pWWID).ToList<Models.SnapshotIndex>();

                    if (Request.Cookies["FilString"] != null)
                    {
                        fFil.dtData = (DataTable)Session["DataFil"];
                        #region Apply Filter
                        if (Session["ValSel"] != null)
                        {

                            List<string> strValue = (List<string>)Session["ValSel"];

                            string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                            string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                            string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                            string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                            string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                            string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                            int inti = 0;

                            foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                            {
                                strPXTValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                            {
                                strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                            {
                                strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                            {
                                strProgValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                            {
                                strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            inti = 0;
                            foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                            {
                                strClasification[inti] = strVal.Split(',')[1].ToString();
                                inti++;
                            }

                            list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();

                            ViewBag.Program = lstProgram.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                        }else
                        {
                            ViewBag.Program = lstProgram;
                        }
                        #endregion
                    }else
                    {
                        if (fFil.oSource.Count > 0)
                        {
                            foreach (Models.ScheduleV sched in fFil.oSource)
                            {
                                resFil.Add(sched);
                            }

                            list = resFil;
                            ViewBag.Program = lstProgram;
                        }
                    }
                    #endregion

                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;
            
                    
                }
                myModel.Schedule=list;

                if(WWIDCompare!=null)
                {
                    list2 = getDBdata(int.Parse(WWIDCompare.ToString()));
                }
                ViewBag.Complete = complete;
                myModel.ScheduleCompare = list2;
                ViewBag.WWIDC = WWIDCompare;
                return View("~/Views/ScheduleV/ScheduleView.cshtml", myModel);
            }
        }

        public List<Models.ScheduleV> getDBdata(int WWID)
        {
            var list = new List<Models.ScheduleV>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0} ", WWID).ToList<Models.ScheduleV>();
                ViewBag.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }
            return list;
        }

        public ActionResult CommentView(int? ScheduleID)
        {
            List<Models.ScheduleV> result = new List<Models.ScheduleV>();
            var userList = new List<Models.Users>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.ScheduleV>(@"SPROCGetSchedule @ScheduleID= {0}", new object[] { ScheduleID }).ToList<Models.ScheduleV>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>().Where(x => x.UserType == 1 && x.validUsers == User.Identity.Name.Remove(0, 4)).ToList();
            }
            ViewBag.ValidUser = userList.Count > 0 ? true : false;
            return PartialView(result[0]);
        }
        public ActionResult PutOnRoadmap(string Continuity,int PTMID,bool isStart,int WWID)
        {
            Models.ScheduleRoadmap SR = new Models.ScheduleRoadmap();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                SR = dc.Database.SqlQuery<Models.ScheduleRoadmap>(@"SPROCmanScheduleRoadmap @ContinuityID= {0}, @PTMID= {1}, @Start= {2}, @WWID= {3}", new object[] { Continuity,PTMID,isStart,WWID }).ToList<Models.ScheduleRoadmap>().SingleOrDefault();
                
            }

            return Json(SR);
        }
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection)
        {
            try
            {
                string usr = User.Identity.Name;
                string comments = SnapshotController.RemoveUnwantedHtmlTags(collection["txtDescription_"], SnapshotController.unwantedTags());
                string strNumber = collection["pSDateWW"].Trim().Length == 1 ? "0" : "";
                strNumber+=collection["pSDateWW"];
                string strValue = "";

                if (strNumber.Trim().Length > 0 && collection["pSDateYear"].Trim().Length>0)
                    strValue= collection["WQS"] + " " + strNumber.Trim() + "'" + collection["pSDateYear"];

                var parames = new object[] { collection["ScheduleID"], strValue, comments, usr, collection["pLink"] };
                //+" - "+collection["pEDateWW"]+"'"+collection["pEDateY"],comments , usr, collection["pLink"] };
                var result = new List<Models.DefaultWW>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {

                    result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWScheduleVal @scheduleID= {0}, @value = {1},@comments = {2},@systemUser = {3}, @Link = {4}", parames).ToList<Models.DefaultWW>();
                }
                System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
                //return RedirectToAction("Index", "ScheduleV", new {pWWID = result[0].WWID,weekName = result[0].weekName });
                return Json(result.SingleOrDefault());
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ScheduleView(int newSchedID, int WWID, string newValue, string newComments)
        {
            string usr = User.Identity.Name;
            List<Models.ScheduleV> list = new List<Models.ScheduleV>();

            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("SPROCUpdateWWScheduleVal @scheduleId= {0}, @value = {1}, @comments = {2}, @systemUser = {3}", new object[] { newSchedID, newValue, newComments, usr });
                    list = this.getDBdata(WWID);

                    #region Load Data Filter
                    List<Models.ScheduleV> resFil = new List<Models.ScheduleV>();
                    if (Session["FilString"] != null)
                    {
                        AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                        fFil.dtData = (DataTable)Session["DataFil"];
                        if (fFil.dtData != null && fFil.dtData.Select(Session["FilString"].ToString()).Count() > 0)
                        {
                            fFil.dtResults = fFil.dtData.Select(Session["FilString"].ToString()).CopyToDataTable();
                            fFil.vParseTo((object)new Models.ScheduleV());

                            if (fFil.oResults.Count > 0)
                            {
                                foreach (Models.ScheduleV sched in fFil.oResults)
                                {
                                    resFil.Add(sched);
                                }

                                list = resFil;
                            }
                        }
                    }
                    #endregion
                   
                    myModel.WWID = WWID.ToString();
                    return View(myModel);
                }
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                throw;
            }

        }

    }
}