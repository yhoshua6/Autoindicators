﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    public class IndicatorTypesController : Controller
    {
        dynamic myModel = new ExpandoObject();

        public ActionResult Index(int? WWID,int TemplateID,string ContinuityID)
        {
            ViewBag.WWID = WWID;
            ViewBag.ContinuityID = ContinuityID;
            ViewBag.Template = TemplateID;
            var IndicatorType = getIndicatorType(TemplateID, ViewBag.WWID, ViewBag.ContinuityID);
           
            return PartialView("IndicatorTypesView",IndicatorType);
        }
        public ActionResult InputIndicatorTypesView(string Y,string X,int WWID,string ContinuityID,int Template)
        {
            string[] strCol = X.Split(';');

            List<Models.IndicatorTypesStruct> lstStrcut = new List<Models.IndicatorTypesStruct>();


            if (strCol.Count() > 0)
            {
                for (int i = 0; i < strCol.Length; i++)
                {
                    string[] strValue = strCol[i].Split(',');
                    Models.IndicatorTypesStruct detail = new Models.IndicatorTypesStruct();
                    detail.id = strValue[0].Trim();
                    detail.strCaption = strValue[1].Trim();
                    detail.bIsTextArea = Convert.ToBoolean(strValue[2].Trim());

                    lstStrcut.Add(detail);
                }
            }

            myModel.Data = getIndicatorType(Template, WWID, ContinuityID).detail;
            
            myModel.WWID = WWID;
            myModel.Struct = lstStrcut;
            myModel.Y = Y;
            myModel.ContinuityID = ContinuityID;
            return PartialView(myModel);
        }
        public ActionResult TemplatesCfgView(int ProgramType, int WWID, string ContinuityID)
        {
            List<Models.IndicatorTypes> types = new List<Models.IndicatorTypes>();
            List<Models.IndicatorTypesStruct> typeStruct = new List<Models.IndicatorTypesStruct>();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                types = dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCGetTemplates @ProgramTypeID = {0}", new object[] { 0 }).ToList<Models.IndicatorTypes>();

                foreach(Models.IndicatorTypes type in types)
                {
                    type.structure = dc.Database.SqlQuery<Models.IndicatorTypesStruct>("SPROCGetIndicatorType @intTypeID = {0}, @bDetail={1}, @ContinuityID = {2}, @WWID= {3} ", new object[] { type.id, 1, ContinuityID, 0 }).ToList<Models.IndicatorTypesStruct>();
                }
            }

            myModel = types;
            ViewBag.ContinuityID = ContinuityID;
            ViewBag.WWID = WWID;
            return PartialView(myModel);
        }
        public ActionResult SaveCfg(string id,string caption,string axis,string area,int typeid,string cid,int WWID,int order,string deleted)
        {
            caption = caption.Replace("&amp;gt;", "<").Replace("&gt;",">");

            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ID", id);
            param[1] = new SqlParameter("@vchCaption", caption);
            param[2] = new SqlParameter("@vchAxis", axis);
            param[3] = new SqlParameter("@bIsArea", Convert.ToBoolean(area));
            param[4] = new SqlParameter("@indicatorTypeID", typeid);
            param[5] = new SqlParameter("@vchContinuityID", cid);
            param[6] = new SqlParameter("@WWID", WWID);
            param[7] = new SqlParameter("@intOrder", order);
            param[8] = new SqlParameter("@bDelete", Convert.ToBoolean(deleted));

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanIndicatorsTypesCfg", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            return JavaScript(intRes.ToString());
        }
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection)
        {
            int intRes=0;
            string strUser = User.Identity.Name;

            foreach(string col in collection.AllKeys)
            {
                if (!col.Equals("row") && !col.Equals("WWID") && !col.Equals("ContinuityID"))
                {
                    SqlParameter[] param = new SqlParameter[7];
                    param[0] = new SqlParameter("@vchValue", collection[col]);
                    param[1] = new SqlParameter("@vchCfgX",col);
                    param[2] = new SqlParameter("@vchCfgY",collection["row"]);
                    param[3] = new SqlParameter("@vchUser", strUser);
                    param[4] = new SqlParameter("@intWWID",Convert.ToInt32(collection["WWID"]));
                    param[5] = new SqlParameter("@vchContinuityID", collection["ContinuityID"]);
                    param[6] = new SqlParameter("@bDelete", false);

                    DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCManIndicatorsTypeValue", param);
                    intRes =Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());
                }
            }

            return RedirectToAction("../Indicator/Indicator", new { WWID = collection["WWID"], ContinuityID = collection["ContinuityID"] });
        }
        public ActionResult SaveTemplate(FormCollection collection)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ID", collection["idTemp"]);
            param[1] = new SqlParameter("@vchName", collection["Name"]);
            param[2] = new SqlParameter("@intWidth", Convert.ToInt32(collection["tmpWidth"]));
            param[3] = new SqlParameter("@intBorder", Convert.ToInt32(collection["tmpBorder"]));
            param[4] = new SqlParameter("@bDelete", Convert.ToInt32(collection["tmpDelete"]));

            return null;
        }
        public ActionResult DeleteTemplate(string Id, string ContinuityID)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@vchContinuityID",ContinuityID);
            param[1] = new SqlParameter("@intIndicatorTypeID", Id);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCRemoveTemplate", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());
            return null;
        }
        private Models.IndicatorTypes getIndicatorType(int Id,int intWWID,string strContinuityID)
        {
            Models.IndicatorTypes mIndicatorTypes = new Models.IndicatorTypes();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                mIndicatorTypes = (dc.Database.SqlQuery<Models.IndicatorTypes>("SPROCGetIndicatorType @intTypeID = {0}, @bDetail={1}, @ContinuityID = {2}, @WWID= {3} ", new object[] { Id, 0,strContinuityID,intWWID }).ToList<Models.IndicatorTypes>())[0];
                mIndicatorTypes.structure = dc.Database.SqlQuery<Models.IndicatorTypesStruct>("SPROCGetIndicatorType @intTypeID = {0}, @bDetail={1}, @ContinuityID = {2}, @WWID= {3} ", new object[] { Id, 1, strContinuityID, intWWID }).ToList<Models.IndicatorTypesStruct>();
                mIndicatorTypes.detail = dc.Database.SqlQuery<Models.IndicatorTypesDetail>("SPROCGetIndicatorTypeDetail @vchContinuityID = {0}, @WWID={1}, @intIndicatorType={2} ", new object[] { strContinuityID, intWWID, Id}).ToList<Models.IndicatorTypesDetail>();
            }

            return mIndicatorTypes;
        }
        
            
    }
}
