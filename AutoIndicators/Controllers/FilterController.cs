﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;

namespace AutoIndicators.Controllers
{
    public class FilterController : Controller
    {
        private dynamic myModel = new ExpandoObject();

        public ActionResult Index()
        {
            List<Models.MasterModel> list = new List<Models.MasterModel>();
            List<Models.FilterCfg> lstFilter = new List<Models.FilterCfg>();
            int? WWID = null;

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Filter";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Filter";

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());


            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();

                    if (WWID == null)
                    {
                        WWID = result[0].WWID;
                        ViewBag.WWID = WWID;
                        ViewBag.WW = result[0].weekName;
                    }else
                    {
                        ViewBag.WWID = WWID;
                        ViewBag.WW = result.Where(x=> x.WWID==WWID).Single().weekName;
                    }

                list = dc.Database.SqlQuery<Models.MasterModel>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.MasterModel>();
                lstFilter = myModel.cmbFilter = dc.Database.SqlQuery<Models.FilterCfg>("SPROCgetFilterList").ToList<Models.FilterCfg>();
            }

            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            List<object> objFil = new List<object>();

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;
            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filters = lstFilter;
            ViewBag.Filter = mFilters;

            foreach (Models.MasterModel master in list)
            {
                Object o = new object();

                o = (object)master;
                objFil.Add(o);
            }


            myModel = objFil;
            
            ViewBag.Complete = false;
            return View(myModel);
        }
        public ActionResult Save(string Name,int Priority,bool Deleted)
        {
            Models.FilterCfg fil = new Models.FilterCfg();
            var parames = new object[] { Name,User.Identity.Name,Priority,Deleted };
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                fil = dc.Database.SqlQuery<Models.FilterCfg>("SPROCmanFilter @vchName = {0}, @vchUser ={1}, @intPriority={2}, @bDeleted={3}", parames).ToList<Models.FilterCfg>().Single();
            }
            if(fil.Id>0)
            {
                return JavaScript(fil.Id.ToString());
            }
            return JavaScript("");
        }
        public ActionResult SaveDetails(FormCollection collection,int id)
        {
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("SPROCresetFilterDetail @intFilter={0}", id);
            }
            foreach (string col in collection.AllKeys)
            {
                if (!col.Equals("Field") && !col.Contains("cmb") && !col.Contains("All") && collection[col].Length > 0)
                {
                    using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                    {
                        dc.Database.ExecuteSqlCommand("SPROCmanFilterDetail @vchValue = {0}, @intFilter ={1}", new object[] { collection[col], id });
                    }
                }
            }
            return null;
        }
        public ActionResult LoadFilter(int id)
        {
            Models.FilterCfg fil = new Models.FilterCfg();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                fil = dc.Database.SqlQuery<Models.FilterCfg>("SPROCgetFilter @id = {0}", id).ToList<Models.FilterCfg>().Single();
                fil.Details = dc.Database.SqlQuery<Models.FilterDetails>("SPROCgetFilterDetails @id = {0}", id).ToList<Models.FilterDetails>();
            }
            
            return Json(fil);
        }
        public ActionResult ManUser(string User,int Filter,bool bDelete)
        {
            List<Models.FilterUsers> lstFilUser = new List<Models.FilterUsers>();
            Models.FilterUsers filUser=new Models.FilterUsers();
            filUser.strUser=User;

            if (!bDelete && filUser.strName.Equals("Unknown User"))
            {
                return JavaScript("alert(\"The email doesn't exists\")");
            }

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                var exec = dc.Database.SqlQuery<Models.FilterUsers>("SPROCmanFiltersUser @vchUser = {0}, @intFilter={1}, @bDelete={2}", new object[] { User, Filter, bDelete }).ToList<Models.FilterUsers>();
                lstFilUser = dc.Database.SqlQuery<Models.FilterUsers>("SPROCgetFilterUsers @intFilter={0}", new object[] { Filter }).ToList<Models.FilterUsers>();
            }
            return Json(lstFilUser);
        }
        public ActionResult getUsers(int Filter)
        {
            List<Models.FilterUsers> lstFilUser = new List<Models.FilterUsers>();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstFilUser = dc.Database.SqlQuery<Models.FilterUsers>("SPROCgetFilterUsers @intFilter={0}", new object[] { Filter }).ToList<Models.FilterUsers>();
            }

            return Json(lstFilUser);
        }
	}
    
}