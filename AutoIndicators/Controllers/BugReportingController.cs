﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Net.Mail;

namespace AutoIndicators.Controllers
{
    public class BugReportingController : Controller
    {
        dynamic myModel = new ExpandoObject();
        string[] validUsers = { "rondarha", "carowlan", "lkmccann", "rlobatox"};

        //
        // GET: /BugReporting/
        public ActionResult Index()
        {
           return View("~/Views/BugReporting/BugList.cshtml");
        }

        public ActionResult ArchivedBugList() 
        {
            var list = new List<Models.ArchivedBugs>();
            string userName = User.Identity.Name;
    
            int res = Array.IndexOf(validUsers, userName.Remove(0, 4));

            if (res > -1) // We enter the administrator view
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.ArchivedBugs>("SPROCgetallArchivedBugs").ToList<Models.ArchivedBugs>();
                }
                myModel.list = list;
                return View(myModel);

            }
            else
            { // We enter the normal user view, showing only those bugs that were reported by that user
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.ArchivedBugs>("SPROCgetArchivedBugsbyUser @Pactor = {0} ", User.Identity.Name).ToList<Models.ArchivedBugs>();
                }
                myModel.list = list;
                return View(myModel);
            }
        }

        public ActionResult BugList()
        {
            var list = new List<Models.BugReporting>();
            var Status = new List<Models.BugStatus>();
            IEnumerable<SelectListItem> BugStatus;
            string userName = User.Identity.Name;
            var userList = new List<Models.Users>();

            int res = Array.IndexOf(validUsers, userName.Remove(0, 4));

            /////////////////////////////////////Getting the Status for the Bugs///////////////////////////////////////
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities()) 
            {
                Status = dc.Database.SqlQuery<Models.BugStatus>("SPROCgetBugStatus").ToList<Models.BugStatus>();
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }

            BugStatus = Status.Select(x =>
                new SelectListItem()
                {
                    Text = x.Status,
                    Value = x.Id.ToString()
                });

            myModel.BugStatus = BugStatus;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
          

            if (userList.Where(x=> x.validUsers==User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\")+1)).First().UserType==1) // We enter the administrator view
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.BugReporting>("SPROCgetallBugs").ToList<Models.BugReporting>();
                }
                myModel.list = list;
            }
            else { // We enter the normal user view, showing only those bugs that were reported by that user
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.BugReporting>("SPROCgetBugsbyUser @Pactor = {0} ",User.Identity.Name).ToList<Models.BugReporting>();
                }
                myModel.list = list;
                
            }
            ViewBag.Filter = new List<Models.mFilter>();
            ViewBag.Complete = false;
            ViewBag.WWID = 0;
            ViewBag.objFil = new List<object>();
            return View(myModel);
        }

        public ActionResult AddBug()
       {
           List<Models.View> v = new List<Models.View>();

           using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
           {
               v = dc.Database.SqlQuery<Models.View>("SPROCgetViews").ToList<Models.View>();
           }

           ViewBag.Views = v;
            return PartialView();
        }

        public ActionResult AnswerBug()
        {
            return View(myModel);
        }

        public ActionResult Save(FormCollection collection)
        {
            string usr = User.Identity.Name;
            List<Models.BugReporting> bugs = new List<Models.BugReporting>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                    var parames = new object[] { collection["pTitle"], collection["pDescription"], usr, collection["pApp"] };
                    bugs=dc.Database.SqlQuery<Models.BugReporting>(@"SPROCCreateNewBug @PTitle = {0},@PDescription = {1}, @PReportingUser = {2}, @App={3}", parames).ToList<Models.BugReporting>();
            }

            string Email = GeneralClasses.General.FindEmailByAccount(User.Identity.Name);
            string UserName = GeneralClasses.General.FindDisplayNametByEmail(Email);

            MailMessage mailObj = new MailMessage();
            mailObj.From = new MailAddress(GeneralClasses.General.strEmailFrom, "Feedback");
            mailObj.To.Add(new MailAddress(Email, UserName));
            mailObj.Body = UserName + " Thanks! for you time , we'll attend your feedback as soon as possible<br><br> Your Ticket Id: <b>"+bugs.Single().id+"</b>";
            mailObj.Subject = "Snapshot - Feedback";
            mailObj.IsBodyHtml = true;

            SmtpClient SMTPServer = new SmtpClient(GeneralClasses.General.strSMTP);
            SMTPServer.Send(mailObj);
            return Json(bugs.Single());

        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BugSave(int pID, int pOPtype, string PNewComment)
        {
            string PUser = User.Identity.Name;

            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    if (pOPtype == 2)
                    {
                        int NewValue = Int32.Parse(PNewComment);
                        var parames = new object[] { pID, NewValue };
                        dc.Database.ExecuteSqlCommand(@"SPROCUpdateBugStatus @PID = {0}, @PNewStatus = {1}", parames);
                    }
                    else 
                    {
                        var parames = new object[] { pID, PNewComment};
                        dc.Database.ExecuteSqlCommand(@"SPROCUpdateBugComment @PID = {0}, @PNewComment = {1}", parames);
                        
 
                    }
                }

                
                return RedirectToAction("BugList", "BugReporting");
            }
            catch (Exception x) {
                string e = x.Message;
                throw;
            }
            
        }
        public ActionResult AcknowledgeSave(int pID)
        {
            string PUser = User.Identity.Name;
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                var parames = new object[] { pID, PUser };
                dc.Database.ExecuteSqlCommand(@"SPROCUpdateBugComment @PID = {0}, @PUser = {1}", parames);
                return Json(PUser);
            }
            return Json("");
            
        }
	}
}