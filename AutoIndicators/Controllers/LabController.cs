﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ZXing;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    public class LabController : Controller
    {
        dynamic myModel;
        List<Models.Lab> Labs = new List<Models.Lab>();
        Models.Lab Lab = new Models.Lab();
        List<Models.GEOs> GEOList = new List<Models.GEOs>();
        List<Models.Campus> CampusList = new List<Models.Campus>();
        //
        // GET: /Lab/
        public ActionResult Index(bool complete=false)
        {
            List<Models.LabPerWeek> Labs = new List<Models.LabPerWeek>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            #region Filter
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            int? WWID = null;
            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Labs";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Labs";

            #region Init Filter

            mFil.strField = "Geo";
            mFil.strLabelField = "Geo";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;
            mFilters.Add(mFil);

            mFil = new Models.mFilter();
            mFil.strField = "CampusName";
            mFil.strLabelField = "Campus";
            mFil.strLinkedField = "Geo";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;
            mFilters.Add(mFil);

            mFil = new Models.mFilter();
            mFil.strField = "LabName";
            mFil.strLabelField = "Lab";
            mFil.strLinkedField = "CampusName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;
            ViewBag.Calendar = SnapshotController.Calendar();

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Labs";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Labs";
            #endregion
            #endregion
            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {

                
                if (WWID == null)
                {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                    WWID = result[0].WWID;
                }
                else {
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x=> x.WWID==WWID).ToList<Models.DefaultWW>();
                }

                Labs = dc.Database.SqlQuery<Models.LabPerWeek>("SPROCgetLabs @WWID={0}", WWID).ToList<Models.LabPerWeek>();
            }

            try
            {
                ViewBag.WWID = WWID;
                ViewBag.WW = "'WW43'16";
                ViewBag.Complete = complete;
            }
            catch { }


            #region Build Filter
            List<object> objFil = new List<object>();

            if (Session["DataSource"] != null)
            {
                Session["CancelActivate"] = true;
            }

            foreach (Models.LabPerWeek lab in Labs)
            {
                Object o = new object();

                o = (object)lab;
                objFil.Add(o);
            }


            ViewBag.objFil = objFil;
            #endregion
            #region PreLoad
            AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

            fFil.oSource = objFil;
            fFil.mFiltersToShow = mFilters;
            fFil.vCreateFilter();

            Session["DataFil"] = fFil.dtData;
            Session["DataSource"] = fFil.oSource;

            #endregion
            ViewBag.Complete = complete;
            return View("LabsView");
        }
        public ActionResult barcode()
        {
            BarcodeReader reader = new BarcodeReader();
            reader.Options.TryHarder = true;
            reader.AutoRotate = true;
            FileStream file = new FileStream("c:/qrcode1.png", FileMode.Open);
            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(file);
            Result res = reader.Decode(bitmap);
            return View();
        }

        public ActionResult Campus()
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                GEOList = dc.Database.SqlQuery<Models.GEOs>("[SPROCcatGEO]").ToList<Models.GEOs>();
                CampusList = dc.Database.SqlQuery<Models.Campus>("[SPROCcatCampus]").ToList<Models.Campus>();
            }
            myModel = CampusList;
            ViewBag.Geo = GEOList;
            return View("CampusView", myModel);
        }
        public ActionResult SaveCampus(int Id, string Name,int GEOID,string Manager)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                CampusList = dc.Database.SqlQuery<Models.Campus>("[SPROCmanCampus] @id={0}, @vchName={1}, @GeoID={2}, @vchManager={3}, @bDeleted={4}", new object[] { Id, Name, GEOID,Manager, false }).ToList<Models.Campus>();
            }
                
                return Json(CampusList[0]);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveCampusComments(int CampusId,string Comments,int WWID)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("SPROCSaveLabComments @CampusId={0}, @txtComments={1}, @WWID={2}", new object[] { CampusId, Comments, WWID });
            }
            return new EmptyResult();
        }
        public ActionResult DataLab(int? LabID)
        {
            
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    GEOList = dc.Database.SqlQuery<Models.GEOs>("[SPROCcatGEO]").ToList<Models.GEOs>();

                    foreach(Models.GEOs geo in GEOList )
                    {
                        geo.Campus = dc.Database.SqlQuery<Models.Campus>("[SPROCcatCampus] @GeoID={0}", geo.id).ToList<Models.Campus>();
                    }

                    if (LabID != null)
                    {
                        Lab = dc.Database.SqlQuery<Models.Lab>("SPROCgetLabs @id={0}", LabID).Single();
                        Lab.Links = dc.Database.SqlQuery<Models.LabLink>("SPROCgetLabsLinks @LabId={0}", LabID).ToList<Models.LabLink>();

                        if (Lab.CampusId > 0)
                        {
                            foreach (Models.GEOs geo in GEOList)
                            {
                                if (geo.Campus.Any(x => x.id == Lab.CampusId))
                                Lab.Campus = geo.Campus.Where(x => x.id == Lab.CampusId).Single();
                            }
                        }
                    }else
                    {
                        Lab.Campus = new Models.Campus();
                    }
                }
            myModel = Lab;
            ViewBag.Geo = GEOList;
            return View("ManLabView",myModel);
        }
        public ActionResult ExcelExport(int WWID)
        {
            List<Models.LabPerWeek> LabPW = new List<Models.LabPerWeek>();
            DataTable dtResult=new DataTable();
            DataTable dtExcel = new DataTable();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                LabPW = dc.Database.SqlQuery<Models.LabPerWeek>("SPROCgetLabs @WWID={0}", WWID).ToList<Models.LabPerWeek>();
                foreach (Models.LabPerWeek lab in LabPW)
                {
                    SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
                    SqlParameter[] param = new SqlParameter[3];
                    param[0] = new SqlParameter("@ContinuityID", lab.LabContinuityID);
                    param[1] = new SqlParameter("@bExport", true);
                    param[2] = new SqlParameter("@WWID", WWID);
                    conn.Open();
                    dtResult.Merge(SqlHelper.ExecuteDataset(conn, "SPROCgetLabsSpace", param).Tables[0]);
                    conn.Close();

                }
            }

            if (Request.Cookies["FilStringLab"]!=null && Request.Cookies["FilStringLab"].Value.Length > 0)
            {
                dtExcel = dtResult.Select(Request.Cookies["FilStringLab"].Value).CopyToDataTable();
            }else
            {
                dtExcel = dtResult;
            }
            foreach(DataRow drData in dtExcel.Rows)
            {
                if (drData["Lab Champion"].ToString().Trim().Length > 0)
                {
                    drData["Lab Champion"] = GeneralClasses.General.FindDisplayNametByEmail(drData["Lab Champion"].ToString());

                }
                if (drData["Lab Manager"].ToString().Trim().Length > 0)
                {
                    drData["Lab Manager"] = GeneralClasses.General.FindDisplayNametByEmail(drData["Lab Manager"].ToString());
                }
            }
 
            
            GeneralClasses.General General = new GeneralClasses.General();

            string strTmpFile = System.Web.HttpContext.Current.Server.MapPath("~/ValidationFiles/tmp"+User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\")+1)+".xlsx");

            General.WriteToExcel(dtExcel,strTmpFile);
            return Json("/ValidationFiles/tmp"+User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\")+1)+".xlsx");
        }
        public ActionResult LabSumary(int WWID)
        {
            List<Models.LabPerWeek> LabPW = new List<Models.LabPerWeek>();
            List<Models.GEOs> Geo = new List<Models.GEOs>();
            List<Models.LabTeam> Team = new List<Models.LabTeam>();
            List<Models.LabSpaceExtraData> Extras = new List<Models.LabSpaceExtraData>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Labs";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Labs";

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                LabPW = dc.Database.SqlQuery<Models.LabPerWeek>("SPROCgetLabs @WWID={0}", WWID).ToList<Models.LabPerWeek>();
                GEOList = dc.Database.SqlQuery<Models.GEOs>("[SPROCcatGEO]").ToList<Models.GEOs>();
                Team = dc.Database.SqlQuery<Models.LabTeam>("SPROCgetLabTeams").ToList<Models.LabTeam>();
                Extras= dc.Database.SqlQuery<Models.LabSpaceExtraData>("SPROCgetLabExtraData").ToList<Models.LabSpaceExtraData>();
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").Where(x => x.WWID == WWID).ToList<Models.DefaultWW>();

                if(result.Count>0)
                {
                    ViewBag.WW = result[0].weekName;
                }

                foreach (Models.LabTeam team in Team)
                {
                    team.Team = dc.Database.SqlQuery<Models.LabTeamMember>("SPROCgetLabTeams @TeamID={0}, @bDetail={1}", team.idTeam, 1).ToList<Models.LabTeamMember>();
                }

                foreach (Models.GEOs geo in GEOList)
                {
                    geo.Campus = dc.Database.SqlQuery<Models.Campus>("[SPROCcatCampus] @GeoID={0}", geo.id).ToList<Models.Campus>();

                    foreach (Models.Campus camp in geo.Campus)
                    {
                        camp.Projects = dc.Database.SqlQuery<Models.LabProjectList>("SPROCgetLabsProjList @CampusID={0}", new object[] { camp.id }).ToList<Models.LabProjectList>();
                      
                    }
                }

                foreach(Models.LabPerWeek data in LabPW)
                {
                    data.Links = dc.Database.SqlQuery<Models.LabLink>("SPROCgetLabsLinks @LabId={0}", data.Id).ToList<Models.LabLink>();
                    foreach (Models.GEOs geo in GEOList)
                    {
                        if (geo.Campus.Any(x => x.id == data.CampusId))
                        {
                            data.Campus = geo.Campus.Where(x => x.id == data.CampusId).Single();
                            data.Campus.AllowedUsers = dc.Database.SqlQuery<Models.LabUser>("SPROgetUsersByCampus @CampusID={0}", new object[] { data.CampusId }).ToList<Models.LabUser>();
                        }
                    }
                }
                
                foreach(Models.LabPerWeek lab in LabPW)
                {
                    
                    lab.Spaces = dc.Database.SqlQuery<Models.LabSpace>("SPROCgetLabsSpace @ContinuityID={0}",lab.LabContinuityID).ToList<Models.LabSpace>();
                    
                    foreach(Models.LabSpace space in lab.Spaces)
                    {
                        space.ExtraData = Extras.Where(x => x.SpaceID == space.SpaceId).ToList();
                        int i = 0;
                        i++;
                        i++; 
                    }
                }
                foreach(Models.LabPerWeek lab in LabPW)
                {
                    lab.Extra = dc.Database.SqlQuery<Models.LabSpaceExtra>("SPROCgetExtraCol @CampusID={0}", new object[] { lab.CampusId }).ToList<Models.LabSpaceExtra>();
                }
            }

            #region Load Data Filter
            List<Models.LabPerWeek> resFil = new List<Models.LabPerWeek>();
            AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();
       
            if (Request.Cookies["FilStringLab"] != null)
            {
                fFil.dtData = (DataTable)Session["DataFil"];
                #region Apply Filter


                List<string> strValue;

                if (Session["ValSelLab"] != null)
                    strValue = (List<string>)Session["ValSelLab"];
                else
                {
                    strValue = Request.Cookies["ValSelLab"].Value.Split(';').ToList<string>();
                    Session["ValSelLab"] = strValue;
                }

                string[] strGeoValue = new string[strValue.Where(x => x.StartsWith("Geo")).Count()];
                string[] strCampusValue = new string[strValue.Where(x => x.StartsWith("CampusName")).Count()];
                string[] strLabValue = new string[strValue.Where(x => x.StartsWith("LabName")).Count()];

                int inti = 0;

                foreach (string strVal in strValue.Where(x => x.StartsWith("Geo")).ToList<string>())
                {
                    strGeoValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }
                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("CampusName")).ToList<string>())
                {
                    strCampusValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }
                inti = 0;
                foreach (string strVal in strValue.Where(x => x.StartsWith("LabName")).ToList<string>())
                {
                    strLabValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }

                if (strGeoValue.Length > 0 && strCampusValue.Length > 0 && strLabValue.Length > 0)
                {
                    resFil = LabPW.Where(x => strGeoValue.Contains(x.Geo) && strCampusValue.Contains(x.CampusName) && strLabValue.Contains(x.LabName)).ToList();
                }
                else if (strGeoValue.Length > 0)
                {
                    if (strCampusValue.Length > 0)
                    {
                        resFil = LabPW.Where(x => strGeoValue.Contains(x.Geo) && strCampusValue.Contains(x.CampusName)).ToList();
                    }
                    else if (strLabValue.Length > 0)
                    {
                        resFil = LabPW.Where(x => strGeoValue.Contains(x.Geo) && strLabValue.Contains(x.CampusName)).ToList();
                    }
                    else
                    {
                        resFil = LabPW.Where(x => strGeoValue.Contains(x.Geo)).ToList();
                    }
                }
                else if (strCampusValue.Length > 0)
                {
                    if (strLabValue.Length > 0)
                    {
                        resFil = LabPW.Where(x => strCampusValue.Contains(x.Geo) && strLabValue.Contains(x.CampusName)).ToList();
                    }
                    else
                    {
                        resFil = LabPW.Where(x => strCampusValue.Contains(x.Geo)).ToList();
                    }
                }
                else if (strLabValue.Length > 0)
                {
                    resFil = LabPW.Where(x => strLabValue.Contains(x.Geo)).ToList();
                }
                else
                    resFil = LabPW;

                #endregion
            }
            else
            {
                resFil = LabPW;
            }
            #endregion
            ViewBag.Geo = GEOList;
            ViewBag.Team = Team;
            ViewBag.WWID = WWID;
            myModel = resFil;
            return View("LabSumaryView", myModel);
        }
        public ActionResult LoadInfo()
        {
            return PartialView("LoadView");
        }
        public ActionResult RequestLab(string LabContinuityID, string Lab)
        {
            ViewBag.Lab = Lab;
            ViewBag.Continuity = LabContinuityID;

            return View("RequestView");
        }
        public ActionResult Spaces(int LPWID,bool bDetail=false)
        {
            List<Models.LabSpace> Spaces = new List<Models.LabSpace>();
            List<Models.LabTeam> Team = new List<Models.LabTeam>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Spaces = dc.Database.SqlQuery<Models.LabSpace>("SPROCgetLabsSpace @LPWID={0}", LPWID).ToList<Models.LabSpace>();
                Team = dc.Database.SqlQuery<Models.LabTeam>("SPROCgetLabTeams").ToList<Models.LabTeam>();

                foreach (Models.LabTeam team in Team)
                {
                    team.Team = dc.Database.SqlQuery<Models.LabTeamMember>("SPROCgetLabTeams @TeamID={0}, @bDetail={1}", team.idTeam, 1).ToList<Models.LabTeamMember>();
            }
            }
            ViewBag.LPWID = LPWID;
            ViewBag.Detail = bDetail;
            ViewBag.Team = Team;

            myModel = Spaces;
            return PartialView("SpaceView", myModel);
        }
        public ActionResult Save(int LabId, string Name, string Layout,string Champeon, int GeoID,int WWID, bool Delete)
        {
            if (LabId == null)
                LabId = 0;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Lab = dc.Database.SqlQuery<Models.Lab>("SPROCmanLabs @id={0}, @vchName={1}, @vchLayout={2}, @vchChampion={3}, @WWID={4}, @CampusID={5}, @bDelete={6} ",
                    new object[] { LabId, Name, Layout, Champeon, WWID, GeoID, Delete }).Single();
            }

            if (Lab.Id > 0)
            {
                return JavaScript("$('.close').click();  $('#Labs').load('Lab/LabSumary?WWID='+WWID);");
            }
            else
                return JavaScript("alert('Error')");
        }
        public ActionResult UpdateChampion(int LabId, string Name, string Layout, string Champeon, int GeoID, int WWID, bool Delete)
        {
            if (LabId == null)
                LabId = 0;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Lab = dc.Database.SqlQuery<Models.Lab>("SPROCmanLabs @id={0}, @vchName={1}, @vchLayout={2}, @vchChampion={3}, @WWID={4}, @CampusID={5}, @bDelete={6} ",
                    new object[] { LabId, Name, Layout, Champeon, WWID, GeoID, Delete }).Single();
            }

            return Json(Lab);
        }
        public ActionResult ManLink(int LabID,string Name,string Link,bool Delete)
        {
            Lab.Id = LabID;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Lab.Links = dc.Database.SqlQuery<Models.LabLink>("SPROCmanLabLinks @LabId={0}, @vchName={1}, @vchLink={2}, @bDelete={3}", new object[] { LabID,Name, Link, Delete }).ToList<Models.LabLink>();
            }

            return Json(Lab);
        }
        [ValidateInput(false)]
        public ActionResult UpdateLabPerWeek(string Field,string Value, string ContinuityID,int WWID)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if(Field.Equals("strUpdate"))
                    Value = SnapshotController.RemoveUnwantedHtmlTags(Value, SnapshotController.unwantedTags()).Trim();
                
                dc.Database.ExecuteSqlCommand("SPROCmanLabPerWeek @vchField={0}, @vchValue={1}, @ContinuityID={2}, @WWID={3}", new object[] { Field, Value, ContinuityID, WWID });
            }
            if(Value.Contains("@"))
            {
                return Json(GeneralClasses.General.FindDisplayNametByEmail(Value));
            }else
            {
                return Json(Value);
            }
            
        }
        public ActionResult SaveRequest(int Id, string LabContinuityID,string Requester,string TeamName,string BuManager,string LabChampion,string NewSpace,string Project,string DesireLoc,int StartWW,int StartYear,int EndWW,int EndYear,int WWID)
        {
            Models.LabRequest Request = new Models.LabRequest();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Request = dc.Database.SqlQuery<Models.LabRequest>("SPROCmanRequestLab @vchRequester={0} ,@vchTeamName={1}, @vchBuManager={2}, @vchLabChampion={3}, @vchNewSpace={4}, @vchProject={5}, @vchDesireLocation={6}, @intStartWW={7}, @intStartYear={8}, @intEndWW={9}, @intEndYear={10}, @vchUser={11}, @WWID={12}, @LabContinuityID={13}"
                    , new object[] { Requester,TeamName,BuManager,LabChampion, NewSpace, Project,DesireLoc, StartWW, StartYear, EndWW, EndYear,User.Identity.Name,WWID,LabContinuityID }).ToList<Models.LabRequest>().First();
            }
            return Json(Request);
        }
        public ActionResult SaveRequestNetwork(int RequestId, int C10MB, int C1GB, int C10GB, int C40GB)
        {
            List<Models.LabRequestNetwork> net = new List<Models.LabRequestNetwork>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                net = dc.Database.SqlQuery<Models.LabRequestNetwork>("SPROCmanLabReqNet @int10MB={0},@int1GB={1},@int10GB={2},@int40GB={3},@LabRequest={4}"
                    , new object[] { C10MB, C1GB, C10GB, C40GB, RequestId }).ToList<Models.LabRequestNetwork>();
            }

            return Json(net);
        }
        public ActionResult SaveRequestPlatform(int RequestId, string System6,string System19,string System23,string DensityBench,string DensityRack,int Type )
        {
            List<Models.LabRequestPlatform> Platform = new List<Models.LabRequestPlatform>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Platform = dc.Database.SqlQuery<Models.LabRequestPlatform>("SPROCmanLabReqPlatform @vchSystem6={0},@vchSystem19={1}, @vchSystem23={2}, @vchDensityBench={3}, @vchDensityRack={4}, @intType={5}, @LabRequestID={6}"
                    , new object[] { System6,System19,System23,DensityBench,DensityRack,Type, RequestId }).ToList<Models.LabRequestPlatform>();
            }

            return Json(Platform);
        }
        public ActionResult SaveRequestTime(int RequestId, string Full,string Part)
        {
            List<Models.LabRequestTime> Time = new List<Models.LabRequestTime>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Time = dc.Database.SqlQuery<Models.LabRequestTime>("SPROCmanLabReqTime @vchFull={0},@vchPart={1},@LabRequestID={2}"
                    , new object[] { Full, Part, RequestId }).ToList<Models.LabRequestTime>();
            }

            return Json(Time);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveSpaces(string Description, int Type, int Lin, int Win, int BBKC, int Util, string Comments,int Row,string Section,string Spot, int Order
            , bool Check, string LabId, bool Delete, bool Duplicate,int BRSize, int ProjectCode,string BKCID,string PlatformType,string NextPrj,string AssetNumber,string FurnType, int Id,int WWID)
        {
            List<Models.LabSpace> Time = new List<Models.LabSpace>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Time = dc.Database.SqlQuery<Models.LabSpace>("SPROCmanLabSpace @vchNumber={0},@intType={1}, @intLin={2}, @intWin={3}, @vchUpdateBy={4}, @intBBKC={5}, @txtComents={6}, @intUtil={7}, @LabID={8}, @bDelete={9}, @bDuplicate={10},@intRow={11}, @vchSection={12}, @vchSpot={13}, @intOrder={14}, @bChecked={15}, @intBRSize={16}, @intProjectCode={17}, @vchBKCID={18}, @vchPlatformType={19} ,@vchNextPrj={20}, @vchAssetNumber={21}, @vchFurnType={22}, @id={23}, @intWWID={24}"
                    , new object[] { Description, Type, Lin, Win,User.Identity.Name, BBKC, Comments,Util ,LabId, Delete,Duplicate,Row,Section,Spot, Order, Check,BRSize, ProjectCode, BKCID, PlatformType, NextPrj, AssetNumber, FurnType, Id,WWID }).ToList<Models.LabSpace>();
            }
                
            return Json(Time.Single());
        }
        public ActionResult Team(bool Detail=false)
        {
            List<Models.LabSpace> Spaces = new List<Models.LabSpace>();
            List<Models.LabTeam> Team = new List<Models.LabTeam>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Team = dc.Database.SqlQuery<Models.LabTeam>("SPROCgetLabTeams").ToList<Models.LabTeam>();

                foreach(Models.LabTeam team in Team)
                {
                    team.Team = dc.Database.SqlQuery<Models.LabTeamMember>("SPROCgetLabTeams @TeamID={0}, @bDetail={1}", team.idTeam,1).ToList<Models.LabTeamMember>();
                }
            }
            ViewBag.Detail = Detail;
            return PartialView("TeamView", Team);
        }
        public ActionResult SaveTeam(int TeamID,string Name,int SpaceID, bool Deleted)
        {
            List<Models.LabTeam> Team = new List<Models.LabTeam>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Team = dc.Database.SqlQuery<Models.LabTeam>("SPROCmanLabTeam @idTeam={0}, @vchName={1}, @idSpace={2},@bDelete={3}", new object[] { TeamID, Name, SpaceID, Deleted }).ToList<Models.LabTeam>();
            }

            return Json(Team.First());
        }
        public ActionResult SaveTeamMember(int TeamID, string Member, int MemberID, bool Deleted)
        {
            List<Models.LabTeamMember> TeamMembers = new List<Models.LabTeamMember>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                TeamMembers = dc.Database.SqlQuery<Models.LabTeamMember>("SPROCmanLabTeam @idTeam={0}, @vchMember={1}, @idMember={2}", new object[] { TeamID, Member, MemberID }).ToList<Models.LabTeamMember>();
            }

            return Json(TeamMembers);
        }
        public ActionResult SaveUpdateInfo(string Space,string Field,int Value)
        {
            List<Models.LabSpace> Spaces = new List<Models.LabSpace>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Spaces = dc.Database.SqlQuery<Models.LabSpace>("SPROCmanLabTeam @vchSpace={0}, @vchField={1}, @vchValue={2}", new object[] { Space, Field, Value }).ToList<Models.LabSpace>();
            }

            return Json(Space);
        }
        public ActionResult Graph(int WWID)
        {
            return View("GraphView");
        }
        public ActionResult ProjList(int Campus)
        {
            List<Models.LabProjectList> Project = new List<Models.LabProjectList>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Project = dc.Database.SqlQuery<Models.LabProjectList>("SPROCgetLabsProjList @CampusID={0}", new object[] { Campus }).ToList<Models.LabProjectList>();
            }

            myModel=Project;
            ViewBag.Campus=Campus;

            return PartialView(myModel);
        }
        public ActionResult GetProjInfo(int Campus,int id)
        {
            List<Models.LabProjectList> Project = new List<Models.LabProjectList>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Project = dc.Database.SqlQuery<Models.LabProjectList>("SPROCgetLabsProjList @CampusID={0}", new object[] { Campus }).ToList<Models.LabProjectList>();
            }
            if (Project.Where(x => x.id == id).Count() > 0)
            {
                return Json(Project.Single(x => x.id == id));
            }
            else
                return new EmptyResult();
        }
        public ActionResult GetProj(int Campus)
        {
            List<Models.LabProjectList> Project = new List<Models.LabProjectList>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Project = dc.Database.SqlQuery<Models.LabProjectList>("SPROCgetLabsProjList @CampusID={0}", new object[] { Campus }).ToList<Models.LabProjectList>();
            }

            return Json(Project);
        }
        public ActionResult SaveProj(string ProjCode,string Org1,string Org2,string Org3,string Val,string CrntStar,string CrntEnd,string BuMgr,string OldName,string OldName2,string Comments,string CC,string BKCMora, bool Deleted, int idCampus,int id)
        {
            List<Models.LabProjectList> Project = new List<Models.LabProjectList>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Project = dc.Database.SqlQuery<Models.LabProjectList>("SPROCmanLabsProjects @id={0}, @ProgCode={1},@Org1={2}, @Org2={3}, @Org3={4}, @VAL={5}, @CrntStart={6}, @CrntEnd={7}, @BuMgr={8}, @OldName={9}, @OldName2={10}, @Comments={11}, @CC={12}, @BKCMoraSqft={13}, @bDeleted={14}, @idCampus={15}"
                    , new object[] { id, ProjCode, Org1, Org2, Org3, Val, CrntStar, CrntEnd, BuMgr, OldName, OldName2, Comments, CC, BKCMora, Deleted, idCampus }).ToList<Models.LabProjectList>();
            }

            return Json(Project.First());
        }
        public ActionResult AddExtaCol(int CampusId)
        {
             List<Models.LabSpaceExtra> extra = new List<Models.LabSpaceExtra>();

             using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
             {
                 extra = dc.Database.SqlQuery<Models.LabSpaceExtra>("SPROCgetExtraCol @CampusID={0}", new object[] { CampusId }).ToList<Models.LabSpaceExtra>();
             }

             myModel = extra;

             return PartialView("ExtrasView", extra);
        }
        public ActionResult SaveExtaColumn(int Id,string Name,int CampusId, bool Deleted)
        {
            List<Models.LabSpaceExtra> extra = new List<Models.LabSpaceExtra>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                extra = dc.Database.SqlQuery<Models.LabSpaceExtra>("SPROCmanLabSpaceExtra @Name={0}, @CampusID={1},@Id={2}, @Delete={3}", new object[] { Name, CampusId, Id, Deleted}).ToList<Models.LabSpaceExtra>();
            }

            return Json(extra.Single());
        }
        public ActionResult SaveExtraData(string Value, int ColumnID,int SpaceID)
        {
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                dc.Database.ExecuteSqlCommand("SPROCUpdateLabSpaceExtraData @Value={0}, @ColumnID={1}, @SpaceID={2}", new object[]{ Value,ColumnID,SpaceID });
            }
            return new EmptyResult();
        }
        public ActionResult EfficiencyReport(string WW,int WWID)
        {
            List<Models.LabEfficiencyReport> Report = new List<Models.LabEfficiencyReport>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Report = dc.Database.SqlQuery<Models.LabEfficiencyReport>("SPROCrepLabEfficiency").ToList<Models.LabEfficiencyReport>();
            }

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            ViewBag.Complete = false;
            ViewBag.WW = WW;
            ViewBag.WWID = WWID;
            ViewBag.Report = 2;
            myModel = Report;

            return View("ReportsView", myModel);
        }
        public ActionResult SweepReport(string Continuity,string Name,string Lead, string WW,int WWID, int CampusId=0)
        {
            List<Models.LabSpaceSweepReport> report = new List<Models.LabSpaceSweepReport>();
            List<Models.LabAvl> Avl = new List<Models.LabAvl>();

            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@vchContinuity", Continuity);
            param[1] = new SqlParameter("@intCampus", CampusId);

            conn.Open();
            DataSet dtData= SqlHelper.ExecuteDataset(conn, "SPROCrepLabSweepReport", param);
            conn.Close();

            if(dtData!=null && dtData.Tables.Count>0)
            {
                foreach(DataRow drData in dtData.Tables[0].Rows)
                {
                    Models.LabSpaceSweepReport rpt = new Models.LabSpaceSweepReport();

                    rpt.Team = drData["Team Name"].ToString();
                    rpt.Lin = Convert.ToInt32(drData["LIN"].ToString());
                    rpt.Benches = Convert.ToInt32(drData["Benches"].ToString());
                    rpt.BKCPlatform = Convert.ToInt32(drData["BKCPlatform"].ToString());
                    rpt.InUse = Convert.ToInt32(drData["InUse"].ToString());
                    rpt.BencRelease = Convert.ToDecimal(drData["BencheRelease"].ToString());
                    rpt.CurrentBBKC = Convert.ToInt32(drData["CurrentBBKC"].ToString());
                    rpt.BBKeff = Convert.ToInt32(drData["BBK eff"].ToString());
                    report.Add(rpt);
                }

                ViewBag.Chart = dtData.Tables[1];
                ViewBag.WeekDetail = dtData.Tables[2];
                ViewBag.WWID = WWID;
                ViewBag.WW = WW;
                ViewBag.Complete = false;
            }

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Avl = dc.Database.SqlQuery<Models.LabAvl>("SPROCgetAvlLab @ContinuityID={0}", new object[] { Continuity }).ToList<Models.LabAvl>();
            }

            ViewBag.Complete = false;
            ViewBag.Name = Name;
            ViewBag.Lead = Lead;
            ViewBag.WW = WW;
            ViewBag.Continuity = Continuity;
            ViewBag.Avl = Avl;
            ViewBag.Report = 1;
            ViewBag.Campus = CampusId;
            myModel = report;
            return View("ReportsView", myModel);
        }
        public ActionResult Users(int CampusId)
        {
            List<Models.LabUser> lstUser = new List<Models.LabUser>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstUser = dc.Database.SqlQuery<Models.LabUser>("SPROgetUsersByCampus @CampusID={0}", new object[] { CampusId }).ToList<Models.LabUser>();
            }
            myModel = lstUser;

            return PartialView("UsersView", myModel);
        }
        public ActionResult ManUser(int CampusId, string Email, int Id, bool bDelete)
        {
            List<Models.LabUser> lstUser = new List<Models.LabUser>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstUser = dc.Database.SqlQuery<Models.LabUser>("SPROCsecLabCampus @vchEmail={0}, @intCampusID={1}, @intId={2}, @bDelete={3}", new object[] { Email,CampusId, Id, bDelete}).ToList<Models.LabUser>();
            }

            return Json(lstUser.First());
        }
        public ActionResult SaveAvlLab(string Continuity,int Value, int Type,string WW)
        {
            List<Models.LabAvl> Avl = new List<Models.LabAvl>();

            WW = WW.Replace("-", "'");

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Avl = dc.Database.SqlQuery<Models.LabAvl>("SPROCmanLabAvl @intValue={0}, @intType={1}, @WW={2}, @ContinuityID={3}", new object[] { Value, Type, WW, Continuity }).ToList<Models.LabAvl>();
            }

            return Json(Avl.Count > 0);
        }
        public ActionResult LabSQF(int LabCampus,int WWID)
        {
            List<Models.LabPerWeek> LabPW = new List<Models.LabPerWeek>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                LabPW = dc.Database.SqlQuery<Models.LabPerWeek>("SPROCgetLabs @WWID={0}", WWID).ToList<Models.LabPerWeek>().Where(x=>x.CampusId==LabCampus).ToList<Models.LabPerWeek>();
            }
            return PartialView("LabSizeView", LabPW);
        }
        public ActionResult SaveLabSQF(decimal Size,string Continuity,int WWID)
        {
            List<Models.Lab> Lab = new List<Models.Lab>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Lab = dc.Database.SqlQuery<Models.Lab>("SPROCmanLabSQF @Size={0}, @Continuity={1}, @WWID={2}", new object[] { Size,Continuity, WWID}).ToList<Models.Lab>();
            }

            return Json(Lab.Count > 0);
        }
        public ActionResult LocalSuitReport()
        {
            List<Models.LabLocalSuitReport> report = new List<Models.LabLocalSuitReport>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            int? WWID = null;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0)
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
            }

            if (WWID == null)
            {
                WWID=result[0].WWID;
                ViewBag.WWID = WWID;
                ViewBag.WW = result[0].weekName;
                Session["FilWW"] = result[0].WWID;
            }else
            {
                ViewBag.WWID = WWID;
                ViewBag.WW = result.Where(x => x.WWID == WWID).SingleOrDefault().weekName;
                Session["FilWW"] = WWID;
            }

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "/Lab/LocalSuitReport";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "/Lab/LocalSuitReport";

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                report = dc.Database.SqlQuery<Models.LabLocalSuitReport>("SPROCgetLabLocalSuitReport @WWID={0}", new object[] { WWID }).ToList<Models.LabLocalSuitReport>();
            }


            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();

            mFil.strField = "Campus";
            mFil.strLabelField = "Campus";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            List<object> objFil = new List<object>();
            List<Models.LabLocalSuitReport> dataFilList = new List<Models.LabLocalSuitReport>();
            dataFilList =report;

            if (Session["DataSource"] != null)
            {
                Session["CancelActivate"] = true;
            }

            foreach (Models.LabLocalSuitReport snap in dataFilList)
            {
                Object o = new object();

                o = (object)snap;
                objFil.Add(o);
            }

            List<Models.LabLocalSuitReport> resFil = new List<Models.LabLocalSuitReport>();

            if (Session["ValSelLab"] != null)
            {
                #region Apply Filter


                List<string> strValue;

                if (Session["ValSelLab"] == null)
                    strValue =new List<string>();
                else
                {
                    strValue = (List<string>)Session["ValSelLab"];
                }

                string[] strCampusValue = new string[strValue.Where(x => x.StartsWith("Campus")).Count()];

                int inti = 0;

                foreach (string strVal in strValue.Where(x => x.StartsWith("Campus")).ToList<string>())
                {
                    strCampusValue[inti] = strVal.Split(',')[1].ToString();
                    inti++;
                }
                resFil = report.Where(x =>  strCampusValue.Contains(x.Campus)).ToList();

                #endregion
            }
            else
            {
                resFil = report;
            }

            ViewBag.objFil = objFil;

            ViewBag.Filter = mFilters;
            ViewBag.Complete = false;
            ViewBag.Report = 3;
            return View("ReportsView", resFil);
        }
	}
}