﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace AutoIndicators.Controllers
{
    public class NotifController : Controller
    {
        dynamic myModel = new ExpandoObject();
        public ActionResult Assignees(int NotifId)
        {
            List<Models.NotifAssignees> lstAssign = new List<Models.NotifAssignees>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstAssign = dc.Database.SqlQuery<Models.NotifAssignees>("SPROCGetNotif @NotifId = {0}", NotifId).ToList<Models.NotifAssignees>();
            }

            return Json(lstAssign);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AssigneesSave(int NotifId, string Email, bool Delete,Models.Notif Notif)
        {
            Models.NotifAssignees Assignees=new Models.NotifAssignees();
            List<Models.NotifAssignees> lstTo=new List<Models.NotifAssignees>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();
            NotifController cNotif = new NotifController();
            
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Assignees = dc.Database.SqlQuery<Models.NotifAssignees>("SPROCmanNotifAssignees @NotifId = {0}, @vchEmail={1}, @bDelete={2}",new object[]{ NotifId, Email,Delete}).ToList<Models.NotifAssignees>().Single();
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
            }

            if (Assignees.Email.Length > 0 && Notif != null)
            {
                lstTo.Add(Assignees);
                cNotif.SendEmail(result[0].WWID, result[0].weekName, Notif.Descr, lstTo, Notif.NotifName);
            }
            return Json(Assignees);
        }
        public ActionResult Updates(int NotifID)
        {
            List<Models.NotifUpdate> Updates = new List<Models.NotifUpdate>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Updates = dc.Database.SqlQuery<Models.NotifUpdate>("SPROCgetNotifUpdates @NotifId = {0}", NotifID).ToList<Models.NotifUpdate>();
            }

            return Json(Updates);
        }
        public ActionResult GotoApp(string ReferenceId, int App,int WWID, int AdditionalID)
        {
            Models.Notif Mod = new Models.Notif();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Mod = dc.Database.SqlQuery<Models.Notif>("[SPROCGetGotoItemNotif] @ContinuityId = {0}, @app= {1}, @wwid= {2}, @additionalId={3}", new object[] { ReferenceId, App, WWID, AdditionalID }).ToList<Models.Notif>().Single();
            }
            Session["Focus"]=Mod.FocusApp;
            return Json(Mod);
        }
        public ActionResult Index()
        {
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
            }
            ViewBag.WWID = result[0].WWID;
            ViewBag.WW = result[0].weekName;
            ViewBag.Complete = false;
            return View("NotiView");
        }
        public ActionResult IsAkcowledge(int NotifId,bool Val)
        {
            Models.NotifAssignees Assign = new Models.NotifAssignees();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Assign = dc.Database.SqlQuery<Models.NotifAssignees>("SPROCisAcknowledge @NotifID = {0}, @vchEmail={1}, @bAcknowledge={2}", new object[] { NotifId, GeneralClasses.General.FindEmailByAccount(User.Identity.Name), Val }).ToList<Models.NotifAssignees>().Single();
            }

            return new EmptyResult();
        }
        public ActionResult NotiDash(int WWID, bool all=false)
        {
            List<Models.Notif> list = new List<Models.Notif>();
            List<Models.NotifStatus> lststatus = new List<Models.NotifStatus>();
            List<Models.NotifTypes> lsttypes = new List<Models.NotifTypes>();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                string strEmail = GeneralClasses.General.FindEmailByAccount(User.Identity.Name);

                if (all)
                    strEmail = "";

                list = dc.Database.SqlQuery<Models.Notif>("SPROCGetNotif @vchEmail = {0}", strEmail).ToList<Models.Notif>();
                lststatus = dc.Database.SqlQuery<Models.NotifStatus>("SPROCgetNotiCombos @intType={0}",1).ToList<Models.NotifStatus>();
                lsttypes = dc.Database.SqlQuery<Models.NotifTypes>("SPROCgetNotiCombos @intType={0}", 2).ToList<Models.NotifTypes>(); 
            }

            myModel.list = list;
            ViewBag.Status = lststatus;
            ViewBag.Types = lsttypes;
            ViewBag.Refresh = false;
            ViewBag.CreateNew = false;
            ViewBag.ShowAcknowledge = all;
            ViewBag.Complete = false;
            ViewBag.UpdateOnly = false;
            ViewBag.WWID = WWID;
            return PartialView("~/Views/Notif/NotifPopUp.cshtml", myModel);
        }
        public ActionResult TotalNotif()
        {
            List<Models.Notif> list = new List<Models.Notif>();
            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    string strEmail = GeneralClasses.General.FindEmailByAccount(User.Identity.Name);
                    list = dc.Database.SqlQuery<Models.Notif>("SPROCGetNotif @vchEmail = {0}", strEmail).ToList<Models.Notif>();
                }
            }
            catch { }

            if (list.Count > 0)
                return Json(list.Where(x=> x.NotifStatusId!=3));
            else
                return Json(list);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NotifSave(int notifId, int NotifType, string Descr, string refId, string NotifName, DateTime dueDate, int fatherApp, int Statusid, int AdditionalId,int SubType=0)
        {
            string usr = User.Identity.Name;
            int? NotifSubType = null;

            if (SubType > 0)
                NotifSubType = SubType;

            List<Models.Notif> lstNotif = new List<Models.Notif>();
            lstNotif = Create(notifId, NotifType,NotifSubType,Descr, refId, NotifName, dueDate, fatherApp, Statusid, AdditionalId, usr, 0);
            
            return Json(lstNotif.Single());
        }
        public List<Models.Notif> Create(int notifId, int NotifType,int? SubType, string Descr, string refId, string NotifName, DateTime dueDate, int fatherApp, int Statusid, int AdditionalId,string User, int WWID, SqlTransaction tran=null)
        {
            List<Models.Notif> lstNotif = new List<Models.Notif>();
            if (tran != null)
            {
                SqlParameter[] param = new SqlParameter[12];
                param[0] = new SqlParameter("@NotifId", notifId);
                param[1] = new SqlParameter("@createuser", User);
                param[2] = new SqlParameter("@NotifType", NotifType);
                param[3] = new SqlParameter("@Descr", Descr);
                param[4] = new SqlParameter("@ReferenceId", refId);
                param[5] = new SqlParameter("@NotifName", NotifName);
                param[6] = new SqlParameter("@DueDate", dueDate);
                param[7] = new SqlParameter("@fatherApp", fatherApp);
                param[8] = new SqlParameter("@CreateWW", WWID);
                param[9] = new SqlParameter("@NotifStatusId", Statusid);
                param[10] = new SqlParameter("@AdditionalId", AdditionalId);
                param[11] = new SqlParameter("@NotifSubType", SubType);
                

                DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCCreateNotif", param);

                Models.Notif notif = new Models.Notif();

                notif.NotifId =Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());
                lstNotif.Add(notif);
            }
            else
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    lstNotif = dc.Database.SqlQuery<Models.Notif>("[SPROCCreateNotif] @NotifId= {0}, @createuser = {1},@NotifType = {2},@Descr = {3}, @ReferenceId = {4},@NotifName = {5},@DueDate = {6},@fatherApp = {7}, @NotifStatusId={8}, @AdditionalId={9}, @NotifSubType={10}, @CreateWW={11}",
                        new object[] { notifId, User, NotifType, Descr, refId, NotifName, dueDate, fatherApp, Statusid, AdditionalId, SubType, WWID }).ToList<Models.Notif>();
                }
            }
            return lstNotif;
        }
        public ActionResult NotifQuery(string NotifData, bool Refresh=false,bool Update=false,bool Track=false)
        {
            //  0           1          2     3    4     5       6       7     8
            //ViewName|continuity|elementId|WWID|icon|Owner|ProgramType|PXT|Subtype
            string[] Notif = NotifData.Split(char.Parse("|"));
            List<Models.Notif> list = new List<Models.Notif>();
            List<Models.NotifStatus> lststatus = new List<Models.NotifStatus>();
            List<Models.NotifTypes> lsttypes = new List<Models.NotifTypes>();
            int intSubtype = 0;

            if (Notif.Length > 8)
                intSubtype =Convert.ToInt32(Notif[8].ToString());

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.Notif>("[SPROCGetNotif] @appId = {0}, @ReferenceId = {1}, @AdditionalId={2}, @SubType={3}", new object[] { Notif[0], Notif[1], Notif[2], intSubtype }).ToList<Models.Notif>();
                lststatus = dc.Database.SqlQuery<Models.NotifStatus>("SPROCgetNotiCombos @intType={0}", 1).ToList<Models.NotifStatus>(); //status
                lsttypes = dc.Database.SqlQuery<Models.NotifTypes>("SPROCgetNotiCombos @intType={0}", 2).ToList<Models.NotifTypes>();  //type
            }


            myModel.list = list;
            ViewBag.Status = lststatus;
            ViewBag.Types = lsttypes;
            ViewBag.Data = Notif;
            ViewBag.DataString = NotifData;
            ViewBag.Refresh = Refresh;
            ViewBag.CreateNew = true;
            ViewBag.UpdateOnly = Update;
            ViewBag.Track = Track;
            ViewBag.WWID = Notif[3];
            return PartialView("~/Views/Notif/NotifPopUp.cshtml", myModel);
        }
        public ActionResult SendEmail(int WWID, string WWName, string strBody, List<Models.NotifAssignees> Assignees,string strTitle)
        {
            MailMessage mailObj = new MailMessage();
            List<Models.Users> Users = new List<Models.Users>();
           
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if(Assignees==null)
                {
                    Assignees = dc.Database.SqlQuery<Models.NotifAssignees>("SPROCgetAssigneesNewNotifWW @intWW={0}", WWID).ToList<Models.NotifAssignees>();
                }

                Users = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }

            foreach (Models.NotifAssignees detail in Assignees)
            {
                //Agregar Validacion de correo (try)
                mailObj.To.Add(new MailAddress(detail.Email, detail.Name));
            }

            //foreach (Models.Users user in Users.Where(x=> x.UserType==1).ToList())
            //{
            //    Models.Users Contact = GeneralClasses.General.GetInfoUserAD(user.validUsers);
            //    mailObj.To.Add(new MailAddress(Contact.strEmail, Contact.strName));
            //}
            

            mailObj.From = new MailAddress(GeneralClasses.General.strEmailFrom, "Notification");
            
            mailObj.Subject =strTitle;
            mailObj.Body = strBody;
            mailObj.IsBodyHtml = true;

            SmtpClient SMTPServer = new SmtpClient(GeneralClasses.General.strSMTP);
            SMTPServer.Send(mailObj);

            return new EmptyResult();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateSave(int Id,int Type,string Desc,int NotifId,bool Aknowledge,int Status,string Assignees)
        {
            Models.NotifUpdate Update = new Models.NotifUpdate();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                Update = dc.Database.SqlQuery<Models.NotifUpdate>("[SPROCCreateNotifUpdate] @NotifUpdateId = {0}, @UpdateType={1}, @value={2}, @Descr={3}, @NotifId={4}, @Acknowledge={5}, @UpdateStatusId={6}, @assigneeEmail={7}, @createuser={8}"
                    , new object[] { Id, Type,"", Desc, NotifId, Aknowledge, Status, Assignees,User.Identity.Name }).ToList<Models.NotifUpdate>().Single();
            }
            return Json(Update);
        }
       
	}
}