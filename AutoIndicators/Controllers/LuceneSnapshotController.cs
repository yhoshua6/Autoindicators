﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoIndicators.Models;

namespace AutoIndicators.Controllers
{
    public class LuceneSnapshotController : Controller
    {
        //
        // GET: /LuceneSnapshot/
        public ActionResult Index(string SearchTerm, string searchField, int? limit) { //SearchTerm is the string to look for, searchfield is the field we look for, and limit is the number of rows shown on the table
            //Create Lucene Directory
            if (!Directory.Exists(LuceneSearch._luceneDir)) Directory.CreateDirectory(LuceneSearch._luceneDir);

            //Lucene Search
            List<SampleData> _searchResults;
            _searchResults = (string.IsNullOrEmpty(searchField)
                            ? LuceneSearch.Search(SearchTerm)
                            : LuceneSearch.Search(SearchTerm, searchField)).ToList();

            if(string.IsNullOrEmpty(SearchTerm) && !_searchResults.Any())
                _searchResults = LuceneSearch.GetAllIndexRecords().ToList();

            var search_field_list = new
                List<SelectedList> {
				                     	new SelectedList {Text = "(All Fields)", Value = ""},
                                        new SelectedList {Text = "Id", Value = "Id"},
				                     	new SelectedList {Text = "ContinuityID", Value = "ContinuityID"},
				                     	new SelectedList {Text = "PXTName", Value = "PXTName"},
				                     	new SelectedList {Text = "ProgName", Value = "ProgName"},
                                        new SelectedList {Text = "ProgType", Value = "ProgType"},
                                        new SelectedList {Text = "ProgTypeLink", Value = "ProgTypeLink"},
                                        new SelectedList {Text = "PLCStatus", Value = "PLCStatus"},
                                        new SelectedList {Text = "OwnerName", Value = "OwnerName"},
                                        new SelectedList {Text = "Updatetxt", Value = "Updatetxt"}
				                     };

            var limitDb = limit == null ? 3 : Convert.ToInt32(limit);
            List<SampleData> allSampleData;
            if (limitDb > 0)
            {
                allSampleData = SampleDataRepository.GetAll().ToList().Take(limitDb).ToList();
                ViewBag.Limit = SampleDataRepository.GetAll().Count - limitDb;
            }
            else allSampleData = SampleDataRepository.GetAll();

            return View(new IndexViewModel
            {
                AllSampleData = allSampleData,
                SearchIndexData = _searchResults,
                SampleData = new SampleData {ContinuityID="A1234",PXTName="TestName",ProgName="TestProgName",ProgDesc="TestProgDesc",ProgType="TestProgType",ProgTypeLink="TestProgTypeLink",PLCStatus="TestPLCStatus",OwnerName="TestOwnerName",Updatetxt="NewTest" },
                SearchFieldList = search_field_list
            });
        }

        public ActionResult Search(string searchTerm, string searchField, string searchDefault)
        {
            return RedirectToAction("Index", new { searchTerm, searchField, searchDefault });
        }

        public ActionResult CreateIndex() //Create the Search Index
        {
            LuceneSearch.AddUpdateLuceneIndex(SampleDataRepository.GetAll());
            TempData["Result"] = "Search index was created successfully!";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddToIndex(SampleData sampleData)
        {
            LuceneSearch.AddUpdateLuceneIndex(sampleData);
            TempData["Result"] = "Record was added to search index successfully!";
            return RedirectToAction("Index");
        }

        public ActionResult ClearIndex()
        {
            if (LuceneSearch.ClearLuceneIndex())
                TempData["Result"] = "Search index was cleared successfully!";
            else
                TempData["ResultFail"] = "Index is locked and cannot be cleared, try again later or clear manually!";
            return RedirectToAction("Index");
        }

        public ActionResult ClearIndexRecord(int id)
        {
            LuceneSearch.ClearLuceneIndexRecord(id);
            TempData["Result"] = "Search index record was deleted successfully!";
            return RedirectToAction("Index");
        }

        public ActionResult OptimizeIndex()
        {
            LuceneSearch.Optimize();
            TempData["Result"] = "Search index was optimized successfully!";
            return RedirectToAction("Index");
        }
        
	}
}