﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Text;
using AutoIndicators.ExtensionMethods;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Specialized;

namespace AutoIndicators.Controllers
{
    public class RoadmapController : Controller
    {
        //
        // GET: /Roadmap/
        dynamic myModel = new ExpandoObject();

        public ActionResult Index()
        {
            int? WWID = null;
            List<Models.Roadmap> lstRoadMap = new List<Models.Roadmap>();
            List<Models.Roadmap> lstValidRoadMap = new List<Models.Roadmap>();
            List<Models.DefaultWW> result = new List<Models.DefaultWW>();
            List<Models.Calendar> lstCalendar = new List<Models.Calendar>();
            Models.Calendar CalendarData = new Models.Calendar();
            Models.DefaultWW oWW = new Models.DefaultWW();
            List<Models.Roadmap> lstNocompliant = new List<Models.Roadmap>();


            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Snapshot";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Snapshot";

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 )
                WWID = Convert.ToInt32(Session["FilWW"].ToString());

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetCurrentWWID").ToList<Models.DefaultWW>().Where(x=> x.weekstatus==1).ToList();
                ViewBag.validUsers = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                if(WWID==null)
                {
                    oWW = result[0];
                    Session["FilWW"] = oWW.WWID;
                    WWID = oWW.WWID;
                }else
                {
                    oWW = result.Where(x => x.WWID == WWID).Single();
                }

                lstRoadMap = dc.Database.SqlQuery<Models.Roadmap>("SPROCgetRoadmap @WWID={0}",WWID).ToList<Models.Roadmap>();

                lstNocompliant = lstRoadMap.Where(x => x.StartWW == 0 || x.EndWW== 0 || x.StartYear==0  || x.EndYear==0 || x.StartMonth==0 || x.EndMonth==0 ).ToList<Models.Roadmap>();
            }

            ViewBag.WeekStatus =oWW.weekstatus;
            ViewBag.WWID = WWID;
            ViewBag.WW = oWW.weekName;
            lstValidRoadMap = lstRoadMap.Where(x => x.StartWW > 0 && x.EndWW > 0  && x.StartYear>0 && x.EndYear>0 && x.EndYear>=x.StartYear && x.StartMonth>0 && x.EndMonth>0).ToList();
            ViewBag.WWID = WWID;
            ViewBag.Noncompliant = lstNocompliant;
            myModel = lstValidRoadMap;
            ViewBag.Complete = false;
            ViewBag.Archived = oWW.weekstatus == 2 ? true : false;
            return View("RoadmapView", myModel);
        }
        public ActionResult DetailView(List<Models.Roadmap> Programs)
        {
            return View(Programs);
        }
        public ActionResult Goto(string Continuity,int WWID)
        {
            var list = new List<Models.ScheduleV>();
            Models.ScheduleV item= new Models.ScheduleV();

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ScheduleV>("SPROCGetWWSchedule @WWID = {0} ", WWID).ToList<Models.ScheduleV>();
            }

            if(list.Count>0)
            {
                item=list.First();
            }

            Session["Focus"] = item.PTMID;
            return Json(true);
        }
            
	}
}