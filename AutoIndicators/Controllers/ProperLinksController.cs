﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;

namespace AutoIndicators.Controllers
{
    public class ProperLinksController : Controller
    {
        //
        // GET: /ProperLinks/
        dynamic myModel = new ExpandoObject();
        //
        // GET: /Acronyms/
        public ActionResult Index()
        {

            return View("~/Views/ProperLinks/ProperLinkList.cshtml");
        }

        public bool getDBdata(string pNameString)
        {
            var list = new List<Models.ProperLinks>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ProperLinks>("SPROCgetletterProperLink @abbreviationString = {0} ", pNameString).ToList<Models.ProperLinks>();
            }
            myModel.list = list;
            return true;
        }

        public ActionResult AddProperLink()
        {
            return View();
        }

        public ActionResult ProperLinkList(string ProperName, string Row)
        {
            getDBdata(ProperName);
            myModel.Row = Row;
            return View(myModel);
        }

        public ActionResult ProperLinkList_noLayout(string ProperName, string Row)
        {
            getDBdata(ProperName);
            myModel.Row = Row;
            return View(myModel);
        }

        public ActionResult EditProperLink(int ID)
        {
            var list = new List<Models.ProperLinks>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ProperLinks>("SPROCgetSingleProperLink @PID = {0} ", ID).ToList<Models.ProperLinks>();
            }
            myModel.list = list;
            return View(myModel);
        }

        public ActionResult SeeProperLink(int ID)
        {
            var list = new List<Models.ProperLinks>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.ProperLinks>("SPROCgetsingleProperLink @PID = {0} ", ID).ToList<Models.ProperLinks>();
            }
            myModel.list = list;
            return View(myModel);

        }

        public ActionResult Save(FormCollection collection)
        {
            string usr = User.Identity.Name;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (collection["OperationType"] == "1") //Create
                {
                    var parames = new object[] { collection["pProperName"], collection["pDefinition"], collection["pLink"], usr, null };
                    dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCCreateProperLinks @PName= {0}, @PDefinition = {1}, @PLink = {2},@PCreatorActor = {3},@PContextID = {4}", parames).ToList<Models.DefaultWW>();
                }
                else if (collection["OperationType"] == "2") //Update
                {
                    var parames = new object[] { collection["ID"], collection["pProperName"], collection["pDefinition"], collection["pLink"] };
                    dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateProperLink @PID={0},@PProperName = {1},@PDefinition = {2}, @PLink = {3}", parames).ToList<Models.DefaultWW>();
                }
            }
            return RedirectToAction("Index");
        }
	}
}