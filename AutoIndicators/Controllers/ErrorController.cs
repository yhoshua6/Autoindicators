﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoIndicators.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ViewResult Index()
        {
            var userList = new List<Models.Users>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
            }
            string userName = User.Identity.Name;

            int size = userList.Count;
            int j = 0;
            int k = 0;
            string[] valid1Users = new string[size];
            string[] valid2Users = new string[size];

            foreach (var item in userList)
            {
                if (item.UserType == 1)
                {
                    valid1Users[j] = item.validUsers;

                    j++;
                }
                else if (item.UserType == 2)
                {
                    valid2Users[k] = item.validUsers;

                    k++;
                }       
            }

            int res = Array.IndexOf(valid1Users, userName.Remove(0, 4)); 
                    if (res > -1)
                    {
                        return View("Error");  
                    }
                res = Array.IndexOf(valid2Users, userName.Remove(0, 4)); 
                    if (res > -1)
                    {
                        return View("UserError");
                    }
                    return View("Error");
                }

        public ViewResult NotFound()
        {
            Response.StatusCode = 404; 
            return View("NotFound");
        }


	}
}