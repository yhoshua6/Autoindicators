﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Data;


namespace AutoIndicators.Controllers
{
    public class PcosController : Controller
    {
        dynamic myModel = new ExpandoObject();
        string[] validUsers = { "rondarha", "carowlan", "lkmccann", "rlobatox" };


        public ActionResult EditSKU(int ID, int WWID, string PID)
        {
            var list = new List<Models.SKUs>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SKUs>("SPROCgetSingleSKU @ID = {0} ", ID).ToList<Models.SKUs>();
            }
            myModel.list = list;
            ViewBag.WW = WWID;
            ViewBag.PID = PID;
            return View(myModel);
        }

        public List<Models.Pcos> getDBdata(int WWID)
        {
            try
            {
                var list = new List<Models.Pcos>();
                var userList = new List<Models.Users>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    list = dc.Database.SqlQuery<Models.Pcos>("SPROCGetWWPcos @WWID = {0} ", WWID).ToList<Models.Pcos>();
                    userList = dc.Database.SqlQuery<Models.Users>("[SPROCgetUsers]").ToList<Models.Users>();
                }

                ViewBag.validUsers = userList;
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult Index(bool complete=false)
        {
            ViewBag.elementFocus = System.Web.HttpContext.Current.Session["elementFocus"]; //focus on row

            List<Models.Pcos> list = new List<Models.Pcos>();
            List<Models.mFilter> mFilters = new List<Models.mFilter>();
            Models.mFilter mFil = new Models.mFilter();
            int? pWWID = null;

            #region Init Filter

            if (Request.Cookies["Module"] != null && !ControllerContext.RouteData.Values["controller"].ToString().Equals(Request.Cookies["Module"].Value.ToString()))
            {
                Response.Cookies["Module"].Value = "Pcos";
                Session.Remove("DataSource");
            }
            else
                Response.Cookies["Module"].Value = "Pcos";

            mFil.strField = "VerticalName";
            mFil.strLabelField = "Swim Lanes";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();
            mFil.strField = "PXTName";
            mFil.strLabelField = "PXT";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgType";
            mFil.strLabelField = "Type";
            //      mFil.strLinkedField = "PXTName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "OwnerName";
            mFil.strLabelField = "Owner";
            //       mFil.strLinkedField = "ProgType";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ClasificationName";
            mFil.strLabelField = "Clasification";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            mFil = new Models.mFilter();

            mFil.strField = "ProgName";
            mFil.strLabelField = "Program";
            //       mFil.strLinkedField = "OwnerName";
            mFil.mHtmlTypeOf = Models.mFilter.mHtmlInputType.fList;

            mFilters.Add(mFil);

            ViewBag.Filter = mFilters;

            if (Session["FilWW"] != null && Convert.ToInt32(Session["FilWW"].ToString()) > 0 && getDBdata(Convert.ToInt32(Session["FilWW"].ToString())).Count > 0)
                pWWID = Convert.ToInt32(Session["FilWW"].ToString());
            #endregion


            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                if (pWWID == null)
                {
                    var result = new List<Models.DefaultWW>();
                    result = dc.Database.SqlQuery<Models.DefaultWW>("SPROCgetWorkingWWID").ToList<Models.DefaultWW>();
                       
                    list = getDBdata(result[0].WWID);
                       
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;
                     
                }
                else
                {
                    list = getDBdata(int.Parse(pWWID.ToString()));
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    ViewBag.WeekStatus = list[0].weekStatus;
                }


                #region Build Filter
                List<object> objFil = new List<object>();

                if (Session["DataSource"] != null)
                {
                    Session["CancelActivate"] = true;
                }
                
                foreach (Models.Pcos snap in list)
                {
                    Object o = new object();

                    o = (object)snap;
                    objFil.Add(o);
                }
                

                ViewBag.objFil = objFil;
                #endregion
                #region PreLoad
                AutoIndicators.GeneralClasses.ViewDataFilter fFil = new AutoIndicators.GeneralClasses.ViewDataFilter();

                fFil.oSource = objFil;
                fFil.mFiltersToShow = mFilters;
                fFil.vCreateFilter();

                Session["DataFil"] = fFil.dtData;
                Session["DataSource"] = fFil.oSource;

                #endregion
                #region Load Data Filter
                List<Models.Pcos> resFil = new List<Models.Pcos>();
                if (Request.Cookies["FilString"] != null)
                {
                    fFil.dtData = (DataTable)Session["DataFil"];
                    //string strFilString = fFil.strValidFilterString(Request.Cookies["FilString"].Value.ToString());

                    #region Apply Filter
                
                        List<string> strValue;

                        if (Session["ValSel"] != null)
                            strValue = (List<string>)Session["ValSel"];
                        else
                        {
                            strValue = Request.Cookies["ValSel"].Value.Split(';').ToList<string>();
                            Session["ValSel"] = strValue;
                        }

                        string[] strPXTValue = new string[strValue.Where(x => x.StartsWith("PXTName")).Count()];
                        string[] strProgTypeValue = new string[strValue.Where(x => x.StartsWith("ProgType")).Count()];
                        string[] strOwnerValue = new string[strValue.Where(x => x.StartsWith("OwnerName")).Count()];
                        string[] strProgValue = new string[strValue.Where(x => x.StartsWith("ProgName")).Count()];
                        string[] strVerticalValue = new string[strValue.Where(x => x.StartsWith("VerticalName")).Count()];
                        string[] strClasification = new string[strValue.Where(x => x.StartsWith("ClasificationName")).Count()];

                        int inti = 0;

                        foreach (string strVal in strValue.Where(x => x.StartsWith("PXTName")).ToList<string>())
                        {
                            strPXTValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgType")).ToList<string>())
                        {
                            strProgTypeValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("OwnerName")).ToList<string>())
                        {
                            strOwnerValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ProgName")).ToList<string>())
                        {
                            strProgValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("VerticalName")).ToList<string>())
                        {
                            strVerticalValue[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        inti = 0;
                        foreach (string strVal in strValue.Where(x => x.StartsWith("ClasificationName")).ToList<string>())
                        {
                            strClasification[inti] = strVal.Split(',')[1].ToString();
                            inti++;
                        }

                        list = list.Where(x => strPXTValue.Contains(x.PXTName) || strOwnerValue.Contains(x.OwnerName) || strProgValue.Contains(x.ProgName) || strProgTypeValue.Contains(x.ProgType) || strVerticalValue.Contains(x.VerticalName) || strClasification.Contains(x.ClasificationName)).ToList();
                    }
                    #endregion
                #endregion
                
            }
            ViewBag.Complete = complete;
            return View("~/Views/Pcos/PcosView.cshtml", list);
        }

        public ActionResult CommentView(int? PcosID)
        {
            try
            {
                List<Models.Pcos> result = new List<Models.Pcos>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    result = dc.Database.SqlQuery<Models.Pcos>(@"SPROCGetPCOS @PcosID= {0}", new object[] { PcosID }).ToList<Models.Pcos>();
                }
                return PartialView(result[0]);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FlagSKU(string PWContinuityId, int SKUID)
        {
            List<Models.SKUs> SKUs = new List<Models.SKUs>();

            try
            {
                var parames = new object[] { PWContinuityId, SKUID };
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("[SPROCFlagSKU] @PWContinuityID = {0}, @FlaggedId = {1}", parames);
                    SKUs = dc.Database.SqlQuery<Models.SKUs>("SPROCListSKUPerProgram @PWContinuityId = {0} ", PWContinuityId).ToList<Models.SKUs>();
                }
                var SKUList = (from s in SKUs
                               select new
                               {
                                   id = s.id,
                                   name = s.Name,
                                   Selected = (s.Flagged == 1) ? true : false
                               }).ToList();


                return Json(SKUList, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetSKUsByProgram(string PWContinuityId)
        {
            List<Models.SKUs> SKUs = new List<Models.SKUs>();
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                SKUs = dc.Database.SqlQuery<Models.SKUs>("SPROCListSKUPerProgram @PWContinuityId = {0} ", PWContinuityId).ToList<Models.SKUs>();
            }
            var SKUList = (from s in SKUs
                           select new
                           {
                               id = s.id,
                               name = s.Name,
                               Selected = (s.Flagged == 1) ? true : false
                           }).ToList();


            return Json(SKUList, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult deleteSKU(string PWContinuityId, string SKU, int WWID)
        {
            string usr = User.Identity.Name;
            List<Models.SKUs> SKUs = new List<Models.SKUs>();
            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("SPRODeactivateSKUPerProgram @PWContinuityId = {0}, @SKU = {1}, @WWID = {2}, @systemUser = {3}", new object[] { PWContinuityId, SKU, WWID, usr });
                    SKUs = dc.Database.SqlQuery<Models.SKUs>("SPROCListSKUPerProgram @PWContinuityId = {0} ", PWContinuityId).ToList<Models.SKUs>();
                }
                var SKUList = (from s in SKUs
                               select new
                               {
                                   id = s.id,
                                   name = s.Name,
                                   Selected = (s.Flagged == 1) ? true : false
                               }).ToList();


                return Json(SKUList, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult insertSKU(string PWContinuityId, string SKU, string Desc, int WWID)
        {
            string usr = User.Identity.Name;
            List<Models.SKUs> SKUs = new List<Models.SKUs>();
            try
            {
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    dc.Database.ExecuteSqlCommand("[SPROCCreateSKU] @PWContinuityId = {0}, @SKU = {1}, @WWID = {2}, @systemUser = {3},@Desc = {4}", new object[] { PWContinuityId, SKU, WWID, usr, Desc });
                    SKUs = dc.Database.SqlQuery<Models.SKUs>("SPROCListSKUPerProgram @PWContinuityId = {0} ", PWContinuityId).ToList<Models.SKUs>();
                }
                var SKUList = (from s in SKUs
                               select new
                               {
                                   id = s.id,
                                   name = s.Name,
                                   Selected = (s.Flagged == 1) ? true : false
                               }).ToList();


                return Json(SKUList, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //[AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SaveSingle(FormCollection collection)
        {
            var WW = collection["WWID"];
            var PID = collection["PID"];

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                var parames = new object[] { collection["ID"], collection["pName"], collection["pDescText"], collection["pLink"] };
                dc.Database.ExecuteSqlCommand(@"SPROCUpdateSKU @PID= {0},@PName= {1}, @PDescText= {2},@PLink = {3}", parames);

            }
            return RedirectToAction("AddSKU", "Pcos", new { WWID = WW, ProgramID = PID });
        }
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection)
        {
            try
            {

                string usr = User.Identity.Name;
                var parames = new object[] { collection["pPcosId"], collection["pTarget"], collection["pActual"], 
                collection["pTrend"], collection["txtDescription_"], usr, collection["txtDescription_Red"] };
                var result = new List<Models.DefaultWW>();
                using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {

                    result = dc.Database.SqlQuery<Models.DefaultWW>(@"SPROCUpdateWWPcosVal @PcosId= {0}, @Targetvalue = {1},
                @Actualvalue = {2},@Trendvalue = {3}, @comments = {4},@systemUser = {5},@CommentsRed = {6}  ", parames).ToList<Models.DefaultWW>();
                }
                System.Web.HttpContext.Current.Session["elementFocus"] = result[0].focusID;
                string url = HttpContext.Request.UrlReferrer.PathAndQuery;
                return Redirect(url);
                //return RedirectToAction("Index", "Pcos", new { pWWID = result[0].WWID, weekName = result[0].weekName });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ActionResult AddSKU(int? WWID, string ProgramID)
        {
            var list = new List<Models.SnapshotIndex>();

            List<SelectListItem> items = new List<SelectListItem>();
            List<Models.SnapshotIndex> orderList = new List<Models.SnapshotIndex>();
            List<Models.SKUs> SKUs = new List<Models.SKUs>();
            IEnumerable<SelectListItem> ProgList;
            IEnumerable<SelectListItem> SKUList;
            string PwContinuity;

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SnapshotIndex>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SnapshotIndex>();
                if (list.Count > 0)
                {
                    orderList = list.OrderBy(o => o.ProgName).ToList();
                    ProgList = orderList.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.ProgName,
                        Value = x.ContinuityID
                    });
                    ViewBag.WW = list[0].weekName;
                    ViewBag.WWID = list[0].WWID;
                    PwContinuity = orderList[0].ContinuityID;

                    SKUs = dc.Database.SqlQuery<Models.SKUs>("SPROCListSKUPerProgram @PWContinuityId = {0} ", PwContinuity).ToList<Models.SKUs>();
                    SKUList = SKUs.Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.id.ToString(),
                        Selected = (x.Flagged == 1) ? true : false
                    });
                    items.Add(new SelectListItem
                    {
                        Text = "Motherboard",
                        Value = "1"
                    });
                    items.Add(new SelectListItem
                    {
                        Text = "System",
                        Value = "2"
                    });
                    items.Add(new SelectListItem
                    {
                        Text = "Spares",
                        Value = "3"
                    });

                    myModel.ProgList = ProgList;
                    myModel.SKUList = SKUList;
                    myModel.items = items;
                    ViewBag.Complete = false;
                    if (ProgramID != null)
                    {
                        myModel.PID = ProgramID;
                    }
                    else
                    {
                        myModel.PID = null;
                    }

                }
            }
            return View(myModel);
        }
    }
}