﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using HtmlAgilityPack;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Reflection;
using System.Net.Mail;
using System.DirectoryServices;

namespace AutoIndicators.Controllers
{
    public class RAPIDController : Controller
    {
        dynamic myModel = new ExpandoObject();
        int weekStatus = 0;
        
        public ActionResult Index()
        {
            string strEmailUser = FindEmailByAccount(User.Identity.Name);
            Models.RAPID rapid = new Models.RAPID();
            List<Models.RAPID> lstRapid = new List<Models.RAPID>();
            List<Models.RAPID> lstResRapid = new List<Models.RAPID>();
            List<Models.RAPIDDetail> lstDetail = new List<Models.RAPIDDetail>();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstRapid = dc.Database.SqlQuery<Models.RAPID>("SPROCgetRAPIDView @vchEmailUser={0}", strEmailUser).ToList<Models.RAPID>();
                lstDetail = dc.Database.SqlQuery<Models.RAPIDDetail>("SPROCgetRAPIDInputDetail").ToList<Models.RAPIDDetail>();
            }

            foreach(Models.RAPID rap in lstRapid)
            {
                lstResRapid.Add(getRAPID(rap.intId));
            }

            myModel.Rapid = lstResRapid;
            myModel.Base = lstDetail;
            ViewBag.EmailUser = strEmailUser;

            
            return View("RapidView",myModel);
        }
        public ActionResult InputRapid(int id,int WWID, int? ccb,string WName)
        {
            Models.RAPID rapid = getRAPID(id);
            List<Models.RAPIDRel> lstRel = new List<Models.RAPIDRel>();
            List<Models.CCB> lstCCb = new List<Models.CCB>();
            CCBController cCcb = new CCBController();
            lstCCb = cCcb.getCCBs(WWID);
      
            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstRel = dc.Database.SqlQuery<Models.RAPIDRel>("SPROCgetRAPIDRel").ToList<Models.RAPIDRel>();

            }
            myModel.Rel = lstRel;
            myModel.CCB =lstCCb;
            myModel.RAPID = rapid;
            ViewBag.WW = WName;
            ViewBag.WWID = WWID;
            ViewBag.CCBID = ccb;
            ViewBag.REmail = FindEmailByAccount(User.Identity.Name);

            if (rapid != null)
            {
                if (rapid.strStatus!=null && rapid.strStatus.Contains("Approve") && !rapid.bApprovedNotified)
                {
                    SendMailApprovedReject(rapid);
                }
            }

            return View("InputRapidView",myModel);
        }
        private Models.RAPID getRAPID(int id)
        {
            Models.RAPID rapid = new Models.RAPID();
            List<Models.RAPIDDetail> lstDetail = new List<Models.RAPIDDetail>();
            List<Models.RAPIDStatus> lstStatus = new List<Models.RAPIDStatus>();

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstDetail = dc.Database.SqlQuery<Models.RAPIDDetail>("SPROCgetRAPIDInputDetail").ToList<Models.RAPIDDetail>();
                lstStatus = dc.Database.SqlQuery<Models.RAPIDStatus>("SPROCgetStatus").ToList<Models.RAPIDStatus>();

                if (id > 0)
                    rapid = dc.Database.SqlQuery<Models.RAPID>("SPROCgetRAPID @intID={0}, @WWID = {1}, @ContinuityID = {2},  @intDecisionID={3}, @bDetail = {4}", new object[] { id, 0, "",id, false }).Single<Models.RAPID>();

                foreach (Models.RAPIDDetail sd in lstDetail)
                {
                    sd.Status = lstStatus.Where(row => row.intRAPIDID == sd.intID).ToList();
                    if (id > 0)
                        sd.Item = dc.Database.SqlQuery<Models.RAPIDItem>("SPROCgetRAPID  @intID={0}, @WWID = {1}, @ContinuityID = {2}, @intDecisionID={3}, @bDetail = {4}", new object[] { sd.intID, 0, 0,id, true }).ToList<Models.RAPIDItem>();

                }
            }

            rapid.lstDetail = lstDetail;
            return rapid;
        }
        public ActionResult SaveComments(int idDetail, string Comment,int idStatus)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@intRAPID",idDetail);
            param[1] = new SqlParameter("@txtComment", Comment);
            param[2] = new SqlParameter("@intStatus", idStatus);
            
            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCUpdateRAPIDStatus", param);
            int intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());

            if(intRes>0)
            {
                return JavaScript("alert('Saved !')");
            }
            return JavaScript("alert('Error !')");
        }
        public ActionResult GetNextRev(int idDecision)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@idRAPIDDecision", idDecision);

            DataSet dsRes = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCgetNextRevRAPID", param);
            string strRev = dsRes.Tables[0].Rows[0][0].ToString();

            
            return JavaScript("$('#txtTitle').val($('#txtTitle').val() + '"+strRev+"')");
        }
        public ActionResult GetRol(string strEmail,string strRole)
        {
            Models.RAPIDItem detail = new Models.RAPIDItem();
            try
            {
                using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
                {
                    detail = dc.Database.SqlQuery<Models.RAPIDItem>("SPROCgetLastRol @vchEmail={0}", new object[] { strEmail }).Single<Models.RAPIDItem>();
                }

                return JavaScript(" $('#" + strRole + "').val('" + detail.strRole + "');");
            }
            catch { }
            return JavaScript("");
        }
        [ValidateInput(false)]
        public ActionResult Save(FormCollection collection, int WWID,bool bDelete,string ContinuityID,string WName)
        {
            SqlConnection conn = new SqlConnection(GeneralClasses.General.strConnectionString);
            SqlTransaction tran;

            
            conn.Open();
            tran = conn.BeginTransaction();
            List<Models.RAPIDDetail> lstDetail = new List<Models.RAPIDDetail>();
            int intRes = 0;
            int intCCb = 0;
            #region Array
            foreach (string strKey in collection.AllKeys)
            {
                if(strKey.Contains("txtEmail"))
                {
                    string[] strField = strKey.Split('-');
                    Models.RAPIDItem det = new Models.RAPIDItem();
                    Models.RAPIDDetail detail = new Models.RAPIDDetail();
                    List<Models.RAPIDItem> lstItem = new List<Models.RAPIDItem>();

                    if (lstDetail.Where(row => row.intID == Convert.ToInt32(strField[1].Trim())).Count() == 0)
                    {
                        detail.intID = Convert.ToInt32(strField[1]);
                        
                        det.intID = Convert.ToInt32(strField[2].Trim());
                        det.strEmail = collection[strKey];
                        

                        if (det.strEmail.Trim().Length > 0)
                        {
                            lstItem.Add(det);
                            detail.Item = lstItem;
                            lstDetail.Add(detail);
                        }
                    }else
                    {
                        det.intID = Convert.ToInt32(strField[2].Trim());
                        det.strEmail = collection[strKey];
                        if (det.strEmail.Trim().Length > 0)
                            lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Add(det);
                    }
                }
            }

            foreach (string strKey in collection.AllKeys)
            {
                if (strKey.Contains("txtRole"))
                {
                    string[] strField = strKey.Split('-');
                    if (collection[strKey].Length > 0 && lstDetail.Exists(row => row.intID == Convert.ToInt32(strField[1].Trim())) && lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Exists(row => row.intID==Convert.ToInt32(strField[2].Trim())))
                      lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Single(row => row.intID==Convert.ToInt32(strField[2].Trim())).strRole=collection[strKey];
                }
            }
            foreach (string strKey in collection.AllKeys)
            {
                if (strKey.Contains("chk") && !strKey.Equals("chkAutoP"))
                {
                    string[] strField = strKey.Split('-');
                    if (collection[strKey].Length > 0 && lstDetail.Exists(row => row.intID == Convert.ToInt32(strField[1].Trim())) && lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Exists(row => row.intID == Convert.ToInt32(strField[2].Trim())))
                        lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Single(row => row.intID == Convert.ToInt32(strField[2].Trim())).intStatus =Convert.ToInt32(collection[strKey]);
                }
            } 
            foreach (string strKey in collection.AllKeys)
            {
                if (strKey.Contains("txtComment") )
                {
                    string[] strField = strKey.Split('-');
                    if (collection[strKey].Length > 0 && lstDetail.Exists(row => row.intID == Convert.ToInt32(strField[1].Trim())) && lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Exists(row => row.intID == Convert.ToInt32(strField[2].Trim())))
                        lstDetail.Single(row => row.intID == Convert.ToInt32(strField[1].Trim())).Item.Single(row => row.intID == Convert.ToInt32(strField[2].Trim())).strComment = collection[strKey];
                }
            }
            #endregion
            try
            {
                if (lstDetail.Count > 0)
                {
                    SqlParameter[] param = new SqlParameter[5];
                    bool bIsNew = collection["idRapid"].Equals("0");

                    param[0] = new SqlParameter("@intId", collection["idRapid"]);
                    param[1] = new SqlParameter("@vchTitle", collection["txtTitle"]);
                    param[2] = new SqlParameter("@bPrivate", collection["ckbPrivate"] == null ? false : true);
                    param[3] = new SqlParameter("@vchDesc", collection["txtDescription"] == null ? "" : collection["txtDescription"]);
                    param[4] = new SqlParameter("@bDelete", bDelete);

                    DataSet dsRes = SqlHelper.ExecuteDataset(tran, "SPROCmanRAPID", param);
                    intRes = Convert.ToInt32(dsRes.Tables[0].Rows[0][0].ToString());


                    foreach (Models.RAPIDDetail det in lstDetail)
                    {
                        foreach (Models.RAPIDItem itemdet in det.Item)
                        {

                                
                                param = new SqlParameter[9];
                                param[0] = new SqlParameter("@intId", bIsNew || itemdet.intID >= 1000 ? 0 : itemdet.intID);
                                param[1] = new SqlParameter("@vchEmail", itemdet.strEmail);
                                param[2] = new SqlParameter("@vchRole", itemdet.strRole == null ? "" : itemdet.strRole);
                                param[3] = new SqlParameter("@intRAPIDID", det.intID);
                                param[4] = new SqlParameter("@intDecisionID", intRes);
                                param[5] = new SqlParameter("@intStatusID", Convert.ToInt32(collection["txtRev"])>0 ? 0 : itemdet.intStatus);
                                param[6] = new SqlParameter("@vchRecommendation", collection["txtRecommend"]);
                                param[7] = new SqlParameter("@vchComment", itemdet.strComment);
                                param[8] = new SqlParameter("@bDelete", 0);

                                DataSet dsRes2 = SqlHelper.ExecuteDataset(tran, "SPROCmanRAPIDDetail", param);
                              //  int intRes2 = Convert.ToInt32(dsRes2.Tables[0].Rows[0][0].ToString());
                        }
                    }

                    if (Convert.ToInt32(collection["linked"]) > 0)
                    {
                        if (Convert.ToInt32(collection["idCCB"]) > 0)
                        {
                            intCCb=Convert.ToInt32(collection["idCCB"]);
                            param = new SqlParameter[4];
                            param[0] = new SqlParameter("@id", 0);
                            param[1] = new SqlParameter("@idRAPID", intRes);
                            param[2] = new SqlParameter("@idRel", intCCb);
                            param[3] = new SqlParameter("@idTypeRel", Convert.ToInt32(collection["linked"]));
                            DataSet dsResCCB = SqlHelper.ExecuteDataset(tran, "SPROCmanRelRAPID", param);
                            int intRes3 = Convert.ToInt32(dsResCCB.Tables[0].Rows[0][0].ToString());
                        }

                    }

                    if (Convert.ToInt32(collection["txtRev"]) > 0)
                    {
                        param = new SqlParameter[2];
                        param[0] = new SqlParameter("@idRAPIDDecisionO", Convert.ToInt32(collection["txtRev"]));
                        param[1] = new SqlParameter("@idRAPIDDecisionR", intRes);

                        DataSet dsRegRev = SqlHelper.ExecuteDataset(tran, "SPROCmanRevRAPID", param);
                        int intRes3 = Convert.ToInt32(dsRegRev.Tables[0].Rows[0][0].ToString());
                    }
                    tran.Commit();
                    conn.Close();
                    tran.Dispose();
                    SendMailNotification(intRes,WWID,intCCb);
                    
                }
            }
            catch
            {
                tran.Rollback();
                conn.Close();
                tran.Dispose();
            }
            return InputRapid(intRes,WWID, intCCb,WName);
        }
        public ActionResult TakeDecision(int id,string email,string role,int rapidid, int status)
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@intId", id);
            param[1] = new SqlParameter("@vchEmail", email);
            param[2] = new SqlParameter("@vchRole", role);
            param[3] = new SqlParameter("@intRAPIDID", 5);
            param[4] = new SqlParameter("@intDecisionID", rapidid);
            param[5] = new SqlParameter("@intStatusID", status);
            param[6] = new SqlParameter("@vchRecommendation", "");
            param[7] = new SqlParameter("@vchComment", "");
            param[8] = new SqlParameter("@bDelete", 0);

            DataSet dsRes2 = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCmanRAPIDDetail", param);
            return new EmptyResult();
        }
        private void Notify(int intDetailID, int intType)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@intRapidDetail", intDetailID);
            param[1] = new SqlParameter("@intType", intType);
            param[2] = new SqlParameter("@bIsNotified",1);

            DataSet dsRes2 = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCsetNotified", param);
        }
        private SearchResultCollection FindAccountByEmail(string pEmailAddress)
        {
            string filter = string.Format("(proxyaddresses=SMTP:{0})", pEmailAddress);

            using (DirectoryEntry gc = new DirectoryEntry("LDAP:"))
            {
                foreach (DirectoryEntry z in gc.Children)
                {
                    using (DirectoryEntry root = z)
                    {
                        using (DirectorySearcher searcher = new DirectorySearcher(root, filter, new string[] { "proxyAddresses", "objectGuid", "displayName", "distinguishedName" }))
                        {
                            searcher.ReferralChasing = ReferralChasingOption.All;
                            SearchResultCollection result = searcher.FindAll();

                            return result;
                        }
                    }
                }
            }
            return null;
        }
        private string FindEmailByAccount(string strAccount)
        {
            String account = strAccount.Substring(strAccount.IndexOf('\\')+1);
            DirectoryEntry entry = new DirectoryEntry();

            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = String.Format("(sAMAccountName={0})", account);
                search.PropertiesToLoad.Add("mail");  // e-mail addressead

                SearchResult result = search.FindOne();
                if (result != null)
                {
                    return result.Properties["mail"][0].ToString();
                }
                else
                {
                    return "Unknown User";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private void SendMailApprovedReject(Models.RAPID rapid,bool bApproved=false)
        {
            string strBody="<table>"
                                +"<tr>"
                                    +"<td>Date:</td>"
                                    +"<td>"+DateTime.Now.ToShortDateString()+"</td>"
                                +"</tr><tr>"
                                    +"<td>RAPID:</td>"
                                    +"<td>"+rapid.strTitle+"</td>"
                                +"</tr><tr>"
                                    +"<td>RAPID STATUS:</td>"
                                    +"<td>"+(bApproved? "Recommendation Approval" : "RAPID \"A\" Reject")+"</td>"
                                +"</tr><tr>"
                                +"<td>RAPID \""+(bApproved? "R" : "A")+"\":</td>"
                                    +"<td>"+rapid.lstDetail.First().Item.First().strEmail+"</td>"
                                +"</tr><tr>"
                                    +"<td>RAPID Link:</td>"
                                    +"<td><a href=\"#\">Go to Desicion Recommendation</a></td>"
                                +"</tr></table>";
            MailMessage mailObj = new MailMessage();

            foreach (Models.RAPIDDetail detail in rapid.lstDetail)
            {
                foreach (Models.RAPIDItem ItemDet in detail.Item)
                {
                    mailObj.To.Add(new MailAddress(ItemDet.strEmail));
                }
            }

            mailObj.From = new MailAddress(GeneralClasses.General.strEmailFrom, "RAPID");
           
            mailObj.Subject = "RAPID Automation: \""+rapid.strTitle+"\" - Participant Decision Communication";
            mailObj.Body = strBody;
            mailObj.IsBodyHtml = true;

            SmtpClient SMTPServer = new SmtpClient(GeneralClasses.General.strSMTP);
            try
            {
                SMTPServer.Send(mailObj);
                
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@intRAPID", rapid.intId);

                DataSet dsRes2 = SqlHelper.ExecuteDataset(GeneralClasses.General.strConnectionString, "SPROCApprovedNotifyRAPID", param);
            }
            catch (Exception ex)
            {

            }
        }
        private void SendMailNotification(int intRAPIDID,int intWWID, int intCCb)
        {

            Models.RAPID rapid = getRAPID(intRAPIDID);
            List<Models.RAPIDStatus> lstStatus = new List<Models.RAPIDStatus>();
            Models.RAPIDStatus status=new Models.RAPIDStatus();
            string strStatus = "";

            MailMessage mailObj = new MailMessage();
            

            using (AutoIndicators.Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                lstStatus = dc.Database.SqlQuery<Models.RAPIDStatus>("SPROCgetStatus").ToList<Models.RAPIDStatus>();
            }

            
            foreach( Models.RAPIDDetail detail in rapid.lstDetail)
            {
                if (detail.strEmailBody.Trim().Length > 0)
                {
                    foreach (Models.RAPIDItem ItemDet in detail.Item)
                    {
                        if (ItemDet.intStatus > 0)
                        {
                            status = lstStatus.Where(row => row.intID == ItemDet.intStatus).First<Models.RAPIDStatus>();
                            strStatus = status.strName;
                        }
                        else
                            strStatus = rapid.strStatus;
                        string strMsg = "";
                        strMsg = detail.strEmailBody
                                .Replace("#Title#", rapid.strTitle)
                                .Replace("#Date#", DateTime.Now.ToShortDateString())
                                .Replace("#Status#",strStatus)
                                .Replace("#Approved#", ItemDet.strEmail)
                                .Replace("#Comment#", ItemDet.strComment)
                                .Replace("#Recommendation#", ItemDet.strRecommendation)
                                .Replace("#Link#", "<a href='" + Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.IndexOf("/RAPID/")) + "/RAPID/InputRapid?id=" + intRAPIDID + "&WWID=" + intWWID.ToString() + "&ccb=" + intCCb.ToString() + "'>Link to RAPID Decision</a>");

                        mailObj.From = new MailAddress(GeneralClasses.General.strEmailFrom, "RAPID");
                        mailObj.To.Add(new MailAddress(ItemDet.strEmail));
                        mailObj.Subject ="RAPID Automation: \""+rapid.strTitle+"\" - "+ detail.strEmailTitle;
                        mailObj.Body = strMsg;
                        mailObj.IsBodyHtml = true; 

                        SmtpClient SMTPServer = new SmtpClient(GeneralClasses.General.strSMTP);
                        try
                        {
                            SMTPServer.Send(mailObj);

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            
        }
    }
}
