﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class IndicatorScheduleComment
    {
        public string Timestamp { get; set; }
        public string Milestone { get; set; }
        public int ID { get; set; }
        public string Updated { get; set; }
        public string UpdatedComments { get; set; }
        public string Prior { get; set; }
        public string PriorComments { get; set; }
        public string Origin { get; set; }
    }
    public class IndicatorHealthIndicatorComment
    {
        public string Timestamp { get; set; }
        public string Category { get; set; }
        public int ID { get; set; }
        public string Updated { get; set; }
        public string UpdatedComments { get; set; }
        public string Prior { get; set; }
        public string PriorComments { get; set; }
        public string Origin { get; set; }
    }
    public class IndicatorBudgetComment
    {
        public string Timestamp { get; set; }
        public decimal Trend { get; set; }
        public int RYG { get; set; }
        public string UpdatedComments { get; set; }
        public int Prior { get; set; }
        public string PriorComments { get; set; }
        public string Origin { get; set; }
    }
    public class IndicatorValidationComment
    {
        public string Timestamp { get; set; }
        public string RYG { get; set; }
        public string UpdatedComments { get; set; }
        public string Prior { get; set; }
        public string PriorComments { get; set; }
        public string Origin { get; set; }
    }
    public class IndicatorSnapshotComment
    {
        public string Timestamp { get; set; }
        public string Updated { get; set; }
        public string Prior { get; set; }

    }
    public class IndicatorsPCOSComment
    {
        public string Timestamp { get; set; }
        public string CostCategory { get; set; }
        public string ProgramSKU { get; set; }
        public decimal ActualValue { get; set; }
        public decimal TrendValue { get; set; }
        public decimal TargetValue { get; set; }
        public decimal ActualValuePrior { get; set; }
        public decimal TrendValuePrior { get; set; }
        public decimal TargetValuePrior { get; set; }
        public string Comments { get; set; }
    }
}