﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ContentLink
    {
        int id { get; set; }
        string Term { get; set; }
        string Definition { get; set; }
        string Link { get; set; }
        string actor { get; set; }
        DateTime CreationDate { get; set; }

    }
}