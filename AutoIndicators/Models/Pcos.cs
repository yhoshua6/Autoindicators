﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Pcos : MasterModel
    {
        public int PcosId { get; set; }
        public string SKU { get; set; }
        public string SKUDesc { get; set; }
        public string PMilestoneName { get; set; }
        public string CostCategory { get; set; }
        public string CostDesc { get; set; }        
        public int PCOSMilestoneId { get; set; }
        public decimal? TargetValue { get; set; }
        public decimal? ActualValue { get; set; }
        public decimal? TrendValue { get; set; }
        public string Actor { get; set; }
        public string Comments { get; set; }
        public int ProgramSKUid { get; set; }
        public int CostCategoryId { get; set; }
        public int OrderAs { get; set; }
        public int PWID { get; set; }
        public string descriptionLink { get; set; }
        public int? oldPcosId { get; set; }
        public decimal? oldTargetValue { get; set; }
        public decimal? oldActualValue { get; set; }
        public decimal? oldTrendValue { get; set; }
        public string oldComments { get; set; }
        public string oldActor { get; set; }
        public string CommentsRed { get; set; }
        public int Flagged { get; set; }
    }
}