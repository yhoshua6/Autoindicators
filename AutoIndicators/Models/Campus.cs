﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Campus
    {
        public int id { get; set; }
        public string Name { get; set; }
        public int GeoID { get; set; }
        public string Manager { get; set; }
        public string ManagerName { get {
            if (this.Manager != null && this.Manager.Length>0)
                return GeneralClasses.General.FindDisplayNametByEmail(this.Manager);
            else
                return "";
        } }

        public List<LabProjectList> Projects { get; set; }
        public List<LabUser> AllowedUsers { get; set; }
    }
}