﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ArchivedBugs
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ReportingUser { get; set; }
        public string ResolvedUser { get; set; }
        public int Version { get; set; }
        public string ResolvingComments { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ResolvedDate { get; set; }
    }
}