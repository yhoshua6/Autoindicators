﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Users : Security
    {
       
        public int id { get; set; }
        public string validUsers { get; set; }
        public int? UserType { get; set; }
        public string strName { get; set; }
        public string strEmail { get; set; }
        public string strTelephone { get; set; }
        public string strDepartment { get; set; }
        public string strTitle { get; set; }
        public string strSystemMessage { get; set; }

    }
   
}