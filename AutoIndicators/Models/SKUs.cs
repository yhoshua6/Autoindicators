﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class SKUs
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string DescText { get; set; }
        public int? Flagged { get; set; }
        public string Link { get; set; }
        public int? Active { get; set; }

    }
}