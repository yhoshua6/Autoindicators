﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoIndicators.Models
{
    public class SnapshotIndex : MasterModel
    {
        [GeneralClasses.LabelSecurity("DEG PXT")]
        public int PWID { get; set; }

        [GeneralClasses.LabelSecurity("Update")]
        public string Updatetxt { get; set; }

        [GeneralClasses.LabelSecurity("Daya 1")]
        public string Newtxt { get; set; }

        [GeneralClasses.LabelSecurity("Daya 1")]
        public int? ProgramId { get; set; }

        [GeneralClasses.LabelSecurity("Vertical Order")]
        public int VerticalOrder { get; set; }

        [GeneralClasses.LabelSecurity("Daya 1")]
        public int PXTOrder { get; set; }

        [GeneralClasses.LabelSecurity("Program Type Order")]
        public int OrderAs { get; set; }

        public IEnumerable<SelectListItem> PTPLCStatus { get; set; }

    }
}