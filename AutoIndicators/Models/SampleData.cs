﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class SampleData : MasterModel
    {
        public int Id { get; set; }

        [GeneralClasses.LabelSecurity("DEG PXT")]
        public int PWID { get; set; }

        [GeneralClasses.LabelSecurity("Update")]
        public string Updatetxt { get; set; }

        [GeneralClasses.LabelSecurity("Daya 1")]
        public string Newtxt { get; set; }

        [GeneralClasses.LabelSecurity("Daya 1")]
        public int? ProgramId { get; set; }       
    }
}