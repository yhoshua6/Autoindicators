﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class AllComments
    {
        public int id { get; set; }
        public string comments { get; set; }
        public string OriginView { get; set; }
        public string category { get; set; }
        public int? CategoryId { get; set; }
        public int? CategoryId2 { get; set; }
        public int selected { get; set; }
        public string PWContinuityID { get; set; }
        public int WWID { get; set; }
    }
}