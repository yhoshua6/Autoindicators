﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class IndicatorTypes
    {
        public int id { set; get; }
        public string strName { set; get; }
        public int intWidth { set; get; }
        public int intBorder { set; get; }
        public List<IndicatorTypesStruct> structure { set; get; }
        public List<IndicatorTypesDetail> detail { set; get; }
    }
    public class IndicatorTypesDetail
    {
        public string strX { get; set; }
        public string strY { get; set; }
        public string strValue { get; set; }
    }
    public class IndicatorTypesStruct
    {
        public string id { set; get; }
        public string strCaption { set; get; }
        public string strAxis { set; get; }
        public bool bGroupBy { set; get; }
        public bool bVisible { set; get; }
        public string strValue { set; get; }
        public bool bIsTextArea { get; set; }
        public int intNoRow { set; get; }
        public int intOrder { set; get;  }

    }
}