﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BudgetHeaders
    {
        public string Header1 { get; set; }
        public string Header2 { get; set; }
        public string Header3 { get; set; }
        public string Header4 { get; set; }
        public string Header5 { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Comments { get; set; }
        public DateTime Date { get; set; }
        public string Extras { get; set; }
        public string[] ExtraColumns
        {
            get
            {
                if (this.Extras !=null && this.Extras.Length > 0)
                {
                    return this.Extras.Split(';');
                }
                else
                    return null;
            }
        }
    }
}