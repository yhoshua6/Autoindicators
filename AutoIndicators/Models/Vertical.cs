﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Vertical
    {
        public int id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int order { get; set; }
    }
}