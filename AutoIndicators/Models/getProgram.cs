﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class getProgram
    {
        public string PXTName { get; set; }
        public int PXTId { get; set; }
        public string RelevantSilicon { get; set; }
        public int RelevantSiliconId { get; set; }
        public string ProjectName { get; set; }
        public string ProgramDescription { get; set; }
        public string ProgramType { get; set; }
        public string GEO { get; set; }
        public string OwnerName { get; set; }
        public string WW { get; set; }
        public int PWID { get; set; }
        public int PID { get; set; }
        public string ProgramLink { get; set; }
        public string PDescLink { get; set; }
        public string SharepointLink { get; set; }
        public bool IsPrivate { get; set; }
        public int WWID { get; set; }
        public int weekStatus { get; set; }
        public string PWContinuityID { get; set; }
        public string MRD { get; set; }
        public string PRD { get; set; }
        List<string> AllowedViewers { get; set; }
        public int VerticalID { get; set; }
        public string VerticalName { get; set; }
        public string VerticalLink { get; set; }
        public int Clasification { get; set; }
        public int PTID { get; set; }
        public string RMStartWW { get; set; }
        public string RMEndWW { get; set; }
        public string StartWW { 
            get { 
                if(this.RMStartWW!=null && this.RMStartWW.Length>0 && this.RMStartWW.Contains("'"))
                {
                    return this.RMStartWW.Split('\'')[0].Replace("W","").Trim();
                }else
                {
                    return "";
                }
        } }
        public string StartWWY
        {
            get
            {
                if (this.RMStartWW != null && this.RMStartWW.Length > 0 && this.RMStartWW.Contains("'"))
                {
                    return this.RMStartWW.Split('\'')[1];
                }
                else
                {
                    return "";
                }
            }
        }
        public string EndWW
        {
            get
            {
                if (this.RMEndWW != null && this.RMEndWW.Length > 0 && this.RMEndWW.Contains("'"))
                {
                    return this.RMEndWW.Split('\'')[0].Replace("W", "").Trim();
                }
                else
                {
                    return "";
                }
            }
        }
        public string EndWWY
        {
            get
            {
                if (this.RMEndWW != null && this.RMEndWW.Length > 0 && this.RMEndWW.Contains("'"))
                {
                    return this.RMEndWW.Split('\'')[1];
                }
                else
                {
                    return "";
                }
            }
        }
        public string ColorStart { get; set; }
        public string ColorEnd { get; set; }
        public int Perc { get; set; }
        public int Position { get; set; }
        public string StartDate { get;set; }
        public string EndDate { get; set; }
     }
}