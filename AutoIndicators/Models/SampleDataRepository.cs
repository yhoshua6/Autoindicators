﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class SampleDataRepository
    {
        public static SampleData Get(int id)
        {
            return GetAll().SingleOrDefault(x => x.Id.Equals(id));
        }

        public static List<SampleData> GetAll()
        {
            int i=1;
            var list = new List<Models.SampleData>();
            int WWID = 76; //Change the WWID on this to change the WW (Remember that this is only for the main db pull, not the search saved)

            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                list = dc.Database.SqlQuery<Models.SampleData>("SPROCListPrograms @WW = {0} ", WWID).ToList<Models.SampleData>();

            }

            foreach (var item in list) {
                item.Id = i;
                i++;
            }

            return list;
        }
    }

}