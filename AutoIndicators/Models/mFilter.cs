﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoIndicators.Models
{
    public class mFilter
    {
        public enum mHtmlInputType{ 
            fComboBox
            ,fTextBox
            ,fList
        }
        public string strField { get; set; }
        public mHtmlInputType mHtmlTypeOf { get; set; }
        public string strLabelField { get; set; }
        public string strValueField { get; set; }
        public string strLinkedField { get; set; }
    }
    
    
}