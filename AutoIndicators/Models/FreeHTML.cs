﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class CfgFreeHTML
    {
        public int id { get; set; }
        public string strView { get; set; }
        public string strProperty { get; set; }
        public string strValue { get; set; }
        public int idFreeHTML { get; set; }
        public string FreeHTML { get; set;}
        public List<string> AllowedUser { get; set; }
        
    }
}