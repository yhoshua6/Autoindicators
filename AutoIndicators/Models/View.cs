﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class View
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
        public string label { get; set; }
    }
}