﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ProperLinks
    {
        public int id { get; set; }
        public string ProperName { get; set; }
        public string Definition { get; set; }
        public string Link { get; set; }
        public string CreatorActor { get; set; }
        public DateTime CreationDate { get; set; }
        public int ContextID { get; set; }
        public DateTime LastModified { get; set; }
    }
}