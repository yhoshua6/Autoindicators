﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ValidationPhases
    {
        public int PhaseId { get; set; }
        public string PWContinuityID { get; set; }
        public int WWID { get; set; }
        public string PhaseName { get; set; }
        public string DescText { get; set; }
        public string link { get; set; }
        public string startWWName { get; set; }
        public string endWWName { get; set; }
        public int? startWWID { get; set; }
        public int? endWWID { get; set; }
        public int? startWW { get; set; }
        public int? endWW { get; set; }
        public int? startYY { get; set; }
        public int? endYY { get; set; }
        public string CreateUser { get; set; }
    }
}