﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class RAPID
    {
        public int intId { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public List<RAPIDDetail> lstDetail { get; set; }
        public bool bIsPrivate { get; set; }
        public int intRelType { get; set; }
        public int intRelId { get; set; }
        public string strStatus { get; set; }
        public bool bTakeDecision { get; set; }
        public bool bApprovedNotified { get; set; }
        public int intRAPIDOrigins { get; set; }
        public int intTotRev { get; set; }
    }
    public class RAPIDDetail
    {
        public int intID    {get; set;}
        public string strIcon { get; set; }
        public string strName { get; set; }
        public string strText { get; set; }
        public string strButton { get; set; }
        public string strEmailBody { get; set; }
        public string strEmailTitle { get; set; }
        public List<RAPIDItem> Item { get; set; }
        public List<RAPIDStatus> Status { get; set; }
    }
    public class RAPIDItem
    {
        public int intID { get; set; }
        public string strRole { get; set; }
        public string strEmail { get; set; }
        public int intStatus { get; set; }
        public string strComment { get; set; }
        public string strRecommendation { get; set; }
    }
    public class RAPIDStatus
    {
        public int intID { get; set; }
        public string strName { get; set; }
        public int intRAPIDID { get; set; }
    }
    public class RAPIDRel
    {
        public int id { get; set; }
        public string strName { get; set; }
    }
}