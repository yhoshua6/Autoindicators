﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BPOProjReport
    {
        public string Name { get; set; }
        public string ProgName { get; set; }
        public string Email { get; set; }
        public int BPOProjId { get; set; }
        public string BPOProjName { get; set; }
        public string PWContinuityID { get; set; }
    }
}