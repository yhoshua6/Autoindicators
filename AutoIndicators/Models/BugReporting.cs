﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class BugReporting
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ReportingUser { get; set; }
        public string ResolvingUser { get; set; }
        public int Version { get; set; }
        public string ResolvingComments { get; set; }
        public int? BugStatusID { get; set; }
        public string Status { get; set; }
        public int AppId { get; set; }
        public string AppName { get; set; }
        public DateTime CreationDate { get; set; }
        public string ResolvedDate { get; set; }
    }
}