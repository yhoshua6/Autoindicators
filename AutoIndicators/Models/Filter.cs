﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class FilterCfg
    {
        public int Id { get; set; }
        public string strName { get; set; }
        public string strCreateUser { get; set; }
        public DateTime dttCreateDate { get; set; }
        public int intPriority { get; set; }
        public bool bDeleted { get; set; }
        public List<FilterDetails> Details { get; set; }
        public List<FilterUsers> Users { get; set; }
        public string strFilterString
        {
            get
            {
                string strPrevField = "";
                string strFil = "";
                string[] strField;
                if (this.Details.Count > 0)
                {
                    foreach (FilterDetails det in this.Details)
                    {
                        strField = det.strValue.Split(',');
                        if (strPrevField != strField[0])
                            strFil += ") AND (" + strField[0] + "='" + strField[1] + "'"; //and +=
                        else
                            strFil += " OR " + strField[0] + "='" + strField[1] + "'"; //or +=
                        strPrevField = strField[0];
                    }

                    if (strFil.Length > 0)
                    {
                        return strFil.Substring(6).Trim() + ")";
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }
        public string strUrl
        {
            get
            {
                return strName.Replace(' ', '_');
            }
        }

    }

    public class FilterDetails
    {
        public int Id { get; set; }
        public string strValue { get; set; }
    }
    public class FilterUsers
    {
        public string strUser { get; set; }
        public string strName
        {
            get
            {
                if (this.strUser != null && this.strUser.Length > 0)
                {
                    return GeneralClasses.General.FindDisplayNametByEmail(this.strUser);
                }
                else
                    return "";
            }
        }
        public int intOrder { get; set; }
    }
}