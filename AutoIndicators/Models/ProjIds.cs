﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ProjIds
    {
        public int Id { get; set; }
        public int Affected { get; set; }
        public string ProjID { get; set; }
        public string Sufix { get; set; }
    }
}