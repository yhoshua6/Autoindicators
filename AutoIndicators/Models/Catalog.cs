﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class MilestoneCatalog
    {
        public string strName { get; set;}
        public string strDefinition { get; set; }
        public int intSecuence { get; set; }
        public int Id { get; set; }
    }
    public class IndicatorCatalog
    {
        public int Id { get; set; }
        public string strName { get; set; }
        public int intSecuence { get; set; }
    }
}