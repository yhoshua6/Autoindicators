﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace AutoIndicators.Models
{
    public class ValidationModel : MasterModel
    {
        public int ValidationId { get; set; }
        public string currentActor { get; set; }
        public string Comment { get; set; }
        public string RYG { get; set; }
        public int? oldValidationId { get; set; }
        public string oldActor { get; set; }
        public string oldComments { get; set; }
        public string valOwner { get; set; }
        public string OwnerName
        {
            get
            {
                if (this.valOwner !=null && this.valOwner.Length > 0)
                {
                    return GeneralClasses.General.FindDisplayNametByEmail(this.valOwner);
                }
                else
                    return "";
            }
        }
        public string valLink { get; set; }
        public int? oldRYG { get; set; }
        public int PWID { get; set; }
        public int HIID { get; set; }
        public bool bFileNew { get; set; }
        public List<Models.ValidationPhases> Phases { get; set; }
        public string ValidationLink
        {
            get
            {
                string strPath = "https://spbiprd.intel.com/sites/ISQ/DCG DEG EPSD/" + this.ProgName + "/eMRC table of " + this.ProgName + ".xlsx";
                string strLocalPath = AppDomain.CurrentDomain.BaseDirectory + "\\ValidationFiles\\tmpVal"+Guid.NewGuid()+".xlsx";
                int inti=0;
               Search: 
               try
                {
                    

                    
                    //if (System.IO.File.Exists(strLocalPath))
                    //{
                    //    System.IO.File.Delete(strLocalPath);
                    //}

                    //WebClient wc = new WebClient();
                    //wc.Credentials = new NetworkCredential("rlobatow", "Cin0mirm*");
                    //wc.DownloadFile(strPath, strLocalPath);
                    //wc.Dispose();
                    //GC.Collect();

                    //if (System.IO.File.Exists(strLocalPath))
                    //{
                    //    System.IO.File.Delete(strLocalPath);
                        return strPath;
                    //}
                    //else
                    //    return null;
                }
                catch
                {
                    //if (inti == 0)
                    //{
                    //    strPath = "https://spbiprd.intel.com/sites/ISQ/DCG DEG EPSD/" + this.ProgName + " Indicators/eMRC table of " + this.ProgName + ".xlsx";
                    //    inti++;
                    //    goto Search;
                    //}
                    //else
                    //{
                         return null;
                    //}
                }
            }
        }
    }

    public class ValidationReport
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
    }
}