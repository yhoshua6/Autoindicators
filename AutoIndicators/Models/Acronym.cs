﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Acronym
    {
        public int id { get; set; }
        public string Term { get; set; }
        public string Abbreviation { get; set; }
        public string Definition { get; set; }
        public string Actor { get; set; }
        public DateTime CreationDate { get; set; }
        public string Link { get; set; }
        public string ContextId { get; set;} 
        public int ContextActorId { get; set;}
        public int ProgType {get; set;}
        public int OwnerId { get; set;}
        public int PXTId { get; set; }
        public string ContinuityID { get; set; }
        public int View { get; set; }
    }
    public class AcronymLinksExternal
    {
        public string Link { get; set; }
        public string Texto { get; set; }
        public int StartPos { get; set;  }
        public int EndPos { get; set; }
    }
}