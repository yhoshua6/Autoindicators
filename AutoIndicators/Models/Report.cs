﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Report
    {
        public string PWContinuityID { get; set; }
        public string Name { get; set; }
        public string Program { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public int SubTypeId { get; set; }
        public string Subtype { get; set; }
        public int Reference { get; set; }
        public int WWID { get; set; }
        public string weekName { get; set; }
        public string Notif { get; set; }
        public int NotifId { get; set; }
        public string IconApp { get; set; }
        public int Owner { get; set; }
        public string OwnerEmail { get; set; }
        public int PT { get; set; }
        public int PXT { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public int Total { get; set; }
    }
    public class NotifReport:Report
    {
        public int Days { get; set; }
        public string Item { get; set; }
        public string AppName { get; set; }
        public int AppId { get; set; }
    }
    public class HIReport:Report
    {
        public int Days { get; set; }
        public string Indicator { get; set; }
    }
    public class ScheduleReport:Report
    {
        public string Milestone { get; set; }
        public string ScheduleType { get; set; }
    }
    public class ReportSanpshotChart
    {
        public int OwnerId { get; set; }
        public string Name { get; set; }
        public string Program { get; set; }
        public string weekName { get; set; }
        public bool Complete { get; set; }
    }
}