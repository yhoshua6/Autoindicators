﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Drawing;

namespace AutoIndicators.Models
{
    public class ValidationImport : MasterModel
    {
        public int intID { get; set; }
        public string strName { get; set; }
        public bool bDelete { get; set; }
        public string strGraph { get; set; }
        public List<ValidationImportSchema> Schema { get; set; }
        public byte[] imgGraph {
            get
            {
                if (strGraph != null)
                {
                    string[] strImg = strGraph.Split('|');
                    byte[] bImg = new byte[strImg.Length];
                    int i = 0;
                    foreach (string strBit in strImg)
                    {
                        bImg[i] = byte.Parse(strBit);
                        i++;
                    }

                    return bImg;
                }
                else
                    return null;
            }
        }
        public string strLastRefresh { get; set; }
    }
    public class ValidationImportSchema
    {
        public int intID { get; set; }
        public string strName { get; set; }
        public List<ValidationImportValues> Values { get; set; }
    }
    public class ValidationImportValues
    {
        public int intID { get; set; }
        public string strValue { get; set; }
    }

    public struct UploadFile
    {
        public DataSet dsData { get; set; }
        public string strFileName { get; set; }
        public byte[] bImage { get; set; }
        public DateTime dttRefreshDate { get; set; }
    }
}