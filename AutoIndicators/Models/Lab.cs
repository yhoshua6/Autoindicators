﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Lab
    {
        public int Id { get; set; }
        public string strName { get; set; }
        public string strLayout { get; set; }
        public string strChampion { get; set; }
        public string ChampionName
        {
            get {
                if (this.strChampion!=null && this.strChampion.Length > 0)
                {
                    return GeneralClasses.General.FindDisplayNametByEmail(this.strChampion);
                }
                else
                    return "";
                }
        }
        public int CampusId { get; set; }
        public Campus Campus { get; set; }
        public List<LabLink> Links { get; set; }
        public List<LabRequest> Resquests { get; set; }
        public List<LabSpace> Spaces { get; set; }
        public List<LabSpaceExtra> Extra { get; set; }
        public decimal Size { get; set; }
    }
    public class LabPerWeek:Lab 
    {
        public int LabsPerWeekID { get; set; }
        public string strLead { get; set; }
        public string strLeadName { get {
            if (strLead != null &&  strLead.Length>0)
            {
                return GeneralClasses.General.FindDisplayNametByEmail(this.strLead);
            }
            else
                return strLead;
        } }
        public string LabContinuityID { get; set; }
        public int WWID { get; set; }
        public string strUpdate { get; set; }
        public string strComments { get; set; }
        public string Geo { get; set; }
        public string CampusName { get; set; }
        public string LabName { get; set; }
        public List<LabAvl> Avl { get; set; }
    }
    public class LabTeam
    {
        public int idTeam { get; set; }
        public string strName { get; set; }
        public List<LabSpace> Spaces { get; set; }
        public List<LabTeamMember> Team { get; set; } 
    }
    public class LabTeamMember
    {
        public int idMember {get; set;}
        public string strMember { get; set; }
        public string strName { get { return GeneralClasses.General.FindDisplayNametByEmail(this.strMember); } }
    }
    public class LabLink
    {
        public string strName { get; set; }
        public string strLink { get; set; }
    }
    public class LabRequest
    {
        public int id { get; set; }
        public string strRequester { get; set; }
        public string strTeamName { get; set; }
        public string strBuManager { get; set; }
        public string strLabChampion { get; set; }
        public string strNewSpace { get; set; }
        public string strProject { get; set; }
        public string strDesireLocation { get; set; }
        public int intStartWW { get; set; }
        public int intStarYear { get; set; }
        public int intEndWW { get; set; }
        public int intEndYear { get; set; }
        public string strCreateUser { get; set; }
        public int WWID { get; set; }
        public Lab Lab { get; set; }
        public LabRequestNetwork Network { get; set; }
        public LabRequestPlatform Platform { get; set; }
        public LabRequestTime Time { get; set; }
    }
    public class LabSpace
    {
        
        public int Row { get; set; }
        public string Section { get; set; }
        public string Spot { get; set; }
        public string ProjectCode { get; set; }
        public string strNumber { get; set; }
        public string BKCID { get; set; }
        public int intBBKC { get; set; }
        public int intUtilization { get; set; }
        public string Org1 { get; set; }
        public string Org2 { get; set; }
        public string Org3 { get; set; }
        public string VAL { get; set; }
        public string BUM { get; set; }
        public int intWWUpdate { get; set; }
        public string strUpdateBy { get; set; }
        public LabSpaceType intType { get; set; }
        public string strComments { get; set; }
        public int BKCWin { get; set; }
        public int intWin { get; set; }
        public int intLin { get; set; }
        public string BKCLocation { get; set; }
        public int BRSize { get; set; }
        public string PlatformType { get; set; }
        public string CrntStartWWID { get; set; }
        public string CrntEndWWID { get; set; }
        public string NextPrj { get; set; }
        public string NextStartWWID { get; set; }
        public string PriorityCN { get; set; }
        public string AssetNumber { get; set; }
        public string FurnType { get; set; }
        public int intRunning { get; set; }
        public int intNotRunning { get; set; }
        public DateTime LastTouched { get; set; }
        public int SpaceId { get; set; }
        public bool bDuplicated { get; set; }
        public decimal BKCMoraSqFt { get; set; } 
        public string HVI { get; set; }
        public int NET { get; set; }
        public int intOrder { get; set; }
        public string Updater { get; set; }
        public bool Checked { get; set; }
        public List<Models.LabSpaceExtraData> ExtraData { get; set; }
    }
    public enum LabSpaceType { WhiteSpace=0, Benches=1, Racks=2,Slots=3}
    public class LabRequestNetwork
    {
        public int id { get; set; }
        public int int10MB { get; set; }
        public int int1GB { get; set; }
        public int int10GB { get; set; }
        public int int40GB { get; set; }
    }
    public class LabRequestPlatform
    {
        public int id { get; set; }
        public string strSystem9 { get; set; }
        public string strSystem19 { get; set; }
        public string strSystem23 { get; set; }
        public string strDensityBench { get; set; }
        public string strDensityRack { get; set; }
        public int intType { get; set; }
    }
    public class LabRequestTime
    {
        public int id { get; set; }
        public string strFull { get; set; }
        public string strPart { get; set; }
    }
    public class LabProjectList
    {
        public int id { get; set; }
        public string ProgCode { get; set; }
        public string Org1 { get; set; }
        public string Org2 { get; set; }
        public string Org3 { get; set; }
        public string Val { get; set; }
        public string CrmtStart { get; set; }
        public string CrmtEnd { get; set; }
        public string BUMgr { get; set; }
        public string OrgMrg { get; set;}
        public string Rows { get; set;}
        public string OldName { get; set; }
        public string OldName2 { get; set; }
        public string Comments { get; set; }
        public string FullNameGroup { get; set;}
        public int CC { get; set; }
        public decimal BKCMoraSqft { get; set; }
        public string TypeValidation { get; set; }
    }
    public class LabSpaceExtra
    {
        public int id { get; set; }
        public string ColumnName { get; set; }
        public int CampusID { get; set; }
        public bool Deleted { get; set; }
    }
    public class LabSpaceExtraData
    {
        public int id { get; set; }
        public string Value { get; set; }
        public int ColumnID { get; set; }
        public string ColumnName { get; set; }
        public int SpaceID { get; set; }
    }
    public class LabSpaceSweepReport
    {
        public string Team { get; set; }
        public int Lin { get; set; }
        public int Benches { get; set; }
        public int BKCPlatform { get; set; }
        public int InUse { get; set; }
        public decimal BencRelease { get; set; }
        public int CurrentBBKC { get; set; }
        public int BBKeff { get; set; }
    }
    public class LabUser
    {
        public int id { get; set; }
        public string Email { get; set; }
        public string Name { get { return GeneralClasses.General.FindDisplayNametByEmail(this.Email!=null && this.Email.Length>0? this.Email :""); } }
    }
    public class LabEfficiencyReport
    {
        public int Efficiency { get; set; }
        public string WW { get; set; }
        public string Campus { get; set; }
    }
    public class LabLocalSuitReport
    {
        public string Geo { get; set; }
        public int Efficiency { get; set; }
        public decimal Density { get; set; }
        public decimal Bubble { get; set; }
        public string Campus { get; set; }
    }
    public class LabAvl
    {
        public int Value { get; set; }
        public int Type { get; set; }
        public int WWID { get; set; }
        public string weekName { get; set; }
    }
}