﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Notif
    {
        public int NotifId { get; set; }
        public DateTime createDate { get; set; }
        public string createUser { get; set; }
        public int NotifTypeId { get; set; }
        public string NotifStatus { get; set; }
        public string Descr { get; set; }
        public string NotifType { get; set; }
        public string referenceID { get; set; }
        public string NotifName { get; set; }
        public DateTime DueDate { get; set; }
        public int appId { get; set; }
        public string appName { get; set; }
        public string appIcon { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public int NotifStatusId { get; set; }
        public string NotifStatusName { get; set; }
        public List<NotifAssignees> Assignees { get; set; }
        public List<NotifUpdate> Updater { get; set; }
        public bool Acknowledge { get; set; }
        public int FocusApp { get; set; }
        public int AdditionalId { get; set; }
    }

    public class NotifUpdate
    {
        public int UpdateId { get; set; }
        public int UpdateTypeId { get; set; }
        public string UpdateType { get; set; }
        public int UpdateStatusId { get; set; }
        public string UpdateStatus { get; set; }
        public string value { get; set; }
        public string Descr { get; set; }
        public bool acknowledge { get; set; }
        public string assigneeEmail { get; set; }
        public string[] Assignees { get {
            return assigneeEmail.Split(';');
        }}
        public DateTime createDate { get; set; }
        public string createUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
      
    }
    public class NotifStatus
    {
        public int idStatus {get; set;}
        public string Name { get; set; }
    }
    public class NotifTypes
    {
        public int idType { get; set; }
        public string Name { get; set; }
    }
    public class NotifAssignees
    {
        public string Email { get; set; }
        public string Name { get { return GeneralClasses.General.FindDisplayNametByEmail(Email); } }
        public bool Acknowledge { get; set; }

    }
    public class NotifMailing
    {
        public List<NotifAssignees> To { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Type { get; set; }
    }
}