﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Roadmap
    {
        public int Id { get; set; }
        public int MilestoneID { get; set; }
        public string ProgName { get; set; }
        public int StartWW { get; set; }
        public int StartYear { get; set; }
        public int EndWW { get; set; }
        public int EndYear { get; set; }
        public string Milestone { get; set; }
        public string Vertical { get; set; }
        public bool isWW { get; set; }
        public int StartMonth { get; set; }
        public int EndMonth { get; set; }
        public string ColorStart { get; set; }
        public string ColorEnd { get; set; }
        public string FontColor
        {
            get
            {
                if (ColorStart == "Purple")
                {
                    return "White";
                }
                else
                    return "Black";
        } }
        public string ContinuityID { get; set; }
        public int Perc { get; set; }
        public int WhitePerc { get { return 100 - this.Perc; } }
        public string StarWWMsg { get; set; }
        public string EndWWMsg { get; set; }
        public int Position { get; set; }
        public string Silicon { get; set; }
        public string ProgramType { get; set; }
        public string PXTName { get; set; }
        public string SiliconFN { get; set; }
        public string ProgTypeFN { get; set; }
        public string PXTFN { get; set; }
        public string Label { get {
            return this.ProgramType + " " + this.PXTName + " " + this.Silicon + " " + this.ProgName+" - "+this.Owner;
        } }
        public string SiliconLink { get; set; }
        public string PTLink { get; set; }
        public string PXTLink { get; set; }
        public bool InSnapshot { get; set; }
        public string Owner { get; set; }
    }
    public class ScheduleRoadmap
    {
        public string ContinuityID { get; set; }
        public int StartPTMID { get; set; }
        public int EndPTMID { get; set; }
        public int WWID { get; set; }
    }
}