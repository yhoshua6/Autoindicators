﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class MasterModel
    {

        public string ContinuityID { get; set; }
        
        [GeneralClasses.LabelSecurity("DEG PXT")]
        public string PXTName { get; set; }

        [GeneralClasses.LabelSecurity("DEG PXT ID")]
        public int PXTId { get; set; }

        [GeneralClasses.LabelSecurity("Project")]
        public string ProgName { get; set; }

        [GeneralClasses.LabelSecurity("Description")]
        public string ProgDesc { get; set; }

        [GeneralClasses.LabelSecurity("Description Link")]
        public string PDescLink { get; set; }

        [GeneralClasses.LabelSecurity("Type")]
        public string ProgType { get; set; }

        [GeneralClasses.LabelSecurity("GEO")]
        public string GEO { get; set; }

        [GeneralClasses.LabelSecurity("PLC Status")]
        public string PLCStatus { get; set; }

        public int? PLCStatusId { get; set; }

        [GeneralClasses.LabelSecurity("Program Owner")]
        public string OwnerName { get; set; }

        [GeneralClasses.LabelSecurity("Program Owner Email")]
        public string OwnerEmail { get; set; }

        [GeneralClasses.LabelSecurity(" ProgramOwner ID")]
        public int OwnerId { get; set; }        

        [GeneralClasses.LabelSecurity("DEG PXT Link")]
        public string PXTLink { get; set; }

        [GeneralClasses.LabelSecurity("Project Link")]
        public string ProgramLink { get; set; }

        [GeneralClasses.LabelSecurity("Working Week")]
        public string weekName { get; set; }

        [GeneralClasses.LabelSecurity("Working Week ID")]
        public int? WWID { get; set; }

        [GeneralClasses.LabelSecurity("Type Link")]
        public string ProgTypeLink { get; set; }
        
        [GeneralClasses.LabelSecurity("Working Week Status")]
        public int? weekStatus { get; set; }

        [GeneralClasses.LabelSecurity("Red Flag")]
        public int RedFlag { get; set; }

        [GeneralClasses.LabelSecurity("Tamplate Name")]
        public string TemplateName { get; set; }

        public int TemplateID { get; set; }
        public int ProgTypeID { get; set; }
        public int? progID { get; set; }

        [GeneralClasses.LabelSecurity("Sharepoint Link")]
        public string SharepointLink { get; set; }

        [GeneralClasses.LabelSecurity("VerticalID")]
        public int VerticalID { get; set; }
        [GeneralClasses.LabelSecurity("VerticalName")]
        public string VerticalName { get; set; }
        public string VerticalLink { get; set; }
        public int Clasification { get; set; }
        public string ClasificationName { get; set; }
    }
}