﻿var pr="";
function addPrj()
{
    var ArrVal= $('#ArrMsg').text(),
        Text= $('#ddlBPO').find('option:selected').text();
    //if its already taken, do not add to added list
    if(ArrVal.indexOf(Text) == -1)
    {   //if added list already contains it, do not add it
        if (!$("#lstAddedBPO option[value='" + $('#ddlBPO').find('option:selected').val() + "']").length > 0)
        {
            $('#lstAddedBPO').append('<option value="' + $('#ddlBPO').find('option:selected').val() + '">' + $('#ddlBPO').find('option:selected').text() + '</option>');
            $('#lstAddBPO').val($('#lstAddBPO').val()+","+$('#ddlBPO').find('option:selected').val())
        }
    }
}

function delPrj(id) {
    pr=$('#lstAddBPO').val();
    pr=pr.replace($("#lstAddedBPO option:selected").val(),'');
    if($('#lstAddBPO').val().length>0)
        $('#lstAddBPO').val(pr);
    else
        $('#lstAddBPO').val('');

    $("#lstAddedBPO option:selected").remove();
        
}

function validateFormEdit(){
    var url = $("#pLink").val();
    var descUrl = $("#pDescLink").val();
        
    //URL validation
    if (validateURL(url)) { } else {
        if (url != "") {
            $('#warning').show();
            $('#warningMessage').text("Please enter a valid URL, remember including http://");
            $('#saveBtn').prop('disabled',false);
            return false;
        }
    }

    if (validateURL(descUrl)) { } else {
        if (descUrl != "") {
            $('#warning').show();
            $('#warningMessage').text("Please enter a valid URL, remember including http://");
            $('#saveBtn').prop('disabled',false);
            return false;
        }
    }

    if (($('#pWWI').val().length>0 && $('#pWWI').val() > 0) && ($('#pWWYI').val().length > 0 && $('#pWWYI').val() > 0))
    {
        if($('#pWWE').val().length==0 || $('#pWWE').val()==0 || $('#pWWYE').val().length==0)
        {
            $('#warning').show();
            $('#warningMessage').text("Please enter a valid date range");
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
    } else if(($('#pWWE').val().length>0 && $('#pWWE').val() > 0) && ($('#pWWYE').val().length > 0 && $('#pWWYE').val() > 0))
    {
        if($('#pWWI').val().length==0 || $('#pWWI').val()==0 || $('#pWWYI').val().length==0)
        {
            $('#warning').show();
            $('#warningMessage').text("Please enter a valid date range");
            $('#saveBtn').removeAttr('disabled');
            return false;
        }
    }

    if($('#pWWE').val().length>2 || $('#pWWYE').val().length>2 || $('#pWWI').val().length>2 || $('#pWWYI').val().length>2)
    {
        $('#warning').show();
        $('#warningMessage').text("Please enter only 2 digit");
        $('#saveBtn').removeAttr('disabled');
        return false;
    }


    $.post('/ProgEdit/ValidatePT', { PTID: $("#ddlPT").val() }, function (response) {
        if((response.Milestones.length==0 || response.HealthIndicator.length==0) && !$('#chk').is(':checked') )
        {
            $('#warning').show();
            $('#warningMessage').text("The selected Type dosen't have Milestones or Indicators");
            $('#saveBtn').removeAttr('disabled');
            return;
        }else
        {
            var proj=$('#lstAddBPO').val();
            $('#saveBtn').prop('disabled', true);

            if(proj.substr(0,1)==',')
                $('#lstAddBPO').val(proj.substring(1));
            else if(proj.substr(proj.length-1,1)==',')
                $('#lstAddBPO').val(proj.substring(0,proj.length-1));
            else
                $('#lstAddBPO').val(proj);

            if($('#chk').is(':visible') && $('#chk').is(':checked'))
            {
                $.post('/ProgEdit/CommentsTerminate',function(data){
                    $('#BmodalContainer').html(data);
                    $('#BModal').modal("show");
                    $('#BmyModalLabel').html("Terminate comments");
                    $('#Bmodalcontent').width(400);
                    $('#Bmodalcontent').height(400);
                    $('#BModal').css('margin-left', '30%');
                });
            }else{

                $.post('/ProgEdit/Save', $('#EditForm').serialize(), function (response) {
                    location.reload();
                });
            }
            $('#EditForm').find('input[type=submit]').attr('disabled', 'disabled');
        }
    });
       
}
function validateURL(url) {
    var reurl = /(ftp|http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    return reurl.test(url);
}
    
function AddtoSnapshot(program,link)
{
    $.post('/ProgEdit/AddProgramSnap',{ProgramId:program,Link:link},function(response)
    {
        if(response.ContinuityID!=null)
        {
            $('#PWContinuityID').val(response.ContinuityID);
            $('#success').show();
        }else
        {
            $('#warning').show();
        }
    });
}
   