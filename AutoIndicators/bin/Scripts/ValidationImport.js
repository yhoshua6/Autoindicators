﻿var arrow=setInterval(function () { AnimateArrow('1') }, 500);
var mov = "right";

function AnimateArrow(number) {
    if (mov == "right") {
        $("#AnimateArrow"+number).animate({ "left": "+=50px" }, "slow");
        mov = "left";
    } else {
        $("#AnimateArrow"+number).animate({ "left": "-=50px" }, "slow");
        mov = "right";
    }
}

function start()
{
    $("#Step1").show();
    $("#Start").hide();
    $.post('LoadData', null, function (response) {
        eval(response);
    })
}
function Step2()
{
    $(".Step1").hide();
    $(".Step2").show();
   
    setInterval(function () { AnimateArrow('2') }, 500);
    $.post('SaveDataFile', null, function (response) {
        eval(response);
    })
}
function Step3()
{
    $(".Step2").hide();
    $('#AnimateArrow2').hide();
    $(".Step3").show();
    window.close();
}
function Error(error)
{
    alert(error);
    $('#Step1').hide();
    $("#Start").show();
}