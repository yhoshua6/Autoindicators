﻿var owners=[];
var remobef=[];

function Show()
{
    
    if ($('#Filter').is(':visible'))
        $('.Tab').text('>>');
    else
        $('.Tab').text('<<');

    $('#Filter').toggle('slide', 'FilContent', 600)
}
function search()
{
    $("#Load").show();
    $('#Block').css('display', 'block');

    var parents = document.querySelectorAll('[data-field-parent]');
    var removedE = "";

    for (j = 0; j < remobef.length; j++ )
    {
        for (i = 0; i < parents.length; i++) {
            if (parents[i].firstChild.nextSibling != null && parents[i].firstChild.nextSibling.defaultValue == remobef[j]) {
                parents[i].firstChild.nextSibling.checked = false;
                removedE = ';' + remobef[j];
            }
        }
    }
    
    $("#removed").val(removedE);
    SearchForm.submit();
}
function cancel(module) {
    $("#btnCancel").prop("disabled", true);
    $.get('/Search/Cancel?Module='+module, function (data) { location.reload(); })
    $('#Block').css('display', 'block');
    owners.splice(0, owners.length);

}
function selParent(parent)
{
    var parents = document.querySelectorAll('[data-field-parent]');

    for (i = 0; i < parents.length; i++) {
        if (parents[i].firstChild.nextSibling != null && parents[i].firstChild.nextSibling.defaultValue == parent) {
            parents[i].firstChild.nextSibling.checked = true;
            remobef[remobef.length] = parent;
        }
    }

}
function SelWW(year)
{
    $("option[data-year]").each(function (index) {
        if ($(this).attr("data-year") != year)
            $(this).hide();
        else
            $(this).show();
    })
}
function addFilter(owner,linked)
{
    if (owners.indexOf(owner) >= 0) {
        owners.splice(owners.indexOf(owner), 1);

        if (linked.length > 0) {
            var components = document.querySelectorAll('[data-value-rel]');
            for (i = 0; i < components.length; i++) {
                if (components[i].getAttribute('data-value-rel') == linked) {
                    if (owners.indexOf(components[i].getAttribute('data-value-rel')) >= 0)
                        owners.splice(owners.indexOf(components[i].getAttribute('data-value-rel')), 1);
                }
            }
        }
    }
    else {
        owners[owners.length] = owner;

    }

}
function subfilter(owner,linked)
{
    addFilter(owner,linked);

    refreshFil(linked,owner);
}
function resetList()
{
    var components = document.querySelectorAll('[data-value-rel]');

    for (i = 0; i < components.length; i++) {
        if (components[i].firstChild.nextSibling!=null)
            components[i].firstChild.nextSibling.checked = false;
    }
}
function refreshFil(linked,owner)
{
    //var components = document.querySelectorAll('[data-value-rel]');
    var tot = 0;
    var index = 0;

    var dependecy = $('[data-value-rel="' + owner + '"]');
    var fieldowner = $($('[data-value-rel="' + owner + '"]')[0]).attr('data-field-owner');
   // $('[data-fil-link="' + linked + '"]').show();
    $('#collapseFil_' + fieldowner).show();
    $('[data-field-owner="' + fieldowner + '"]').hide();
    
      //  $(dependecy).show();

        if (owners.length > 0) {
            $.each(owners, function () {
                $('[data-value-rel="' + this + '"]').show();
                tot++;
            });
        } else {
            $('[data-field-rel="' + linked + '"]').show();
            $('[data-field-owner="' + fieldowner + '"]').show();
            $('[data-fil-link="' + linked + '"]').hide();
        }

        //if (linked != null) {
            
        //    $('[value="' + $('input[value*="' + owner + '"]').parent().data('field-rel') + ',' + $('input[value*="' + owner + '"]').parent().data('value-rel') + '"]').attr('checked', 'checked');
        //    var parent = $('[value="' + $('input[value*="' + owner + '"]').parent().data('field-rel') + ',' + $('input[value*="' + owner + '"]').parent().data('value-rel') + '"]').parent(0);
        //    if ($(parent)!=null && $(parent).data('field-rel').length > 0)
        //        $('[value="' + $(parent).data('field-rel') + ',' + $(parent).data('value-rel') + '"]').attr('checked', 'checked');
        //}

        
        //Validation();

        //if (tot == $('[data-field-parent="' + linked + '"]').length-1) {
        //    $($('[data-field-parent="' + linked + '"]')[0]).children().attr('checked', 'checked');
        //}

        if ($('#Filter').height() > $(window).height()) {
            $('#FilContent').height($('#Filter').height())
            $('.Tab').height($('#FilContent').height());
            $('#Block').height($('#FilContent').height());

        }
    
}
function Validation() {

    var visible = 0
    var before = "";

    var data = $('[data-field-rel]');

    $.each(data, function () {
        if($(this).is(':visible'))
        {
            visible++;
        }
        if (before != $(this).attr('data-field-owner')) {
            before = $(this).attr('data-field-owner');
            if (visible != 0)
                $('#Found_' + before).text('Found : '+visible);
            visible = 0;
        }
    })
}
function FilAll(value)
{
    var data =$('div[data-field-owner="' + value + '"]');


    for(i=1; i<data.length;i++)
    {
        
        if ($($(data[i]).children()[0]).is(':checked')) {
            $($(data[i]).children()[0]).prop('checked',false);
        }
        else
            $($(data[i]).children()[0]).prop('checked', true);
    }
}
function SelectFilRun(val)
{
    var values = '';
    values = val.split('AND');

    $.each(values, function () {
        if(this.indexOf('OR')>-1)
        {
            var orvals = this.split('OR');

            $.each(orvals, function () {
                var vals = this.replace('(', '').replace(')', '').replace('=', ',').trim();
                $('input[value="' + vals + '"]').parent().show();
                $('[data-value-rel="' + vals.split(',')[1] + '"]').show();
            });
        }else
        {
            var vals = this.replace('(', '').replace(')', '').replace('=', ',').trim();
            $('input[value="' + vals + '"]').parent().show();
            $('[data-value-rel="' + vals.split(',')[1] + '"]').show();
        }        
    });
}