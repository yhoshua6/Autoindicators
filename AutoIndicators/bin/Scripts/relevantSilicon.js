﻿var RelevantModel = {
    id: 0,
    Name: '',
    ShortName: '',
    Link: '',
    parse: function (data) {
        var newObject = Object.create(this);
        if (data !== undefined) {
            if (data[0] != undefined)
                newObject.id = data[0];
            newObject.Name = data[1];
            newObject.ShortName = data[2];
            newObject.Link = data[3];
        }
        return newObject;
    }

}
var Selidx = -1;

function changeToGridRel() {
    var tbl = $("#DataRel");
    var obj = $.paramquery.tableToArray(tbl);
    var newObj = {
        width: 550,
        height: 700,
        title: "Relevant Silicon",
        //DataGrid Menu
        editable: true,
        toolbar: {
            items: [{
                type: 'button', label: 'Save Changes', listeners: [{
                    click: function () {
                        $gridPT.pqGrid("saveEditCell");
                        total = $("#gridRel").pqGrid("option", "dataModel.data").length;
                        itemcount = 0;
                        for (i = 0; i < total; i++) {
                            var item = RelevantModel.parse($("#gridRel").pqGrid("getRowData", { rowIndxPage: i }));
                            if (item.Name != undefined && item.Name.length > 0) {
                                $.post('/Totalnput/SaveSilicon', { id: item.id, Name: item.Name, ShortName: item.ShortName, Link: item.Link, Delete: false },function(response)
                                {
                                    itemcount++;
                                    if(itemcount==total)
                                        $('#Modal').modal('hide');
                                    
                                });
                            }
                            
                        }
                        setTimeout(2000, $('#Modal').modal('hide'));
                    }
                }]
            }
                , {
                    type: 'button', label: 'Add New', listeners: [{
                        "click": function () {
                                $("#gridRel").pqGrid("addRow", { rowData: {}, rowIndx: 0 });
                        }
                    }]
                }
                //, {
                //    type: 'button', label: 'Delete', listeners: [{
                //        "click": function () {
                //            var item = RelevantModel.parse($("#gridRel").pqGrid("getRowData", { rowIndxPage: Selidx }));
                //            $.post('/Totalnput/SaveSilicon', { id: item.id, Name: item.Name, ShortName: item.ShortName, Link: item.Link, Delete: true }, function (response) {
                //                //if (response) {
                //                    $('#gridRel').pqGrid("deleteRow", { rowIndx: Selidx });
                //                //}
                //            });
                //        }
                //    }]
                //}
            ]
        },
        rowBorders: true,
        filterModel: { mode: 'AND', header: true, on: true },
        datatype: 'local'
    };

    newObj.dataModel = {
        data: obj.data,
        sorting: "local",
        sortDir: ["up", "down", "up"]
    };
    newObj.colModel = obj.colModel;

    newObj.colModel[0].hidden =true;
    newObj.colModel[1].width = 250;
    newObj.colModel[2].width = 300;
    newObj.colModel[2].width = 200;

    for (i = 0; i < newObj.colModel.length; i++) {
        newObj.colModel[i].filter = { type: 'textbox', condition: 'contain', listeners: ['keyup'] };
    }

    newObj.pageModel = { rPP: 30, type: "local" };
    newObj.rowSelect = function (evt, ui) {
        if (ui.rowData) {
            Selidx = ui.rowIndx;
        }
    };
    $gridPT = $("#gridRel").pqGrid(newObj);


    try {

        $gridPT.pqGrid("option", "dataModel.sortIndx", 0);
        $($($('#gridRel').children()[0]).children()[1]).click();
        $($($('#gridRel').children()[0]).children()[1]).click();
        $gridPT.on("pqgridquiteditmode", function (evt, ui) {
            if (evt.keyCode != $.ui.keyCode.ESCAPE) {
                $gridPT.pqGrid("saveEditCell");
            }
        });
    } catch (err) { }
}