﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class PXTs
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Link { get; set; }
        public int order { get; set; }
    }
}