﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class Calendar
    {
        public int id { get; set; }  
        public string weekName { get; set; }
        public int weekStatus { get; set; }   
        public DateTime dateBegin { get; set; }
        public DateTime dateEnd { get; set; }
        public int QuarterId { get; set; }
        public int weekNumber { get; set; }
        public int Year { get; set; }
    }
}