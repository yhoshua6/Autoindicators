﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class PQuery
    {
        public string Name { get; set; }
        public string Comments { get; set; }
        public string Value { get; set; }
        public DateTime CreateDate { get; set; }
    }
}