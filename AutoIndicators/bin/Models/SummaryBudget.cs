﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class SummaryBudget
    {
        public string CategoryName { get; set; }
        public string PRJID { get; set; }
        public string BPOProjName { get; set; }
        public int? Data1 { get; set; }
        public int? Data2 { get; set; }
        public int? Data3 { get; set; }
        public int? Data4 { get; set; }
        public int? Data5 { get; set; }
        public int? thisAmmnt { get; set; }
        public int? nextAmmnt { get; set; }
        public int? Trend { get; set; }
        public int? TrendP { get; set; }
        public string PXTName { get; set; }
        public string OwnerName { get; set; }
        public string ProgName { get; set; }
        public string ProgType { get; set; }
        public string VerticalName { get; set; }
        public string ClasificationName { get; set; }
    }
}