﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class GEOs
    {
        public int id { get; set; }
        public string Name { get; set; }
        public List<Campus> Campus { get; set; }
    }
}