﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoIndicators.Models
{
    public class ScheduleV : MasterModel
    {
        public int? ScheduleId { get; set; }
        public int? PTMID { get; set; }
        public string value { get; set; }
        public string Comments { get; set; }
        public int PWID { get; set; }
        public string Milestone { get; set; }
        public int? oldScheduleId { get; set; }
        public string oldValue { get; set; }
        public string oldComments { get; set; }
        public DateTime? deactivationDate { get; set; }
        public string currentActor { get; set; }
        public string oldActor { get; set; }
        public string link { get; set; }
        public string descriptionLink { get; set; }
        public int? Updated { get; set; }
        public bool Delinquent { get; set; }
        public string RoadmapValue { get; set; }
    }
}