﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoIndicators.Models;
using System.Dynamic;


namespace AutoIndicators.Controllers
{
    public class PQueryController : Controller
    {
        //
        // GET: /PQuery/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PQuery()
        {
            return View();
        }

        public ActionResult Run(FormCollection collection)
        {
            var paramt = new object[] { collection["pText"] };
            var PList = new List<Models.PQuery>();
            dynamic myModel = new ExpandoObject(); 
            using (Models.DevSQLUserEntities dc = new Models.DevSQLUserEntities())
            {
                PList = dc.Database.SqlQuery<Models.PQuery>("[SPROCPQuery] @WWID = {0}", paramt).ToList<Models.PQuery>();
            }
            myModel.PList = PList;
            return View("~/Views/PQuery/PQueryR.cshtml", myModel);
        }
	}
}